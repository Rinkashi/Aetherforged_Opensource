﻿using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

public class AssetImporter : AssetPostprocessor {

	public void OnPreprocessModel(){
		ModelImporter modelImporter = (ModelImporter) assetImporter;
			modelImporter.importMaterials = false;
	}
}
