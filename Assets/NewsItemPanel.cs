﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewsItemPanel : MonoBehaviour {

    public Button TitleAction;
    public Button LinkAction;
    public Text Title;
    public Text DateTime;
    public Text Body;

    public string LinkToItem { get; set; }

	// Use this for initialization
	void Start () {
        TitleAction.onClick.AddListener(OnAction_Clicked);
        LinkAction.onClick.AddListener(OnAction_Clicked);
	}
	
    public void Initialize(string title, string body, DateTime date, string link)
    {
        Title.text = title;
        Body.text = body;
        DateTime.text = date.ToLocalTime().ToString();
        LinkToItem = link;
    }

	public void OnAction_Clicked()
    {
        Application.OpenURL(LinkToItem);
    }
}
