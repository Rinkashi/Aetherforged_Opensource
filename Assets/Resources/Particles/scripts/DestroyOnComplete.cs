using UnityEngine;
using System.Collections;

public class DestroyOnComplete : MonoBehaviour
{

    private ParticleSystem[] thesesystems;

	void Start ()
	{
		thesesystems = GetComponents<ParticleSystem> ();

        if (thesesystems == null || thesesystems.Length == 0) {
            thesesystems = GetComponentsInChildren<ParticleSystem>();
        }
	}

	void Update ()
	{
        int totalParticles = 0;
        bool timeOver = false;
        foreach (var thisSystem in thesesystems) {
            totalParticles += thisSystem.particleCount;
            if (thisSystem.time > 0)
                timeOver = true;
        }

		if (totalParticles == 0 & timeOver) {
			Destroy (gameObject);
		}

	}
}