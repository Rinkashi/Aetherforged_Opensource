﻿using UnityEngine;
using System.Collections;



public class PlayOnDestroy : MonoBehaviour
{
	Vector3 particles_vector;

	void Update ()
	{
		particles_vector = gameObject.transform.position;
	}

	public ParticleSystem particles;

	void OnDestroy ()
	{
		Instantiate (particles, particles_vector, Quaternion.identity); 
	}
}