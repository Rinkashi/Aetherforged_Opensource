using UnityEngine;
using System.Collections.Generic;

namespace audio
{
  [RequireComponent(typeof(AudioSource))]
  public class NibraSFX : MonoBehaviour
  {
    public List<AudioClip> AttackSoundClips;
    AudioSource SFX;

    void Start() 
    {
      SFX = GetComponent<AudioSource>();
    }

    void Autoatk() 
    {
      int clipIndex = Random.Range( 0, AttackSoundClips.Count );
      
      SFX.clip = AttackSoundClips[clipIndex];

      SFX.volume = Random.Range( 0.8f, 1f );
      SFX.pitch = Random.Range( 0.7f, 0.9f );
      SFX.Play();
    }
  }
}
