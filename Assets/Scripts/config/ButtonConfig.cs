﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ButtonConfig {
    
    string name;

    Sprite image;

    GameObject inGameSpacer;

    public List<UnityAction> callbacks;

    public ButtonConfig(string name, Sprite image, params UnityAction[] actions) {
        this.name = name;
        this.image = image;

        callbacks = new List<UnityAction>(actions);
    }

    public void SetUp(GameObject spacer) {
        this.inGameSpacer = spacer;

        var imageComponent = inGameSpacer.GetComponent<Image>();

        var buttonComponent = inGameSpacer.GetComponent<Button>();

//        imageComponent.sprite = image;
//        imageComponent.color = color;

        foreach (UnityAction action in callbacks) {
            buttonComponent.onClick.AddListener(action);
        }

        // Label
        inGameSpacer.GetComponentInChildren<Text>().text = name;
    }

}
