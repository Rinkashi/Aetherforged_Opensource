﻿using System.Collections.Generic;
using UnityEngine;
using audio;

namespace controllers
{
    public class AudioController : Singleton<AudioController>
    {
        
        public static AudioController Instance
        {
            get;
            private set;
        }

        // Why is it OK for this to reach zero?
        public float FadeDuration;

        public AudioClip NormalIntro;      

        public AudioClip NormalMusic;

        [SerializeField]
        private int primary;
        [SerializeField]
        private int secondary;
        [SerializeField]
        private int announcement;

        public List<AudioSource> sources = new List<AudioSource>();

        [SerializeField]
        private AudioClip loopClip;

        public AudioSource primarySource {
            get {
                if (sources.Count == 0) {
                    return null;
                }
                return sources [primary]; 
            }
        }

        public AudioSource secondarySource {
            get {
                if (sources.Count == 0) {
                    return null;
                }
                if (secondary == primary) {
                    secondary++;
                    secondary %= sources.Count;
                }

                return sources [secondary];
            }
        }

        private AudioSource _AnnouncementSource;
        public AudioSource AnnouncementSource {
            get {
                if (sources.Count == 0) {
                    return null;
                }
                GameObject source = transform.Find("AnnouncementSource").gameObject;
                return source.GetComponent<AudioSource>();
            }
            private set {
                _AnnouncementSource = value;
            }
        }



        private float introLength;
        private string currentLocation;
        private bool introTime;
        private bool loopTime;    

        void Awake ()
        {
            if( Instance != null )
                    Debug.LogError( "Only one AudioController should be present at a time." );
            Instance = this;
        }

        void Start () 
        {
            sources.Clear();
            sources.AddRange(GetComponentsInChildren<AudioSource>());

            introLength = NormalIntro.length;

            IntroAndLoop(NormalIntro, NormalMusic);

            GameObject currSource = transform.Find("AnnouncementSource").gameObject;

            AnnouncementSource = currSource.GetComponent<AudioSource>();

            announcement = sources.IndexOf(AnnouncementSource);
        }


        void Update()
        {
            if (introTime && !primarySource.isPlaying) {
                introTime = false;
                primarySource.clip = loopClip;
                primarySource.loop = true;
                primarySource.Play();
            }

//            if( Time.fixedTime <= introLength )
//                CrossFade( NormalIntro );
//            else
//                CrossFade( NormalMusic );

            // Fade things
            if (primarySource != null && primarySource.isPlaying && primarySource.volume <= 1) {
                // Fade in our primary source
                var newVol = Mathf.MoveTowards(primarySource.volume, 1f, Time.deltaTime / FadeDuration);
                primarySource.volume = newVol;
            }

            foreach (var otherSource in sources) {
                if (otherSource == primarySource) {
                    continue;
                }
                if (otherSource != null && otherSource.volume > 0 && otherSource != AnnouncementSource) {
                    
                    // Fade it out
                    var newVol = Mathf.MoveTowards(otherSource.volume, 0f, Time.deltaTime / FadeDuration);
                    if (newVol == 0) {
                        otherSource.Stop();
                    }
                    otherSource.volume = newVol;
                }
            }
        }

        /// <summary>
        /// Starts the intro clip immediately. Set the clip to loop so we know what to swap to later. 
        /// </summary>
        /// <param name="intro">Intro.</param>
        /// <param name="loop">Loop.</param>
        public void IntroAndLoop( AudioClip intro, AudioClip loop) {
            primarySource.clip = intro;
            primarySource.Play();

            primarySource.loop = false;
            introTime = true;

            loopClip = loop;

        }

        /// <summary>
        /// Fades the source clip in, but only if we're changing clips.
        /// </summary>
        /// <param name="source">An audio clip.</param>
        public void FadeIn( AudioClip source) {
            if( primarySource.clip == source )
                return;

            if (primarySource.isPlaying) {
                primary++;
                primarySource.Stop();
                primarySource.volume = 0;
                primarySource.clip = source;
                primarySource.Play();
            }
        }

        public void PlayOneShot(AudioClip noise, float volumeScale) {
            primarySource.PlayOneShot(noise, volumeScale: volumeScale);
        }

        public void PlayAnnouncement( AudioClip source) {
            if (!AnnouncementSource.isPlaying) {
                AnnouncementSource.volume = 1f;
                AnnouncementSource.clip = source;
                AnnouncementSource.Play();
            }
        }

        /// <summary>
        /// Starts crossfading between a primary clip and (this might not work right)
        /// </summary>
        /// <param name="source">Source.</param>
        void CrossFade( AudioClip source )
        {
            if( primarySource.clip == source )
                    return;

            if (primarySource.isPlaying) {
                primary++;

//                var tmp = primarySource;
//                primarySource = secondarySource;
//                secondarySource = tmp;
//                secondarySource.time = primarySource.time;


            }


            primarySource.volume = 0;
            primarySource.clip = source;

            primarySource.Play();

            currentLocation = primarySource.clip.name;
        }                      
    }
}