using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.UI;

using core;

namespace controllers {
    /// <summary>
    /// Responsible for moving the camera around.
    /// </summary>
    public class CameraController : MonoBehaviour {
        // Deprecated field: UnityEngine.Component.camera
        new internal Camera camera;
        GameObject cameraDolly;
        Camera fOWCamera;
        Camera uICamera;
        bool lockedOn;
        bool snapped;
        bool panLock;
        bool appFocussed;
        GameObject target;
        MinimapController minimapCon;

        public float EdgeSpeed = 1.0f;
        public float ArrowSpeed = 1.0f;
        public float panSensitivity = 1.0f;
        public float EdgeWidth = 0.1f;
        public float FOVUpper = 65.0f;
        public float FOVLower = 47.0f;
        public float screenOffsetUpper = -0.52f;
        public float screenOffsetLower = -0.5f;
        public GameObject cameraUpper;
        public GameObject cameraLower;
        public float zoomSpeed;
        public Vector2 boundary;
        private float distRatio = 1.0f;

        bool AllowPanning {
            get { return !panLock && appFocussed; }
        }

        public static CameraController instance;
        public static CameraController Instance {
            get { return instance; }
        }

        void OnApplicationFocus(bool hasFocus) {
            appFocussed = hasFocus;
        }

        /// <summary>
        /// Toggles a camera lock-on. The camera can be locked onto any
        /// game object.
        /// </summary>
        /// <param name="target"></param>
        public void ToggleLockOn(GameObject target) {
            this.target = target;
            // Toggle lock on, except if the game object is null
            lockedOn = (!lockedOn) && (target != null);
        }

        /// <summary>
        /// Toggles the pan lock. Pan lock keeps the camera from panning. 
        /// </summary>
        public void TogglePanLock () {
            panLock = !panLock;
        }

        /// <summary>
        /// Locks on to the target. 
        /// As long as the target isn't null, we lock on.
        /// </summary>
        /// <param name="target">Target.</param>
        public void LockOnExplicit(GameObject target) {
            this.target = target;
            // Turn lock on, except if the game object is null
            lockedOn = (target != null);
        }

        /// <summary>
        /// Unlocks the camera.
        /// </summary>
        public void UnlockExplicit() {
            this.target = null;
            // Turn lock off
            lockedOn = false;
        }

        /// <summary>
        /// Snaps the camera to a given offset. This snapping point is where
        /// the on which the camera is focused on.
        /// </summary>
        /// <param name="t"></param>
        public void SnapTo(Vector3 t) {
            transform.position = t;
            snapped = true;
        }

        void Start() {
            instance = this;
            camera = GetComponentInChildren<Camera>();
            cameraDolly = transform.Find("CameraDolly").gameObject;
            fOWCamera = GameObject.Find("FoW Camera").GetComponent<Camera>();
            uICamera = GameObject.Find("UI Camera").GetComponent<Camera>();

            Assert.IsNotNull(camera);

            lockedOn = false;
            snapped = false;

            //ADD PLAYERPREFS FOR CAMERA SETTINGS
        }

        
        public void floatUpdate(string name, float value)
        {
            switch (name){
                case "EdgeWidth":
                {
                    EdgeWidth = value;
                    break;
                }
                case "ArrowSpeed":
                {
                    ArrowSpeed = value;
                    break;
                }
                case "panSensitivity":
                {
                    panSensitivity = value;
                    break;
                }
                case "EdgeSpeed":
                {
                    EdgeSpeed = value;
                    break;
                }
                case "zoomSpeed":
                {
                    zoomSpeed = value;
                    break;
                }
            }
        }
        
        public void LateUpdate() {
            Vector3 location = gameObject.transform.position;
            float dist = Vector3.Distance(cameraUpper.transform.position, cameraLower.transform.position);

            if (minimapCon == null) {
                minimapCon = GameObject.FindObjectOfType<MinimapController>();
            }

            if (lockedOn && target == null)
                lockedOn = false;

            if (lockedOn) {
                transform.position = target.transform.position;
            } else if (!snapped) {
                Vector3 center = new Vector3(Screen.width / 2, Screen.height / 2);
                Vector3 mpos = Input.mousePosition - center;
                Vector3 ratio = new Vector3(mpos.x / center.x, mpos.y / center.y, 0);
                Vector3 ratioXZ = new Vector3(ratio.x, 0, ratio.y);

                float inner = 1.0f - EdgeWidth;
                float edgeSpeed = EdgeSpeed * Time.deltaTime;
                float arrowSpeed = ArrowSpeed * Time.deltaTime;
                float count = 0f;

                bool left = false;
                bool right = false;
                bool back = false;
                bool forward = false;


                if (AllowPanning) {   // Panning 

                    //Middle mouse button panning
                    if (KeyConfig.Instance.IsPressed(ICE.MousePanCamera)) {
                        transform.Translate(Vector3.right * -Input.GetAxis("Mouse X") * panSensitivity);
                        transform.Translate(Vector3.forward * -Input.GetAxis("Mouse Y") * panSensitivity);
                        BoundaryCheck();
                    } else {

                        // we're over the minimap
                        bool blockTheBox = (minimapCon != null && minimapCon.RecRatio.Contains(Input.mousePosition));

                        //Arrow key panning
                        bool leftArrowPress = Input.GetKey(KeyCode.LeftArrow);
                        bool rightArrowPress = Input.GetKey(KeyCode.RightArrow);
                        bool downArrowPress = Input.GetKey(KeyCode.DownArrow);
                        bool upArrowPress = Input.GetKey(KeyCode.UpArrow);

                        //Edge border panning w/ arrowkey
                        if (leftArrowPress || !blockTheBox && ratio.x < -inner && ratio.x >= -1.0f) {
                            left = true;
                            count += 1;
                        }
                        if (rightArrowPress || !blockTheBox && ratio.x > inner && ratio.x <= 1.0f) {
                            right = true;
                            count += 1;
                        }
                        if (downArrowPress || !blockTheBox && ratio.y < -inner && ratio.y >= -1.0f) {
                            back = true;
                            count += 1;
                        }
                        if (upArrowPress || !blockTheBox && ratio.y > inner && ratio.y <= 1.0f) {
                            forward = true;
                            count += 1;
                        }

                        // Slow down on corners
                        if (count >= 2) {
                            arrowSpeed = arrowSpeed / 1.5f;
                            edgeSpeed = edgeSpeed / 2;
                        }

                        //Either Edge or Arrow key panning  
                        if (left == true) {
                            if (leftArrowPress) {
                                transform.Translate(Vector3.left * arrowSpeed);
                            } else {
                                transform.Translate(ratioXZ * edgeSpeed);
                            }
                            BoundaryCheck();
                        }

                        if (right == true) {
                            if (rightArrowPress) {
                                transform.Translate(Vector3.right * arrowSpeed);
                            } else {
                                transform.Translate(ratioXZ * edgeSpeed);
                            }

                            BoundaryCheck();
                        }

                        if (back == true) {
                            if (downArrowPress) {
                                transform.Translate(Vector3.back * arrowSpeed);
                            } else {
                                transform.Translate(ratioXZ * edgeSpeed);
                            }

                            BoundaryCheck();
                        }

                        if (forward == true) {
                            if (upArrowPress) {
                                transform.Translate(Vector3.forward * arrowSpeed);
                            } else {
                                transform.Translate(ratioXZ * edgeSpeed);
                            }

                            BoundaryCheck();
                        }


                    }
                }
            }
            snapped = false;

            //Zoom and camera rotate
            if (Input.GetAxis("Mouse ScrollWheel") > 0) {
                distRatio -= Time.deltaTime * zoomSpeed;
            } else if (Input.GetAxis("Mouse ScrollWheel") < 0) {
                distRatio += Time.deltaTime * zoomSpeed;
            }

            distRatio = Mathf.Clamp(distRatio, 0, 1);

            cameraDolly.transform.position = (distRatio *
              (cameraUpper.transform.position
                - cameraLower.transform.position)
                  + cameraLower.transform.position);

            //check if the camera has moved
            //Change FOV
            camera.fieldOfView = (distRatio * (FOVUpper - FOVLower) + FOVLower);

            //Rotate to match lower & upper values
            cameraDolly.transform.localEulerAngles = (distRatio *
              (cameraUpper.transform.localEulerAngles
                - cameraLower.transform.localEulerAngles)
                  + cameraLower.transform.localEulerAngles);

            //adjust Camera offset
            float offset = (distRatio * (screenOffsetUpper - screenOffsetLower) + screenOffsetLower);
            camera.transform.localPosition = new Vector3(0f, offset, 0f);

            //Setting the other Cameras transform to the main one
            fOWCamera.transform.position = camera.transform.position;
            uICamera.transform.position = camera.transform.position;
            fOWCamera.transform.rotation = cameraDolly.transform.rotation;
            uICamera.transform.rotation = cameraDolly.transform.rotation;
            fOWCamera.fieldOfView = camera.fieldOfView;
            uICamera.fieldOfView = camera.fieldOfView;
        }
        public void BoundaryCheck() {
            //ensure that the camera is not outwidth bounds
            transform.position = new Vector3(Mathf.Clamp(transform.position.x,
              -boundary.x, boundary.x), transform.position.y,
                Mathf.Clamp(transform.position.z, -boundary.y, boundary.y));
        }
    }
}