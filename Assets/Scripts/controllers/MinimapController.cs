using UnityEngine;
using UnityEngine.UI;

using core;

namespace controllers
{
    /// <summary>
    /// Responsible for handling the minimap
    /// </summary>
    public class MinimapController : MonoBehaviour
    {
        internal Camera minimapCamera;
        GameObject mainCameraRail;
        CameraController cameraController;
        bool mouseOverViewport;
        PlayerKnob playerController;


        public Rect RecRatio {
            get {
                return new Rect(minimapCamera.rect.x * Screen.width,
                    minimapCamera.rect.y * Screen.height,
                    minimapCamera.rect.width * Screen.width,
                    minimapCamera.rect.height * Screen.height);
            }
        }

        void Start()
        {
            minimapCamera = GetComponent<Camera>();
            mainCameraRail = GameObject.Find("CameraRail");
            cameraController = mainCameraRail.GetComponent("CameraController") as CameraController;
            playerController = FindObjectOfType<PlayerKnob>();
        }

        public void Update()
        {
            if (playerController == null) {
                playerController = FindObjectOfType<PlayerKnob>();
            }

            gameObject.transform.position = new Vector3 (0,50,0);
            gameObject.transform.rotation = Quaternion.Euler(90, 45, 0);

            Rect screenRect = new Rect(minimapCamera.rect.x*Screen.width,
                minimapCamera.rect.y*Screen.height,
                minimapCamera.rect.width*Screen.width,
                minimapCamera.rect.height*Screen.height);

            if (screenRect.Contains(Input.mousePosition)){
                RaycastHit hit;
                Ray altray = minimapCamera.ScreenPointToRay (Input.mousePosition);
                if( Input.GetMouseButton(0))
                if(Physics.Raycast(altray, out hit))
                    mainCameraRail.transform.position = new Vector3(hit.point.x, 0, hit.point.z);
                
                cameraController.BoundaryCheck();
                if (KeyConfig.Instance.IsPressed(ICE.MoveActionForger))
                if (Physics.Raycast(altray, out hit)) {
                    playerController.SetPoint(new Vector3(hit.point.x, 0, hit.point.z));
                }
            }
        }
    }
}