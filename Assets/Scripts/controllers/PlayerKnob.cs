using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using System.Collections;
using System;


using ui;
using core;
using managers;
using core.forger;
using controllers;
using controllers.ai;
using core.utility;
using itemdata = feature.ItemLoader;

namespace controllers
{

    public enum Click
    {
        Target,
        Move
    }

    public class PlayerKnob : NetworkBehaviour
    {
        [SyncVar]
        NetworkInstanceId forgerID;
        [SerializeField]
        uint localForgerID;

        [SyncVar]
        [SerializeField]
        string syncedName;
        public string PlayerName;

        public Team MyTeam;

        [SerializeField]
        Forger forger;
        public Forger ControlForger {
            get { return forger; }
            set { 
                forger = value;
                forgerID = value.netId;
                localForgerID = forgerID.Value;

                intellect = forger.GetComponent<ForgerAI>();
                var holder = forger.GetComponentInChildren<KitHolder>();
                if (holder != null)
                {
                    kit = holder.kit;
                }
            }
        }

        [SyncVar]
        float gameTime;
        public float SyncedTime {
            get { return gameTime; }
            set {
                if (NetworkServer.active) {
                    gameTime = value;
                }
            }
        }

        public RawImage LiveMap {
            get { return mapClockPanel.liveMinimap; }
        }

        private bool MouseOverMap;

        ForgerAI intellect;

		CameraController playerCamCon;  // disambiguation with 'camera'
        int abilityToCastIndex;

        private Castable abilityToCast;
        public Castable AbilityToCast {
            get { return abilityToCast; }
        }
        public bool AbilitySetToCast {
            get { return (abilityToCast != null); }
            set { // Kinda janky approach, but ehhhhh it works. 
                if (!value) {
                    abilityToCast = null;
                }
            }
        }
        public void ResetAbilitySetToCast() {
            abilityToCast = null;
        }


        GameObject pointer;
        GameObject miniMapCamera;
		MinimapController miniMapCon;
        Kit kit;

        bool kitReady {
            get {
                if (kit == null)
                {
                    kit = forger.GetComponentInChildren<Kit>();
                }
                return (kit != null);
            }
        }

        [SerializeField]
        BottomPanel bottomPanel = new BottomPanel();
        [SerializeField]
        StatsClockPanel clockPanel = new StatsClockPanel();
        [SerializeField]
        LocalStatsPanel localStatsPanel = new LocalStatsPanel();
        [SerializeField]
        MiniMapPanel mapClockPanel = new MiniMapPanel();

		[SerializeField]
		private bool attackCursor;

        void Start ()
        {
            if (isLocalPlayer) {
                pointer = Instantiate(Resources.Load("Pointer") as GameObject);

                HidePointer();
                
                miniMapCamera = Instantiate(Resources.Load("MiniMapCamera") as GameObject);
                miniMapCamera.transform.SetParent(gameObject.transform);

				miniMapCon = miniMapCamera.GetComponent<MinimapController>();

                var preferredName = PlayerPrefs.GetString("PlayerName", "<Name Not Found>");
                StartCoroutine(GameManager.Instance.DoDelay(() =>
                {
                    CmdSetName(preferredName);
                    return 0f;
                }, 0.1f));

                CmdRequestGameTime();

            }

        }


        void Update()
        {         
            if (isServer && GameManager.Instance.GameOver) {
                RpcEndgameMessage((int)GameManager.Instance.Victor);
            }

            if (syncedName != PlayerName) {
                if (isLocalPlayer) {
                    if (NetworkServer.active) {
                        // If we're on the server, 
                        syncedName = PlayerName;    // set synced name

                    } else {
                        // if this is the local player on a client,
                        CmdSetName(PlayerName);     // tell the server
                    }
                } else {
                    // If this isn't the local player and we're on the client,
                    PlayerName = syncedName;
                }
            }


            if (forger == null) {
                localForgerID = forgerID.Value;
                var gob = ClientScene.FindLocalObject(forgerID);
                if (gob != null) {
                    forger = gob.GetComponent<Forger>();

                    forger.SetPlayer(this);

                    if (isLocalPlayer) {
                        intellect = forger.GetComponent<ForgerAI>();
                        kit = forger.GetComponentInChildren<Kit>();
                    } 
                }
            }

            if (NetworkServer.active) {
                SyncedTime = GameTime.time;
            } 



            if (isLocalPlayer) {



                if (UnitManager.Instance.LocalKnob == null && isLocalPlayer) {
                    UnitManager.Instance.LocalKnob = this;
                }

                if (UnitManager.Instance.LocalForger == null && isLocalPlayer) {
                    UnitManager.Instance.LocalForger = forger;
                    UnitManager.Instance.HomeTeam = forger.Team;
                }


                #region controls
                if (playerCamCon == null) {
                    playerCamCon = CameraController.Instance;
                    playerCamCon.ToggleLockOn(forger.gameObject);
                }


                if (KeyConfig.Instance.WentDown(ICE.ToggleLockCamera)) {
                    playerCamCon.ToggleLockOn(forger.gameObject);
                }

                if (KeyConfig.Instance.IsPressed(ICE.CenterForgerCamera)) {
                    playerCamCon.SnapTo(forger.transform.position);
                }

                if (KeyConfig.Instance.WentDown(ICE.StopActionsForger)) {
                    CmdStopActions();
                }

                if (GetComponent<OtherProjectileController>() != null) {
                    transform.LookAt(GetComponent<OtherProjectileController>().getTargetLocation());
                    transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
                }

                {   // The Unit Raycast Scope
                    RaycastHit hit;
                    Ray ray = CameraController.Instance.camera.ScreenPointToRay(Input.mousePosition);

                    Unit unit = null;
                    if (Physics.Raycast(ray, out hit, 200, 1 << LayerMask.NameToLayer("Unit"))) {
                        unit = hit.transform.gameObject.GetComponent<Unit>();
                    }


                    {
                        if (attackCursor) {
                            var overUnit = (unit != null && unit.Team != forger.Team);

                            if (KeyConfig.Instance.WentDown(ICE.CursorConfirmForger)) {
                                if (overUnit) {
                                    CmdSetAttackTarget(unit.netId, unit.name);
                                } else {
                                    // if we don't click on a unit directly, activate attack move behavior

                                    GetMouseOnPoint()
                                    .With(point => {
                                        PlacePointer(point);

                                        CmdAttackMove(point);
                                    }
                                    );
                                }

                                attackCursor = false;
                                CursorManager.Instance.SetCursor(CursorType.pointer);
                            } else if (overUnit) {
                                // Highlight a valid target
                                CursorManager.Instance.SetCursor(CursorType.selectHighlighted, unit);
                            } else {
                                CursorManager.Instance.SetCursor(CursorType.select);
                            }


                        }
					
                        if (AbilitySetToCast && AbilityToCast.CastType == CastType.UnitTargeted) {
                            // Highlight valid targets

                            if (abilityToCast.CanCastOnUnitTarget(ControlForger, unit)) {
                                // pretty sure this is (logically) back where it was in Unit
                                if (KeyConfig.Instance.WentDown(ICE.CursorConfirmForger)) {
                                    TryAbilityCastOnUnit(unit);
                                    CursorManager.Instance.SetCursor(CursorType.pointer);
                                } else {
                                    // Highlight a valid target
                                    CursorManager.Instance.SetCursor(CursorType.selectHighlighted, unit);
                                }
                            } else {
                                CursorManager.Instance.SetCursor(CursorType.select);    // is this still right? 
                            }



                        }

                    }


                    if (KeyConfig.Instance.IsPressed(ICE.MoveActionForger)) {
                        // If we give a specific command to move, cancel attack move. 
                        CmdDoNotAttackMove();

                        if (attackCursor) {
                            attackCursor = false;
                            CursorManager.Instance.SetCursor(CursorType.pointer);
                        }
                        if (AbilitySetToCast) {	
                            ResetAbilitySetToCast(); // Cancel ability cast
                            CursorManager.Instance.SetCursor(CursorType.pointer);
                        } else if (UIBlockMouseInputOnWorld.CanInteract) {
                            if (unit != null && unit.Team != forger.Team)
                            {
                                // We are hovering over a unit, and it's an enemy.
                                // Let's attack it. 

                                CmdSetAttackTarget(unit.netId, unit.name);
                                HidePointer();
                            }
                            else if (MouseOverMap)
                            {
                                // Ignore cause we're clicking the minimap
                                Debug.Log("clicked the minimap");
                            }
                            else {    
                                // aware that this will still fire if allied Unit was clicked
                                // ^-- is totally intended cause we want to not be UX-blocked from moving in busy teamfights. 
                                GetMouseOnPoint()
								.With(point => {
                                    PlacePointer(point);

                                    CmdSetDestination(point);
                                    //if (forger.Interrupt()) ;
                                    //movementController.MoveToPoint(point, null);
                                }
                                );
                            }
                        }
                    }
                }
                #endregion
            }
        }

        void barFill( Image bar, float ratio) {
            if (bar.type == Image.Type.Filled) {
                bar.fillAmount = ratio;
            } else {
                var barTransform = bar.GetComponent<RectTransform>();
                barTransform.anchorMax = new Vector2(forger.HPRatio, 1);
            }
        }


        void HidePointer () {
            pointer.SetActive(false); 
        }

        void PlacePointer (Vector3 point) { 
            pointer.SetActive(false); 
            pointer.SetActive(true); 
            // Wooo! Fixed. when we fix the pointer pivot point, fix this too. 
            pointer.transform.position = point; 
        }


        public bool BuyItem(Item capitalism) {
            // MEME consume obey stay asleep obey consume

            if (NetworkServer.active) {
                return forger.BuyItem(capitalism);
            } else {
                CmdBuyItem(capitalism.Name);

                // FIXME we'll need to make this connection with an RPC at some point
                return true;
            }

        }

        public void SellItem(Item capitalism)
        {
            if (NetworkServer.active) {
                forger.SellItem(capitalism);
            } else {
                CmdSellItem(capitalism.Name, -1);
            }
        }

        [Command]
        public void CmdBuyItem(string itemName) {
            var itemlist = itemdata.Instance.InitItems();

            if (itemlist.ContainsKey(itemName)) {
                forger.BuyItem(itemlist [itemName]);

                // FIXME send an rpc back to the client telling them we bought the item
            } 
        }


        [Command]
        void CmdSellItem(string itemName, int index) {
            var itemlist = itemdata.Instance.InitItems();

            if (index >= 0 && itemlist.ContainsKey(forger.itemNames[index])) {
                forger.SellItem( itemlist[ forger.itemNames [index]]);
            } else {
                
                if (itemlist.ContainsKey(itemName)) {
                    forger.SellItem(itemlist [itemName]);
                }
            }
        }

        [Command]
        public void CmdMoneyCheat_DEBUG() {
            ControlForger.GainSalt(5000, false);
        }

        [Command]
        void CmdStartRecall() {
            intellect.StopActions();
            StartCoroutine(GameManager.Instance.DoDelay(() =>
            {
                GameManager.Instance.SetRecall(forger);
                return 0f;
            }, 0.1f));
        }

        [Command]
        private void CmdAttackMove (Vector3 area) {
            intellect.AttackMove(area);
        }

        [Command]
        private void CmdDoNotAttackMove () {
            intellect.DoNotAttackMove();
        }

        [Command]
        private void CmdSetAttackTarget (NetworkInstanceId targetID, string debug_name) {
            var gob = NetworkServer.FindLocalObject(targetID);
            if (gob == null) {
                Debug.LogWarningFormat("target {0} not found. It should be named {1}.", targetID, debug_name);
                return;
            }
            var unit = gob.GetComponent<Unit>();
            if (unit == null) {
                Debug.LogWarningFormat("target {0} not a unit. It should be named {1}.", targetID, debug_name);
                return;
            }

            intellect.SetAttackTarget(unit);

        }


        [Command]
        public void CmdSetDestination (Vector3 destination) {
            intellect.JustMove(destination);
            //intellect.Destination = destination;

            intellect.CancelAttackTarget();
        }

        [Command]
        public void CmdSetName (string playerName) {
            PlayerName = playerName;
            syncedName = playerName;
            forger.SetName(playerName);
        }

        [Command]
        private void CmdStopActions () {
            intellect.StopActions();
        }

        [Command]
        private void CmdPlaceWard (Vector2 location) {
            // TODO add ward placing code. 
        }

        [Command]
        private void CmdCastAbility (int abilityInput, Vector2 point, bool pointHit ) {
            SetAbilityCast(abilityInput, point, pointHit);
        }

        [Command]
        public void CmdLevelUpAbility(int slot) {
            forger.SkillUp(slot);
        }

        [Command]
        void CmdInitiateAbilityCastUnit(int abilityIndex, GameObject unit)
        {

            //RpcInitiateAbilityCastUnit(abilityIndex, unit);
            _initiateAbilityCastUnit(abilityIndex, unit);
        }
        [ClientRpc]
        void RpcInitiateAbilityCastUnit(int abilityIndex, GameObject unit)
        {

            //_initiateAbilityCastUnit(abilityIndex, unit);
        }

        [Command]
        public void CmdSwapForger_DEBUG(string imprintName) {
            var savelocation = default(Vector3);
            bool existedBefore = (ControlForger != null);
            float saltAmount = 0;
            float expAmount = 0;
            if (existedBefore)
            {   // if we already have a control forger,
                // Save its location
                savelocation = ControlForger.transform.position;
                // Save its experience
                expAmount = ControlForger.TotalExperience;
                // Save its money
                saltAmount = ControlForger.Salt;

                // Remove it
                NetworkServer.Destroy(ControlForger.gameObject);

            }

            //    //Compare value to our forger array
            //string[] forgers = { "Ember", "Warcardinal", "Visionary" };

            //string forgerName = forgers[forgerDropdown.value];

            GameManager.Instance.SpawnFromImprint(this, imprintName);

            if(existedBefore)
            {
                var additionalSalt = saltAmount - Constants.STARTING_SALT;
                // restore its experience and salt
                ControlForger.GainXP(expAmount);
                ControlForger.GainSalt(additionalSalt, false);

                // Restore its location!
                ControlForger.transform.position = savelocation;

            }
            else
            {
                ControlForger.transform.position = GameManager.Instance.GetMySpawn(ControlForger);
            }

        }

        void _initiateAbilityCastUnit(int abilityIndex, GameObject unitGameObject)
        {
            if (unitGameObject != null)
            {
                Castable ability;
                Entity entity = unitGameObject.GetComponent<Entity>();

                if (forger.TryActiveInSlot(abilityIndex, out ability))
                {
                    forger.CastAbilityOnUnit(ability, entity);
                }
            }
        }


        void InitiateAbilityCast(int abilityIndex, Vector2 areaPoint) {
            //RpcAbilityCastPredictionCorrection(abilityIndex);
            _initiateAbilityCast(abilityIndex, areaPoint);
        }
        [ClientRpc]
        void RpcAbilityCastPredictionCorrection(int abilityIndex)
        {
            /* TODO ultimately the journey is so long that client prediction will have already fired by the time this happens. 
             * though we might be able to "fix" some visuals or something. Will need some thinking. 
             */
        }

        [Command]
        void CmdRequestGameTime() 
        {
            RpcClockSync(GameTime.time);
        }

        [ClientRpc]
        void RpcClockSync(float time)
        {
            GameTime.time = time;
        }

        void _initiateAbilityCast(int abilityIndex, Vector2 point)
        {
            Castable ability;

            if (forger.TryActiveInSlot(abilityIndex, out ability))
            {
                forger.CastAbility(ability, point);
            }
        }


        public Option<Unit> GetClickedOnUnit(Click click)
        {
            return ClickRaycast(click == Click.Target ? 0 : 1, Utils.UNIT_LAYER)
                .AndThen<Unit>(x =>
                {

					var u = x.transform.gameObject.GetComponent<Unit>();

                    return Option<Unit>.Some(u);
                });
        }

        public Option<Vector3> GetClickedOnPoint(Click click)
        {
            return ClickRaycast(click == Click.Target ? 0 : 1, Utils.GROUND_LAYER)
                    .Map<Vector3>(x => x.point);
        }

        public Option<Vector3> GetMouseOnPoint()
        {
            return GetMouseRaycast(Utils.GROUND_LAYER)
                    .Map<Vector3>(x => x.point);
        }

        private Option<RaycastHit> ClickRaycast(int mouseButton, int layer)
        {
            if (playerCamCon)
            {
                if (Input.GetMouseButton(mouseButton)) {
                    Ray clickRay = playerCamCon.camera.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;

                    if (Physics.Raycast(clickRay, out hit, 200, 1 << layer))
                    {
                        return Option<RaycastHit>.Some(hit);
                    }
                }
            }

            return Option<RaycastHit>.None();
        }

        public Option<RaycastHit> GetMouseRaycast(int layer)
        {
            if (playerCamCon) {
                Ray clickRay = playerCamCon.camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(clickRay, out hit, 500, LayerMask.GetMask("Ground")))
                {
                    return Option<RaycastHit>.Some(hit);
                }
            }

            return Option<RaycastHit>.None();
        }

		public void SetPoint(Vector3 MiniMapHit) {
            CmdSetDestination( MiniMapHit );

            PlacePointer( MiniMapHit );
		}


        // DO WE NEED This? We have 
        public bool TryGetPlanarPoint (out Vector2 position, int layer = Utils.GROUND_LAYER) {
            if (playerCamCon) {
                Ray clickRay = playerCamCon.camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(clickRay, out hit, 500, 1 << layer)) {
                    position = Utils.PlanarPoint(hit.point);
                    return true;
                }
            }
            position = Vector2.one;
            return false;
        }

        public bool SetAbilityCast(int abilityIndex, Vector2 point, bool hit) {
            Debug.AssertFormat(kit != null, "Setting ability to cast without a Kit.");

            Castable ability;

            if (kitReady && ControlForger.TryActiveInSlot(abilityIndex, out ability)) {
                if (ability.CanCast(ControlForger))
                {
                    StartAbilityCast(abilityIndex, point, hit);

                    return true;
                }
            }
            return false;
        }


        private void StartAbilityCast(int abilityIndex, Vector2 targetPoint, bool hit)
        {
            Castable ability;
            if (!forger.TryActiveInSlot(abilityIndex, out ability))
            {
                return;
            }

            if (forger.SkillOnCooldown(ability)) {
                var cd = ability.Cooldowns;
                return;
            }

            if (ability.CastType == CastType.NonTargeted) {
                InitiateAbilityCast(abilityIndex, targetPoint);
            }
            if (hit && ability.CastType == CastType.AreaTargeted)
            {
                InitiateAbilityCast(abilityIndex, targetPoint);
            }

            if (ability.CastType == CastType.LineTargeted)
            {
                InitiateAbilityCast(abilityIndex, targetPoint);
            }
            if (ability.CastType == CastType.ConeTargeted)
            {
                InitiateAbilityCast(abilityIndex, targetPoint);
            }
            if (ability.CastType == CastType.UnitTargeted) {
                CursorManager.Instance.SetCursor(CursorType.select);
                abilityToCast = ability;
            }
        }

        public bool TryAbilityCastOnUnit(Unit target) {
            int index;
            if (abilityToCast.CanCastOnUnitTarget(ControlForger, target) && forger.TryGetIndexOfSkill(abilityToCast, out index)) {
                CmdInitiateAbilityCastUnit(index, target.gameObject);
                abilityToCast = null;
                return true;
            }
            return false;
        }

        [Serializable]
        public class BottomPanel : UIPanel
        {

            public Image healthBar;

            public Text[] cooldownAbility;

            public Image[] iconsAbility;

            public Text[] hotKeyAbility;

            public Button[] levelUpAbility;

            public Image[] abilityLevelAbility;


            public Image resourceBar;

            public Text healthText;

            public Text resourceText;

            public Image experienceBar;

            public Image moneyBar;

            public Text moneyText;

            public Text saltAmountDebug;

            public Text levelNumber;

        }

        [Serializable]
        public class StatsClockPanel : UIPanel
        {

            public Text ingameclock;

            public Text pingReadout;

            public Text killNumber;
            //
            public Text helpNumber;
            //
            public Text deadNumber;
            //
            public Text creepNumber;

        }
        [Serializable]
        public class LocalStatsPanel : UIPanel
        {
            public Text powerStatText;
            public Text armorStatText;
            public Text mrStatText;
            public Text attackDamageStatText;
            public Text hasteStatText;
            public Text movementSpeedStatText;
        }

        [Serializable]
        public class MiniMapPanel : UIPanel
        {

            public Text ingameclock;

            public Text kdaText;

            public Text csText;

            public RawImage liveMinimap;
        }


        private void OnGUI()
        {
            if (Event.current.isKey) {
                var keyEvent = Event.current;

                if(isLocalPlayer)
                {
                if (keyEvent.type == EventType.KeyDown) {
                    // FIXME We may wish to suppress these while any menu (shop, options) is open.
                    if (ControlForger != null
                        && forger.CanControl //Usually set during things like dashes, stuns, etc.
                        && forger.CanCast //Usually set during things like Silences
                        && (!UIManager.Ready || !UIManager.Instance.IsMenuOpen))
                    {
                        Vector2 targetPoint;
                        bool hitPoint = Utils.TryGetPlanarPointFromMousePosition(out targetPoint);

                        if (KeyConfig.Instance.SkillUp(ICE.AbilityOneForger, keyEvent))
                        {
                            CmdLevelUpAbility(1);
                        }   
                        else if (KeyConfig.Instance.QuickCast(ICE.AbilityOneForger, keyEvent))
                        {
                            CmdCastAbility(1, targetPoint, hitPoint);
                        }
                        else if (KeyConfig.Instance.IndicatorCast(ICE.AbilityOneForger, keyEvent))
                        {
                            // show indicator and select for casting
                        }

                        if (KeyConfig.Instance.SkillUp(ICE.AbilityTwoForger, keyEvent))
                        {
                            CmdLevelUpAbility(2);
                        }
                        else if (KeyConfig.Instance.QuickCast(ICE.AbilityTwoForger, keyEvent))
                        {
                            CmdCastAbility(2, targetPoint, hitPoint);
                        }
                        else if (KeyConfig.Instance.IndicatorCast(ICE.AbilityTwoForger, keyEvent))
                        {
                            // show indicator and select for casting
                        }

                        if (KeyConfig.Instance.SkillUp(ICE.AbilityThreeForger, keyEvent))
                        {
                            CmdLevelUpAbility(3);
                        }
                        else if (KeyConfig.Instance.QuickCast(ICE.AbilityThreeForger, keyEvent))
                        {
                            CmdCastAbility(3, targetPoint, hitPoint);
                        }
                        else if (KeyConfig.Instance.IndicatorCast(ICE.AbilityThreeForger, keyEvent))
                        {
                            // show indicator and select for casting
                        }

                        if (KeyConfig.Instance.SkillUp(ICE.AbilityFourForger, keyEvent))
                        {
                            CmdLevelUpAbility(4);
                        }
                        if (KeyConfig.Instance.QuickCast(ICE.AbilityFourForger, keyEvent))
                        {
                            CmdCastAbility(4, targetPoint, hitPoint);
                        }
                        else if (KeyConfig.Instance.IndicatorCast(ICE.AbilityFourForger, keyEvent))
                        {
                            // show indicator and select for casting
                        }

                        if (KeyConfig.Instance.QuickCast(ICE.PlaceWardForger, keyEvent))
                        {
                            CmdPlaceWard(targetPoint);
                        }
                        else if (KeyConfig.Instance.IndicatorCast(ICE.PlaceWardForger, keyEvent))
                        {
                            // show indicator and select for casting
                        }

                        if (KeyConfig.Instance.QuickCast(ICE.AttackCursorForger, keyEvent))
                        {
                            // just auto attack toward the selected point or target
                        }
                        else if (KeyConfig.Instance.IndicatorCast(ICE.AttackCursorForger, keyEvent))
                        {
                            // show indicator and set cursor for attack
                            attackCursor = !attackCursor;

                            if (attackCursor)
                            {
                                CursorManager.Instance.SetCursor(CursorType.select);
                            }
                            else
                            {
                                CursorManager.Instance.SetCursor(CursorType.pointer);
                            }
                        }


                        if (KeyConfig.Instance.QuickCast(ICE.RecallForger, keyEvent))
                        {
                            // Tell the server we're ready to recall. 
                            CmdStartRecall();
                        }
                        else if (KeyConfig.Instance.IndicatorCast(ICE.RecallForger, keyEvent))
                        {
                            // show indicator and set cursor for just move

                            // queue up recall as soon as we've reached the destination

                        }


                    }
                }
                }
            }
        }


        [TargetRpc]
        public void TargetObeliskAlert(NetworkConnection target) {
            
            // TODO redo this once we have an audio clip manager of some sort.
            var alertClip = (AudioClip)Resources.Load("Audio/SFX/Obelisk Alert_mixdown");
            AudioController.Instance.PlayOneShot(alertClip, 20f);

        }

        [ClientRpc]
        public void RpcEndgameMessage (int teamEnum) {
            Team team = (Team)teamEnum;

            GameManager.Instance.SetGameOver(true, team);

        }

        #if UNITY_EDITOR
        public float CameraRayLength = 10000f;

        void OnDrawGizmosSelected () {
            if (playerCamCon != null) {
                Ray clickRay = playerCamCon.camera.ScreenPointToRay(Input.mousePosition);
                Gizmos.DrawLine(clickRay.origin, clickRay.direction * CameraRayLength);
            }

        }
        #endif
    }


    public class MapTrigger : EventTrigger
    {
        public PlayerKnob knob;
        public Camera mapCamera;

        public override void OnPointerClick(PointerEventData eventData)
        {
            Debug.Log("clicked on live minimap");

            if (eventData.button == PointerEventData.InputButton.Right)
            {
                /*  // This doesn't work
                Vector2 localCursor;
                var rect1 = GetComponent<RectTransform>();
                var pos1 = eventData.position;
                var hudCamera = GameObject.Find("HudCamera").GetComponent<Camera>();
                Debug.LogFormat("event cursor Pos: {0} {1}", eventData.position.x, eventData.position.y);


                if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(rect1, pos1,
                    hudCamera, out localCursor))
                    return;
                

                int xpos = (int)(localCursor.x);
                int ypos = (int)(localCursor.y);

                xpos = xpos + (int)(rect1.rect.width / 2);


                ypos = ypos + (int)(rect1.rect.height / 2);


                Debug.LogFormat("Correct Cursor Pos: {0} {1}", xpos, ypos);

                RaycastHit hit;
                Ray altray = mapCamera.ScreenPointToRay(new Vector3(xpos, ypos));
                if (Physics.Raycast(altray, out hit))
                {
                    knob.SetPoint(new Vector3(hit.point.x, 0, hit.point.y));
                }
                */
            }            
        }

    }
}