﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SteeringAgent : MonoBehaviour {

    public FieldNodeTuple nearestNode;
    public float moveSpeed;
    public bool moving;
    public NavMeshAgent agent;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (moving && nearestNode != null)
        {
            
            var direction = new Vector3(nearestNode.direction.x, 0, nearestNode.direction.y);

            agent.Move(direction.normalized * moveSpeed * Time.deltaTime);

            //agent.steeringTarget = transform.position + direction.normalized;


        }
	}
}
