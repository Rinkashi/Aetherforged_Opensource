﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;
using core.lane;

namespace controllers.ai {
    public class TowerAI : UnitAI<Tower> {


        Unit lastTarget;


        // Update is called once per frame
        protected override void DoBrain ()
        {
            base.DoBrain();

            currentAction = ActionState.HoldGround;

            if (HasTargetUnit && me.IsInRange(TargetUnit, me.AttackRange))
            {
                me.RpcShowAttackIndicator();
                me.RpcAttackIndicator(TargetUnit.transform.position);
                if (lastTarget != TargetUnit)
                {
                    lastTarget = TargetUnit;
                    TargetUnit.ObeliskAlert();
                }
            }
            else
            {
                me.RpcHideAttackIndicator();
                lastTarget = null;
            }

        }

        protected override int GetEnemyPriority (Unit unit)
        {
            // The tower initially targets minions, prioritizing pet units, then super minions, then melee minions, and finally ranged minions. 
            // If there are no minions, the tower will begin targeting forgers instead. 
            // If a pet unit or forger deals damage to an enemy forger and was targeting minions, the tower re evaluates its target, aiming for the forger that was the source of the damage. 
            // It retains that forger as its target until the forger is either dead or leaves the range of the tower, at which point it returns to minion wave prioritization. 
            // The more times the tower attacks a forger, its penetration against forger armor increases, up to a cap.

            // -1 Aggro forger (sticky target)
            // 4 Pets
            // 5 super minions
            // 6 Melee Minions 
            // 7 ranged Minions
            // 8 Forger


            int priority = 10;
            switch (unit.Kind) {
            case UnitKind.Minion:
                // TODO add super minions and pets. 
                if (unit.AttackRange == Constants.MELEE_DEFAULT_ATTACK_RANGE) {
                    priority = 6;
                } else {
                    priority = 7;
                } 
                break;
            case UnitKind.Forger:
                priority = 8;
                break;
            default:
                priority = 9;     // this is a unit in aggro range. 
                //                  AddTarget(9,unit);    
                break;
            }
            return priority;

        }

        //      Minion aggro priority: (which also works for towers if we ignore 2,3,4,5, and 7)
        //  1 A An enemy forger designated by a call for help from an allied forger. (Enemy forger attacking an Allied forger)
        //  2 B     An enemy minion designated by a call for help from an allied forger. (Enemy minion attacking an Allied forger)
        //  3 C         An enemy minion designated by a call for help from an allied minion. (Enemy minion attacking an Allied minion)
        //  4 D             An enemy turret designated by a call for help from an allied minion. (Enemy turret attacking an Allied minion)
        //  5 E                 An enemy forger designated by a call for help from an allied minion. (Enemy forger attacking an Allied minion)
        //  6 F                     The closest enemy minion.
        //  7 F                     The closest enemy turret.
        //  8 F                     The closest enemy forger.



        public void AttackMyTarget( Unit caller, Unit target ) {
            if (caller.Kind == UnitKind.Forger && target.Kind == UnitKind.Forger 
                && caller.TeamQualifiesAs(TeamQualifier.FriendlyTeam, me.Team)) {

                AddTarget(1, target);   // forger threat, aggro attack.
            }
        }
    }
}
