﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using UnityEditor;

class Buildotron {
    static string[] SCENES = FindEnabledEditorScenes();

    static string APP_NAME = "AetherForged"; //JUST DP OT
    static string TARGET_DIR = "../Builds/";

    [MenuItem("Buildotron/BuildWindows64")]
    static void PerformBuildW64() {
#if UNITY_EDITOR_OSX
        string branch_name =  "Probably minions ;_;";
#else
        string branch_name =  Environment.GetCommandLineArgs()[Environment.GetCommandLineArgs().Length - 1];
#endif
        GenericBuild(SCENES, TARGET_DIR + "/" + branch_name + "/" + APP_NAME + ".exe", BuildTarget.StandaloneWindows64, BuildOptions.None);
    }

    private static string[] FindEnabledEditorScenes() {
        List<string> EditorScenes = new List<string>();
        foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes) {
            if (!scene.enabled) continue;
            EditorScenes.Add(scene.path);
        }
        return EditorScenes.ToArray();
    }

    static void GenericBuild(string[] scenes, string target_dir, BuildTarget build_target, BuildOptions build_options) {
        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, build_target);
        // string res = BuildPipeline.BuildPlayer(scenes, target_dir, build_target, build_options);
        // if (res.Length > 0) {
        //     throw new Exception("BuildPlayer failure: " + res);
        // }
    }
}

#endif