using UnityEngine;
using System;

namespace core {
    [Serializable]
	public class ForgerResource {
        public ResourceFrame frame;

        public float Amount = 0;

        public string Name {
            get {
                if (frame != null)
                {
                    return frame.Name;
                }
                return "n/a";
            }
        }


        public float MaxAmount {
            get {
                if (frame != null)
                {
                    return frame.MaxAmount;
                }
                return 0;
            }
        }
        public float MinAmount {
            get {
                if (frame != null)
                {
                    return frame.MinAmount;
                }
                return 0;
            }
        }

        public Action<ForgerResource, float> Callback 
        {
            get;
            set;
        }

        public Color Color {
            get {
                if (frame != null)
                {
                    return frame.Color;
                }
                return Color.grey;
            }
        }

        public float Ratio {
            get {return (Amount - MinAmount) / (MaxAmount - MinAmount); }
        }

        public string Prettified {
            get { return string.Format("{0} / {1}", Mathf.Round(Amount * 10f) / 10f, MaxAmount); }
        }
            

        public void Degrade(float targetValue, float rate) {
            Amount = Mathf.MoveTowards(Amount, targetValue, rate);
        }

        private static void ResourceChanged(ForgerResource resource, float delta) {

            //resource.Callback(resource, delta);
            /*
            resource.Unit.GetPassivesAndBuffs((pair) => {return pair.ability.OnResourceChanged != null;} ).ForEach((SourcePair pair) => {
                var data = resource.Unit.GetPassiveData(pair);
                pair.ability.OnResourceChanged(pair,resource.Unit, data, delta);
            });
            */
        }

        public static ForgerResource operator +(ForgerResource resource, float amount) {
            if (resource == null) {
                resource = new ForgerResource() { Amount = 0 };
            }
            var oldAmount = resource.Amount;
            resource.Amount += amount;
            resource.Amount = Mathf.Clamp(resource.Amount, resource.MinAmount, resource.MaxAmount);
            if(resource.Amount != oldAmount) {
                ResourceChanged(resource, resource.Amount - oldAmount);
            }
            return resource;
        }

        public static ForgerResource operator -(ForgerResource resource, float amount) {
            return resource + (0f - amount);
        }
        public static bool operator <(ForgerResource resource, float amount) {
            return resource.Amount < amount;
        }
        public static bool operator >(ForgerResource resource, float amount) {
            return resource.Amount > amount;
        }
        public static bool operator >=(ForgerResource resource, float amount) {
            return resource.Amount >= amount;
        }
        public static bool operator <=(ForgerResource resource, float amount) {
            return resource.Amount <= amount;
        }
        public override int GetHashCode() {
            return (Name.ToString() + Amount.ToString() + MaxAmount.ToString() + MinAmount.ToString() + Color.ToString()).GetHashCode();
        }
        public override bool Equals(object obj) {
            if(obj.GetType() == typeof(ForgerResource)) {
                return ((ForgerResource)obj).Amount == Amount;
            }
            return base.Equals(obj);
        }
    }
}