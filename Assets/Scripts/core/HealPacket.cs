﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace core{

    public class HealPacket {
        public HealPacket(Entity source, HealActionType kind) {
            Kind = kind;
            Source = source;
        }

        public HealPacket(HealActionType kind, Entity source, bool fountain = false) {
            Kind = kind;
            Source = source;
            IsSaltwater = fountain;
        }

        public Entity Source
        {
            get;
            protected set;
        }

        public HealActionType Kind
        {
            get;
            protected set;
        }
        public float HealAmount;
        public float ShieldAmount;
        public float PeriodOfTime;

        public Unit PrimaryTarget;
        /// <summary>
        /// True if this packet should ignore reduction.
        /// </summary>
        public bool IsSaltwater
        {
            get;
            protected set;
        }

        public float PercentPreHealModifier;
        public float PercentPreShieldModifier;

        public float PreMitigationHeal
        {
            get;
            protected set;
        }
        public float PreMitigationShield
        { 
            get;
            protected set;
        }
        public bool HasMortalStrike;

        private bool precalculated = false;
        private bool mitigated = false;

        public float PostMitigationHeal;
        public float PostMitigationShield;
        public float PostHealOverflow
        {
            get;
            protected set;
        }

        public bool IsPrimaryTarget
        {
            get;
            protected set;
        }

        public float PercentPostHealModifier;
        public float PercentPostShieldModifier;

        /// <summary>
        /// Resets the modifiers that are specific to damage targets.
        /// We generally only need to do this when closing out Passive/Buff triggers 
        /// that modify incoming damage. Especially in a multi-hit situation. 
        /// </summary>
        public void ResetTargetOrientedModifiers() {
            PercentPostHealModifier = 0;
            PercentPostShieldModifier = 0;
            HasMortalStrike = false;

            mitigated = false;
        }

        public void Precalculate() {
            PreMitigationHeal = HealAmount * PercentPreHealModifier.BonusPercent();
            PreMitigationShield = ShieldAmount * PercentPreShieldModifier.BonusPercent();

            precalculated = true;
            mitigated = false;
        }


        public void CalculateForTarget(Unit targetUnit) {
            if (!precalculated)
                Precalculate();

            IsPrimaryTarget = (PrimaryTarget == targetUnit);

            if (HasMortalStrike && !IsSaltwater)
            {
                PostMitigationHeal = (PreMitigationHeal * PercentPostHealModifier.BonusPercent() 
                    * -50f.BonusPercent());
                PostMitigationShield = (PreMitigationShield * PercentPostShieldModifier.BonusPercent() 
                    * -50f.BonusPercent());
            }
            else
            {
                PostMitigationHeal = (PreMitigationHeal * PercentPostHealModifier.BonusPercent());
                PostMitigationShield = (PreMitigationShield * PercentPostShieldModifier.BonusPercent());
            }

            var overheal = PostMitigationHeal + targetUnit.Health - targetUnit.MaxHealth;

            if (overheal > 0)
            {
                PostHealOverflow = overheal;
            }

            mitigated = true;
        }

    }

    public class HealActionType {
        public bool IsShield;

        public static readonly HealActionType HealBurst = new HealActionType();
        public static readonly HealActionType Shield = new HealActionType { IsShield = true };
        public static readonly HealActionType HealOverTime = new HealActionType();
        public static readonly HealActionType MagicShield = new HealActionType { IsShield = true };
        public static readonly HealActionType PhysicalShield = new HealActionType { IsShield = true };
    }
}