﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

namespace core {


    public abstract class Kit : ScriptableObject {
        [SerializeField]
        string kitName = "";

        [SerializeField]
        float attackHitTime = 1;

        public Dictionary<object, Passive> Passives = new Dictionary<object, Passive>();
        public Dictionary<object, Castable> Skills = new Dictionary<object, Castable>();

        [SerializeField]
        public Passive[] passives = new Passive[0];
        [SerializeField]
        public SkillRecord[] skills = new SkillRecord[0];

        [System.Obsolete("Use Initialize instead") ]
        public virtual Ability[] InitAbilities(){ return new Ability[0];}

        [System.Obsolete("Define passives in Initialize()")]
        public virtual Passive[] InitPassives() { 
            return new Passive[0]; 
        } 

        [System.Obsolete("Define castables in Initialize()")]
        public virtual Castable[] InitCastables() { 
            return new Castable[0]; 
        } 

        public void InitForUnit(Unit unit)
        {
            Initialize(unit);
            hasInitialized = true;
        }

        /// <summary>
        /// define Passives and Skills (Castable) somewhere in here.
        /// </summary>
        protected virtual void Initialize (Unit me) {
            hasInitialized = true;
        }

        public string Name {
            get{
                return kitName;
            }
        }

        /// <summary>
        /// Gets the fraction of the whole attack animation that the wind up takes. 
        /// </summary>
        /// <value>The attack hit time.</value>
        public float AttackHitTime {
            get {
                return attackHitTime;
            }
        }

        public virtual string AutoAttackProjectile {
            get { return "AA_projectile"; }
        }

        [SerializeField]
        public ResourceFrame resourceFrame;

        public ParticleHelper ParticleHelper;

        public abstract UnitStats GetBaseStats();


        public virtual UnitStats PerLevelStats {
            get;
            protected set;
        }

        private bool hasInitialized = false;
		public bool HasInitialized {
		    get { return hasInitialized; }
		}

        public bool Ready {
            get {
                //if (!hasInitialized) {
                //    Initialize();
                //}
                return true;
            }
        }

        //void Update() {
        //    if (Ready) {   
        //        foreach (KeyValuePair<object, Passive> kv in Passives) {
        //            var ability = kv.Value;
        //            ability.CurrentCooldown = Mathf.Max(0, ability.CurrentCooldown - Time.deltaTime);
        //        }
        //        // TODO 2017 Jun 29 We may want other skills to cooldown too, but don't want to double up on the current QWER.
        //        for (int index = 1; index <= 4; index++) {
        //            if (Skills.ContainsKey(index)) {
        //                var currentCooldown = Skills [index].CurrentCooldown;
        //                Skills [index].CurrentCooldown = Mathf.MoveTowards(currentCooldown, 0, Time.deltaTime);
        //            }
        //        }
        //    }
        //}

        private void DoPrivateInit () {
            //ParticleHelper = new ParticleHelper();
            //ParticleHelper.Unit = GetComponentInParent<Unit>();
            //Initialize();


            // FIXME don't actually need to do this. 
            //GetBaseStats().applyStatsToUnit(GetComponentInParent<Unit>());

            //                GetComponentInParent<Unit>().Spawn();

            hasInitialized = true;
        }

          

        public static void ForEnemiesInRadius(float radius, Unit caster, System.Action<Unit> action) {
            Unit.ForEachEnemyInRadius(radius, caster.Position, caster.Team, action);
        }
        
        //same as above, just checks for allied units instead
        public static void ForAlliesInRadius(float radius, Unit caster, System.Action<Unit> action) {
            Unit.ForEachAllyInRadius(radius, caster.Position, caster.Team, action);
        }

        /*
        public override void OnStartClient() {
            // Find our player (UnitForger) local game object
            GameObject player = ClientScene.FindLocalObject(parentNetID);
            var unit = GetComponent<Unit>();



            if (player != null)
            {
                // attach our model to the UnitForger and zero it's nested position.
                transform.SetParent(player.transform);
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                if (unit == null)
                {
                    // the parent is the unit. Just go along for the ride. 
                    transform.localPosition = Vector3.zero;
                    unit = player.GetComponent<Unit>();
                }
                else
                {
                    // the parent is probably our "frame" our map location should match. 

                    Vector3 posey = transform.localPosition;
                    posey.x = 0;
                    posey.z = 0;
                    transform.localPosition = posey;

                    // TODO 2017 Jun 12 might slave the frame animationController to ours. 
                }
            }

            base.OnStartClient();
        }
        */

        [System.Obsolete("Please use UnitStats.Stats instead. ")]
        public UnitStats Stats(string Name = "_carry", bool upsearch = false, int Level = 0
			, float Health = 0f, float MaxHealth = 0f, float HealthRegen = 0, float Armor = 0, float Resistance = 0
			, float Power = 0f, float Haste = 0, float Mastery = 0, float Penetration = 0, float Lifedrain = 0
			, float BaseAttackTime = 0f, float AttackSpeed = 0f, float AttackHitTime = 0f, float BaseAttackRange = 0f
			, float BasePhysicalDamage = 0f, float FlatPhysicalDamage = 0f, float IncreasedPhysicalDamage = 0f
            , float Hephisalt =0f, float Experience = 0f
			, float BaseMovementSpeed = 0f, float FlatMovementSpeed = 0f, float PercentMovementSpeed = 0f) {

            UnitStats unitStats = new UnitStats();

			unitStats.Power = Power;
			unitStats.Haste = Haste;
			unitStats.Mastery = Mastery;       
			unitStats.Penetration = Penetration;       
			unitStats.Lifedrain = Lifedrain;

			unitStats.MaxHealth = MaxHealth;
			unitStats.HealthRegen = HealthRegen;
			unitStats.Armor = Armor;
			unitStats.Resistance = Resistance;

			unitStats.BaseAttackTime = BaseAttackTime;
			unitStats.AttackSpeed = AttackSpeed;
			unitStats.AttackRange = BaseAttackRange;
			unitStats.AutoAttackDamage = BasePhysicalDamage;


			unitStats.MovementSpeed = BaseMovementSpeed;


            unitStats.HephisaltBounty = Hephisalt;
            unitStats.ExperienceBounty = Experience;


			return unitStats;
		}

        /// <summary>
        /// Scalable stats.
        /// </summary>
        /// <returns>The stats.</returns>
        /// <param name="power">Power.</param>
        /// <param name="haste">Haste.</param>
        public virtual UnitStats ScalableStats(float power, float haste) {
            // Most units don't have scaling ratios.
            return UnitStats.Zero;
        }
    }
}