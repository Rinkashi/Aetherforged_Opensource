﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace core
{
    [CreateAssetMenu(fileName = "New Resource", menuName = "Units/Resource")]
    public class ResourceFrame : ScriptableObject
    {

        public string Name = "n/a";

        public float MaxAmount = 0;
        public float MinAmount = 0;

        public Color Color = new Color(0.3f, 0.3f, 0.3f);

    }
}