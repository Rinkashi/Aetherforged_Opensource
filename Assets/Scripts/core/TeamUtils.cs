﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace core {

    public static class TeamUtils {
        public static Team Opposite(this Team team) {
            if (team == Team.Neutral)
                return Team.Neutral;

            return team == Team.DownLeft ? Team.UpRight : Team.DownLeft;
        }
    }

    public enum Team {
        Neutral,
        UpRight,
        DownLeft,
        Jungle,
        /*  // FIXME this is a bit bonkers
        UpJungle,
        DownJungle,
        LeftJungle,
        RightJungle,
        UpMidJungle,
        DownMidJungle
        */
    }

    public enum TeamQualifier {
        FriendlyTeam = 1 << 0,
        EnemyTeam = 1 << 1,
        NeutralTeam = 1 << 2,
        AnyTeam = 7
    }
}