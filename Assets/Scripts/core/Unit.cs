using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.AI;

using core.utility;
using core.forger;
using core.lane;
using managers;
using controllers;
using feature;
using ui;

namespace core {
	

    public abstract class Unit : Entity {

        readonly int NumberOfSkills = 4;

        public static List<Unit> ListOf = new List<Unit>();

        [SyncVar]
        protected string unitName = "Nameless <debug>";
		public virtual string Name {    // this is OK. I'm OK.
			get { return unitName; }
        }


        [SerializeField]
        [SyncVar]
        protected string kitName;

        public string KitName {
            get {return kitName.ToLower();}
        }

		[SerializeField]
		protected Kit kit;

		[SerializeField]
		protected GameObject modelPrefab;

        /// <summary>
        /// This is the model of this unit in the world. 
        /// </summary>
        [SerializeField]
        protected GameObject model;

        [SerializeField]
        public ParticleHelper ParticleHelper;

        public void SetKit(string kitName) {
            this.kitName = kitName;
		}

        #region statblocks

        [SyncVar]
        [SerializeField]
        protected int level = 1;
        public int Level {
            get { return level; }
            protected set { 
                if (value > level) {
                    level = value;
                    LevelUp(); 
                } else {
                    // should it really though?
                    level = value;
                }
                    
            }
        }

        [SyncVar]
        [SerializeField]
        protected float health;
        public float Health {
            get { return health; }
            protected set {

                var delta = value - health;
                health = value; 

                foreach (KeyValuePair<SourcePair,PassiveTracker> KeyValue in allPassives
                    .Where((KeyValuePair<SourcePair, PassiveTracker> keyValue) => {
                        return keyValue.Key.ability.OnHealthChanged != null;
                    }))
                {
                    var pair = KeyValue.Key;
                    var info = KeyValue.Value;
                    if (pair.CanTrigger && pair.ability.OnHealthChanged != null)
                    {
                        pair.ability.OnHealthChanged(pair, this, info, delta);
                    }
                }
            }
        }

        [SyncVar]
        protected float primaryShield;

        [SyncVar]
        protected float magicalShield;

        [SyncVar]
        protected float physicalShield;

        protected ShieldTracker shields;


        public float Shield {
            get {
                return shields.Value; 
            }
        }

        public float MagicShield {
            get {
                return magicalShield;
            }
        }

        [SerializeField]
        protected UnitStats bareStats = new UnitStats();


        [SerializeField]
        protected UnitStats levelStats = new UnitStats();

        public UnitStats BaseStats {
            get;
            protected set;
        }

        public UnitStats BonusStats
        {
            get;
            protected set;
        }

        public UnitStats TotalStats {
            get { return totalStats; }
        }

        [SerializeField]
        protected UnitStats flatBonus = new UnitStats();

        [SerializeField]
        protected UnitStats percentBonus = new UnitStats();

        [SerializeField]
        protected UnitStats totalStats = new UnitStats();

        [SyncVar]
        [SerializeField]
        protected float bravoSum;

        [SerializeField]
        protected float sumBravo {
            get { return totalStats.BravoSum; }
        }

        public SyncListFloat BravoList = new SyncListFloat();

        [SyncVar]
        [SerializeField]
        protected float alphaSum;

        [SerializeField]
        protected float sumAlpha {
            get { return totalStats.AlphaSum; }
        }

        public SyncListFloat AlphaList = new SyncListFloat();


        [SyncVar]
        [SerializeField]
        protected float charlieSum;

        [SerializeField]
        protected float sumCharlie {
            get { return totalStats.CharlieSum; }
        }

        public SyncListFloat CharlieList = new SyncListFloat();

        /// <summary>
        /// A list with the levels of each ability.
        /// </summary>
        public SyncListInt LevelList = new SyncListInt();

        /// <summary>
        /// The skill slots. First Skill is in slot one (1). 
        /// </summary>
        protected List<Castable> SkillSlots = new List<Castable>();

        /// <summary>
        /// Hashtable associates a skill (ability) with the index in the active ability synclists
        /// </summary>
        Dictionary<Ability, int> SkillHash = new Dictionary<Ability, int>();

        Dictionary<Ability, int> PassiveHash = new Dictionary<Ability, int>();

        public SyncListFloat ActiveCooldowns = new SyncListFloat();

        public SyncListFloat PassiveCooldowns = new SyncListFloat();

        /// <summary>
        /// Hashtable associates an ability with the appropriate slot. 
        /// So far this is only used to get the proper level of a slot. 
        /// </summary>
        Dictionary<Ability, int> AbilityLevelHash = new Dictionary<Ability, int>();

        public float MaxHealth {
            get { return (totalStats.MaxHealth); }
        }

        public float HealthRegen {
            get { return totalStats.HealthRegen; }
        }

        public float HPRatio {
            get {return  Health / MaxHealth; }
        }

        public string HPPrettified {
            get {
                if (Health < MaxHealth) {
                    return string.Format("{0} / {1} +{2}"
                        , Mathf.Round(Health), Mathf.Round(MaxHealth)
                        , Mathf.Round(HealthRegen * 10f) / 10f);
                }
                return string.Format("{0} / {1}", Mathf.Round(Health), MaxHealth);
            }
        }

        public float AttackRange {
            // TODO add adjustments
            get { return bareStats.AttackRange; }
        }


        public float Power {
            get {
                return totalStats.Power;
            }
        }

        public float Resistance {
            get {
                return totalStats.Resistance;
            }
        }

        public float Armor {
            get {
                return totalStats.Armor;
            }
        }

        public float MovementSpeed {
            get {
                return totalStats.MovementSpeed;
            }
        }
        public float AutoAttackDamage {
            get {
                return totalStats.AutoAttackDamage;
            }
        }
        public float TotalAttackTime {
            get {
                return totalStats.BaseAttackTime * (1f + totalStats.AttackSpeed / 100f);
            }
        }

        public float PercentPenetration {
            get {
                return percentBonus.Penetration;
            }
        }

        public float PercentBonusCriticalDamage {
            get {
                // TODO Percent Bonus Critical Damage
                return 0f;
            }
        }

        public float WindUpTime {
            get {
                return totalStats.BaseAttackTime * kit.AttackHitTime;
            }
        }

        public float WindDownTime {
            get {
                return totalStats.BaseAttackTime * (1f - kit.AttackHitTime);
            }
        }

        public float AnimationAttackSpeed {
            get {
                if (kit == null) {
                    return 1f / (TotalAttackTime * 0.5f);
                }
                return 1f / (TotalAttackTime * kit.AttackHitTime);
            }
        }

        // Server receives information from buying/selling and works out new total stats
        public void ServerStatsUpdate(UnitStats incomingStats = null, bool healWithMaxHP = false, bool adding = false)
        {
            Debug.Log("SERVER : Processing purchased item on the unit");

            if(adding)
            {
                if(modelReady && kitReady)
                {
                    incomingStats += kit.ScalableStats(incomingStats.Power, incomingStats.Haste);
                }

                if(healWithMaxHP)
                {
                    Health += incomingStats.MaxHealth;
                }

                BonusStats += UnitStats.IncreasedByPercent(incomingStats, percentBonus);
            }
            else
            {
                if (healWithMaxHP) 
                {
                    Health -= incomingStats.MaxHealth;
                }
                if(modelReady && kitReady)
                {
                    incomingStats += kit.ScalableStats(incomingStats.Power, incomingStats.Haste);
                }

                BonusStats -= UnitStats.IncreasedByPercent(incomingStats, percentBonus);
            }
            totalStats = BaseStats + BonusStats;

            Debug.Log("SERVER : " + "BareStats movementSpeed : " + BaseStats.MovementSpeed + " | BonusStats movementSpeed : " + BonusStats.MovementSpeed);
            RpcAdjustStats(totalStats);
        }
        
        // Server calls this to adjust the stats on the clients
        [ClientRpc]
        public void RpcAdjustStats(UnitStats updatedTotal)
        {
            Debug.Log("CLIENT : Adjusting Stats client side");

            totalStats = updatedTotal;

            Debug.Log("CLIENT : " + " updatedTotal movementSpeed : " + updatedTotal.MovementSpeed + "MovementSpeed in total stats now : " + totalStats.MovementSpeed);
        }

        /// <summary>
        /// Adjusts the stats associated with a named passive/item/.
        /// </summary>
        /// <returns>The difference between incoming and current stats.</returns>
        /// <param name="name">Name.</param>
        /// <param name="incomingStats">Incoming unit stats.</param>
        private UnitStats AdjustStats(string name, UnitStats incomingStats) {
            UnitStats current;
            UnitStats delta;

            if (staticStats.ContainsKey(name))
            {
                current = staticStats[name];
                delta = incomingStats - current;
                staticStats[name] = incomingStats;
            }
            else
            {
                current = UnitStats.Zero;
                delta = incomingStats;
                staticStats.Add(name, incomingStats);
            }

            flatBonus += delta + kit.ScalableStats(delta.Power, delta.Haste);

            BonusStats += UnitStats.IncreasedByPercent(delta, percentBonus);

            totalStats = BaseStats + BonusStats;
            return delta;
        }


        /// <summary>
        /// Adjusts the stats associated with a named castable ability.
        /// </summary>
        /// <returns>The difference between incoming and current stats.</returns>
        /// <param name="ability">Ability.</param>
        /// <param name="incomingStats">Incoming unit stats.</param>
        public UnitStats AdjustStats(Castable ability, UnitStats incomingStats) {
            return AdjustStats(ability.Name, incomingStats);
        }

        /// <summary>
        /// Adjusts the stats associated with an _item_.
        /// </summary>
        /// <returns>The difference between incoming and current stats.</returns>
        /// <param name="item">Item.</param>
        /// <param name="incomingStats">Incoming unit stats.</param>
        public UnitStats AdjustStats(Item item, UnitStats incomingStats) {
            return AdjustStats(item.Name, incomingStats);
        }

        /// <summary>
        /// Adjusts the stats associated with a named passive.
        /// </summary>
        /// <returns>The difference between incoming and current stats.</returns>
        /// <param name="ability">Ability.</param>
        /// <param name="incomingStats">Incoming unit stats.</param>
        public UnitStats AdjustStats(SourcePair pair, UnitStats incomingStats) {
            return AdjustStats(pair.ID, incomingStats);
        }

        public bool RecalculateDynamicStats(SourcePair pair) {
            if (allPassives.ContainsKey(pair) && pair.ability.DynamicStats != null)
            {
                var data = allPassives[pair];
                AdjustStats(pair, pair.ability.DynamicStats(pair, this, data));
                return true;
            }
            return false;
        }

        public void RemoveStats (UnitStats incomingStats, bool HealWithMaxHP = false) {

            flatBonus -= incomingStats;

            BonusStats -= UnitStats.IncreasedByPercent(incomingStats, percentBonus);

            if (HealWithMaxHP) {
                Health -= incomingStats.MaxHealth;
            }

            totalStats = BaseStats + BonusStats;

        }

        /// <summary>
        /// Adjusts the percentile stats associated with a named passive/item/.
        /// </summary>
        /// <returns>The difference between incoming and current stats.</returns>
        /// <param name="name">Name.</param>
        /// <param name="incomingStats">Incoming unit stats.</param>
        private UnitStats AdjustPercentileStats(string name, UnitStats incomingStats) {
            UnitStats current;
            UnitStats delta;

            if (percentileStats.ContainsKey(name))
            {
                current = percentileStats[name];
                delta = incomingStats - current;
                percentileStats[name] = incomingStats;
            }
            else
            {
                current = UnitStats.Zero;
                delta = incomingStats;
                percentileStats.Add(name, incomingStats);
            }


            percentBonus += delta;

            BonusStats += UnitStats.IncreasedByPercent(flatBonus, delta) + UnitStats.OnlyPercent(BaseStats, delta);

            totalStats = BaseStats + BonusStats;
            return delta;
        }



        /// <summary>
        /// Adjusts the stats associated with a named castable ability.
        /// </summary>
        /// <returns>The difference between incoming and current stats.</returns>
        /// <param name="ability">Ability.</param>
        /// <param name="incomingStats">Incoming unit stats.</param>
        public UnitStats AdjustPercentileStats(Castable ability, UnitStats incomingStats) {
            return AdjustPercentileStats(ability.Name, incomingStats);
        }

        /// <summary>
        /// Adjusts the stats associated with an _item_.
        /// </summary>
        /// <returns>The difference between incoming and current stats.</returns>
        /// <param name="item">Item.</param>
        /// <param name="incomingStats">Incoming unit stats.</param>
        public UnitStats AdjustPercentileStats(Item item, UnitStats incomingStats) {
            return AdjustPercentileStats(item.Name, incomingStats);
        }

        /// <summary>
        /// Adjusts the stats associated with a named passive.
        /// </summary>
        /// <returns>The difference between incoming and current stats.</returns>
        /// <param name="ability">Ability.</param>
        /// <param name="incomingStats">Incoming unit stats.</param>
        public UnitStats AdjustPercentileStats(Passive ability, UnitStats incomingStats) {
            return AdjustPercentileStats(ability.Name, incomingStats);
        }

        /// <summary>
        /// Removes the unit stats associated stats.
        /// </summary>
        /// <returns>The unit stats being removed.</returns>
        /// <param name="name">Name.</param>
        private UnitStats RemovePercentileStats(string name) {

            if (percentileStats.ContainsKey(name))
            {
                UnitStats current = percentileStats[name];
                percentileStats.Remove(name);

                percentBonus -= current;

                BonusStats -= UnitStats.IncreasedByPercent(flatBonus, current) + UnitStats.OnlyPercent(BaseStats, current);

                totalStats = BaseStats + BonusStats;
                return current;
            }

            // All we gotta do if there's nothing to remove is return nil unit stats.
            return UnitStats.Zero;
        }



        /// <summary>
        /// Removes unit stats associated with the castable ability.
        /// </summary>
        /// <returns>The unit stats being removed.</returns>
        /// <param name="ability">Ability with associated stats.</param>
        public UnitStats RemovePercentileStats(Castable ability) {
            return RemovePercentileStats(ability.Name);
        }

        /// <summary>
        /// Removes unit stats associated with the passive ability.
        /// </summary>
        /// <returns>The unit stats being removed.</returns>
        /// <param name="ability">Ability with associated stats.</param>
        public UnitStats RemovePercentileStats(Passive ability) {
            return RemovePercentileStats(ability.Name);
        }

        /// <summary>
        /// Removes unit stats associated with the item.
        /// </summary>
        /// <returns>The unit stats being removed.</returns>
        /// <param name="item">Item with associated stats.</param>
        public UnitStats RemovePercentileStats(Item item) {
            return RemovePercentileStats(item.Name);
        }


        public void ApplyBaseStats (UnitStats incomingStats, bool HealWithMaxHP = true) {

            BonusStats -= UnitStats.OnlyPercent(BaseStats, percentBonus);
            BaseStats += incomingStats;
            BonusStats += UnitStats.OnlyPercent(BaseStats, percentBonus);

            if (HealWithMaxHP) {
                Health += incomingStats.MaxHealth;
            }

        }

        public void UndoBaseStats (UnitStats incomingStats, bool HealWithMaxHP = false) {

            BonusStats -= UnitStats.OnlyPercent(BaseStats, percentBonus);
            BaseStats -= incomingStats;
            BonusStats += UnitStats.OnlyPercent(BaseStats, percentBonus);

            if (HealWithMaxHP) {
                Health -= incomingStats.MaxHealth;
            }

        }

        #endregion


        #region passives, buffs, and debuffs
        /// <summary>
        /// The stackable and unique passives in a hashed dictionary.
        /// </summary>

        protected Dictionary<SourcePair, PassiveTracker> allPassives = new Dictionary<SourcePair, PassiveTracker>();

        protected List<SourcePair> allBuffs = new List<SourcePair>();


        /// <summary>
        /// The calculated bonus stats provided by each source of bonus stats. 
        /// </summary>
        protected Dictionary<string, UnitStats> staticStats = new Dictionary<string, UnitStats>();

        /// <summary>
        /// The calculated bonus percentile stats provided by various sources.
        /// </summary>
        protected Dictionary<string, UnitStats> percentileStats = new Dictionary<string, UnitStats>();

        /// <summary>
        /// Some floating float values.
        /// </summary>
        protected Dictionary<string, float> floatingFloatValues = new Dictionary<string, float>();


        public DisablesList Disables;
        public float CurrentSlowAmount = 0f;
        public float RunnerUpSlowAmount = 0f;
        public SourcePair TopSlow {
            get;
            protected set;
        }
        /// <summary>
        /// Gets or sets the next highest slow.
        /// </summary>
        /// <value>The runner up slow.</value>
        public SourcePair NextMostSlow
        {
            get;
            protected set;
        }

        public bool CanControl {
            get {
                return
                    !InForcedAbilityAnim &&
                    !IsDead &&
                    GetComponent<OtherProjectileController>() == null &&
                    (GetComponent<KnockbackController>() == null
                        || !GetComponent<KnockbackController>().IsKnockedBack);
            }
        }

        public void ApplyDisablingFeatures(DisablingFeatures statusDisables)
        {
            Disables.Add(statusDisables);
        }

        public void RemoveDisablingFeatures(DisablingFeatures statusDisables)
        {
            Disables.Remove(statusDisables);
        }

        public bool CanMove
        {
            get
            {
                return !Disables.Disablings.Movement
                                // FIXME knockbacks should probably be integrated with Statuses
                                && (knockbackController == null || !knockbackController.IsKnockedBack);
            }
        }

        public bool CanCast
        {
            get
            {
                return !Disables.Disablings.Casting;
            }
        }

        public bool Crippled
        {
            get
            {
                return Disables.Disablings.Cripple;
            }
        }


        public bool InForcedAbilityAnim;
        public bool InAbilityAnim;

        public void PullSlow(SourcePair pair)
        {
            if (pair.ability.Equals(TopSlow))
            {
                var data = allPassives[pair];
                var incomingSlowPercent = ((SlowStatus)pair.ability).SlowAmountAtTime(data.TimeSinceApplication);
                float incomingFlatSlow = 0f;
                if (((SlowStatus)pair.ability).IsFlat)
                {
                    // Wait nevermind. It's not a percent. 
                    incomingFlatSlow = incomingSlowPercent;
                }
                else
                {
                    // get our movement speed without the current slow.
                    var noSlowMovementSpeed = MovementSpeed + CurrentSlowAmount;
                    incomingFlatSlow = noSlowMovementSpeed * incomingSlowPercent.Percent();
                }

                if (incomingFlatSlow < RunnerUpSlowAmount)
                {
                    PushSlow(NextMostSlow);
                }
                else if (incomingFlatSlow < CurrentSlowAmount) 
                {
                    CurrentSlowAmount = incomingFlatSlow;

                    AdjustStats("Top Slow", new UnitStats
                    {
                        MovementSpeed = -CurrentSlowAmount,
                    });
                }
            }
        }

        /// <summary>
        /// Pushes a slow onto unit. If the amount is greater than the current slow, bump up the current slow. 
        /// </summary>
        /// <returns><c>true</c>, if slow was pushed up, <c>false</c> otherwise.</returns>
        /// <param name="pair">Source Pair with a SlowStatus.</param>
        public bool PushSlow(SourcePair pair)
        {
            if(pair.ability is SlowStatus)
            {
                var data = allPassives[pair];
                var incomingSlowPercent = ((SlowStatus)pair.ability).SlowAmountAtTime(data.TimeSinceApplication);
                float incomingFlatSlow = 0f;
                if (((SlowStatus)pair.ability).IsFlat)
                {
                    // Wait nevermind. It's not a percent. 
                    incomingFlatSlow = incomingSlowPercent;
                }
                else 
                {
                    // get our movement speed without the current slow.
                    var noSlowMovementSpeed = MovementSpeed + CurrentSlowAmount;
                    incomingFlatSlow = noSlowMovementSpeed * incomingSlowPercent.Percent();
                }

                if (incomingFlatSlow > CurrentSlowAmount)
                {
                    if (!pair.Equals(TopSlow))
                    {
                        NextMostSlow = TopSlow;
                        RunnerUpSlowAmount = CurrentSlowAmount;
                        TopSlow = pair;
                    }

                    CurrentSlowAmount = incomingFlatSlow;

                    AdjustStats("Top Slow", new UnitStats
                    {
                        MovementSpeed = -incomingFlatSlow,
                    });
                    return true;
                }
                else if (incomingFlatSlow > RunnerUpSlowAmount)
                {
                    RunnerUpSlowAmount = incomingFlatSlow;
                    NextMostSlow = pair;
                }
            }
            return false;
        }

        /// <summary>
        /// Removes the current top slow if its pair is removed. Look for the next highest slow.
        /// </summary>
        /// <param name="pair">Source Pair.</param>
        public void RemoveSlow(SourcePair pair)
        {
            if (pair.Equals(NextMostSlow))
            {
                NextMostSlow = null;
            }


            if (pair.Equals(TopSlow) && NextMostSlow != null)
            {
                PushSlow(NextMostSlow);
            }
            else if (pair == TopSlow) {

                SourcePair tempTop = null;
                float highestSoFar = 0f;

                foreach (var slow in allBuffs.Where((arg) => { return (arg.ability is SlowStatus); }))
                {
                    var data = allPassives[slow];
                    float sliceFlatAmount;

                    float sliceSlowPercent = ((SlowStatus)slow.ability).SlowAmountAtTime(data.TimeSinceApplication);

                    if (((SlowStatus)slow.ability).IsFlat)
                    {
                        // Wait nevermind. It's not a percent. 
                        sliceFlatAmount = sliceSlowPercent;
                    }
                    else
                    {
                        // get our movement speed without the old slow.
                        var noSlowMovementSpeed = MovementSpeed + CurrentSlowAmount;
                        sliceFlatAmount = noSlowMovementSpeed * sliceSlowPercent.Percent();
                    }

                    if (sliceFlatAmount > highestSoFar)
                    {
                        highestSoFar = sliceFlatAmount;
                        tempTop = slow;
                    }
                }

                AdjustStats("Top Slow", new UnitStats
                {
                    MovementSpeed = -highestSoFar,
                });

                TopSlow = tempTop;
                CurrentSlowAmount = highestSoFar;
            }
        }

        [Obsolete("Transition to unit.PushSlow(pair)")]
        public void ApplySlow(Status status)
        {
            var slowAmount = status.RelevantNumbers["Initial Slow Number"];
            var isFlat = (status.RelevantNumbers["Is Flat Slow"] > 0);
            if (!isFlat)
            {
                slowAmount = totalStats.MovementSpeed * slowAmount;
            }

            if (CurrentSlowAmount < slowAmount)
            {
                CurrentSlowAmount = slowAmount;
                ServerStatsUpdate(new UnitStats { MovementSpeed = slowAmount }, false, false);
            }
        }

        [Obsolete("Transition to RemoveSlow(pair)")]
        public void RemoveSlow(Status status)
        {
            //TODO: REMOVE SLOW AND RECALCULATE STATS - More officially
            var slowAmount = status.RelevantNumbers["Initial Slow Number"];
            var isFlat = (status.RelevantNumbers["Is Flat Slow"] > 0);
            if (!isFlat)
            {
                slowAmount = (totalStats.MovementSpeed / slowAmount) - totalStats.MovementSpeed;
            }
            ServerStatsUpdate(new UnitStats { MovementSpeed = slowAmount }, true, false);

            var slowList = allBuffs.Where(x => x.ability.Name == "Slow");

            if (slowList.Any())
            {
                //FIXME we should use a MaxHeap probably
                var newSlowStatus = new Status { };// (Status)Buffs.OrderByDescending(m => (m.ability as Status).RelevantNumbers["Initial Slow Number"]);

                ApplySlow(newSlowStatus);
            }
        }

        /// <summary>
        /// Adds the buff.
        /// </summary>
        /// <param name="buffPair">Buff pair.</param>
        /// <param name="parent">Parent.</param>
        /// <param name="number">Number.</param>
        public void AddBuff(SourcePair buffPair, int number = 1) {

            if (buffPair.ability is Status)
            {
                var statusDebuff = buffPair.ability as Status;
                foreach (var keyValue in allPassives)
                {
                    var pair = keyValue.Key;
                    var data = keyValue.Value;
                    if (pair.CanTrigger && pair.ability.BeforeTakeCC != null)
                    {
                        buffPair = pair.ability.BeforeTakeCC(pair, this, buffPair);
                    }
                }

            }
            else if (buffPair.ability is ShieldBuff)
            {
                shields.Add(buffPair);
            }

            if (buffPair.ability != null)
            {
                AddPassive(buffPair, number);
                AddBuffDebuff_internal(buffPair, buffPair.DefaultDuration);
            }
        }

        public void AddBuff(BuffDebuff buff, Unit source, int number = 1) {
            AddBuff(buff.WithSource(source), number);
        }

        public void AddShield(SourcePair pair) {
            AddBuff(pair);
        }

        private void AddBuffDebuff_internal(SourcePair pair, float duration) {
            var buff = pair.ability as BuffDebuff;
            if (float.IsNaN(duration))
            {
                duration = buff.Duration;
            }

            if (!allBuffs.Contains(pair))
            {
                allBuffs.Add(pair);
            }

            if (buff.AffectedByTenacity)
            {
                // Zakki added the Status tenacity which is now in an obsoleted code block. 
                duration = Status.CalculateDurationWithTenacity(duration: duration, tenacity: TotalStats.Tenacity);
            }
                
             


            if (!allPassives.ContainsKey(pair))
            {
                allPassives.Add(pair, new PassiveTracker { appliedTime = GameTime.time, timer = new Timer() });
            }

            if (Mathf.Approximately(duration, 0f) || float.IsNaN(duration))
            {
                // No duration and we've already added the buff. Done. o7
            }
            else if (buff.DoNotRefreshOnStack)
            {
                /* explanation of when we don't refresh on stack: 
                 * We technically don't *need* to track timers, but we do for situations where we 
                 * want to manipulate the timer again. If it doesn't refresh, we don't care if we
                 * have a pre-existing timer; each timer just does what it does without any need
                 * for interaction.
                 */

                allPassives[pair].timer =
                    new Timer().Start(() =>
                    {
                        this.RemoveBuff(pair);
                        return 0f;
                    }, duration);
                allPassives[pair].Duration = duration;
                                                        
            }
            else if (allPassives.ContainsKey(pair) && allPassives[pair].timer != null)
            {
                allPassives[pair].timer.Adjust(duration);
                allPassives[pair].appliedTime = GameTime.time;
                allPassives[pair].Duration = duration;
            }
            else if (buff.DropOffAltogether)
            {
                allPassives[pair].appliedTime = GameTime.time;
                allPassives[pair].Duration = duration;
                allPassives[pair].timer =
                    new Timer().Start(() =>
                    {
                        this.RemoveBuff(pair, fullClear: true);
                        return 0f;
                    }, duration);
            }
            else
            {   // if it drops off one by one AND it's not in timers
                allPassives[pair].appliedTime = GameTime.time;
                allPassives[pair].Duration = duration;
                allPassives[pair].timer = 
                    new Timer().Start(() =>
                    {
                        if (this.RemoveBuff(pair))
                        {   // if there's still buffs remaining, 
                            return duration;
                        }
                        else
                        {
                            return 0f;
                        }
                    }, duration);
            }
        }

        [Obsolete("Use AddBuff(SourcePair buffPair, [int number]) or AddBuff(BuffDebuff buff, Unit source, [int number]) instead. ")]
        public void AddBuff(BuffDebuff buff, Unit source = null, Ability parent = null, int number = 1) {
            SourcePair pair = buff.WithSource(source);
            AddBuff(pair, number);
        }

        /// <summary>
        /// Adds the passive. Optionally add more than one stack.
        /// </summary>
        /// <param name="pair">Pair.</param>
        /// <param name="number">Number.</param>
        protected void AddPassive(SourcePair pair, int number = 1) {
            if (number < 1)
            {
                Debug.LogWarningFormat("number is {0} so we're not adding any stacks.", number);
                return;
            }


            if (allPassives.ContainsKey(pair))
            {
                allPassives[pair].stacks += number;

                if (!pair.DoNotRefreshOnStack)
                {
                    allPassives[pair].appliedTime = GameTime.time;
                }
            }
            else
            {
                allPassives.Add(pair, new PassiveTracker {stacks = number, appliedTime = GameTime.time});
            }
            // regardless, let the source sign their name.
            allPassives[pair].Source = pair.Source;
            // ^ might be redundant
            allPassives[pair].Target = this;

            if (pair.ability.OnApply != null)
            {
                pair.ability.OnApply(pair, this);
            }
        }

        protected void AddPassive(Passive ability, Unit source, int number = 1) {
            SourcePair pair = new SourcePair { ability = ability, Source = source };
            AddPassive(pair, number);
        }

        public bool RemoveBuff(SourcePair pair, int number = 1, bool fullClear = false) {
            if (RemovePassive(pair, number, fullClear))
            {
                return true;
            }
            else
            {
                if (pair.ability is ShieldBuff)
                {
                    shields.Remove(pair);
                }

                allBuffs.Remove(pair);
                return false;
            }
        }

        [Obsolete]
        public void RemoveBuff(BuffDebuff buff, int number = 1, Unit source = null, bool fullClear = false) {
            SourcePair pair = buff.WithSource(source);
            RemoveBuff(pair, number, fullClear);
        }

        /// <summary>
        /// Removes *number* stack of the the passive, or the whole thing if there was only one stack.
        /// </summary>
        /// <returns><c>true</c>, if any stacks of the passive remain, <c>false</c> if the passive is completely gone.</returns>
        /// <param name="pair">A tuple of the passive ability and source.</param>
        /// <param name="fullClear">set to true if you want to clear every stack regardless</param>
        protected bool RemovePassive(SourcePair pair, int number = 1, bool fullClear = false) {
            if (allPassives.ContainsKey(pair))
            {

                // Whenever a stack is removed do the on remove action. 
                if (pair.ability.OnRemove != null)
                {
                    pair.ability.OnRemove(pair, this);
                }

                if (fullClear || allPassives[pair].stacks <= number)
                {
                    allPassives.Remove(pair);
                    return false;
                }
                else
                {
                    allPassives[pair].stacks -= number;
                    return true;
                }

            }
            return false;
        }


        /// <summary>
        /// Determines whether this unit can trigger the specified ability.
        /// </summary>
        /// <returns><c>true</c> if this instance can trigger the specified ability; otherwise, <c>false</c>.</returns>
        /// <param name="ability">Ability.</param>
        public bool CanTrigger(Castable ability) {
            return !SkillOnCooldown(ability) && LevelList[SkillHash[ability]] > 0;
        }

        /// <summary>
        /// Determines whether this unit can trigger the specified ability.
        /// </summary>
        /// <returns><c>true</c> if this instance can trigger the specified ability; otherwise, <c>false</c>.</returns>
        /// <param name="ability">Ability.</param>
        public bool CanTrigger(SourcePair pair) {
            if (pair.Source != null && allPassives.ContainsKey(pair))
            {
                PassiveTracker tracker = allPassives[pair];

                return (pair.ability.TriggerCondition == null || pair.ability.TriggerCondition(pair, this, tracker)) && tracker.Cooldown <= 0f;
            }
            return false;
        }

        public float AbilityLevelBarFillAmount(int slot)
        {

            if (slot > 0 && slot < SkillSlots.Count)
            {
                var ability = SkillSlots[slot];
                if (ability.IsUltimate)
                {
                    return (Math.Max(0, LevelList[slot])) / 3f;
                }
                // FIXME remove magic numbers
                return (Math.Max(0, LevelList[slot])) / 5f;
            }
            else if (slot == 0)
            {
                return LevelList[0] / 20f;
            }

            return 0f;
        }

        /// <summary>
        /// Gets one less than the ability level.
        /// </summary>
        /// <returns>One less than the ability level, for use as an index.</returns>
        /// <param name="ability">Ability.</param>
        public int GetAbilityLevelLessOne(Ability ability) {
            if (ability is BuffDebuff)
            {
                // TODO get level from buff list
                Debug.LogWarning("Buffs and debuffs should be checked via their pair");
                return 0;
            }
            else if (ability is Castable && AbilityLevelHash.ContainsKey(ability))
            {
                int skill = AbilityLevelHash[ability];
                return Math.Max(0, LevelList[skill] - 1);
            }
            else if (ability is Passive && kit.Passives.ContainsValue((Passive)ability))
            {
                return Math.Max(0, LevelList[0] - 1);
            }
            else
            {
                // otherwise assume the level is one.
                return 0;
            }
        }

        public int GetAbilityLevelLessOne(SourcePair pair)
        {
            if (allPassives.ContainsKey(pair))
            {
                return allPassives[pair].Level - 1;
            }
            return 0;
        }

        /// <summary>
        /// Sets the remaining cooldown to a specific time. Optionally can adjust this for cool down reduction
        /// </summary>
        /// <param name="ability">Ability.</param>
        /// <param name="value">Value.</param>
        /// <param name="exact">If set to <c>true</c> use exact value instead of adjusting.</param>
        public void SetCooldown(Castable ability, float value, bool exact = true) {
            if (SkillHash.ContainsKey(ability))
            {
                int index = SkillHash[ability];

                if (exact || totalStats.CooldownReduction <= 0)
                {
                    ActiveCooldowns[index] = value;
                }
                else
                {
                    float cooldownModifier = 1f - totalStats.CooldownReduction.Percent();
                    ActiveCooldowns[index] = value * cooldownModifier;
                }
            }

        }




        /// <summary>
        /// Sets the remaining cooldown to a specific time. Optionally can adjust this for cool down reduction
        /// </summary>
        /// <param name="ability">Ability.</param>
        /// <param name="value">Value.</param>
        /// <param name="exact">If set to <c>true</c> use exact value instead of adjusting.</param>
        public void SetCooldown(SourcePair pair, float value, bool exact = true) {
            if (allPassives.ContainsKey(pair))
            {
                if (exact || totalStats.CooldownReduction <= 0)
                {
                    allPassives[pair].Cooldown = value;
                }
                else
                {
                    float cooldownModifier = 1f - totalStats.CooldownReduction.Percent();
                    allPassives[pair].Cooldown = value * cooldownModifier;
                }

            }

        }


        /// <summary>
        /// Sets the cooldown for every castable of specified kind on this unit.
        /// </summary>
        /// <param name="kind">Castable kind. Basic, Ultimate, or Spell. </param>
        /// <param name="value">Desired value of cooldown.</param>
        /// <param name="exact">If set to <c>false</c> we want the cooldown adjusted for CDR.</param>
        public void SetCooldownForEvery(CastableKind kind, float value, bool exact = true) {
            foreach (Castable ability in SkillHash.Keys)
            {
                if (ability.Kind == kind)
                {
                    SetCooldown(ability, value, exact);
                }
            }
        }


        /// <summary>
        /// Starts the default, adjusted cooldown based on level.
        /// </summary>
        /// <param name="ability">Ability.</param>
        public void StartBareCooldown(Castable ability) {
            if (SkillHash.ContainsKey(ability))
            {
                int index = SkillHash[ability];

                int skillLevel = GetAbilityLevelLessOne(ability);
                skillLevel = Math.Max(0, skillLevel);
                ActiveCooldowns[index] = ability.Cooldowns[skillLevel];
            }
        }


        /// <summary>
        /// Starts the default cooldown without adjusting for cdr.
        /// </summary>
        /// <param name="ability">Ability.</param>
        public void StartBareCooldown(SourcePair pair) {
            if (allPassives.ContainsKey(pair))
            {
                // when the passive level is 1, this is 0.
                int passiveLevelLessOne = GetAbilityLevelLessOne(pair.ability);

                //FIXME might not scale on unit level
                allPassives[pair].Cooldown = pair.ability.Cooldowns[passiveLevelLessOne];
            }
        }


        /// <summary>
        /// Starts the default, adjusted cooldown based on level.
        /// </summary>
        /// <param name="ability">Ability.</param>
        public void StartCooldown(Castable ability) {
            if (SkillHash.ContainsKey(ability))
            {
                int index = SkillHash[ability];

                int skillLevel = GetAbilityLevelLessOne(ability);
                skillLevel = Math.Max(0, skillLevel);
                
                float bareCooldown = ability.Cooldowns[skillLevel]; 

                float cooldownModifier = 1f - totalStats.CooldownReduction / 100f;
                ActiveCooldowns[index] = bareCooldown * cooldownModifier;
            }
        }


        /// <summary>
        /// Starts the default, adjusted cooldown based on level.
        /// </summary>
        /// <param name="ability">Ability.</param>
        public void StartCooldown(SourcePair pair) {
            if (allPassives.ContainsKey(pair))
            {   
                if (Kind == UnitKind.Forger && totalStats.CooldownReduction > 0)
                {
                    float cooldownModifier = 1f - totalStats.CooldownReduction / 100f;
                    allPassives[pair].Cooldown = pair.ability.Cooldowns[Level] * cooldownModifier;
                }
                else
                {
                    StartBareCooldown(pair);
                }
            }
        }

        /// <summary>
        /// Tries to get the duration of the buff or debuff.
        /// </summary>
        /// <returns><c>true</c>, if get duration was gotten, <c>false</c> otherwise.</returns>
        /// <param name="pair">Source pair.</param>
        /// <param name="remainingDuration">Remaining duration.</param>
        public bool TryToGetDuration(SourcePair pair, out float remainingDuration) {
            
            if (allPassives.ContainsKey(pair) && pair.ability is BuffDebuff)
            {
                var buff = pair.ability as BuffDebuff;

                remainingDuration = buff.Duration - allPassives[pair].TimeSinceApplication;
                return true;

            }
            else
            {
                remainingDuration = float.NaN;
                return false;
            }

        }

        /// <summary>
        /// Tries to get the original duration.
        /// </summary>
        /// <returns><c>true</c>, if to get original duration was tryed, <c>false</c> otherwise.</returns>
        /// <param name="pair">Pair.</param>
        /// <param name="originalDuration">Original duration.</param>
        public bool TryToGetOriginalDuration(SourcePair pair, out float originalDuration)
        {

            if (allPassives.ContainsKey(pair) && pair.ability is BuffDebuff)
            {
                var buff = pair.ability as BuffDebuff;

                originalDuration = buff.Duration;
                return true;

            }
            else
            {
                originalDuration = float.NaN;
                return false;
            }
        }

        /// <summary>
        /// Tries the past duration of the buff or debuff.
        /// </summary>
        /// <returns><c>true</c>, if to get past duration was tryed, <c>false</c> otherwise.</returns>
        /// <param name="pair">Pair.</param>
        /// <param name="pastDuration">Past duration.</param>
        public bool TryToGetPastDuration(SourcePair pair, out float pastDuration)
        {
            if (allPassives.ContainsKey(pair))
            {
                pastDuration = allPassives[pair].TimeSinceApplication;
                return true;
            }

            pastDuration = float.NaN;
            return false;
        }

        /// <summary>
        /// Tries to get the remaining duration of the buff or debuff.
        /// </summary>
        /// <returns><c>true</c>, if get duration was gotten, <c>false</c> otherwise.</returns>
        /// <param name="pair">Source pair.</param>
        /// <param name="remainingDuration">Remaining duration.</param>
        public bool TryToGetRemainingDuration(SourcePair pair, out float remainingDuration)
        {

            if (allPassives.ContainsKey(pair) && pair.ability is BuffDebuff)
            {
                var buff = pair.ability as BuffDebuff;

                remainingDuration = buff.Duration - allPassives[pair].TimeSinceApplication;
                return true;

            }
            else
            {
                remainingDuration = float.NaN;
                return false;
            }

        }

        /// <summary>
        /// Tries the active in slot.
        /// </summary>
        /// <returns><c>true</c>, if active is in slot, <c>false</c> otherwise.</returns>
        /// <param name="slot">Slot.</param>
        /// <param name="active">Active.</param>
        public bool TryActiveInSlot(int slot, out Castable active) {
            if (slot > 0 && slot < SkillSlots.Count)
            {
                active = SkillSlots[slot];
                return true;
            }

            active = null;
            return false;
        }

        public float CooldownInSlot(int slot) {
            
            if (slot > 0 && slot < SkillSlots.Count)
            {
                return CooldownOf(SkillSlots[slot]);
            }
            else if (slot == 0)
            {
                foreach(Passive ability in kit.passives)
                {
                    if (HasPassive(ability.WithSource(this)))
                    {
                        return CooldownOf(ability.WithSource(this));
                    }
                }
                //return CooldownOf(new SourcePair{ Source = this, ability = kit.Passives[0] });
            }

            // FIXME debug value
            return float.NaN;
        }

        /// <summary>
        /// Get the cooldown of the specified ability.
        /// </summary>
        /// <returns>The cooldown of the ability.</returns>
        /// <param name="ability">A castable ability.</param>
        public float CooldownOf(Castable ability) {
            if (SkillHash.ContainsKey(ability))
            {
                int index = SkillHash[ability];
                if (ActiveCooldowns.Count > index) {
                    return ActiveCooldowns[index];
                }
            }
            return float.NaN;
        }

        /// <summary>
        /// Get the cooldown of the specified passive (by source).
        /// </summary>
        /// <returns>The cooldown of specified passive (by source). NaN if that passive, source pair isn't found. </returns>
        /// <param name="pair">The passive and source</param>
        public float CooldownOf(SourcePair pair) {
            if (allPassives.ContainsKey(pair))
            {
                return allPassives[pair].Cooldown;
            }
            return float.NaN;
        }


        /// <summary>
        /// Determines whether this unit has the specified buff pair.
        /// </summary>
        /// <returns><c>true</c> if this instance has the specified buff pair; otherwise, <c>false</c>.</returns>
        /// <param name="pair">Pair.</param>
        public bool HasBuff(SourcePair pair) {
            if (pair.ability is BuffDebuff) {
                return HasPassive(pair);
            } else {
                if(pair.ability != null)
                {
                    Debug.LogWarningFormat("{0} is not a buff or debuff.", pair.ability.Name);
                }
                return false;
            }
        }

        /// <summary>
        /// Determines whether this instance has the specified source pair.
        /// </summary>
        /// <returns><c>true</c> if this instance has the specified source passive pair; otherwise, <c>false</c>.</returns>
        /// <param name="pair">Pair.</param>
        public bool HasPassive(SourcePair pair) {
            if (allPassives.ContainsKey(pair))
            {
                return allPassives[pair].stacks > 0;
            }
            return false;
        }

        public int StacksOf(Passive passive, Unit fromSource) {
            var pair = passive.WithSource(fromSource);
            if (allPassives.ContainsKey(pair))
            {
                if (passive.NoMaxStacks)
                {
                    return allPassives[pair].stacks;
                }
                return Math.Min(passive.MaximumStacks, allPassives[pair].stacks);
            }
            return 0;
        }

        /// <summary>
        /// Gets the stacks of a passive. 
        /// 
        /// </summary>
        /// <returns>The number of stacks</returns>
        /// <param name="pair">the pair of source and passive</param>
        public int StacksOf(SourcePair pair) {
            if (allPassives.ContainsKey(pair))
            {
                if (pair.ability.NoMaxStacks)
                {
                    return allPassives[pair].stacks;
                }
                return Math.Min(pair.ability.MaximumStacks, allPassives[pair].stacks);
            }
            return 0;
        }


        /// <summary>
        /// Deletes a float value attached to this unit.
        /// </summary>
        /// <returns>The value deleted.</returns>
        /// <param name="variableName">Variable name.</param>
        public float DeleteFloat(string variableName) {
            if (floatingFloatValues.ContainsKey(variableName))
            {
                float result = floatingFloatValues[variableName];
                floatingFloatValues.Remove(variableName);
                return result;
            }

            return default(float);
        }

        /// <summary>
        /// Gets the current value of a float value attached to this unit.
        /// </summary>
        /// <returns>The value.</returns>
        /// <param name="variableName">Variable name.</param>
        public float GetFloat(string variableName) {
            if (floatingFloatValues.ContainsKey(variableName))
            {
                return floatingFloatValues[variableName];
            }

            return 0f;
        }

        /// <summary>
        /// Adds or adjusts the value of a float attached to the unit.
        /// </summary>
        /// <returns>The delta between incoming and current value.</returns>
        /// <param name="variableName">Variable name.</param>
        /// <param name="incoming">incoming value.</param>
        public float SetFloat(string variableName, float incoming) {
            float delta;

            if (floatingFloatValues.ContainsKey(variableName))
            {
                delta = incoming - floatingFloatValues[variableName];
                floatingFloatValues[variableName] = incoming;
                return delta;
            }

            floatingFloatValues.Add(variableName, incoming);
            return incoming;
        }



        [Obsolete]
        public bool HasBuff(BuffDebuff thisBuff, Unit source = null) {
            
            // When we just "add a buff" it is our buff often enough.
            if (source == null)
            {
                source = this;
            }

            return HasPassive(new SourcePair{ Source = source, ability = thisBuff });
        }

        public bool CleanseStatus(DisablingFeatures disables) {
            // FIXME this doesn't work properly
//            foreach (BuffDebuff buff in Buffs)
//            {
//                if (buff is Status) {
//                    Status status = ((Status)buff);
//                    if( status.CanBeCleansed ) {
//                        if (disables.Movement && status.Disables.Movement) {
//                            
//                        }
//                        // TODO other disables
//                    }
//                }
//            }
            return false;
        }

        #endregion
        protected Transform channelRoot;
        public Transform plumbobRoot;
        public Transform attachPoint;

        private Timer channelTimer;
        public bool IsChanneling {
            get { return channelTimer != null; }
        }
        public Timer CurrentChannel {
            get { return channelTimer; }
            set { 
                if (IsChanneling) {
                    // Do not start a new channel! 
                    value.Stop();
                } else {   
                    channelTimer = value;
                }
            }
        }
        public bool InterruptChannel () {
            if (IsChanneling) {
                channelTimer.Stop(this);
                channelTimer = null;
                RpcKillChannelDressings();
                return true;
            }
            return false;
        }

		private Timer basicAttackTimer; 

        private Timer combatTimer;
        private bool _inCombat = false;
        public bool InCombat {
            get {
                return _inCombat;
            }
            set {
                if(_inCombat == false && value == true) {
                    foreach (var keyValue in allPassives)
                    {
                        var pair = keyValue.Key;
                        var data = keyValue.Value;
                        if (pair.CanTrigger && pair.ability.OnEnterCombat != null)
                        {
                            pair.ability.OnEnterCombat(pair, this, data);
                        }
                    }
                }
                _inCombat = value;
                if(_inCombat) {
                    if(combatTimer != null) {
                        combatTimer.Stop();
                    }
                    combatTimer = new Timer().Start(() => {
                        InCombat = false;
                        foreach (var pair in allPassives.Keys)
                        {
                            var data = GetPassiveData(pair);
                            if (pair.CanTrigger && pair.ability.OnExitCombat != null) {
                                pair.ability.OnExitCombat(pair, this, data);
                            }
                        }
                        return 0f;
                    }, Constants.IN_COMBAT_TIMEOUT);
                }
            }
        }

        public List<SourcePair> GetPassivesAndBuffs (Func<SourcePair, bool> filter) {
            List<SourcePair> abilities = new List<SourcePair>();

            abilities.AddRange(allPassives.Keys.Where(filter));

            return abilities;
        }

        public PassiveTracker GetPassiveData(SourcePair pair) {
            if (allPassives.ContainsKey(pair))
            {
                return allPassives[pair];
            }

            return new PassiveTracker{ };
        }


        public Unit lastAggro {
            get;
            protected set;
        }

        public virtual List<Forger> FullAggro {
            get {
                var list = new List<Forger>();
                if (lastAggro != null && lastAggro.Kind == UnitKind.Forger) {
                    list.Add(lastAggro.GetComponent<Forger>());
                }
                return list;
            }
        }

        public virtual void AddAggro(Forger forger) {
            lastAggro = forger;
        }

        /// <summary>
        /// specific classifier. which is this unit type within the kind.
        /// </summary>
        [SyncVar]
        public char Spec = (char)0;

        public abstract BountyType Bounty {
            get ;
        }

        public virtual BountyType XPBountyKind {
            get { return Bounty; }
        }

        public virtual BountyType MoneyBountyKind {
            get { return Bounty; }
        }

        public virtual float SaltBounty {
            get { return totalStats.HephisaltBounty; }
        }

        public virtual float GlobalSaltBounty {
            get { return 0; }
        }

        public float ExperienceBounty {
            get { return totalStats.ExperienceBounty; }
        }

        public virtual float GlobalExperienceBounty {
            get { return 0; }
        }

        [SyncVar]
        [SerializeField]
        protected bool dyingOrDead;

        private bool hasAnnouncedDeath = false;

        public bool IsDead {
            get { return dyingOrDead; }
        }

        public bool IsDying {
            get { return kit != null && kit.HasInitialized && Health <= 0; }
        }

        public AnimationController animationController;
        public KnockbackController knockbackController;

        public bool TryGetIndexOfSkill(Castable ability, out int result) {
            if (SkillSlots.Contains(ability))
            {
                result = SkillSlots.IndexOf(ability);
                return true;
            }

            if (AbilityLevelHash.ContainsKey(ability))
            {
                result = AbilityLevelHash[ability];
                return true;
            }

            result = -1;
            return false;
        }

        public bool SkillOnCooldown(Castable ability) {
            return SkillOnCooldown(SkillHash[ability]);
        }

        public bool SkillOnCooldown(int index) {
            if (kitReady && ActiveCooldowns[index] <= 0)
            {   // our kit is ready, our cooldown lists are ready, and cooldown is or under zero 
                return false;   // we are not on cooldown
            }
            return true;    // we are on cooldown
        }

        public bool PassiveOnCooldown(Passive ability) {
            return PassiveOnCooldown(PassiveHash[ability]);

        }

        public bool PassiveOnCooldown(int index) {
            if (kitReady && PassiveCooldowns[index] <= 0)
            {   // our kit is ready, our cooldown lists are ready, and cooldown under zero 
                return true;
            }
            return false;

        }


        public override DamagePacket DealDamage(Unit targetUnit, DamagePacket packet) {

            if (kitReady)
            {
                InCombat = true;

                foreach (SourcePair pair in allPassives.Keys)
                {
                    if (pair.CanTrigger && pair.ability.BeforeDealDamage != null)
                    {
                        packet = pair.ability.BeforeDealDamage(pair, this, targetUnit, packet);
                    }
                }
            }

            packet = base.DealDamage(targetUnit, packet);
            if (totalStats.Lifedrain > 0)
            {
                HealPacket drain;
                if (packet.IsPrimaryTarget)
                {
                    drain = 
                    new HealPacket(HealActionType.HealBurst, this)
                    {
                        HealAmount = packet.TotalDamage * totalStats.Lifedrain.Percent()
                    };
                }
                else
                {
                    drain = 
                    new HealPacket(HealActionType.HealBurst, this)
                    {
                        HealAmount = packet.TotalDamage * totalStats.Lifedrain.Percent() * 35f.Percent()
                    };
                }

                ReceiveHeal(drain);
            }

            return packet;
        }

        public override void ProvideHeal(Unit targetUnit, HealPacket packet) {
            foreach (SourcePair pair in allPassives.Keys)
            {
                if (pair.CanTrigger && pair.ability.BeforeProvideHeal != null)
                {
                    packet = pair.ability.BeforeProvideHeal(pair, this, targetUnit, packet);
                }
            }

            targetUnit.ReceiveHeal(packet);
        }

        public void TriggerKillUnitPassives (Unit theDeadOne) {

            foreach (var keyValue in allPassives)
            {
                var pair = keyValue.Key;
                var data = keyValue.Value;
                if (pair.CanTrigger && pair.ability.OnKillUnit != null)
                {
                    pair.ability.OnKillUnit(pair, this, data, theDeadOne);
                }
            }

        }

        public void TriggerTakedownPassives (Unit theDeadOne) {
            foreach (var keyValue in allPassives)
            {
                var pair = keyValue.Key;
                var data = keyValue.Value;
                if (pair.CanTrigger && pair.ability.OnKillOrAssistForger != null)
                {
                    pair.ability.OnKillOrAssistForger(pair, this, data, theDeadOne);
                }
            }

        }

        /// <summary>
        /// Distributes the xp and gold bounty.
        /// Also triggers OnKillUnit and OnForgerKillOrAssist for
        /// </summary>
        public void DistributeBounty () {
            // Identify the last hitter / kill securer
            Forger winner = null;
            if (lastAggro != null) {
                winner = lastAggro.GetComponent<Forger>();

                lastAggro.TriggerKillUnitPassives(this);
            }

            // Get the forgers
            var globalTeam =  UnitManager.GetForgersOnTeam(this.Team.Opposite());
            List<Forger> teamForgers = new List<Forger>();
            foreach (var unit in globalTeam) {
                var forger = unit.GetComponent<Forger>();
                teamForgers.Add(forger);
            }

            // give bonus global money and XP regardless.
            foreach (var forger in teamForgers) {
                if (this.GlobalExperienceBounty > 0f) {
                    forger.GainXP(this.GlobalExperienceBounty);
                }
                if (this.GlobalSaltBounty > 0f) {
                    forger.GainSalt(this.GlobalSaltBounty, true);
                }
            }

            // this really is used often enough that I'll do this here. 

                // boil out the forgers who are nearby.
                var localTeam = teamForgers.Where(forger => {
                    return forger.IsInRange(this, Constants.BOUNTY_DISTRIBUTION_RANGE);
                });
//            }



            if (Kind == UnitKind.Forger) {
                var myself = GetComponent<Forger>();


                Distribute<Forger>(BountyType.Assist, teamForgers, 0f, (forger, amount, assist) => {
                    forger.GainKillXP(myself);
                    forger.TriggerTakedownPassives(myself);

                    if (assist) {
                        forger.EarnAssist();
                    } else {
                        forger.EarnKill();
                    }
                }, winner);
            } else {
                Distribute<Forger>(XPBountyKind, teamForgers, this.ExperienceBounty, (forger, amount, runoff) => {
                    forger.GainXP(amount);
                }, winner);
            }

            // Money!
            Distribute<Forger>(MoneyBountyKind, teamForgers, this.SaltBounty, (forger, amount, runoff) => {
                forger.GainSalt(amount, runoff);
            }, winner);

        }

        protected void Distribute<T> (BountyType bountyKind, IEnumerable<T> river, float amount, System.Action<T, float, bool> Grant, T winner = null) where T : Unit {
            
            if (bountyKind == BountyType.Global) {
                foreach (var fish in river) {
                    Grant(fish, amount, false);
                }
                return;
            }

            // boil out the forgers who are nearby.
            var localTeam = river.Where(forger => {
                return forger == winner || forger.IsInRange(this, Constants.BOUNTY_DISTRIBUTION_RANGE);
            });

            switch (bountyKind) {
            case BountyType.Assist:

                if (winner != null) {
                    // a winner is you!
                    Grant(winner, amount, false);
                    var forgetful = winner.GetComponent<Forger>();
                    if (forgetful != null) {
                        TargetLastHitDressings(forgetful.player.GetComponent<NetworkIdentity>().connectionToClient);
                    }
                }

                var helpers = FullAggro;

                float eachAssistMult = 1f / (2f * helpers.Count);

                // Note: unlike other methods this method needs to distribute to Forgers
                // It also ignores the incoming IEnumerable
                if (typeof(T) == typeof(Forger)) {
                    foreach (Forger forger in helpers) {


                        if (forger != winner) {
                            Grant(forger as T, amount * eachAssistMult, true);
                        }

                    }
                } else {
                    // TODO 2017 Jun 10 perhaps an alternate form will be a thing at some point

                }
                break;
            case BountyType.Local:
                {
                    if (winner != null && !localTeam.Contains(winner)) {
                        // congratulations winner 
                        Grant(winner, amount, false);
                        var forgetful = winner.GetComponent<Forger>();
                        if (forgetful != null) {
                            TargetLastHitDressings(forgetful.player.GetComponent<NetworkIdentity>().connectionToClient);
                        }
                    }

                    var splitMult = splitMultiplier(localTeam.Count());

                    foreach (var forger in localTeam) {
                        Grant(forger, amount * splitMult, true);
                    }

                }
                break;
            case BountyType.Runnoff:

                if (winner != null) {
                    // winner gets the dinner
                    Grant(winner, amount, false);
                    var forgetful = winner.GetComponent<Forger>();
                    if (forgetful != null) {
                        TargetLastHitDressings(forgetful.player.GetComponent<NetworkIdentity>().connectionToClient);
                    }
                }

                float runoffMultiplier = 0.4f;
                foreach (T forger in localTeam) {
                    if (forger != winner) {
                        Grant(forger, amount * runoffMultiplier, true);
                    }
                }

                break;

            case BountyType.Solo:
                if (winner != null) {
                    // eat the cake. Eat it! :P
                    Grant(winner, amount, false);
                    var forgetful = winner.GetComponent<Forger>();
                    if (forgetful != null) {
                        TargetLastHitDressings(forgetful.player.GetComponent<NetworkIdentity>().connectionToClient);
                    }
                }
                break;
            
            }


        }

        protected float splitMultiplier (int number) {
            /*
                1:1    =1
                2:0.7    =1.4
                3:0.55    =1.65
                4:0.4    =1.6
                5:0.25    =1.25
            */
            switch (number) {
            case 1:
                return 1f;
            case 2:
                return 0.7f;
            case 3:
                return 0.55f;
            case 4:
                return 0.4f;
            case 5:
                return 0.25f;
            }
            return 1f/number;
        }

        public void ReceiveHeal(HealPacket packet) {
            var receiveHealPassives = allPassives.Where((KeyValuePair<SourcePair, PassiveTracker> arg) =>
                {
                    return arg.Key.ability.BeforeReceiveHeal != null
                        || arg.Key.ability.BeforeReceiveHealPostMit != null
                        || arg.Key.ability.AfterReceiveHeal != null;
                });
            
            foreach (var keyValue in receiveHealPassives)
            {
                var pair = keyValue.Key;
                var data = keyValue.Value;
                if (pair.CanTrigger && pair.ability.BeforeReceiveHeal != null)
                {
                    packet = pair.ability.BeforeReceiveHeal(pair, this, data, packet);
                }
            }

            packet.CalculateForTarget(this);

            foreach (var keyValue in receiveHealPassives)
            {
                var pair = keyValue.Key;
                var data = keyValue.Value;
                if (pair.CanTrigger && pair.ability.BeforeReceiveHealPostMit != null)
                {
                    packet = pair.ability.BeforeReceiveHealPostMit(pair, this, data, packet);
                }
            }

            if (packet.Kind == HealActionType.HealBurst)
            {
                if (Health + packet.PostMitigationHeal > MaxHealth)
                {
                    Health = MaxHealth;
                }
                else
                {
                    Health += packet.PostMitigationHeal;
                }
            }
            else if (packet.Kind == HealActionType.HealOverTime)
            {
                if(Health + packet.PostMitigationHeal > MaxHealth)
                {
                    Health = MaxHealth;
                }
                else
                {
                    Health += packet.PostMitigationHeal;
                }
            }
            else if (packet.Kind.IsShield)
            {
                ShieldBuff shieldBuff = new ShieldBuff();
                shieldBuff.Name = "Generic Shield" + GameTime.time;
                shieldBuff.AlsoObsolete_Duration = (pair) =>
                {
                    return packet.PeriodOfTime;
                };
                shieldBuff.Amount = (pair) =>
                {
                    return packet.PostMitigationShield;
                };
                shieldBuff.Magical = (packet.Kind == HealActionType.MagicShield || packet.Kind == HealActionType.Shield);
                shieldBuff.Physical = (packet.Kind == HealActionType.PhysicalShield || packet.Kind == HealActionType.Shield);
            }
             

            foreach (var keyValue in receiveHealPassives)
            {
                var pair = keyValue.Key;
                var data = keyValue.Value;
                if (pair.CanTrigger && pair.ability.AfterReceiveHeal != null)
                {
                    pair.ability.AfterReceiveHeal(pair, this, data);
                }
            }
        }


        public void Obsolete_ReceiveHealOverTime(float amount, float overTime, Unit source, float tickRate = 0.5f) { }

        public void ReceiveHealOverTime(HealPacket packet, float overTime, float tickRate = 0.5f)
        {

            foreach (SourcePair pair in allPassives.Keys)
            {
                if (pair.CanTrigger && pair.ability.BeforeReceiveHeal != null)
                {
                    packet = pair.ability.BeforeReceiveHeal(pair, this, GetPassiveData(pair), packet);
                }
            }

            //FIXME this isn't right
            float perTickAmount = packet.HealAmount / (overTime / tickRate);
            float timeRemaining = overTime;
            var hotCoroutine = GameManager.Instance.DoDelay(() =>
                {
                    if (Health + perTickAmount > MaxHealth)
                    {
                        Health = MaxHealth;
                    }
                    else
                    {
                        Health += perTickAmount;
                    }
                    timeRemaining -= tickRate;
                    if (timeRemaining > 0)
                        return tickRate;
                    else 
                        return 0f;
                }, tickRate);
            GameManager.Instance.StartCoroutine(hotCoroutine);
        }

        protected override DamagePacket TakeDamage (DamagePacket packet) {
            InCombat = true;

            if (packet.Kind != DamageActionType.AutoAttack && packet.Kind != DamageActionType.SiegeEffect)
            {
                bool builtNotBorn = allPassives.Keys.Where((pair) => { return pair.ability.HolderIsStructure; }).Count() > 0;

                if (builtNotBorn)
                {
                    // if we are a structure, and the damage (even splash) doesn't apply to us, ignore it. Move on. :)
                    packet.PostMitigationMagical = 0;
                    packet.PostMitigationPhysical = 0;
                    return packet;
                }

            }

            var source = packet.Source;
            if (source != null)
            {
                if (source.Kind == UnitKind.Forger)
                {
                    // this makes it easier to get the list for other kinds of Unit. (e.g. Monsters, Forgers)
                    AddAggro(source.GetComponent<Forger>());
                }
                else
                {
                    // Note: this one always works.
                    lastAggro = source;
            
                }
            }

            var takeDamagePassives = allPassives.Where((KeyValuePair<SourcePair, PassiveTracker> arg) =>
                {
                    return arg.Key.ability.AfterTakeDamage != null
                    || arg.Key.ability.BeforeTakeDamage != null
                    || arg.Key.ability.BeforeTakeDamagePostMit != null;
                });

            // Pre mitigation
            foreach (var keyValue in takeDamagePassives)
            {
                var pair = keyValue.Key;
                var data = keyValue.Value;
                if (pair.CanTrigger && pair.ability.BeforeTakeDamage != null)
                {
                    packet = pair.ability.BeforeTakeDamage(pair, this, data, packet);
                }
            }

            packet.CalculateForTarget(this);

            // Post mitigation
            foreach (var keyValue in takeDamagePassives)
            {
                var pair = keyValue.Key;
                var data = keyValue.Value;
                if (pair.CanTrigger && pair.ability.BeforeTakeDamagePostMit != null)
                {
                    packet = pair.ability.BeforeTakeDamagePostMit(pair, this, data, packet);
                }
            }

            {
                float magicalDamage = packet.PostMitigationMagical;
                float physicalDamage = packet.PostMitigationPhysical;

                if (magicalDamage > 0)
                {
                    packet.PostMitigationMagical = shields.MagicalImpact(magicalDamage);
                }
                if (physicalDamage > 0)
                {
                    packet.PostMitigationPhysical = shields.PhysicalImpact(physicalDamage);
                }
            }

            float damage = packet.TotalDamage;



            // reduce damage by shields
            if (shields.Value > 0)
            {
                DamageShield(packet);
            }

            if (packet.TotalDamage > 0) {
                Health -= packet.TotalDamage;
            }

            // After damage resolves
            foreach (var keyValue in takeDamagePassives)
            {
                var pair = keyValue.Key;
                var data = keyValue.Value;
                if (pair.CanTrigger && pair.ability.AfterTakeDamage != null)
                {
                    pair.ability.AfterTakeDamage(pair, this, data, packet.Source);
                }
            }
            return packet;
        }


        public void ApplyDamageOverTime (DamagePacket packet, float overTime, float tickRate = 0.5f) {
            float timeRemaining = overTime;

            Unit source = packet.Source;
            if (source != null)
            {
                if (source.Kind == UnitKind.Forger)
                {
                    // this makes it easier to get the list for other kinds of Unit. (e.g. Monsters, Forgers)
                    AddAggro(source.GetComponent<Forger>());
                }
                else
                {
                    // Note: this one always works.
                    lastAggro = source;
                }
            }

            foreach (SourcePair pair in allPassives.Keys)
            {
                if (pair.CanTrigger && pair.ability.BeforeTakeDamage != null)
                {
                    packet = pair.ability.BeforeTakeDamage(pair, this, GetPassiveData(pair), packet);
                }
            }

            packet.CalculateForTarget(this);

            foreach (SourcePair pair in allPassives.Keys)
            {
                if (pair.CanTrigger && pair.ability.BeforeTakeDamagePostMit != null)
                {
                    packet = pair.ability.BeforeTakeDamagePostMit(pair, this, GetPassiveData(pair), packet);
                }
            }


            float damage = packet.SliceOfTotalDamageDealtTo(this, overTime / tickRate);
            var sourcePassives = source.allPassives.Keys;


            var dotCoroutine = GameManager.Instance.DoDelay(() =>
            {
            if (damage < shields.Value)
            {
                shields.GeneralImpact(damage);
            }
            else
            {
                // reduce damage by shields
                damage = shields.GeneralImpact(damage);

                Health -= damage;
            }


            foreach (SourcePair pair in allPassives.Keys)
            {
                    if (pair.CanTrigger&&pair.ability.AfterTakeDamage != null)
                    {
                        pair.ability.AfterTakeDamage(pair, source, GetPassiveData(pair), source);
                    }
                }

                timeRemaining -= tickRate;
                if (timeRemaining > 0)
                    return tickRate;
                else
                    return 0f;
            }, tickRate);

            GameManager.Instance.StartCoroutine(dotCoroutine);

        }

        private Vector2 lastPosition;

        [SyncVar]
        private Vector2 _velocity;
        public Vector2 Velocity {
            get {
                return _velocity;
            }
            private set {
                _velocity = value;
            }
        }

        [SerializeField]
        private ForgerResource _resource;

        public ForgerResource Resource {
            get { 
                if (_resource == null) {
                    _resource = new ForgerResource();
                }
                return _resource; 
            }
            set { _resource = value; }
        }

        internal virtual void Start() {
            animationController = GetComponent<AnimationController>();
            knockbackController = GetComponent<KnockbackController>();
            gameObject.AddComponent<KnockbackController>();

            lastPosition = Position;

            CheckAllSyncLists();

            Disables = new DisablesList( this );

            channelRoot = new GameObject("Channel Root").transform;
            channelRoot.transform.SetParent(transform);
            channelRoot.transform.localPosition = Vector3.zero;

            if (plumbobRoot == null) {
                plumbobRoot = new GameObject("Plumbob Root").transform;
                plumbobRoot.SetParent(transform);
                plumbobRoot.localPosition = Vector3.up * 5f;
            }
        }

        public void OnEnable() {
            ListOf.Add(this);
            Activate();

            #if UNITY_EDITOR
            switch (Kind) {
            case UnitKind.Forger:
                transform.SetParent(GameManager.Instance.forgerRoot);
                break;
            case UnitKind.Minion:
                transform.SetParent(GameManager.Instance.minionRoot);
                break;
            case UnitKind.Tower:
                // Note: Towers are a "frame" type unit.
                //FIXME not working
//                transform.parent.SetParent(GameManager.Instance.towerRoot);
                break;
            }
            #endif
        }

        public void OnDisable() {
            ListOf.Remove(this);
            Deactivate();
        }

        protected virtual void Activate () { }

        protected virtual void Deactivate () { }

        protected virtual void Initialize () {
            shields = new ShieldTracker(this);


            ParticleHelper = new ParticleHelper();
            ParticleHelper.Unit = this;


            if (!modelReady || !kitReady)
            {
                Debug.LogError("Show me!");
            }
            if (modelReady && kitReady)
            {
                kit.InitForUnit(this);

                if (kit.passives.Length > 0)
                {
                    foreach (Passive ability in kit.passives)
                    {
                        AddPassive(ability.WithSource(this));
                    }
                }
                else
                {
                    foreach (Passive ability in kit.Passives.Values)
                    {
                        AddPassive(ability, this);
                    }
                }

                Resource.Callback = (resource, delta) => 
                {
                    foreach(SourcePair pair in allPassives.Keys.Where((pair) => {
                        return pair.ability.OnResourceChanged != null;
                    }))
                    {
                        var data = GetPassiveData(pair);
                        pair.ability.OnResourceChanged(pair, this, data, delta);
                    }
                };

                ForceStatRecalculate();

                hasAnnouncedDeath = false;

                Health = MaxHealth;


                LevelList.Clear();
                // Add five zeroes to LevelList
                for (int index = 0; index < 5; ++index)
                {
                    LevelList.Add(0);
                }

                ActiveCooldowns.Clear();
                SkillHash.Clear();

                // If we have filled in kit actives (preferably using the asset editor) use them.
                if (kit.skills.Length > 0)
                {
                    int index;

                    for (index = 0; index <= 4; ++index)
                    {
                        SkillSlots.Add(null);
                    }

                    // reserved active cooldown slot
                    ActiveCooldowns.Add(0);
                    index = 1;

                    //for (index = 0; index < kit.skills.Length; ++index)
                    foreach(var record in kit.skills)
                    {
                        Castable ability = record.skill;
                        // Associate this skill with its slot level
                        AbilityLevelHash.Add(record.skill, record.slot);
                        if (SkillSlots[record.slot] == null)
                        {
                            SkillSlots[record.slot] = record.skill;
                        }
                        // Add active cooldown, and set the hash to track its specific cooldown.
                        SkillHash.Add(record.skill, index);
                        ActiveCooldowns.Add(0);
                        index++;
                    }
                }
                // If we don't have the new skills format, use the dictionaries
                else if (kit.Skills.Count >= 4)
                {
                    int index;


                    ActiveCooldowns.Add(0);

                    for (index = 1; index <= 4; ++index)
                    {   // add each castable ability and set it to be off cooldown
                        Castable ability = kit.Skills[index];
                        AbilityLevelHash.Add(ability, index);
                        SkillSlots.Add(ability);
                        SkillHash.Add(ability, index);
                        ActiveCooldowns.Add(0);
                    }
                    for (index = 5; index < kit.Skills.Count; ++index)
                    {
                        // TODO rewrite to handle more than four abilities

                        ActiveCooldowns.Add(0);
                    }
                }

                initialized = true;
            }


        }




        public virtual void Colourize () {}

        void DeadenUnit (float delay = 30f) {
            if (Kind == UnitKind.Forger || Kind == UnitKind.Monster) {
                #if UNITY_EDITOR
                transform.SetParent(GameManager.Instance.recycleBin);
                #endif
                RpcSetInactive(delay);
                _setInactive(delay);
            } else {
                NetworkServer.Destroy(gameObject);
            }
        }

        public void Spawn() {
            ForceStatRecalculate();

            hasAnnouncedDeath = false;



            Health = MaxHealth;
            dyingOrDead = false;

            this.enabled = true;    // Enable the Unit component.
            Colourize();

            if (!kitReady) {
                // no more 
            //} else if (kit.gameObject == gameObject) {
//                foreach (Ability ability in GetActiveAbilitiesAndBuffs()) {
//                    if (ability.OnSpawn != null) {
//                        ability.OnSpawn(ability, this);
//                    }
//                }
            } else {
                // reintegrate unit and animation bits
                model.transform.SetParent(transform);
                model.transform.localPosition = Vector3.zero;
                model.transform.localRotation = Quaternion.Euler(0, 0, 0);
            }

            if (kitReady) {
                Resource.frame = kit.resourceFrame;

                /* Commented since dying with a resource causes null here
                Resource.Callback = (resource, delta) =>
                {
                    foreach (SourcePair pair in allPassives.Keys)
                    {
                        if(pair != null)
                        {
                            var data = GetPassiveData(pair);
                            pair.ability.OnResourceChanged(pair, this, data, delta);
                        }
                    }
                };
                */

                foreach (var keyValue in allPassives)
                {
                    var pair = keyValue.Key;
                    var data = keyValue.Value;
                    if (pair.CanTrigger && pair.ability.OnSpawn != null)
                    {
                        pair.ability.OnSpawn(pair, this, data);
                    }
                }
            }

        }

        private bool initialized = false;

        private bool kitReady {
            get {
                if (kit != null)
                {
                    return kit.Ready;
                }
                if (this == null)
                {
                    return false;
                }
                var holder = GetComponent<KitHolder>();
                if (holder !=null)
                {
                    kit = holder.kit;
                }
                if (kit != null && !kit.Ready)
                {
                    kit.InitForUnit(this);
                    return kit.Ready;
                }
                holder = GetComponentInChildren<KitHolder>();
                if (holder != null)
                {
                    kit = holder.kit;
                }
                if (kit != null)
                {
                    return kit.Ready;
                }
                return false;
            }
        }

        protected virtual bool modelReady
        {
            get
            {
                if (model == null && UnitManager.Ready)
                {   
                    var spawnPrefab = UnitManager.Instance.GetUnitModel(this);
                    model = Instantiate<GameObject>(spawnPrefab,transform);


                    foreach (Transform child in transform)
                    {
                        var train = child.Find("AttachPoint");
                        if (train != null)
                        {
                            attachPoint = train;
                        }
                    }

                    Colourize();

                }
                return (model != null);
            }
        }

        private bool CooldownsPrepared() {
            if (kitReady)
            {

                if (PassiveCooldowns.Count > kit.Passives.Count)
                {
                    PassiveCooldowns.Clear();
                    PassiveHash.Clear();
                }

                if (PassiveCooldowns.Count < kit.Passives.Count)
                {
                    foreach (Passive ability in kit.Passives.Values)
                    {
                        if (!PassiveHash.ContainsKey(ability))
                        {
                            PassiveHash.Add(ability, PassiveHash.Count);
                            PassiveCooldowns.Add(ability.Cooldowns[0]);
                        }
                    }
                }

                if (ActiveCooldowns.Count == kit.Skills.Count + 1 && PassiveCooldowns.Count == kit.Passives.Count)
                {
                    return true;
                }
            }
            return false;
        }

        protected virtual void AnnounceDeath(Unit Killer)
        {
            
        }

        protected virtual void AnnounceDeath()
        {

        }

        void Update() {
            if (!initialized)
            {
                Initialize();
            }

            if (NetworkServer.active)
            {
                Velocity = (Position - lastPosition) / Time.deltaTime;

                if (lastPosition != Position)
                {
                    foreach (var keyValue in allPassives)
                    {
                        var pair = keyValue.Key;
                        var data = keyValue.Value;
                        if (pair.CanTrigger && pair.ability.OnMove != null)
                        {
                            pair.ability.OnMove(pair, this, data, Time.deltaTime);
                        }
                    }
                }

                lastPosition = Position;


                if (modelReady && kitReady)
                {
                    foreach (var keyValue in allPassives)
                    {
                        var pair = keyValue.Key;
                        var data = keyValue.Value;
                        if (pair.CanTrigger && pair.ability.OnTick != null)
                        {
                            pair.ability.OnTick(pair, this, data, Time.deltaTime);
                        }
                    }

                    if (dyingOrDead)
                    {
                        // Set a unit to be recycled or removed. 
                        DeadenUnit();
                    }

                    if (IsDead && !hasAnnouncedDeath)
                    {
                        if (Kind == UnitKind.Forger || Kind == UnitKind.Tower)
                        {
                            if (lastAggro != null)
                            {
                                AnnounceDeath(lastAggro);
                            }
                            else
                            {
                                AnnounceDeath();
                            }
                            hasAnnouncedDeath = true;
                        }
                    }

                    CheckAllSyncLists();

                    if (IsDying && !dyingOrDead)
                    {
                        dyingOrDead = true;
                        bool deathOver = false;
                        if (NetworkServer.active)
                        {

                            foreach (var keyValue in allPassives)
                            {
                                var pair = keyValue.Key;
                                var data = keyValue.Value;

                                if (pair.ability.DeathBehaviourOverride)
                                {
                                    deathOver = true;
                                }

                                if (pair.CanTrigger && pair.ability.OnDeath != null)
                                {
                                    pair.ability.OnDeath(pair, this, data, lastAggro);
                                }
                            }


                            DistributeBounty();

                            if (!deathOver)
                            {
                                if (Kind == UnitKind.Forger)
                                {
                                    managers.GameManager.Instance.SetRespawn(this);
                                    GetComponent<Forger>().EarnDeath();
                                }
                                else if (Kind == UnitKind.Tower)
                                {
                                    // not this. 
                                }
                                else if (
                                    lastAggro != null && (lastAggro.Kind == UnitKind.Forger)
                                    && (Kind == UnitKind.Minion
                                    || Kind == UnitKind.Monster))
                                {

                                    // This has the side effect of making them not corpsify when last hit. 
                                    // We should keep this behavior for Minions. ^ 

                                    lastAggro.GetComponent<Forger>().LastHitCreep();
                                }
                                else
                                {

                                    Corpsify(30f);
                                }
                            }
                        }

                    }
                    else if (Health < MaxHealth)
                    {
                        // TODO check if this is right.
                        Health += Time.deltaTime * HealthRegen;

                    }
                    else if (Health > MaxHealth)
                    {
                        Health = MaxHealth;
                    }

                    // tick down passive cooldown
                    foreach (PassiveTracker tracker in allPassives.Values)
                    {
                        if (tracker.Cooldown > 0)
                        {
                            tracker.Cooldown -= Time.deltaTime;
                        }
                        if (tracker.Cooldown < 0)
                        {
                            tracker.Cooldown = 0;
                        }
                    }

                    for (int index = 0; index < ActiveCooldowns.Count; ++index)
                    {
                        float cooldown = ActiveCooldowns[index];
                        if (cooldown > 0)
                        {
                            ActiveCooldowns[index] -= Time.deltaTime;
                        }
                        if (cooldown < 0)
                        {
                            ActiveCooldowns[index] = 0;
                        }
                    }
                }
            }


            /*
             * FIXME all in UnitAI?
			// Note: Maybe move this to Unit too. >_< it's somewhat control-ish though. 
			if (CanControl && HasTargetUnit) { 
				//If we have a target enemy unit, check range and stuff

				if (IsInRange( targetUnitToAttack, BaseAttackRange )) {
					//If we're in (Melee) basic attack range, melee attack

					MeleeAttack(targetUnitToAttack);
				} else {
					// if not, we move to the target unit. 
					Destination = targetUnitToAttack.transform.position;
				}
			}
            */


        }

        void CheckAllSyncLists () {
            if (NetworkServer.active || NetworkClient.active)
            {
                if (bravoSum != sumBravo)
                {
                    if (NetworkServer.active)
                    {
                        BravoList.Clear();
                        foreach (float staff in totalStats.BravoList)
                        {
                            BravoList.Add(staff);
                        }

                        bravoSum = sumBravo;
                    }
                    else
                    {
                        totalStats.BravoList = BravoList.ToArray();

                    }
                }


                if (alphaSum != sumAlpha)
                {
                    if (NetworkServer.active)
                    {
                        AlphaList.Clear();
                        foreach (float staff in totalStats.AlphaList)
                        {
                            AlphaList.Add(staff);
                        }

                        alphaSum = sumAlpha;
                    }
                    else
                    {
                        totalStats.AlphaList = AlphaList.ToArray();

                    }
                }


                if (charlieSum != sumCharlie)
                {
                    if (NetworkServer.active)
                    {
                        CharlieList.Clear();
                        foreach (float staff in totalStats.CharlieList)
                        {
                            CharlieList.Add(staff);
                        }

                        charlieSum = sumCharlie;
                    }
                    else
                    {
                        totalStats.CharlieList = CharlieList.ToArray();

                    }
                }
            }
        }


        /// <summary>
        /// Forces all stats on this unit to recalculate then 
        /// hotdrops the recalculated values into Base, Bonus, and total values.
        /// </summary>
        protected virtual void ForceStatRecalculate () {
            

            if (modelReady && kitReady)
            {
                bareStats = kit.GetBaseStats();

                levelStats = kit.PerLevelStats * (Level - 1);

                flatBonus = new UnitStats();

                percentBonus = new UnitStats();

                var buffsAndPassives = allPassives.Keys;

                foreach (var keyValue in allPassives)
                {
                    SourcePair pair = keyValue.Key;
                    PassiveTracker data = keyValue.Value;
                    var passive = pair.ability;
                    if (passive.StaticStats != null)
                    {
                        flatBonus = passive.StaticStats;
                    }

                    if (passive.PercentileStaticStats != null)
                    {
                        percentBonus += passive.PercentileStaticStats;
                    }

                    // give all dynamic stats a chance to recalculate
                    if (passive.DynamicStats != null)
                    {
                        flatBonus += passive.DynamicStats(pair, this, data);
                    }

                    if (passive.DynamicPercentileStats != null)
                    {
                        percentBonus += passive.DynamicPercentileStats(pair, this, data);
                    }
                }

                if (Kind == UnitKind.Forger)
                {
                    Forger forger = GetComponent<Forger>();

                    foreach (Item item in forger.items)
                    {
                        if (item.StaticItemStats != null && item.StaticItemStats != default(UnitStats))
                        {
                            flatBonus += item.StaticItemStats;
                        }

                        if (item.PercentItemStats != null && item.PercentItemStats != default(UnitStats))
                        {
                            percentBonus += item.PercentItemStats;
                        }
                    }
                }

                // add the ratio-based bonus stats into the bonus stats
                flatBonus += kit.ScalableStats(flatBonus.Power, flatBonus.Haste);
                //Note: assuming no bare imprint comes with unattached power or haste.

                // Finally set the values here. Try to do the update as close to atomic as we can manage. 
                BaseStats = bareStats + levelStats;
                BonusStats = UnitStats.IncreasedByPercent(flatBonus, percentBonus) + UnitStats.OnlyPercent(BaseStats, percentBonus);
                totalStats = BaseStats + BonusStats;
            }
        }


        void LevelUp () {
            if (kitReady && NetworkServer.active)
            {
                levelStats = kit.PerLevelStats;
                ServerStatsUpdate(levelStats, true, true);

                foreach (var keyValue in allPassives)
                {
                    var pair = keyValue.Key;
                    var data = keyValue.Value;
                    if (pair.CanTrigger && pair.ability.OnLevelUp != null)
                    {
                        pair.ability.OnLevelUp(pair, this, data);
                    }
                }

            }
        }

        public void PlayAnimation(string animationTriggerName, float angle = 0)
        {
            if (NetworkServer.active)
            {
                RpcPlayAnimationAndSetAngle(animationTriggerName, angle);
            }
            else
            {
                CmdPlayAnimationSetAngle(animationTriggerName, angle);
            }

            //            var anim = GetComponentInChildren<AnimationController>();
            //            if (anim != null) {
            //                anim.Play(animationTriggerName);
            //                anim.SetAngle(angle);
            //            }
        }

        [ClientRpc]
        public void RpcActivate()
        {
            //            model.transform.SetParent(transform);
            //            model.transform.localPosition = Vector3.zero;
            model.SetActive(true);
            //gameObject.SetActive(true);
        }

        [ClientRpc]
        public void RpcSetInactive(float delay)
        {
            _setInactive(delay);
        }

        private void _setInactive(float delay) {
            // TODO once we have a death animation for forgers, the delay should be useful.
            //            model.transform.SetParent(null);
            //gameObject.SetActive(false);
            model.SetActive(false);
            //StartCoroutine(GameManager.Instance.DoDelay(() =>
            //{
            //    //model.SetActive(false);
            //    gameObject.SetActive(false);
            //    return 0f;
            //}, delay));           
        }

        [Server]
        public void Corpsify (float delay = 30f) {
            _corpsify(delay);
            RpcCorpsify(delay);
        }

        [ClientRpc]
        public void RpcCorpsify(float delay)
        {
            _corpsify(delay);
        }

        private void _corpsify(float delay)
        {

#if UNITY_EDITOR
            // separate model from unit, and stow it in our corpse folder
            model.transform.SetParent(GameManager.Instance.corpseRoot);
#else
            // separate model from unit
            model.transform.SetParent(null);
#endif
            // Right away clear out the unit
            Destroy(gameObject);
            // clean up the model after a delay
            Destroy(model, delay);
        }

		[ClientRpc]
		public void RpcPlayAnimationAndSetAngle(string animationTriggerName, float angle) {
			var anim = GetComponentInChildren<AnimationController>();
			if (anim != null) {
				anim.Play(animationTriggerName);
				anim.SetAngle(angle);
			}
		}

		[Command]
		public void CmdPlayAnimationSetAngle(string animationTriggerName, float angle) {
			RpcPlayAnimationAndSetAngle(animationTriggerName, angle);
//			var anim = GetComponentInChildren<AnimationController>();
//			if (anim != null) {
//				anim.Play(animationTriggerName);
//				anim.SetAngle(angle);
//			}
		}

        public void PlayAnimation(string animationTriggerName, Vector2 look_at) {
			if (NetworkServer.active) {
				RpcPlayAnimation(animationTriggerName, look_at);
			} else {
				CmdPlayAnimation(animationTriggerName, look_at);
			}

//			// FIXME when we drop hosts, we won't need to do this on the server.  
//            var anim = GetComponentInChildren<AnimationController>();
//            if (anim != null)
//                anim.Play(animationTriggerName);
//            var victor = (look_at - Position);
//
//            var angle = Mathf.Atan2(victor.y, victor.x);
//            var facing = Utils.PlanarPoint(transform.forward);
//            angle -= Mathf.Atan2(facing.y, facing.x);
//            angle *= 180 / Mathf.PI;
//            anim.SetAngle(angle);
        }

		[ClientRpc]
		public void RpcPlayAnimation(string animationTriggerName, Vector2 look_at) {
			var anim = GetComponentInChildren<AnimationController>();
			if (anim != null)
				anim.Play(animationTriggerName);
			var victor = (look_at - Position);

			var angle = Mathf.Atan2(victor.y, victor.x);
			var facing = Utils.PlanarPoint(transform.forward);
			angle -= Mathf.Atan2(facing.y, facing.x);
			angle *= 180 / Mathf.PI;
			anim.SetAngle(angle);
		}

		public void CmdPlayAnimation(string animationTriggerName, Vector2 look_at) {
			RpcPlayAnimation(animationTriggerName, look_at);

//			var anim = GetComponentInChildren<AnimationController>();
//			if (anim != null)
//				anim.Play(animationTriggerName);
//			var victor = (look_at - Position);
//
//			var angle = Mathf.Atan2(victor.y, victor.x);
//			var facing = Utils.PlanarPoint(transform.forward);
//			angle -= Mathf.Atan2(facing.y, facing.x);
//			angle *= 180 / Mathf.PI;
//			anim.SetAngle(angle);
		}

        public void ActivateRecallDressings () {
            RpcEndRecallDressings(transform.position);

        }

        public void RecallDressings() {            
            RpcStartRecallDressings();

        }

        private Ability abilityToCast;

        public void ObeliskAlert () {
            if (Kind == UnitKind.Forger)
            {
                var player = GetComponent<Forger>().player;
                foreach(var connection in NetworkServer.connections) 
                {
                    if (connection.clientOwnedObjects.Contains(player.netId))
                        player.TargetObeliskAlert(connection);
                }
            }

        }
		

        [TargetRpc]
        protected void TargetLastHitDressings(NetworkConnection netCong) {
            // TODO redo this once we have a particle manager of some sort.
            GameObject saltGainParticle;
            saltGainParticle = (GameObject)Resources.Load("Particles/General/saltgain/saltgain_lasthit");

            var saltyObject = Instantiate(saltGainParticle, plumbobRoot.position, transform.rotation);
            saltyObject.transform.position = plumbobRoot.position;
            saltyObject.transform.localScale = plumbobRoot.localScale;

            var saltgainAudio = saltyObject.AddComponent<AudioSource>();
            // TODO redo this once we have an audio manager of some sort.
            var saltClip = (AudioClip)Resources.Load("Audio/SFX/Salt/SaltAcquisition05");
            saltgainAudio.PlayOneShot(saltClip);
        }

        [ClientRpc]
        protected void RpcSaltDressings(bool passive) {
        
            // TODO redo this once we have a particle manager of some sort.
            GameObject saltGainParticle;
//            if (passive) {
                saltGainParticle = (GameObject)Resources.Load("Particles/General/saltgain/saltgain_passivegain");
//            } else {
////                saltGainParticle = (GameObject)Resources.Load("Particles/General/saltgain/saltgain_lasthit");
////                var saltgainAudio = GetComponent<AudioSource>();
////                // TODO redo this once we have an audio manager of some sort.
////                saltgainAudio.clip = (AudioClip)Resources.Load("Audio/SFX/Salt/SaltAcquisition05");
////                saltgainAudio.Play();
//            }
            var channelObject = Instantiate(saltGainParticle, plumbobRoot.position, transform.rotation, plumbobRoot);
            channelObject.transform.localPosition = Vector3.zero;
            channelObject.transform.localScale = Vector3.one;

        }


        [ClientRpc]
        void RpcEndRecallDressings(Vector3 position) {
            var activateRecallParticle = (GameObject)Resources.Load("Particles/General/recall/recall_activate_particle");
            var activateObject = Instantiate(activateRecallParticle); 
            activateObject.transform.position = position;

            var spawnAudio = GetComponent<AudioSource>();

            PlayAnimation("Interrupt");

            // TODO redo this once we have an audio manager of some sort.
            var spawnClip = (AudioClip)Resources.Load("Audio/SFX/Spawn");
            spawnAudio.PlayOneShot(spawnClip);
            spawnAudio.Play();
        }

        [ClientRpc]
        void RpcStartRecallDressings() {

            var recallAudio = GetComponent<AudioSource>();
            // TODO redo this once we have an audio manager of some sort.
            recallAudio.clip = (AudioClip)Resources.Load("Audio/SFX/7SecRecall");
            recallAudio.Play();
            // TODO redo this once we have a particle manager of some sort.
            var channelRecallParticle = (GameObject)Resources.Load("Particles/General/recall/recall_channel_particle");
            var channelObject = Instantiate(channelRecallParticle, transform.position, transform.rotation, channelRoot);

            PlayAnimation("Recall");

            var empowerRecallParticle = (GameObject)Resources.Load("Particles/General/recall/recall_empower_particle");
            var empowerObject = Instantiate(empowerRecallParticle, channelRoot);
            empowerObject.transform.localPosition = Vector3.zero;
        }

        [ClientRpc]
        void RpcKillChannelDressings() {

            // FIXME when we make proper audio management.
            GetComponent<AudioSource>().Stop();
            {
                var deleteThis = new List<GameObject>();
                foreach (Transform train in channelRoot) {
                    deleteThis.Add(train.gameObject);
                }
                channelRoot.DetachChildren();
                foreach (var scrub in deleteThis) {
                    Destroy(scrub);
                }

            }
        }

        public void CastAbility(Castable skill, Vector2 point) {
            if (CanTrigger(skill))
            {
                foreach (SourcePair pair in allPassives.Keys)
                {
                    if (pair.CanTrigger && pair.ability.OnCastSkill != null)
                    {
                        pair.ability.OnCastSkill(pair, this, skill);
                    }
                }

                if (skill.CastType == CastType.LineTargeted)
                {
                    
                    skill.LineTargetCast(skill, this, point);

                }

                if (skill.CastType == CastType.AreaTargeted)
                {
                    skill.AreaCast(skill, this, point);
                }

                if (skill.CastType == CastType.ConeTargeted)
                {
                    skill.ConeTargetCast(skill, this);
                }

                if (skill.CastType == CastType.NonTargeted)
                {
                    skill.OnNonTargetedCast(skill, this);
                }
            }
        }

        public void CastAbilityOnUnit(Castable skill, Entity targetUnit) {

//            forger.InCombat = true;
            if (kitReady && CanTrigger(skill) && skill.CastType == CastType.UnitTargeted)
            {
                if (SkillOnCooldown(skill))
                {
                    return;
                }

                foreach (SourcePair pair in allPassives.Keys.Where( (pair) => {
                    return (pair.CanTrigger && pair.ability.OnCastUnitTargetedSkill != null);}))
                {
                    pair.ability.OnCastUnitTargetedSkill(pair, this, skill, targetUnit);
                }

                skill.UnitTargetedCast(skill, this, targetUnit as Unit);
            }
        }

        public bool Blink(Ability ability, Vector2 targetPosition) {
            if (model != null && model.activeInHierarchy)
            {
                var agent = GetComponent<NavMeshAgent>();

                var unityPosition = new Vector3(targetPosition.x, 0, targetPosition.y) * Constants.TO_UNITY_UNITS;

                NavMeshHit hit;

                if (NavMesh.SamplePosition(unityPosition, out hit, 4.0f, 1 << NavMesh.GetAreaFromName("Walkable")))
                {
                     agent.Warp(hit.position);
                    return true;
                }


                //agent.enabled = false;


                //Position = targetPosition;

                //agent.enabled = true;
                //if (agent.isActiveAndEnabled && agent.isOnNavMesh)
                //{
                //    agent.SetDestination(targetPosition);
                //}

            }
            return false;
        }

        public void Dash(Ability ability, Vector2 targetPosition, float Speed, System.Action<DashController> onStop = null, System.Action<DashController, Unit> onUnitInRadius = null, float effectRadius = 130) {
            var pc = gameObject.GetComponent<DashController>();
            if (pc == null)
                pc = gameObject.AddComponent<DashController>();

            pc.agent = GetComponent<NavMeshAgent>();
            pc.unit = this;

            //pc.Source = this;   // The dash source is gonna be me.
            //pc.Radius = Radius;
            pc.StopOnTerrain = true;
//            pc.StopOnTarget = true;
//            pc.StopOnTerrain = true;
            pc.Velocity = Speed;
//            pc.Target = target.transform;
            pc.OnStop = (dash) => {
                onStop(dash);
            };
            pc.OnHitUnit = (dash, unit) => {
                onUnitInRadius(dash, unit);
            };

            pc.SetPlanar(targetPosition);


        }

        public void HaltAndFaceTarget(Vector2 point, float duration)
        {
            // Code by Duncan, feat Mox meddling
            this.transform.LookAt(new Vector3(point.x * Constants.TO_UNITY_UNITS, this.transform.position.y, point.y * Constants.TO_UNITY_UNITS));
            NavMeshAgent navMeshAgent = this.GetComponent<NavMeshAgent>();
            Vector3 destination = navMeshAgent.destination;
            navMeshAgent.isStopped = true;

            Func<float> dothing = () =>
            {
                navMeshAgent.isStopped = false;
                navMeshAgent.destination = destination;
                return 0;
            };
            var corrie = GameManager.Instance.DoDelay(dothing, duration);
            GameManager.Instance.StartCoroutine(corrie);
        }

		public void MeleeAttack(Unit target) {

			transform.LookAt(target.transform); // look at our enemy
			transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0); // stick to horizontal orientation.

            if (basicAttackTimer == null)
            {
                string basicAttackAnimation = "BasicAttack";
                foreach (SourcePair pair in allPassives.Keys)
                {
                    if (pair.CanTrigger)
                    {
                        if (pair.ability.BasicAttackAnimationOverride != "" && pair.ability.BasicAttackAnimationOverride != null)
                        {
                            basicAttackAnimation = pair.ability.BasicAttackAnimationOverride;
                        }

                        if (pair.ability.OnBasicAttackStart != null)
                        {
                            pair.ability.OnBasicAttackStart(pair, this, target);
                        }
                    }
                }
                PlayAnimation(basicAttackAnimation); // Hands it off to the animation controller.

                // Set the Wind Up to the attack timer
                basicAttackTimer = new Timer().Start(() => {
                    if (this == null || this.IsDead)
                        return 0f;

                    BasicAttackWindDown();

                    // auto attack damage
                    var packet = new DamagePacket(DamageActionType.AutoAttack, this) {PhysicalDamage = AutoAttackDamage};

                    foreach (SourcePair pair in allPassives.Keys)
                    {
                        if(pair != null)
                        {
                            if(pair.CanTrigger && pair.ability.OnBasicAttack != null) {
                                packet = pair.ability.OnBasicAttack( pair, this, target, packet );
                            }
                        }
                    }
                    DealDamage(target, packet);
                    return 0f;
                }, WindUpTime);
            }
		}

        public void ProjectileAttack(Unit target) {

            if (Kind == UnitKind.Tower)
            {
                model.transform.LookAt(target.transform);
                model.transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0); // stick to horizontal orientation.
            } 
            else 
            {
                transform.LookAt(target.transform); // look at our enemy
                transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0); // stick to horizontal orientation.
            }

            // reset basic attack timer
            if (basicAttackTimer == null)
            {
                string basicAttackAnimation = "BasicAttack";
                foreach (SourcePair pair in allPassives.Keys)
                {
                    // Can this passive trigger for its source?
                    if (pair.CanTrigger)
                    {
                        // check for basic attack animation overrides
                        if (pair.ability.BasicAttackAnimationOverride != "" && pair.ability.BasicAttackAnimationOverride != null)
                        {
                            basicAttackAnimation = pair.ability.BasicAttackAnimationOverride;
                        }

                        if (pair.ability.OnBasicAttackStart != null)
                        {
                            pair.ability.OnBasicAttackStart(pair, this, target);
                        }
                    }
                }

                PlayAnimation(basicAttackAnimation); // Hands it off to the animation controller.

                // start the wind up basic attack timer for projectile attack
                basicAttackTimer = new Timer().Start(() => {
                    if (this == null || this.IsDead)
                        return 0f;
                    
                    BasicAttackWindDown();

                    // Make a projectile
                    var pcob = ProjectileManager.Instance.Gimme(kit.AutoAttackProjectile, target, this, 800);
                    // FIXME look up projectile speed from somewhere. Maybe in kit?

                    pcob.OnStop = (ProjectileController obj) => {
                        obj.Source.ResetBasicAttack();

                        obj.BreakMe();
                    };

                    pcob.OnHitTarget = (procob, source, hitUnit) => {

                        // auto attack damage
                        var packet = new DamagePacket(DamageActionType.AutoAttack, this) {PhysicalDamage = this.AutoAttackDamage};

                        if (source != null) {

                            var sourcePassives = source.allPassives.Keys;

                            foreach(SourcePair pair in sourcePassives) {
                                if (pair.CanTrigger && pair.ability.OnBasicAttack != null) {
                                    packet = pair.ability.OnBasicAttack(pair, this, hitUnit, packet);
                                }
                            }
                        }

                        if (source != null)
                            source.DealDamage(hitUnit,packet);
                        else
                            hitUnit.TakeDamage(packet);

                        procob.BreakMe();
                    };

                    return 0f;
                    // TODO make a property for this
                }, WindUpTime);
            }
        }



        public bool IsInRange(Unit other, float range, bool needsLineOfSight = false) {
            var a = Position;
            var b = other.Position;

            // edge to edge distance, include both radii
            if (Vector2.Distance(a, b) <= range + radius + other.radius) {
                // FIXME line of sight needs unity units
//                if (needsLineOfSight)
//                    return CheckLineOfSight(a, b);

                return true;
            }

            return false;
			// (a - b).magnitude i
        }

        public bool IsInRange(Vector2 point, float range)
        {
            // edge to edge distance, include our radius. We can add the other radius to range.
            return (Vector2.Distance(Position, point) <= range + radius);
        }

		public bool IsInRange(Vector3 point, float range, bool needsLineOfSight = false) {
			var a = transform.position;
			var b = point;
			// Set the y-coordinates to the same value since we don't 
			// take height into account.
			a.y = b.y;

			if (Vector3.Distance(a, b) <= range * Constants.TO_UNITY_UNITS) {
				if (needsLineOfSight)
					return CheckLineOfSight(a, b);

				return true;
			}

			return false;
			// (a - b).magnitude i
		}

        /// <summary>
        /// Gets the distance betwee this unit and another unit.
        /// </summary>
        /// <returns>The distance.</returns>
        /// <param name="other">Other.</param>
        public float GetDistance(Unit other) {
            return GetDistance(other.Position);
        }

        /// <summary>
        /// Gets the distance between this unit and a point.
        /// </summary>
        /// <returns>The distance.</returns>
        /// <param name="point">Point.</param>
        public float GetDistance(Vector2 point) {
            
            return Vector2.Distance(Position, point);
        }

        // WARNING: If this is going to be used then it should be revised
        bool CheckLineOfSight(Vector3 a, Vector3 b) {
            // TODO 19/1/16: Change the layer when the map gets fixed
            return !Physics.Raycast(a, b, 1 << Utils.GROUND_LAYER);
        }

        public float GetMoneyGain() {
            return GameManager.Instance.GetMoneyGainForTeam(Team);
        }


        public void DamageShield(DamagePacket packet) {
            if (packet.PostMitigationMagical > 0)
            {
                packet.PostMitigationMagical = shields.MagicalImpact(packet.PostMitigationMagical);
            }
            if (packet.PostMitigationPhysical > 0)
            {
                packet.PostMitigationPhysical = shields.PhysicalImpact(packet.PostMitigationPhysical);
            }
            if (packet.PureDamage > 0)
            {
                packet.PureDamage = shields.GeneralImpact(packet.PureDamage);
            }
        }

        public void Knockback(Vector2 position, float duration, int distance, int height)
        {
            foreach (var pair in allPassives.Keys) {
                if (pair.ability.HolderIsStructure)
                {
                    // Skip all this if we're a structure. 
                    return;
                }
            }


            if (knockbackController == null)
            {
                knockbackController = GetComponent<KnockbackController>();
            }

            if (knockbackController == null)
            {
                knockbackController = gameObject.AddComponent<KnockbackController>();
            }

            if (knockbackController != null)
            {
                knockbackController.AddKnockback(position, duration, distance, height);
            }
        }

        /// <summary>
        /// Resets the basic attack.
        /// </summary>
        public void ResetBasicAttack() {
            if(basicAttackTimer != null) {
                basicAttackTimer.Stop();
                basicAttackTimer = null;
            }
        }

        /// <summary>
        /// Starts the attack wind down timer.
        /// </summary>
		public void BasicAttackWindDown () { // Or wind up. The part after we attack before we can attack again. 
			basicAttackTimer =new Timer().Start(() => {
				ResetBasicAttack();
				return 0f;
                // TODO make a property for this.
			}, WindDownTime);
		}



        #if UNITY_EDITOR
        void OnDrawGizmosSelected () {
            // Display the radius when selected

            Utils.DrawCircle(Position, Radius * Constants.TO_UNITY_UNITS);

            Utils.DrawCircle(Position, (Radius + totalStats.AttackRange) * Constants.TO_UNITY_UNITS, Color.red);

        }
        #endif
    }
}
