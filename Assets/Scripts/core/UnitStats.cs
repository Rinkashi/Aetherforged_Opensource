using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

namespace core {
    [System.Serializable]
    public class UnitStats {

        public float Power;
        public float Haste;
        public float Mastery;
        public float Penetration;
        public float Lifedrain;
        public float Tenacity;
        public float BaseAttackTime;
        public float AttackRange;

        public float AlphaSum {
            get { return Power + Haste + Mastery + Penetration + Lifedrain + Tenacity + AttackRange; }
        }

        public float[] AlphaList {
            get {
                return new float[] { AlphaSum, Power, Haste, Mastery, Penetration, Lifedrain, Tenacity, AttackRange };
            }
            set {
                Power = value [1];
                Haste = value [2];
                Mastery = value [3];
                Penetration = value [4];
                Lifedrain = value [5];
                Tenacity = value [6];
                AttackRange = value [7];
            }
        }

        public float MaxHealth;
        public float HealthRegen;
        public float AutoAttackDamage = 0f;
        public float AttackSpeed;
        public float Armor;
        public float Resistance;
        public float CooldownReduction;
        public float BonusCriticalDamage;

        public float BravoSum {
            get { return MaxHealth + HealthRegen + AttackSpeed + AutoAttackDamage + Armor + Resistance; }
        }

        public float[] BravoList {
            get {
                return new float[] {
                    BravoSum,
                    MaxHealth,
                    HealthRegen,
                    0f,
                    0f,
                    AutoAttackDamage,
                    Armor,
                    Resistance
                };
            }
            set {
                MaxHealth = value [1];
                HealthRegen = value [2];


                AutoAttackDamage = value [5];
                Armor = value [6];
                Resistance = value [7];
            }
        }

		public float MovementSpeed = 0f;
        public float HephisaltBounty;
        public float ExperienceBounty;
        public float CharlieSum {
            get { return 0f + 0f + MovementSpeed + 0f + 0f; }
        }

        public float[] CharlieList {
            get {
                return new float[] {
                    CharlieSum,
                    Haste,
                    0f,
                    MovementSpeed,
                    AttackSpeed,
                    0f,
                    HephisaltBounty,
                    ExperienceBounty
                };
            }
            set {
                Haste = value [1];

                MovementSpeed = value [3];
                AttackSpeed = value [4];

                HephisaltBounty = value [6];
                ExperienceBounty = value [7];
            }
        }

		public float TotalAttackTime {
			get {
				return BaseAttackTime * (1f + AttackSpeed * 0.01f);
			}
		}

        public static UnitStats ResetToZero (UnitStats statBlock) {
            statBlock.Power = 0;
            statBlock.HealthRegen = 0;
            statBlock.Armor = 0;
            statBlock.Resistance = 0;
            statBlock.Haste = 0;
            statBlock.Mastery = 0;
            statBlock.Penetration = 0;
            statBlock.Lifedrain = 0;
            statBlock.MaxHealth = 0;

            statBlock.BaseAttackTime = 0;
            statBlock.AttackSpeed = 0;

            statBlock.AttackRange = 0;

            statBlock.AutoAttackDamage = 0;

            statBlock.MovementSpeed = 0;

            return statBlock;
        }

        public static UnitStats Zero {
            get {
                return ResetToZero(new UnitStats());
            }
        }

        public static UnitStats operator *(UnitStats stats, int factor) {
            UnitStats result = new UnitStats();

            result.Power = stats.Power * factor;
            result.HealthRegen = stats.HealthRegen * factor;
            result.Armor = stats.Armor * factor;
            result.Resistance = stats.Resistance * factor;
            result.Haste = stats.Haste * factor;
            result.Mastery = stats.Mastery * factor;
            result.Penetration = stats.Penetration * factor;
            result.Lifedrain = stats.Lifedrain * factor;
            result.MaxHealth = stats.MaxHealth * factor;

            result.BaseAttackTime = stats.BaseAttackTime * factor;
            result.AttackSpeed = stats.AttackSpeed * factor;

            result.AttackRange = stats.AttackRange * factor;

            result.AutoAttackDamage = stats.AutoAttackDamage * factor;

            result.MovementSpeed = stats.MovementSpeed * factor;


            result.HephisaltBounty = stats.HephisaltBounty * factor;
            result.ExperienceBounty = stats.ExperienceBounty * factor;

            return result;
        }

        public static UnitStats operator +(UnitStats stats, UnitStats addend) {
            UnitStats result = new UnitStats();

            result.Power = stats.Power + addend.Power;
            result.HealthRegen = stats.HealthRegen + addend.HealthRegen;
            result.Armor = stats.Armor + addend.Armor;
            result.Resistance = stats.Resistance + addend.Resistance;
            result.Haste = stats.Haste + addend.Haste;
            result.Mastery = stats.Mastery + addend.Mastery;
            result.Penetration = stats.Penetration + addend.Penetration;
            result.Lifedrain = stats.Lifedrain + addend.Lifedrain;
            result.MaxHealth = stats.MaxHealth + addend.MaxHealth;

            result.BaseAttackTime = stats.BaseAttackTime + addend.BaseAttackTime;
            result.AttackSpeed = stats.AttackSpeed + addend.AttackSpeed;

            result.AttackRange = stats.AttackRange + addend.AttackRange;

            result.AutoAttackDamage = stats.AutoAttackDamage + addend.AutoAttackDamage;

            result.MovementSpeed = stats.MovementSpeed + addend.MovementSpeed;


            result.HephisaltBounty = stats.HephisaltBounty + addend.HephisaltBounty;
            result.ExperienceBounty = stats.ExperienceBounty + addend.ExperienceBounty;

            return result;
        }

        public static UnitStats operator -(UnitStats stats, UnitStats addend) {
            UnitStats result = new UnitStats();

            result.Power = stats.Power - addend.Power;
            result.HealthRegen = stats.HealthRegen - addend.HealthRegen;
            result.Armor = stats.Armor - addend.Armor;
            result.Resistance = stats.Resistance - addend.Resistance;
            result.Haste = stats.Haste - addend.Haste;
            result.Mastery = stats.Mastery - addend.Mastery;
            result.Penetration = stats.Penetration - addend.Penetration;
            result.Lifedrain = stats.Lifedrain - addend.Lifedrain;
            result.MaxHealth = stats.MaxHealth - addend.MaxHealth;

            result.BaseAttackTime = stats.BaseAttackTime - addend.BaseAttackTime;
            result.AttackSpeed = stats.AttackSpeed - addend.AttackSpeed;

            result.AttackRange = stats.AttackRange - addend.AttackRange;

            result.AutoAttackDamage = stats.AutoAttackDamage - addend.AutoAttackDamage;

            result.MovementSpeed = stats.MovementSpeed - addend.MovementSpeed;


            result.HephisaltBounty = stats.HephisaltBounty - addend.HephisaltBounty;
            result.ExperienceBounty = stats.ExperienceBounty - addend.ExperienceBounty;

            return result;
        }

        public static UnitStats operator *(UnitStats stats, UnitStats factor) {
            UnitStats result = new UnitStats();

            result.Power = stats.Power * factor.Power;
            result.HealthRegen = stats.HealthRegen * factor.HealthRegen;
            result.Armor = stats.Armor * factor.Armor;
            result.Resistance = stats.Resistance * factor.Resistance;
            result.Haste = stats.Haste * factor.Haste;
            result.Mastery = stats.Mastery * factor.Mastery;
            //NOTE: Penetration probably shouldn't be 
            result.Lifedrain = stats.Lifedrain * factor.Lifedrain;
            result.MaxHealth = stats.MaxHealth * factor.MaxHealth;

            result.BaseAttackTime = stats.BaseAttackTime * factor.BaseAttackTime;
            result.AttackSpeed = stats.AttackSpeed * factor.AttackSpeed;

            result.AttackRange = stats.AttackRange * factor.AttackRange;

            result.AutoAttackDamage = stats.AutoAttackDamage * factor.AutoAttackDamage;

            result.MovementSpeed = stats.MovementSpeed * factor.MovementSpeed;


            result.HephisaltBounty = stats.HephisaltBounty * factor.HephisaltBounty;
            result.ExperienceBounty = stats.ExperienceBounty * factor.ExperienceBounty;

            return result;
        }

        public static UnitStats IncreasedByPercent(UnitStats stats, UnitStats percent) {
            UnitStats result = new UnitStats();

            result.Power = stats.Power * (1f + percent.Power / 100f);
            result.HealthRegen = stats.HealthRegen * (1f + percent.HealthRegen / 100f);
            result.Armor = stats.Armor * (1f + percent.Armor / 100f);
            result.Resistance = stats.Resistance * (1f + percent.Resistance / 100f);
            result.Haste = stats.Haste * (1f + percent.Haste / 100f);
            result.Mastery = stats.Mastery * (1f + percent.Mastery / 100f);
            /// NOTE: % Penetration is a completely different stat from flat penetration
            result.Lifedrain = stats.Lifedrain * (1f + percent.Lifedrain / 100f);
            result.MaxHealth = stats.MaxHealth * (1f + percent.MaxHealth / 100f);

            result.BaseAttackTime = stats.BaseAttackTime * (1f + percent.BaseAttackTime / 100f);
            result.AttackSpeed = stats.AttackSpeed * (1f + percent.AttackSpeed / 100f);

            result.AttackRange = stats.AttackRange * (1f + percent.AttackRange / 100f);

            result.AutoAttackDamage = stats.AutoAttackDamage * (1f + percent.AutoAttackDamage / 100f);

            result.MovementSpeed = stats.MovementSpeed * (1f + percent.MovementSpeed / 100f);


            result.HephisaltBounty = stats.HephisaltBounty * (1f + percent.HephisaltBounty / 100f);
            result.ExperienceBounty = stats.ExperienceBounty * (1f + percent.ExperienceBounty / 100f);

            return result;
        }


        public static UnitStats OnlyPercent(UnitStats stats, UnitStats percent) {
            UnitStats result = new UnitStats
            {
                Power = stats.Power * (percent.Power / 100f),
                HealthRegen = stats.HealthRegen * (percent.HealthRegen / 100f),
                Armor = stats.Armor * (percent.Armor / 100f),
                Resistance = stats.Resistance * (percent.Resistance / 100f),
                Haste = stats.Haste * (percent.Haste / 100f),
                Mastery = stats.Mastery * (percent.Mastery / 100f),
                /// NOTE: % Penetration is a completely different stat from flat penetration
                Lifedrain = stats.Lifedrain * (percent.Lifedrain / 100f),
                MaxHealth = stats.MaxHealth * (percent.MaxHealth / 100f),

                BaseAttackTime = stats.BaseAttackTime * (percent.BaseAttackTime / 100f),
                AttackSpeed = stats.AttackSpeed * (percent.AttackSpeed / 100f),

                AttackRange = stats.AttackRange * (percent.AttackRange / 100f),

                AutoAttackDamage = stats.AutoAttackDamage * (percent.AutoAttackDamage / 100f),

                MovementSpeed = stats.MovementSpeed * (percent.MovementSpeed / 100f),


                HephisaltBounty = stats.HephisaltBounty * (percent.HephisaltBounty / 100f),
                ExperienceBounty = stats.ExperienceBounty * (percent.ExperienceBounty / 100f)
            };
            return result;
        }

        public static UnitStats Stats(string Name = "_carry"
            , float MaxHealth = 0f, float HealthRegen = 0, float Armor = 0, float Resistance = 0
            , float Power = 0f, float Haste = 0, float Mastery = 0, float Penetration = 0, float Lifedrain = 0
            , float BaseAttackTime = 0f, float AttackSpeed = 0f, float BaseAttackRange = 0f
            , float AutoAttackDamage = 0f
            , float CooldownReduction = 0f
            , float Hephisalt = 0f, float Experience = 0f
            , float PercentTenacity = 0f
            , float PercentBonusCriticalDamage = 0f
            , float MovementSpeed = 0f) {

            UnitStats unitStats = new UnitStats();

            unitStats.Power = Power;
            unitStats.Haste = Haste;
            unitStats.Mastery = Mastery;       
            unitStats.Penetration = Penetration;       
            unitStats.Lifedrain = Lifedrain;

            unitStats.MaxHealth = MaxHealth;
            unitStats.HealthRegen = HealthRegen;
            unitStats.Armor = Armor;
            unitStats.Resistance = Resistance;


            unitStats.BaseAttackTime = BaseAttackTime;
            unitStats.AttackSpeed = AttackSpeed;
            unitStats.AttackRange = BaseAttackRange;
            unitStats.AutoAttackDamage = AutoAttackDamage;


            unitStats.MovementSpeed = MovementSpeed;
            unitStats.CooldownReduction = CooldownReduction;
            unitStats.BonusCriticalDamage = PercentBonusCriticalDamage;
            unitStats.Tenacity = PercentTenacity;

            unitStats.HephisaltBounty = Hephisalt;
            unitStats.ExperienceBounty = Experience;

            return unitStats;
        }
    }
}