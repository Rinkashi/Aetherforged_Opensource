﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace core {
    public class Passive : Ability {
        
        #region properties

        /// <summary>
        /// The parent <see cref="core.Ability"/>. It can be a <see cref="core.Passive"/> or, more likely, a <see cref="core.Castable"/>.
        /// </summary>
        public Ability Parent;

        /// <summary>
        /// The trigger condition of this passive. When null, treated as a true result.
        /// 
        /// returns: true if we should allow this passive to trigger.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data
        /// </summary>
        public Func<SourcePair, Unit, PassiveTracker, bool> TriggerCondition;


        /// <summary>
        /// The unit stats that are based on algorithms.
        /// 
        /// Algs: SourcePair pair, Unit holder, PassiveTracker data
        /// </summary>
        public Func<SourcePair, Unit, PassiveTracker, UnitStats> DynamicStats;

        /// <summary>
        /// The percentile unit stats that are based on algorithms.
        /// 
        /// Algs: SourcePair pair, Unit holder, PassiveTracker data
        /// </summary>
        public Func<SourcePair, Unit, PassiveTracker, UnitStats> DynamicPercentileStats;

        /// <summary>
        /// Any unit stats that don't change.
        /// </summary>
        public UnitStats StaticStats;

        /// <summary>
        /// Any percent unit stats that don't change.
        /// </summary>
        public UnitStats PercentileStaticStats;

        /// <summary>
        /// The maximum stacks this passive may have unless NoMaxStacks is set to true.
        /// </summary>
        public int MaximumStacks = 1;

        /// <summary>
        /// If we can just keep stacking, this is set to true and we ignore MaximumStacks.
        /// </summary>
        public bool NoMaxStacks = false;

        /// <summary>
        /// Whether this <see cref="core.Passive"/> is stackable. 
        /// </summary>
        /// <value><c>true</c> if stackable; otherwise, <c>false</c>.</value>
        public bool Stackable {
            get { return NoMaxStacks || MaximumStacks > 1; }
        }

        /// <summary>
        /// The holder is a structure.
        /// </summary>
        public bool HolderIsStructure = false;

        #endregion

        #region triggers

        /// <summary>
        /// After receiving a heal. 
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data
        /// </summary>
        public Action<SourcePair, Unit, PassiveTracker> AfterReceiveHeal;

        /// <summary>
        /// After removing Crowd Control.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data, Status removedCC
        /// </summary>
        public Action<SourcePair, Unit, PassiveTracker, Status> AfterRemoveCC;

        /// <summary>
        /// After damage is dealt to the owner by damageSource.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data, Unit    damageSource
        /// </summary>
        public Action<SourcePair, Unit, PassiveTracker, Unit> AfterTakeDamage;

        /// <summary>
        /// Before owner deals damage.
        /// Args: SourcePair pair, Unit holder, Unit target, DamagePacket incoming
        /// returns a DamagePacket. It may be modified or brand new. 
        /// </summary>
        public Func<SourcePair, Unit, Unit, DamagePacket, DamagePacket> BeforeDealDamage;

        /// <summary>
        /// Before owner provides heal.
        /// 
        /// Args: SourcePair pair, Unit holder, Unit target, HealPacket incoming
        /// returns a HealPacket. It may be modified or brand new. 
        /// </summary>
        public Func<SourcePair, Unit, Unit, HealPacket, HealPacket> BeforeProvideHeal;

        /// <summary>
        /// Before receiving a heal. Can modify HealPacket.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data, HealPacket packet
        /// returns: modified packet HealPacket
        /// </summary>
        public Func<SourcePair, Unit, PassiveTracker, HealPacket, HealPacket> BeforeReceiveHeal;

        /// <summary>
        /// Before receiving a heal, but after mitigation calculations. 
        /// Can modify HealPacket.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data, HealPacket packet
        /// returns: modified packet HealPacket
        /// </summary>
        public Func<SourcePair, Unit, PassiveTracker, HealPacket, HealPacket> BeforeReceiveHealPostMit;

        /// <summary>
        /// When we are hit by CC (before it takes effect).
        /// 
        /// Args: SourcePair pair, Unit holder, Unit debuffer, Status incoming
        /// Returns: Status resulting
        /// </summary>
        public Func<SourcePair, Unit, SourcePair, SourcePair> BeforeTakeCC;


        /// <summary>
        /// Before the unit takes damage. Can modify DamagePacket
        /// Args: SourcePair pair, Unit holder, PassiveTracker data, DamagePacket packet
        /// returns: modified packet DamagePacket
        /// </summary>
        public Func<SourcePair, Unit, PassiveTracker, DamagePacket, DamagePacket> BeforeTakeDamage;

        /// <summary>
        /// Before the owner takes damage, but after mitigation calculations. 
        /// Still before the damage resolves, and after post mitigation calculation.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data, DamagePacket packet
        /// returns: modified packet DamagePacket
        /// </summary>
        public Func<SourcePair, Unit, PassiveTracker, DamagePacket, DamagePacket> BeforeTakeDamagePostMit;

        /// <summary>
        /// When this passive is applied to the target.
        /// 
        /// Args: SourcePair, Unit holder
        /// </summary>
        public Action<SourcePair, Unit> OnApply;

        /// <summary>
        /// When the acting unit basic attacks.
        /// returns: DamagePacket outgoing
        /// Args: SourcePair pair, Unit holder, Unit attackTarget, DamagePacket incoming
        /// </summary>
        public Func<SourcePair, Unit, Unit, DamagePacket, DamagePacket> OnBasicAttack;

        /// <summary>
        /// When the basic attack starts.
        /// 
        /// Args: SourcePair pair, Unit holder, Unit attackTarget
        /// </summary>
        public Action<SourcePair, Unit, Unit> OnBasicAttackStart;

        /// <summary>
        /// When an ability is cast, also do this.
        /// Args: SourcePair ability, Unit holder, Castable skill
        /// </summary>
        public Action<SourcePair, Unit, Castable> OnCastSkill;

        /// <summary>
        /// When point-click unit targeted ability is cast, also do this.
        /// 
        /// Args: Passive ability, Unit holder, Castable skill, Entity targetUnit
        /// </summary>
        public Action<SourcePair, Unit, Castable, Entity> OnCastUnitTargetedSkill;

        /// <summary>
        /// When the unit dies.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data, Unit killer
        /// </summary>
        public Action<SourcePair, Unit, PassiveTracker, Unit> OnDeath;

        /// <summary>
        /// On entering combat.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data
        /// </summary>
        public Action<SourcePair, Unit, PassiveTracker> OnEnterCombat;

        /// <summary>
        /// A fixed amount of time after leaving combat.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data
        /// </summary>
        public Action<SourcePair, Unit, PassiveTracker> OnExitCombat;

        /// <summary>
        /// When the unit's health changes. 
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker info, float delta
        /// </summary>
        public Action<SourcePair, Unit, PassiveTracker, float> OnHealthChanged;

        /// <summary>
        /// when we get a takedown on a forger.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data, Unit deceased
        /// </summary>
        public Action<SourcePair, Unit, PassiveTracker, Unit> OnKillOrAssistForger;

        /// <summary>
        /// When we kill a unit.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data, Unit deceased
        /// </summary>
        public Action<SourcePair, Unit, PassiveTracker, Unit> OnKillUnit; 

        /// <summary>
        /// When the unit levels up.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data
        /// </summary>
        public Action<SourcePair, Unit, PassiveTracker> OnLevelUp;

        /// <summary>
        /// When the unit moves.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data, float deltaTime
        /// </summary>
        public Action<SourcePair, Unit, PassiveTracker, float> OnMove;

        /// <summary>
        /// When this passive is removed from the holder.
        /// 
        /// Args: SourcePair self, Unit holder
        /// </summary>
        public Action<SourcePair, Unit> OnRemove;

        /// <summary>
        /// When the resource of the holder changes.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data, float delta
        /// </summary>
        public Action<SourcePair, Unit, PassiveTracker, float> OnResourceChanged;

        /// <summary>
        /// When the unit spawns.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data
        /// </summary>
        public Action<SourcePair, Unit, PassiveTracker> OnSpawn;

        /// <summary>
        /// On each frame tick.
        /// 
        /// Args: SourcePair pair, Unit holder, PassiveTracker data, float deltaTime
        /// </summary>
        public Action<SourcePair, Unit, PassiveTracker, float> OnTick;

        #endregion
        // ================= Divider for Obsolete Triggers


        [Obsolete]
        public Action<Ability, Unit, Unit> Obsolete_OnBasicAttackStart;                                      //caster, target

        [Obsolete]
        /// <summary>
        /// When the owner is taking damage, but after OnTakeDamge. 
        /// Still before the damage resolves, and after post mitigation calculation.
        /// 
        /// Args: Passive self, Unit caster, Unit source, DamagePacket packet
        /// returns: modified packet DamagePacket
        /// </summary>
        public Func<Passive, Unit, Unit, DamagePacket, DamagePacket> Obsolete_OnTakeDamagePostMit;



        [Obsolete]
        public Action<Passive, Unit, Unit> Obsolete_OnKillUnit; 
        [Obsolete("OnKillOrAssistForger")]
        public Action<Passive, Unit, Unit> Obsolete_OnForgerKillOrAssist;



        public bool IsAura;
        public float AuraRadius;


    }

    /// <summary>
    /// Pair of Source and passive ability.
    /// </summary>
    public class SourcePair : IEquatable<SourcePair> {
        public Passive ability;
        public Unit Source;

        public int AbilityLevelLessOne {
            get {
                return Source.GetAbilityLevelLessOne(ability);
            }
        }

        public bool CanBeCleansed {
            get {
                if (ability is BuffDebuff)
                {
                    return (ability as BuffDebuff).CanBeCleansed;
                }
                return false;
            }            
        }

        public bool CanTrigger {
            get {
                return Source.CanTrigger(this);
            }
        }

        public Castable CastableParent {
            get {
                if (ability.Parent is Castable)
                {
                    return (ability.Parent as Castable);
                }
                return null;
            }
        }

        public float DefaultDuration {
            get {
                if (ability is BuffDebuff)
                {
                    BuffDebuff buff = ability as BuffDebuff;
                    return buff.Duration;
                }
                return float.NaN;
            }
        }

        public DisablingFeatures Disables {
            get {
                if (ability is Status)
                {
                    Status status = ability as Status;
                    return status.Disables;

                }
                return new DisablingFeatures
                {
                    Movement = false,
                    Casting = false,
                    Cripple = false,
                };
            }

        }

        /// <summary>
        /// When stacking this buff, do not refresh the timer. 
        /// Allow each timer to run out in their own time. 
        /// </summary>
        public bool DoNotRefreshOnStack {
            get {
                if (ability is BuffDebuff)
                {
                    return (ability as BuffDebuff).DoNotRefreshOnStack;
                }
                return false;
            }
        }

        public SourcePair PassiveParent {
            get {
                if (ability.Parent is Passive)
                {
                    return (ability.Parent as Passive).WithSource(Source);
                }
                return null;
            }
        }

        public bool Equals(SourcePair other) {
            if(other == null)
            {
                return false;
            }
            return ID == other.ID;
            //return (other.ability.Name == ability.Name && Source.netId == other.Source.netId);
        }

        public int ParentAbilityLevelLessOne {
            get
            {
                if (PassiveParent != null)
                {
                    return PassiveParent.AbilityLevelLessOne;
                }
                if (CastableParent != null)
                {
                    return Source.GetAbilityLevelLessOne(CastableParent);
                }
                return 0;
            }
        }

        public string ID {
            get {
                if (Source != null && ability != null)
                {
                    return string.Format("{0} {1}", ability.Name, Source.netId);
                }

                if(ability == null)
                {
                    return string.Format("Null");
                }

                return string.Format("{0} missing No.", ability.Name);
            }
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

    }

    /// <summary>
    /// Passive on unit tracker.
    /// Probably we'll need this at some point.
    /// ^ Good call.  : )
    /// </summary>
    public class PassiveTracker {
        public int stacks;
        private Stack<Unit> sources = new Stack<Unit>();

        [Obsolete]
        public Unit Source {
            get {
                // if we're empty, return null. Otherwise return the last source.
                if (sources.Count == 0)
                    return null;
                return sources.Peek();
            }
            set {
                sources.Push(value);
            }

        }
        /// <summary>
        /// The target of the passive.
        /// </summary>
        public Unit Target;
        //Note: pretty redundant

        /// <summary>
        /// Optional tracked mark used by certain passives/buffs.
        /// </summary>
        public Unit Mark;

        /// <summary>
        /// The cooldown of the passive.
        /// </summary>
        public float Cooldown;

        /// <summary>
        /// The full, projected duration of this passive.
        /// </summary>
        public float Duration;

        public Timer timer;
        public bool HasDuration
        {
            get
            {
                return (timer != null);
            }
        }

        /// <summary>
        /// The timestamp when we applied this passive.
        /// </summary>
        public float appliedTime;

        /// <summary>
        /// The level of this buff or passive.
        /// </summary>
        public int Level;

        /// <summary>
        /// Gets the amount of time since application.
        /// </summary>
        /// <value>The time since application.</value>
        public float TimeSinceApplication
        {
            get
            {
                return GameTime.time - appliedTime;
            }
        }

        /// <summary>
        /// Gets the ratio of the time this buff has been active to the total, projected duration.
        /// A floating point number between zero and one. At one, the buff has run its course.
        /// </summary>
        /// <value>The time slice ratio.</value>
        public float TimeSliceRatio
        {
            get
            {
                if (Duration > 0)
                {
                    return TimeSinceApplication / Duration;
                }
                return 0f;
            }
        }
    }

    /// <summary>
    /// Passive utilities. We might want to consolidate these somewhere else at some point. 
    /// </summary>
    public static class PassiveUtils {
        public static SourcePair WithSource(this Passive ability, Unit source) {
            return new SourcePair { ability = ability, Source = source };
        }
    }
}
