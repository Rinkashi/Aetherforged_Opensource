﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace core {
    public class ShieldBuff : BuffDebuff {

        public Func<SourcePair, float> Amount;
        public Func<SourcePair, float> Maximum;

        public bool Magical = true;
        public bool Physical = true;

        [Obsolete]
        public Func<Passive, float> Obsolete_Amount;
        [Obsolete]
        public Func<Passive, float> Obsolete_MaxValue;

    	
    }

    public class ShieldData {
        public float Amount;
        public bool Magical;
        public bool Physical;
    }

}