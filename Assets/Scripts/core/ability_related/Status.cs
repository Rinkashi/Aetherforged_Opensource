﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace core
{
    public class Status : BuffDebuff
    {
        public override Color OutlineColor
        {
            get
            {
                return Color.red;
            }
        }




        public DisablingFeatures Disables;

        public Dictionary<string, float> RelevantNumbers { get; set; }

        public Func<Unit, float> DurationOnApply;

        public static float CalculateDurationWithTenacity(float duration, float tenacity)
        {
            return duration * (tenacity / 100);
        }

        public static Status CreateStun(Func<Unit, float> durationOnApply)
        {
            var status = new Status();
            status.Name = "Stun";
            status.DurationOnApply = durationOnApply;
            status.CanBeCleansed = true;
            status.AffectedByTenacity = true;
            status.Disables = new DisablingFeatures { NameOfStatus = status.Name, Movement = true, Casting = true, Cripple = false };

            status.OnApply = (SourcePair statusDebuff, Unit targetUnit) => {
                targetUnit.Disables.Add(((Status)statusDebuff.ability).Disables);
            };
            status.OnRemove = (SourcePair statusDebuff, Unit targetUnit) => {
                targetUnit.Disables.Remove(((Status)statusDebuff.ability).Disables);
            };

            return status;
        }

        public static Status CreateRoot(Func<Unit, float> durationOnApply)
        {
            var status = new Status();
            status.Name = "Root";
            status.DurationOnApply = durationOnApply;
            status.CanBeCleansed = true;
            status.AffectedByTenacity = true;
            status.Disables = new DisablingFeatures { NameOfStatus = status.Name, Movement = true, Casting = false, Cripple = false };

            status.OnApply = (SourcePair statusDebuff, Unit targetUnit) => {
                targetUnit.Disables.Add(((Status)statusDebuff.ability).Disables);
            };
            status.OnRemove = (SourcePair statusDebuff, Unit targetUnit) => {
                targetUnit.Disables.Remove(((Status)statusDebuff.ability).Disables);
            };

            return status;
        }

        public static Status CreateSuppression(Func<Unit, float> durationOnApply)
        {
            var status = new Status();
            status.Name = "Suppression";
            status.DurationOnApply = durationOnApply;
            status.CanBeCleansed = false;
            status.AffectedByTenacity = true;
            status.Disables = new DisablingFeatures { NameOfStatus = status.Name, Movement = true, Casting = true, Cripple = false };

            status.OnApply = (SourcePair statusDebuff, Unit targetUnit) => {
                targetUnit.Disables.Add(((Status)statusDebuff.ability).Disables);
            };
            status.OnRemove = (SourcePair statusDebuff, Unit targetUnit) => {
                targetUnit.Disables.Remove(((Status)statusDebuff.ability).Disables);
            };

            return status;
        }

        public static Status CreateSilence(Func<Unit, float> durationOnApply)
        {
            var status = new Status();
            status.Name = "Silence";
            status.DurationOnApply = durationOnApply;
            status.CanBeCleansed = true;
            status.AffectedByTenacity = true;
            status.Disables = new DisablingFeatures { NameOfStatus = status.Name, Movement = false, Casting = true, Cripple = false };

            status.OnApply = (SourcePair statusDebuff, Unit targetUnit) => {
                targetUnit.Disables.Add(((Status)statusDebuff.ability).Disables);
            };
            status.OnRemove = (SourcePair statusDebuff, Unit targetUnit) => {
                targetUnit.Disables.Remove(((Status)statusDebuff.ability).Disables);
            };

            return status;
        }

        public static Status CreateStaticSlow(Func<Unit, float> durationOnApply, float percentageAmount, bool isFlat)
        {
            var status = new Status();
            status.Name = "Slow";
            status.DurationOnApply = durationOnApply;
            status.CanBeCleansed = true;
            status.AffectedByTenacity = true;
            status.Disables = new DisablingFeatures { NameOfStatus = status.Name, Movement = false, Casting = false, Cripple = false };
            status.RelevantNumbers = new Dictionary<string, float>();
            status.RelevantNumbers["Initial Slow Number"] = percentageAmount;
            status.RelevantNumbers["Is Flat Slow"] = (isFlat) ? 1f : 0f;

            status.OnApply = (SourcePair statusDebuff, Unit targetUnit) => {
                var st = (Status)statusDebuff.ability;
                targetUnit.ApplyDisablingFeatures((st).Disables);
                targetUnit.ApplySlow(st);
            };
            status.OnRemove = (SourcePair statusDebuff, Unit targetUnit) => {
                var st = (Status)statusDebuff.ability;
                targetUnit.RemoveDisablingFeatures((st).Disables);
                targetUnit.RemoveSlow(st);
            };

            return status;
        }
    }
}