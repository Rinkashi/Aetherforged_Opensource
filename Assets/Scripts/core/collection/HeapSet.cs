﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace core.collection
{
    [System.Serializable]
    public abstract class HeapSet<K,T> where K : System.IComparable {
        
        [SerializeField]
		K[] keys;
        [SerializeField]
		T[] values;
        [SerializeField]
        HashSet<T> fold = new HashSet<T>();
        [SerializeField]
        KeyValuePair<K,T>[] data = new KeyValuePair<K, T>[initialSize];

		private const int growMult = 2;
        private const int minimumGrow = 1;
        private const int initialSize = 8;

        [SerializeField]
		int lastIndex = 0;
        int currentSize = 0;

		public int Count {
			get { return lastIndex; }
		}
        public int Capacity {
            get { return currentSize; }
        }

        protected abstract bool Dominates (K x, K y);


        private static int Parent(int index) {
            return (index + 1) / 2 - 1;
        }

        private static int LeftChild(int index) {
            return (index + 1) * 2 - 1;
        }

        private static int RightChild(int index) {
            return (index + 1) * 2;
        }

        /// <summary>
        /// Gets the values. This is not fast.
        /// </summary>
        /// <value>The values.</value>
		public T[] Values {
            get { 
                T[] result = new T[Count];
                fold.CopyTo(result);
                return result; 
            }
		}

		private bool growCheck() {
            /*
            Debug.LogFormat("B: count = {0}, capacity = {1}", Count, Capacity);
            */
            if (Count == Capacity) {
                var temporary = data;

                if (currentSize == 0) {
                    currentSize += minimumGrow;
                } else {
                    currentSize *= growMult;
                }

                data = new KeyValuePair<K, T>[currentSize];
                for (int index = 0; index < lastIndex; index++) {
                    data [index] = temporary [index];
                }
                /*
                Debug.LogFormat("A: count = {0}, capacity = {1}", Count, Capacity);
                */
                return true;
            }
            /*
            Debug.LogFormat("A: count = {0}, capacity = {1}", Count, Capacity);
            */
			return false;
		}

        protected HeapSet () {
            

		}

        public void Clear () {
            lastIndex = 0;
            fold.Clear();
        }

		private void swap (int indexA, int indexB) {
            var temp = data [indexA];

            data [indexA] = data [indexB];
            data [indexB] = temp;
		}

		private void dressDown (int startIndex) {
			while (startIndex * 2 + 1 < lastIndex) {
				if (keys [startIndex].CompareTo(keys [startIndex * 2]) > 0) {
					swap(startIndex, startIndex * 2);
					startIndex *= 2;
				} else if (keys [startIndex].CompareTo(keys [startIndex * 2 + 1]) > 0) {
					swap(startIndex, startIndex * 2 + 1);
					startIndex = startIndex * 2 + 1;
				}
			}
		}

        public bool AddExclusive (K key, T value) {
            // Add to the back, and swap up until it's in a good place

            growCheck();

            if (fold.Contains(value))
                return false;
            

            fold.Add(value);
            data [lastIndex++] = new KeyValuePair<K, T>(key, value);
            BubbleUp (lastIndex - 1);
            #if UNITY_EDITOR
            // DEBUG stuff
            keys = new K[lastIndex];
            values = new T[lastIndex];
            for (int index = 0; index < lastIndex; ++index) {
                keys[index] = data[index].Key;
                values[index] = data[index].Value;
            }
            #endif


            return true;
        }

        public bool Add (K key, T value) {
            // Add to the back, and swap up until it's in a good place

            growCheck();

            if (fold.Contains(value)) {
                // remove and place again, adjusting the key
                Remove(value);
            }

            fold.Add(value);
            data [lastIndex++] = new KeyValuePair<K, T>(key, value);
            BubbleUp(lastIndex - 1);

            #if UNITY_EDITOR
            // DEBUG stuff
            keys = new K[lastIndex];
            values = new T[lastIndex];
            for (int index = 0; index < lastIndex; ++index) {
                keys[index] = data[index].Key;
                values[index] = data[index].Value;
            }
            #endif


            return true;

        }

        // "more optimized" way to adjust a key with the same value
        public bool Adjust (K key, T value) {

            // we might need that last slot
            growCheck();

            if (fold.Contains(value)) {
                for (int index = 0; index < lastIndex; index++) {
                    if (value.Equals(data[index].Value)) {
                        swap(lastIndex, index); // put the found value past the end.
                        BubbleDown(index);
                        break;
                    }
                }

                if (!value.Equals( data [lastIndex].Value)) {
                    return false;
                }
                data [lastIndex] = new KeyValuePair<K, T>(key, value);
                BubbleUp(lastIndex);

                #if UNITY_EDITOR
                // DEBUG stuff
                keys = new K[lastIndex];
                values = new T[lastIndex];
                for (int index = 0; index < lastIndex; ++index) {
                    keys[index] = data[index].Key;
                    values[index] = data[index].Value;
                }
                #endif
                return true;
            }

            return false;

        }


		private void BubbleUp(int index) {
			// !(index == 0 || Dominates(data[Parent[index], data[index])
            while (index != 0 && Dominates(data [index].Key, data[Parent (index)].Key)) {
				swap(index, Parent(index));
				index = Parent(index);
			}

		}

        private void BubbleDown(int index) {
            int dominatingNode = Dominating(index);
            while (dominatingNode != index) {
                swap(index, dominatingNode);
                index = dominatingNode;
            }
        }

        private int Dominating(int index) {
            int dominatingNode = index;
            dominatingNode = GetDominating(LeftChild(index), dominatingNode);
            dominatingNode = GetDominating(RightChild(index), dominatingNode);

            return dominatingNode;
        }

        private int GetDominating(int newNode, int dominatingNode) {
            // from a heap example
            if (newNode < lastIndex && !Dominates(data [dominatingNode].Key, data [newNode].Key)) {
                return newNode;
            } else {
                return dominatingNode;
            }
        }

        [Obsolete]
        private void BubbleUpRE(int index) {
            if(index == 0 || Dominates(data[Parent(index)].Key, data[index].Key)) {
                return;
            }
				
                swap(index, Parent(index));
                BubbleUpRE(Parent(index));
        }

        public List<T> BetterThan(K thisKey) {
            List<T> result = new List<T>();
            for (int index = 0; index < Count; ++index) {
                if (Dominates(data [index].Key, thisKey)) {
                    result.Add(data [index].Value);
                }
            }
            return result;
        }

		public T Top () {
			return data [0].Value;
		}

        public K TopRank () {
            return data [0].Key;
        }

		public void RemoveByKey (K key) {
			
		}


		/// <summary>
		/// Remove the specified value.
		/// </summary>
		/// <param name="value">a value of the latter type that you want to remove. </param>
		/// <returns>false if value not found. true if value found and removed. </returns>
		public bool Remove(T value) {
			if (lastIndex == 0) {
				return false;
			}

            if (fold.Contains(value)) {
    			for (int index = 0; index < lastIndex; index++) {
    				if (value.Equals(data[index].Value)) {
                        --lastIndex;
    					swap(lastIndex, index); // put the found value at the end.
                        BubbleDown(index);
                        break;
    				}
    			}

                fold.Remove(value);

                #if UNITY_EDITOR
                // DEBUG stuff
                keys = new K[lastIndex];
                values = new T[lastIndex];
                for (int index = 0; index < lastIndex; ++index) {
                    keys[index] = data[index].Key;
                    values[index] = data[index].Value;
                }
                #endif
                return true;
            }

			return false;   // Not found.
		}

		public T Pop() {
            if (Count <= 0) {
                throw new InvalidOperationException("Tried to pop the top off an empty Heap");
            }

            T result = data[0].Value;
            --lastIndex;
            swap(lastIndex, 0);
            fold.Remove(result);
            BubbleDown(0);

            #if UNITY_EDITOR
            // DEBUG stuff
            keys = new K[lastIndex];
            values = new T[lastIndex];
            for (int index = 0; index < lastIndex; ++index) {
                keys[index] = data[index].Key;
                values[index] = data[index].Value;
            }
            #endif
			return result;
		}

        public override string ToString ()
        {
            string temp = "";
            foreach (var thing in data) {
                temp += string.Format("\nKey: {0} Value: {1} ", thing.Key, thing.Value);
            }

            return string.Format ("[HeapSet: Count={0}, Capacity={1}, Values:{2}]", Count, Capacity, temp);
        }

        public IEnumerator<T> GetEnumerator() {
            return fold.GetEnumerator();
        }

	}

    [System.Serializable]
    public class MinHeap<K,T> : HeapSet<K,T> where K : IComparable {
        protected override bool Dominates (K x, K y)
        {
            return x.CompareTo(y) < 0;
        }
    }

    [System.Serializable]
    public class MaxHeap<K,T> : HeapSet<K,T> where K : IComparable {
        protected override bool Dominates (K x, K y)
        {
            return x.CompareTo(y) > 0;
        }
    }

}

