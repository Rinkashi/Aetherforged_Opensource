﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;

namespace core.forger {
    public abstract class Obsolete_Archetype {
        public readonly float movementSpeedPerHaste = 0.5f;

        public abstract UnitStats BaseStats {
            get;
        }

        public abstract UnitStats PerLevelStats {
            get;
        }

        public abstract float basicAttackDamagePerPower {
            get;
        }

        public abstract float attackSpeedPerHaste {
            get;
        }

        public abstract float cooldownReductionPerHaste {
            get;
        }

        public UnitStats RatioStats(float Power, float Haste) {
            return UnitStats.Stats( "ratios"
                , AutoAttackDamage: Power * basicAttackDamagePerPower
                , AttackSpeed: Haste * attackSpeedPerHaste
                , CooldownReduction: Haste * cooldownReductionPerHaste
                , MovementSpeed: Haste * movementSpeedPerHaste
            );
        }
    }
}
