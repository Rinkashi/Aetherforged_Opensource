namespace core.forger
{

  public abstract class Role
  {
    public abstract void AttachListeners( Forger owner );
  
    public virtual void Update()
    {
    }
  }

}