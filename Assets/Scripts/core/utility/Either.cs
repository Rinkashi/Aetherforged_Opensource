﻿using System;

class Either<L, R>
{
  public L left;
  public R right;

  public bool isLeft
  {
    get { return left != null; }
  }

  public bool isRight
  {
    get { return right != null; }
  }

  private Either( L l, R r )
  {
    left = l;
    right = r;
  }

}
