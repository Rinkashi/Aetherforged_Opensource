using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

using controllers;

namespace core.utility
{
    public class Utils
    {
        public const int UNIT_LAYER                 = 8;
        public const int SIGHT_LAYER                = 9;
        public const int PROJECTILE_LAYER           = 10;
        public const int SKILLSHOTPROJECTILE_LAYER  = 11;
        public const int GROUND_LAYER               = 12;
        public const int TERRAIN_LAYER              = 13;

        // How far appart can two units be before they considered touching without
        // intersecting.
        const float CONTACT_DISTANCE_THRESHOLD = 0.01f;

        public static bool CollidesWith( Vector2 aPos, float aRadius, Vector2 bPos, float bRadius )
        {

            var distance = Vector2.Distance( aPos, bPos );

            return distance <= aRadius + bRadius + CONTACT_DISTANCE_THRESHOLD;
        }

        public static void ResolveCollision( Vector2 subjectPos, float subjectRadius
            , Vector2 otherPos    , float otherRadius
            , out Vector2 newPos )
        {
            var distance = Vector2.Distance( subjectPos, otherPos );

            var displacementDirection = (subjectPos - otherPos).normalized;
            var displacementDistance =
                subjectRadius + otherRadius - distance + CONTACT_DISTANCE_THRESHOLD;

            newPos = subjectPos + displacementDirection * displacementDistance;
        }

        public static void ResolveCollision( Vector2 subjectPos, float subjectRadius
            , Vector2 velocity
            , Vector2 otherPos    , float otherRadius
            , out Vector2 newPos )
        {
            for( int i = 0; i < 8; i++ ) {
                var oldPos = subjectPos - velocity;
                var ejectionNormal = (subjectPos - otherPos).normalized;
                var ejectionLength = subjectRadius + otherRadius - Vector2.Distance( subjectPos, otherPos );

                if( ejectionLength <= 0 )
                    break;

                var ejection = ejectionNormal * ejectionLength;
                var ejectionVelUnadjucted = (velocity + ejection);
                var ejectionVelocity = ejectionVelUnadjucted;

                if( Vector2.Dot( ejectionVelUnadjucted, velocity ) >= 0 )
                    ejectionVelUnadjucted = ejectionVelUnadjucted.normalized * velocity.magnitude;

                subjectPos = oldPos + ejectionVelocity;

                // DrawCircle( subjectPos, subjectRadius, Color.red );
                // DrawCircle( otherPos, otherRadius, Color.blue );
                // DrawLine2D( oldPos, oldPos + ejectionNormal * subjectRadius, Color.magenta );
                // DrawLine2D( oldPos, oldPos + ejection, Color.yellow );
                // DrawLine2D( oldPos, subjectPos, Color.cyan );
                // DrawLine2D( oldPos, oldPos + velocity, Color.white );

                if( Vector2.Distance( subjectPos, otherPos ) >= otherRadius + subjectRadius )
                    break;
            }

            newPos = subjectPos;
        }
        public static void DrawCircle( Vector2 pos, float radius, Color color = default( Color ) ) {
            var prevI = 0;
            for( int i = 1; i < 180; i++ ) {
                var rad = Mathf.Deg2Rad * i * 2;
                var prevRad = Mathf.Deg2Rad * prevI * 2;
                var dir = new Vector2( Mathf.Cos( rad ), Mathf.Sin( rad ) ) * radius;
                var preDir = new Vector2( Mathf.Cos( prevRad ), Mathf.Sin( prevRad ) ) * radius;
                DrawLine2D( pos + preDir, pos + dir, color );
                prevI = i;
            }
        }

        public static void DrawLine2D( Vector2 a, Vector2 b, Color c ) {
            Debug.DrawLine( new Vector3( a.x, 0.1f, a.y ), new Vector3( b.x, 0.1f, b.y ), c );
        }

        public static Vector2 PlanarPoint( Vector3 point )
        {
            return new Vector2( point.x, point.z ) * Constants.TO_AETHERFORGED_UNITS;
        }

        public static float GetPlanarAngle( Vector2 direction )
        {
            var dotProduct = Vector2.Dot( direction, Vector2.up );
            // Derived from: ax * by - bx * ay
            // Where a is direction (dx, dy) and b is Vector2.up (0, 1)
            // dx * 1 - 0 * dy => dx * 1 => dx
            var determinant = direction.x;
            return Mathf.Atan2( determinant, dotProduct );
        }
        
        public static float SoftCap( float value, float[] capValues, float[] capRatios )
        {
            Debug.Assert( capValues.Length == capRatios.Length );
            
            for( int i = 0; i < capValues.Length; i++ )
            {
                var val = capValues[i];
                
                if( value < val )
                    break;
                
                var rat = capRatios[i];
                value = val + (value - val) * rat;
            }
            
            return value;
        }
        
        public static void DebugBall( Vector3 pos )
        {
            var ball = GameObject.CreatePrimitive( PrimitiveType.Sphere );
            ball.transform.position = pos;
            ball.transform.localScale = Vector3.one * 0.1f;
        }
        
        public static bool CheckFlags( uint val, uint flags )
        {
            return (val & flags) == flags;
        }

        public static Vector3 DistanceFromPointToFiniteLine( Vector3 start, Vector3 end, Vector3 pnt )
        {
            var line = ( end - start );
            var len = line.magnitude;
            line.Normalize();

            var v = pnt - start;
            var d = Vector3.Dot( v, line );
            d = Mathf.Clamp( d, 0f, len );
            return v - line * d;
        }

		public static Vector3 DistanceFromPointToFiniteLine( Vector2 start, Vector2 end, Vector2 pnt ) {
			var line = ( end - start );
			var len = line.magnitude;
			line.Normalize();

			var v = pnt - start;
			var d = Vector2.Dot( v, line );
			d = Mathf.Clamp( d, 0f, len );
			return v - line * d;
		}

        public static Vector3 NearestPointOnFiniteLine( Vector3 start, Vector3 end, Vector3 point ) {
            var line = end - start;
            var edge = point - start;
            var projectedLength = Vector3.Dot(edge, line);
            var hype = edge + line * projectedLength / line.sqrMagnitude;
            return point - hype;
        }

        public static Vector3 NearbyPoint3( Vector3 mover, Vector3 target, float range ) {
            var line = (target - mover);
            line.Normalize();

            return target + line * range * Constants.TO_UNITY_UNITS;
        }

        public static Vector2 NearbyPoint( Vector2 toPosition, Vector2 fromPosition, float range) {
            var line = (fromPosition - toPosition).normalized; 



            return (fromPosition - line * range);


        }

        public static Vector2 PointWithinRange( Vector2 fromPosition, Vector2 toPosition, float maximum, float minimum) {
            var line = (toPosition - fromPosition).normalized; 
            var range = (toPosition - fromPosition).magnitude;

            if (range < minimum) {
                range = minimum;
            } else if (range > maximum) {
                range = maximum;
            }
            return (fromPosition + line * range);
        }


        public static bool TryGetPlanarPointFromMousePosition (out Vector2 position, int layer = Utils.GROUND_LAYER) {
            if (CameraController.Instance) {
                Ray clickRay = CameraController.Instance.camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(clickRay, out hit, 500, 1 << layer)) {
                    position = Utils.PlanarPoint(hit.point);
                    return true;
                }
            }

            Ray freeRay = CameraController.Instance.camera.ScreenPointToRay(Input.mousePosition);
            Vector3 camPosition = CameraController.Instance.transform.position;
            Vector2 calculated = new Vector2(camPosition.x, camPosition.z) + new Vector2(freeRay.direction.x, freeRay.direction.z) / freeRay.direction.y * camPosition.y;

            calculated *= Constants.TO_AETHERFORGED_UNITS;

            position = calculated;
            return false;
        }
        
        /// <summary>
        /// Returns all units in a line of set width and length at target location
        /// </summary>
        /// <param name="posA">position of origin in AF units</param>
        /// <param name="posB">position of line target in AF units</param>
        /// <param name="width">width in AF units</param>
        /// <param name="length">length in AF units</param>
        /// <returns>List of all units found within the target area</returns>
        public static List<Unit> GetUnitsInLine(Vector2 posA, Vector2 posB, float width, float length)
        {
            List<Unit> inMe = new List<Unit>();

            foreach(var unit in Unit.ListOf.Where(((Unit arg) => {
                Vector2 loc = PlanarPoint(arg.gameObject.transform.position);
                return (Utils.DistanceFromPointToFiniteLine(posA, posB, loc).magnitude <= (width/2));
            })))
            {
                inMe.Add(unit);
            }

            Debug.Log("Found " + inMe.Count.ToString() + " units \n");
            Debug.Log("Units found: \n");
            foreach(Unit unit in inMe)
            {
                Debug.Log(unit.name + "\n");
            }

            return inMe;

        }

        public static List<Unit> GetUnitsInCone(Vector2 posA, Vector2 posB, float arc, float length)
        {
            List<Unit> inMe = new List<Unit>();

            foreach(var unit in Unit.ListOf.Where(((Unit arg) => {
                Vector3 orig = new Vector3();
                orig.x = (posA.x * Constants.TO_UNITY_UNITS);
                orig.z = (posA.y * Constants.TO_UNITY_UNITS);
                orig.y = 0f;

                Vector2 loc = Utils.PlanarPoint(arg.gameObject.transform.position);
				Collider[] hits = Physics.OverlapSphere(orig, length * Constants.TO_UNITY_UNITS);
                float angle = Vector2.Angle((posB - posA), (loc - posA));
                return (hits.Contains(arg.GetComponent<Collider>()) && (angle < (arc/2)));
            })))
            {
                inMe.Add(unit);
            }

            Debug.Log("Found " + inMe.Count.ToString() + " units \n");
            Debug.Log("Units found: \n");
            foreach(Unit unit in inMe)
            {
                Debug.Log(unit.name + "\n");
            }

            return inMe;
        }


        /// <summary>
        /// Tries the get planar point within range from mouse position.
        /// </summary>
        /// <returns><c>true</c>, if get planar point within range from mouse position was tryed, <c>false</c> otherwise.</returns>
        /// <returns><c>true</c>, if the mouse is over anything in the , <c>false</c> otherwise.</returns>
        /// <param name="result">a position in AF units.</param>

        /// <param name="position">Position in AF units.</param>
        /// <param name="range">Range in AF units.</param>

        /// <param name="range">Range.</param>
        /// <param name="layer">Layer.</param>
        public static bool TryGetPlanarPointWithinRangeFromMousePosition (out Vector2 result, Vector2 position, float maximum_range, float minimum_range = 0, int layer = Utils.GROUND_LAYER) {
            if (CameraController.Instance) {
                Ray clickRay = CameraController.Instance.camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(clickRay, out hit, 500, 1 << layer)) {
                    
                    var posey = Utils.PlanarPoint(hit.point);

                    result = Utils.PointWithinRange(position, posey, maximum_range, minimum_range);
                    return true;
                }
            }
            result = Vector2.one;
            return false;
        }




    }
}
