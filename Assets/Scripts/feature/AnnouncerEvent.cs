﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using controllers;
using core;
using core.forger;
using core.lane;
using managers;

namespace feature 
{
	public enum AnnouncementType {
		ThirtySecs,
		Astounding,
		Dominating,
		Exceptional,
		Execute,
		Extraordinary,
		Inexhorable,
		KillSpree,
		Legendary,
		Phenomenal,
		Slain,
		Supreme,
		Unstoppable,
		Double,
		Triple,
		Quadra,
		Penta,
		MinionSpawn,
		ForgesLight,
		TowerDestroyed,
		TowerAttacked
	}

	public enum AnnouncerSource {
		Announcement,
		KillFeed,
		Alert,
		TeamPanel
	}
	public class AnnouncerEvent 
	{

		public string FeedText;

		public AnnouncementType Type;
		public String AnnouncementText;
		public List<Unit> Actor;
		public List<Unit> Recipient;

		private GameObject AnnouncementPanel = GameObject.Find("AnnouncementPanel");

		private Image AnnouncementBar = GameObject.Find("AnnouncementBar").GetComponent<Image>();

		private Text AnnouncementBarText = GameObject.Find("AnnouncementText").GetComponent<Text>();

		private Image ActorPortrait = GameObject.Find("ActorPortrait").GetComponent<Image>();

		private Image RecipientPortrait = GameObject.Find("RecipientPortrait").GetComponent<Image>();

		private Image ActorRing = GameObject.Find("ActorRing").GetComponent<Image>();

		private Image RecipientRing = GameObject.Find("RecipientRing").GetComponent<Image>();

		private Image OmniFrame = GameObject.Find("OmniFrame").GetComponent<Image>();
		
		private GameObject KillFeedPanel;

		public AnnouncerEvent(AnnouncementType type, String text, List<Unit> actor, List<Unit> recipient)
		{
			this.AnnouncementText = text;
			this.Type = type;
			this.Actor = actor;
			this.Recipient = recipient;
		}

		public AnnouncerEvent(AnnouncementType type, String text, List<Forger> actor, List<Forger> recipient)
		{
			this.AnnouncementText = text;
			this.Type = type;
			this.Actor = new List<Unit>();
			foreach(Forger forger in actor)
			{
				this.Actor.Add(forger.gameObject.GetComponent<Unit>());
			}
			this.Recipient = new List<Unit>();
			foreach(Forger forger in recipient)
			{
				this.Recipient.Add(forger.gameObject.GetComponent<Unit>());
			}
		}

		public AnnouncerEvent(AnnouncementType type, String text, Forger actor, Forger recipient)
		{
			this.AnnouncementText = text;
			this.Type = type;
			this.Actor = new List<Unit> {actor.gameObject.GetComponent<Unit>()};
			this.Recipient = new List<Unit> {recipient.gameObject.GetComponent<Unit>()};
		}

		public AnnouncerEvent(AnnouncementType type, String text, Forger actor, List<Forger> recipient)
		{
			this.AnnouncementText = text;
			this.Type = type;
			this.Actor = new List<Unit> {actor.gameObject.GetComponent<Unit>()};
			this.Recipient = new List<Unit>();
			foreach(Forger forger in recipient)
			{
				this.Recipient.Add(forger.gameObject.GetComponent<Unit>());
			}
		}

		public AnnouncerEvent(AnnouncementType type, String text, Forger forger, bool isActor)
		{
			this.AnnouncementText = text;
			this.Type = type;
			if(isActor)
			{
				this.Actor = new List<Unit> {forger.gameObject.GetComponent<Unit>()};
				this.Recipient = null;
			}
			else
			{
				this.Actor = null;
				this.Recipient = new List<Unit> {forger.gameObject.GetComponent<Unit>()};
			}
		}

		public AnnouncerEvent(AnnouncementType type, String text, Unit actor, Unit recipient)
		{
			this.AnnouncementText = text;
			this.Type = type;
			this.Actor = new List<Unit> {actor};
			this.Recipient = new List<Unit> {recipient};
		}

		public AnnouncerEvent(AnnouncementType type, String text, Unit actor, List<Unit> recipient)
		{
			this.AnnouncementText = text;
			this.Type = type;
			this.Actor = new List<Unit> {actor};
			this.Recipient = recipient;
		}

		public AnnouncerEvent(AnnouncementType type, String text, List<Unit> actor, Unit recipient)
		{
			this.AnnouncementText = text;
			this.Type = type;
			this.Actor = actor;
			this.Recipient = new List<Unit> {recipient};
		}

		public AnnouncerEvent(AnnouncementType type, String text)
		{
			this.AnnouncementText = text;
			this.Type = type;
			this.Actor = null;
			this.Recipient = null;
		}

		// public void PlayEvent(AnnouncerEvent currEvent, AnnouncerSource currType)
		// {
		// 	AudioClip currClip = new AudioClip();
		// 	AudioController controller = AudioController.Instance;



		// 	switch(currType)
		// 	{
		// 		case AnnouncerSource.Announcement:
		// 			var tempColor = AnnouncementBar.color; 
		// 			tempColor.a = 1f; 
		// 			AnnouncementBar.color = tempColor;
		// 			Team ActorTeam = Team.Neutral;
		// 			Team RecipientTeam = Team.Neutral;
		// 			AnnouncementBar.CrossFadeAlpha(1.0f, 1f, false);
		// 			var textColor = AnnouncementBarText.color;
		// 			textColor.a = 1f;
		// 			AnnouncementBarText.color = textColor;
		// 			AnnouncementBarText.CrossFadeAlpha(1.0f, 1f, false);
		// 			if(currEvent.Actor != null)
		// 			{
		// 				ActorTeam = currEvent.Actor[0].Team;
		// 			}
		// 			if(currEvent.Recipient != null)
		// 			{
		// 				RecipientTeam = currEvent.Recipient[0].Team;
		// 			}

		// 			Unit PlayerUnit = null;
		// 			Forger PlayerForger = null;

		// 			if(UnitManager.Instance.LocalForger != null)
		// 			{
		// 				PlayerUnit = UnitManager.Instance.LocalForger;
		// 				PlayerForger = UnitManager.Instance.LocalForger;
		// 			}					


		// 			switch(currEvent.Type)
		// 			{
		// 				case AnnouncementType.ForgesLight:
		// 				case AnnouncementType.MinionSpawn:
		// 				case AnnouncementType.ThirtySecs:
		// 					currClip = (AudioClip)Resources.Load("Audio/AnnouncerVO/GameState/" + currEvent.Type.ToString());
		// 					AnnouncementBarText.text = currEvent.AnnouncementText;
		// 					break; 
		// 				case AnnouncementType.Slain:
		// 					ActorPortrait.color = tempColor;
		// 					RecipientPortrait.color = tempColor;
		// 					ActorRing.color = tempColor;
		// 					RecipientRing.color = tempColor;
		// 					if(ActorTeam == PlayerForger.Team)
		// 					{
		// 						currClip = (AudioClip)Resources.Load("Audio/AnnouncerVO/Enemy/Slain");
		// 						AnnouncementBarText.text = "An enemy has been slain";
		// 						RecipientRing.overrideSprite = Resources.Load<Sprite>("UI/Announcer/Enemy/Plate");
		// 						ActorRing.overrideSprite = Resources.Load<Sprite>("UI/Announcer/Ally/Plate");
		// 					}
		// 					else if(Recipient[0] == PlayerUnit)
		// 					{
		// 						currClip = (AudioClip)Resources.Load("Audio/AnnouncerVO/Player/Slain");
		// 						AnnouncementBarText.text = "You have been slain";
		// 						RecipientRing.overrideSprite = Resources.Load<Sprite>("UI/Announcer/Ally/Plate");
		// 						ActorRing.overrideSprite = Resources.Load<Sprite>("UI/Announcer/Enemy/Plate");
		// 					}
		// 					else
		// 					{
		// 						currClip = (AudioClip)Resources.Load("Audio/AnnouncerVO/Ally/Slain");
		// 						AnnouncementBarText.text = "An ally has been slain";
		// 						RecipientRing.overrideSprite = Resources.Load<Sprite>("UI/Announcer/Ally/Plate");
		// 						ActorRing.overrideSprite = Resources.Load<Sprite>("UI/Announcer/Enemy/Plate");
		// 					}

		// 					RecipientRing.CrossFadeAlpha(1.0f, 1f, false);
		// 					ActorRing.CrossFadeAlpha(1.0f, 1f, false);
		// 					///ActorPortrait.overrideSprite = Resources.Load<Sprite>("Images/Portraits/" + currEvent.Actor[0].Name + "128");
		// 					ActorPortrait.CrossFadeAlpha(1.0f, 1f, false);
		// 					///RecipientPortrait.overrideSprite = Resources.Load<Sprite>("Images/Portraits/" + currEvent.Recipient[0].Name + "128");
		// 					RecipientPortrait.CrossFadeAlpha(1.0f, 1f, false);
		// 					break;
		// 				case AnnouncementType.Double:
		// 					break;
		// 				case AnnouncementType.Triple:
		// 					break;
		// 				case AnnouncementType.Quadra:
		// 					break;
		// 				case AnnouncementType.Penta:
		// 					AnnouncementPanel.transform.position = new Vector3(0, -125, 0);

		// 					break;
		// 				case AnnouncementType.TowerDestroyed:
		// 					if(RecipientTeam == PlayerUnit.Team)
		// 					{
		// 						currClip = (AudioClip)Resources.Load("Audio/AnnouncerVO/Ally/TowerDestroyed");
		// 						AnnouncementBarText.text = "An allied obelisk has been destroyed";
		// 					}
		// 					else
		// 					{
		// 						currClip = (AudioClip)Resources.Load("Audio/AnnouncerVO/Enemy/TowerDestroyed");
		// 						AnnouncementBarText.text = "An enemy obelisk has been destroyed";
		// 					}
		// 					break;
		// 				case AnnouncementType.KillSpree:
		// 					if(ActorTeam == PlayerUnit.Team)
		// 					{
		// 						currClip = (AudioClip)Resources.Load("Audio/AnnouncerVO/Ally/KillSpree");
		// 						AnnouncementBarText.text = "Allied killing spree";
		// 						ActorRing.overrideSprite = Resources.Load<Sprite>("UI/Announcer/Ally/Plate");

		// 					}
		// 					else
		// 					{
		// 						currClip = (AudioClip)Resources.Load("Audio/AnnouncerVO/Enemy/KillSpree");
		// 						AnnouncementBarText.text = "Enemy killing spree";
		// 						ActorRing.overrideSprite = Resources.Load<Sprite>("UI/Announcer/Enemy/Plate");
		// 					}
		// 					ActorRing.CrossFadeAlpha(1.0f, .5f, false);
		// 					break;
		// 				case AnnouncementType.Astounding:
		// 				case AnnouncementType.Exceptional:
		// 				case AnnouncementType.Extraordinary:
		// 				case AnnouncementType.Inexhorable:
		// 				case AnnouncementType.Legendary:
		// 				case AnnouncementType.Phenomenal:
		// 				case AnnouncementType.Supreme:
		// 				case AnnouncementType.Unstoppable:
		// 					if(ActorTeam == PlayerUnit.Team)
		// 					{
		// 						currClip = (AudioClip)Resources.Load("Audio/AnnouncerVO/Ally/" + currEvent.Type.ToString());
		// 						AnnouncementBarText.text = "An ally is " + currEvent.Type.ToString();
		// 						ActorRing.overrideSprite = Resources.Load<Sprite>("UI/Announcer/Ally/Plate");
		// 					}
		// 					else
		// 					{
		// 						currClip = (AudioClip)Resources.Load("Audio/AnnouncerVO/Enemy/" + currEvent.Type.ToString());
		// 						AnnouncementBarText.text = "An enemy is " + currEvent.Type.ToString();
		// 						ActorRing.overrideSprite = Resources.Load<Sprite>("UI/Announcer/Enemy/Plate");
		// 					}
		// 					break;
		// 				case AnnouncementType.Dominating:
		// 					break;
		// 				case AnnouncementType.TowerAttacked:
		// 					break;
		// 			}

		// 			break;

		// 		case AnnouncerSource.Alert:
		// 			break;

		// 		case AnnouncerSource.KillFeed:
		// 			KillFeedPanel = GameObject.Find("KillFeedPanel");				
		// 			break;

		// 		case AnnouncerSource.TeamPanel:
		// 			break;

		// 	}
			
		// 	if(currClip != null)
		// 	{
		// 		if(controller.AnnouncementSource.isPlaying)
		// 		{
		// 			controller.AnnouncementSource.Stop();
		// 		}
		// 		if(!controller.AnnouncementSource.isPlaying)
		// 		{
		// 			controller.PlayAnnouncement(currClip);
		// 			Debug.Log("Trying to play announcement audio");
		// 		}
		// 	}
		// }
	}
}