﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;

namespace feature
{
    public static class FeatureUtils
    {

        public static float GetStatValue(this UnitStats stats, StatType type)
        {
            float value = 0f;
            switch (type)
            {
                case StatType.Armor:
                    value = stats.Armor;
                    break;
                case StatType.Haste:
                    value = stats.Haste;
                    break;
                case StatType.HealthRegen:
                    value = stats.HealthRegen;
                    break;
                case StatType.Lifedrain:
                    value = stats.Lifedrain;
                    break;
                case StatType.Mastery:
                    value = stats.Mastery;
                    break;
                case StatType.MaxHealth:
                    value = stats.MaxHealth;
                    break;
                case StatType.Penetration:
                    value = stats.Penetration;
                    break;
                    /*
                case StatType.PercentMovementSpeed:
                    value = percentStats.MovementSpeed;
                    break;
                    */
                case StatType.Power:
                    value = stats.Power;
                    break;
                case StatType.Resistance:
                    value = stats.Resistance;
                    break;
            }
            return value;
        }
    }
}