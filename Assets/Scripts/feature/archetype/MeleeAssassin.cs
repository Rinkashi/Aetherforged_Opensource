﻿using core;
using core.forger;

namespace feature.kit
{
    public class MeleeAssassin : Obsolete_Archetype
    {
        public override UnitStats BaseStats
        {
            get
            {
                return UnitStats.Stats(
                    MaxHealth: 530,
                    HealthRegen: 1.5f,
                    MovementSpeed: 390,
                    AutoAttackDamage: 59,
                    BaseAttackTime: 1.5f,
                    BaseAttackRange: Constants.MELEE_DEFAULT_ATTACK_RANGE
                );
            }
        }

        public override UnitStats PerLevelStats
        {
            get
            {
                // Note: Right now (level - 1) is applied from per level stats. 
                // E.g. At level one, they get BaseStats only. At level two, they get BaseStats + 1 x PerLevelStats. 
                // And At level six, they get BaseStats + 5 x PerLevelStats.

                return UnitStats.Stats(
                    MaxHealth: 78,
                    HealthRegen: .12f,
                    AutoAttackDamage: 2.6f,
                    AttackSpeed: 2.6f
                );
            }
        }

        public override float basicAttackDamagePerPower
        {
            get
            {
                return .3f;
            }
        }

        public override float attackSpeedPerHaste
        {
            get
            {
                return .3f;
            }
        }

        public override float cooldownReductionPerHaste
        {
            get
            {
                return .6f;
            }
        }

    }
}