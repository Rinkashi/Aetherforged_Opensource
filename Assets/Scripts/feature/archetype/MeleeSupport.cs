﻿using core;
using core.forger;

namespace feature.kit
{
    public class MeleeSupport : Obsolete_Archetype
    {
        public override UnitStats BaseStats
        {
            get
            {
                return UnitStats.Stats(
                    MaxHealth: 540,
                    HealthRegen: 1.6f,
                    MovementSpeed: 385,
                    AutoAttackDamage: 58,
                    BaseAttackTime: 1.5f,
                    BaseAttackRange: Constants.MELEE_DEFAULT_ATTACK_RANGE
                );
            }
        }

        public override UnitStats PerLevelStats
        {
            get
            {
                // Note: Right now (level - 1) is applied from per level stats. 
                // E.g. At level one, they get BaseStats only. At level two, they get BaseStats + 1 x PerLevelStats. 
                // And At level six, they get BaseStats + 5 x PerLevelStats.

                return UnitStats.Stats(
                    MaxHealth: 83,
                    HealthRegen: .17f,
                    AutoAttackDamage: 2.7f,
                    AttackSpeed: 2.6f
                );
            }
        }

        public override float basicAttackDamagePerPower
        {
            get
            {
                return .8f;
            }
        }

        public override float attackSpeedPerHaste
        {
            get
            {
                return .3f;
            }
        }

        public override float cooldownReductionPerHaste
        {
            get
            {
                return .5f;
            }
        }

    }
}
