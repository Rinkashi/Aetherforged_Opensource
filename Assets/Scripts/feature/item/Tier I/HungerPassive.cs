﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
    [CreateAssetMenu(fileName = "HungerPassive", menuName = "Passives/Items/Hunger")]
    public class HungerPassive : Passive
    {
        public float[] HungerPassiveCooldown = new float[] { 1.5f };
        public float HungerPassiveHealed = 5;
        public float HungerPassiveHealedForgers = 10;




        public HungerPassive()
        {
			Name = "Hunger passive";
			Cooldowns = HungerPassiveCooldown;
			BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) => {
				float amountHealed = (target.Kind == UnitKind.Forger) ? HungerPassiveHealedForgers : HungerPassiveHealed;
				float totalAmountHealed = pair.ability.StackValue(amountHealed, 1, owner.StacksOf(pair) - 1);
				HealPacket healPacket = new HealPacket(pair.Source, HealActionType.HealBurst);
				healPacket.HealAmount += totalAmountHealed;
				if (packet.Kind == DamageActionType.AutoAttack || packet.Kind == DamageActionType.SkillOrAbility) {
					owner.ProvideHeal(owner, healPacket);
				}
				return packet;
			};
			NoMaxStacks = true;
		}
    }
}