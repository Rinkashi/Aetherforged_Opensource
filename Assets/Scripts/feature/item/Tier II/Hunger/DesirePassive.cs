﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
    [CreateAssetMenu(fileName = "Desire", menuName = "Passives/Items/Desire")]
    public class DesirePassive : Passive
    {
        public float DesireBuffDuration = 3;
        public float DesireBuffDamagePerStack = 5;
        public float DesireBuffHealPercent = .3f;
        public int DesireBuffMaxStacks = 3;
        public float DesirePassiveRangedHealPenalty = .5f;

        [SerializeField] BuffDebuff DesireBuff;

        public DesirePassive()
        {
            Name = "Desire passive";
            BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
            {
                if (packet.Kind == DamageActionType.AutoAttack)
                {
                    float totalDamageBonus = owner.StacksOf(DesireBuff, pair.Source) * DesireBuffDamagePerStack * owner.StacksOf(pair);
                    float healRatio = owner.AttackRange >= Constants.MIDRANGE_DEFAULT_ATTACK_RANGE ? DesirePassiveRangedHealPenalty : 1;
                    float totalHeal = totalDamageBonus * DesireBuffHealPercent * healRatio;
                    DamagePacket bonusPacket = new DamagePacket(DamageActionType.ItemEffect, owner);
                    HealPacket healPacket = new HealPacket(owner, HealActionType.HealBurst);
                    bonusPacket.PhysicalDamage += totalDamageBonus;
                    healPacket.HealAmount += totalHeal;
                    owner.DealDamage(target, bonusPacket);
                    owner.ProvideHeal(owner, healPacket);
                    owner.AddBuff(DesireBuff.WithSource(pair.Source));
                }
                return packet;
            };
            NoMaxStacks = true;
        }
    }
}