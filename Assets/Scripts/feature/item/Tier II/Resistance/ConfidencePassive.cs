﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
	[CreateAssetMenu(fileName = "Confidence Passive", menuName = "Passives/Items/Confidence")]
	public class ConfidencePassive : Passive
	{
		[SerializeField]
		BuffDebuff ConfidenceDebuff;

		public ConfidencePassive()
		{
			Name = "Confidence Passive";

			BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
			{
				target.AddBuff(ConfidenceDebuff.WithSource(pair.Source));
				return packet;
			};
		}
	}
}