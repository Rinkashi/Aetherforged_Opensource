﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
	[CreateAssetMenu(fileName = "Integrity Passive", menuName = "Passives/Items/Integrity")]
	public class IntegrityPassive : Passive
	{
		[SerializeField]
		float IntegrityPassiveReductionPercentage;

		public IntegrityPassive()
		{
			Name = "Integrity Passive";

			BeforeTakeDamage = (SourcePair pair, Unit owner, PassiveTracker data, DamagePacket packet) =>
			{
				if(packet.Kind == DamageActionType.AutoAttack)
				{
					packet.PercentPostPhysicalModifier += IntegrityPassiveReductionPercentage;
					packet.PercentPostMagicalModifier += IntegrityPassiveReductionPercentage;
				}
				return packet;
			};
		}
	}
}