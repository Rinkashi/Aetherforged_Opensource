using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
	[CreateAssetMenu(fileName = "Protection Passive", menuName = "Passives/Items/Protection")]
	public class ProtectionPassive : Passive
	{

		[SerializeField]
        BuffDebuff ArmorBuff;
		[SerializeField]
        BuffDebuff ResistanceBuff;

		public ProtectionPassive()
		{
			Name = "Protection Passive";
            BeforeTakeDamage = (SourcePair pair, Unit holder, PassiveTracker data, DamagePacket packet) =>
			{
				if(packet.PhysicalDamage > 0 || packet.MagicalDamage > 0)
				{
					if(packet.PhysicalDamage >= packet.MagicalDamage)
					{
						holder.AddBuff(ArmorBuff.WithSource(pair.Source));
					}
                    else
                    {
                        holder.AddBuff(ResistanceBuff.WithSource(pair.Source));
                    }
                    holder.StartCooldown(pair);
				}
				
				return packet;
			};
		}
	}
}