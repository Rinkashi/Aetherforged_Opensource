﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
    [CreateAssetMenu(fileName = "Envy Passive", menuName = "Passives/Items/Envy")]
    public class EnvyPassive : Passive
    {
        public float EnvyPassiveBaseDamage = 40;
        public float EnvyPassivePowerScaling = .25f;
        public float EnvyPassiveSplashRange = 200;
        public float EnvyPassiveHealPercentage = .125f;

        public EnvyPassive()
        {
            Name = "Envy passive";
            BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
            {
                float totalDamage = EnvyPassiveBaseDamage + (EnvyPassivePowerScaling * owner.Power);
                float healPerUnit = totalDamage * EnvyPassiveHealPercentage;
                DamagePacket bonusPacket = new DamagePacket(DamageActionType.ItemEffect, owner, singletarget: false);
                HealPacket healPacket = new HealPacket(pair.Source, HealActionType.HealBurst);
                bonusPacket.PhysicalDamage += totalDamage;
                healPacket.HealAmount += healPerUnit;
                if (packet.Kind == DamageActionType.AutoAttack)
                {
                    Unit.ForEachEnemyInRadius(EnvyPassiveSplashRange, target.Position, owner.Team, (Unit enemy) =>
                    {
                        owner.DealDamage(enemy, bonusPacket);
                        pair.Source.ProvideHeal(owner, healPacket);
                    });
                }
                return packet;
            };
        }
    }
}