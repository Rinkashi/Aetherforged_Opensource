﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
    [CreateAssetMenu(fileName = "Lust Buff", menuName = "Passives/Items/Lust Buff")]
    public class LustBuff : BuffDebuff
    {
        [SerializeField]
        float LustBuffDuration = 3;
        [SerializeField]
        float LustPassiveDamagePercentage = -15f;
        [SerializeField]
        float LustBuffHealIncrease = 25f;

        public LustBuff()
        {
            Name = "Lust buff";
            AlsoObsolete_Duration = (pair) =>
            {
                return LustBuffDuration;
            };
            BeforeReceiveHeal = (SourcePair pair, Unit owner, PassiveTracker data, HealPacket packet) =>
            {
                if (packet.Source == owner)
                {
                    packet.PercentPreHealModifier += LustBuffHealIncrease;
                }
                return packet;
            };
        }
    }
}