﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Insight Debuff", menuName = "Passives/Items/Insight Debuff")]
    public class InsightDebuff : BuffDebuff
    {
        [SerializeField]
        float InsightImmunityDuration = 10;
        [SerializeField]
        int InsightDebuffStackPerRanged = 1;
        [SerializeField]
        int InsightDebuffStackPerMelee = 2;
        [SerializeField]
        float InsightDebuffDuration = 4;
        [SerializeField]
        int InsightDebuffMaxStacks = 5;
        [SerializeField]
        float InsightRootDuration = 1;

        [SerializeField]
        BuffDebuff InsightImmunity;

        [SerializeField]
        BuffDebuff InsightDebuffTimer;
        [SerializeField]
        Status InsightRoot;

        public InsightDebuff()
        {
            Name = "Insight debuff";
            MaximumStacks = InsightDebuffMaxStacks;
        }
    }
}