﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Insight Passive", menuName = "Passives/Items/Insight")]
    public class InsightPassive : Passive
    {
        [SerializeField]
        float InsightImmunityDuration = 10;
        [SerializeField]
        int InsightDebuffStackPerRanged = 1;
        [SerializeField]
        int InsightDebuffStackPerMelee = 2;
        [SerializeField]
        float InsightDebuffDuration = 4;
        [SerializeField]
        int InsightDebuffMaxStacks = 5;
        [SerializeField]
        float InsightRootDuration = 1;

        [SerializeField]
        BuffDebuff InsightImmunity;
        [SerializeField]
        BuffDebuff InsightDebuff;
        [SerializeField]
        BuffDebuff InsightDebuffTimer;
        [SerializeField]
        Status InsightRoot;

        public InsightPassive()
        {
            Name = "Insight passive";
            BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
            {
                if (!target.HasBuff(InsightDebuffTimer) && !target.HasBuff(InsightImmunity, pair.Source) && packet.Kind == DamageActionType.AutoAttack && target.Kind == UnitKind.Forger)
                {
                    target.AddBuff(InsightDebuffTimer.WithSource(pair.Source));
                }
                return packet;
            };
        }
    }
}