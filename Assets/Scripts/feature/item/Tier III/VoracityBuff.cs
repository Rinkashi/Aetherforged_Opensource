﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Voracity Buff", menuName = "Passives/Items/Voracity Buff")]
    public class VoracityBuff : BuffDebuff
    {
        public float VoracityBuffLifeDrainPerStack = 3;
        public int VoracityBuffMaxStacks = 5;
        public float VoracityBuffDuration = 4;
        public int VoracityPassiveStacksForger = 2;

        public VoracityBuff()
        {
            Name = "Voracity buff";
            AlsoObsolete_Duration = (pair) =>
            {
                return VoracityBuffDuration;
            };
            MaximumStacks = VoracityBuffMaxStacks;
            DynamicStats = (SourcePair pair, Unit owner, PassiveTracker data) =>
            {
                float totalLifedrain = VoracityBuffLifeDrainPerStack * pair.Source.StacksOf(pair);
                return UnitStats.Stats(Lifedrain: totalLifedrain);
            };
        }
    }
}