﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Corruption", menuName = "Passives/Items/Corruption")]
    public class CorruptionPassive : Passive
    {
        public float CorruptionImmunityDuration = 5;
        public float CorruptionDebuffDuration = 3;
        public float CorruptionSpeedBuffDuration = 2;
        public float CorruptionSpeedBuffIncrease = 20;

        [SerializeField] BuffDebuff CorruptionSpeedBuff;
        [SerializeField] BuffDebuff CorruptionDebuff;
        [SerializeField] BuffDebuff CorruptionImmunity;
        [SerializeField] BuffDebuff CorruptionLockout;

        public CorruptionPassive()
        {
            Name = "Corruption passive";
            BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
            {
                if ((packet.Kind == DamageActionType.AutoAttack || packet.Kind == DamageActionType.SkillOrAbility) && !target.HasBuff(CorruptionImmunity.WithSource(pair.Source)))
                {
                    target.AddBuff(CorruptionDebuff.WithSource(pair.Source));
                    target.AddBuff(CorruptionImmunity.WithSource(pair.Source));
                }
                return packet;
            };
        }
    }
}