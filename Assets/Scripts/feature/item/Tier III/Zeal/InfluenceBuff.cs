﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Influence Buff", menuName = "Passives/Items/Influence Buff")]
    public class InfluenceBuff : BuffDebuff
    {
        public int InfluenceBuffBuildingMaxStacks = 3;
        public float InfluenceSpeedBuffIncrease = 30;
        public float InfluenceSpeedBuffDuration = 2;
        public float InfluencePassiveBonusAttackDamage = .5f;
        public float InfluenceMarkDuration = 6;

        [SerializeField] BuffDebuff InfluenceBuffBuilding;

        [SerializeField] BuffDebuff InfluenceMark;

        public InfluenceBuff()
        {
            Name = "Influence buff check";
            AlsoObsolete_Duration = (pair) =>
            {
                return InfluenceMarkDuration;
            };
            OnBasicAttack = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
            {
                if (!target.HasBuff(InfluenceMark.WithSource(pair.Source)) && target == packet.PrimaryTarget)
                {
                    owner.RemoveBuff(InfluenceBuffBuilding.WithSource(pair.Source), fullClear: true);
                }
                return packet;
            };
                
        }
    }
}