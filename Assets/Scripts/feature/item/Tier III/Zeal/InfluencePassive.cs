﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Influence", menuName = "Passives/Items/Influence")]
    public class InfluencePassive : Passive
    {
        public int InfluenceBuffBuildingMaxStacks = 3;
        public float InfluenceSpeedBuffIncrease = 30;
        public float InfluenceSpeedBuffDuration = 2;
        public float InfluencePassiveBonusAttackDamage = .5f;
        public float InfluenceMarkDuration = 6;

        [SerializeField] BuffDebuff InfluenceSpeedBuff;
        [SerializeField] BuffDebuff InfluenceBuffBuilding;
        [SerializeField] BuffDebuff InfluenceBuff;
        [SerializeField] BuffDebuff InfluenceMark;

        public InfluencePassive()
        {
            Name = "Influence passive";
            BeforeDealDamage = (SourcePair pair, Unit owner, Unit target, DamagePacket packet) =>
            {
                if (packet.Kind == DamageActionType.AutoAttack)
                {
                    if (owner.GetPassiveData(pair).Mark == null)
                    {
                        owner.GetPassiveData(pair).Mark = target;
                    }
                    if (owner.GetPassiveData(pair).Mark != null && owner.GetPassiveData(pair).Mark != target)
                    {
                        owner.GetPassiveData(pair).Mark.RemoveBuff(InfluenceMark.WithSource(pair.Source));
                        owner.GetPassiveData(pair).Mark = target;
                    }
                    if (owner.GetPassiveData(pair).Mark != null)
                    {
                        owner.GetPassiveData(pair).Mark.AddBuff(InfluenceMark.WithSource(pair.Source));
                    }
                }
                return packet;
            };
        }
    }
}