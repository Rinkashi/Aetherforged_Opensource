﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
	[CreateAssetMenu(fileName = "Cold Steel", menuName = "Ability/Ember/Cold Steel")]
	public class ColdSteel : Castable
	{
		//[SerializeField] float[] damage; - Damage done in the buff
		[SerializeField] BuffDebuff ColdSteelBuff;
		public ColdSteel()
		{
			OnNonTargetedCast = (ability, caster) =>
			{
				caster.AddBuff(ColdSteelBuff.WithSource(caster));
			};
		}
	}
}