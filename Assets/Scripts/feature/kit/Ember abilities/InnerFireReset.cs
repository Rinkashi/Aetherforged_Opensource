﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
	[CreateAssetMenu(fileName = "Inner Fire Reset", menuName = "Ability/Ember/Inner Fire Reset")]
	public class InnerFireReset : BuffDebuff
	{
		[SerializeField] BuffDebuff InnerFireResetBuff;
		public InnerFireReset()
		{
			OnKillOrAssistForger = (pair, holder, data, deadUnit) =>
			{
				holder.Resource.Amount = holder.Resource.MaxAmount;
				holder.RemoveBuff(InnerFireResetBuff.WithSource(pair.Source), fullClear : true);
			};
		}
	}
}