﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
	[CreateAssetMenu(fileName = "Wild Fire", menuName = "Ability/Ember/Wild Fire")]
	public class WildFire : Castable
	{
		[SerializeField] float[] damage;
		[SerializeField] float range;
		[SerializeField] float width;
		[SerializeField] float powerScaling;
		[SerializeField] float innerFireCost;

		[SerializeField] BuffDebuff WildFireBuff;

		public WildFire()
		{	
			LineTargetCast = (ability, caster, location) =>
			{
				float totalDamage = damage[caster.GetAbilityLevelLessOne(ability)] + powerScaling * caster.Power;
				
				Vector2 locationTarget = Utils.PointWithinRange(caster.Position, location, range, range);

				Unit.ForEachEnemyInLine(range, width, caster.Position, locationTarget, caster.Team, (Unit enemy) =>
				{
					caster.DealDamage(enemy, new DamagePacket(DamageActionType.SkillOrAbility, caster) 
						{PhysicalDamage = totalDamage});
				});

				if(caster.Resource >= innerFireCost)
				{
					caster.AddBuff(WildFireBuff.WithSource(caster));
					caster.Resource -= innerFireCost;
				}

				caster.PlayAnimation("Ability2");
				caster.StartCooldown(ability);
			};
		}
	}
}