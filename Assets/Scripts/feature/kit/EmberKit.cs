﻿// using UnityEngine;
// using System;
// using System.Collections;
// using core.forger;
// using core;
// using controllers;
// using System.Collections.Generic;
// using managers;
// using core.utility;

// namespace feature.kit {
//     [CreateAssetMenu(fileName = "Ember Imprint", menuName = "Units/Ember Imprint")]
//     public class EmberKit : Imprint {
//         /*
//         public override Obsolete_Archetype obsolete_archetype {
//             get {
//                 return new CarryBruiser();
//             }
//         }

//         public override ForgerResource characterResource()
//         {
//             return new ForgerResource()
//             {
//                 Name = "inner_fire",
//                 MinAmount = 0,
//                 MaxAmount = 100,
//                 Amount = 0,
//                 Color = new Color(1, 0, 0, 1.0f),
//             };
//         }
//         */
    
//         //ember base variables
//         public float[] EmberCooldown = new float[] { 20 };
//         //ember timer variables
//         Timer EmberOutOfCombatTimer = null;
//         public float EmberTimerDuration = 1;
//         //ember resource variables
//         public float InnerFireBaseGain = 10;
//         public float InnerFireEnhancedGain = 20;
//         public float InnerFireResourceDegrade = 10;
//         public float InnerFireEnhancedThreshold = .4f;
//         //ember buff variables
//         public static BuffDebuff EmberBuff;
//         public float EmberBuffDuration = 8.0f;
//         public float EmberBuffHealthThreshold = .4f;
//         public float EmberAttackSpeed = .3f;
//         public float InnerFireResourceGain = 1000;
//         public static Passive EmberPassive;
//         public static Passive InnerFirePassive;
//         public static BuffDebuff InnerFireOutOfCombatDrain;

//         //cold steel base variables
//         public float[] ColdSteelCooldown = new float[] { 10, 10, 10, 10, 10 };
//         //cold steel shield variables
//         public float[] ColdSteelShieldValue = new float[] { 30, 40, 50, 60, 70 };
//         public float[] ColdSteelBonusDamage = new float[] { 10, 25, 40, 55, 70 };
//         public float ColdSteelPowerBonus = .1f;
//         public float ColdSteelShieldDuration = 3.0f;  
//         public static ShieldBuff ColdSteelShield;
//         //cold steel buff variables
//         public static BuffDebuff ColdSteelBuff;
//         public static BuffDebuff ColdSteelCooldownBuff;
//         public float ColdSteelBuffDuration =  5.0f;
//         public float ColdSteelCooldownReduction = .4f;
//         //cold steel buff resource variables
//         public float ColdSteelResourceCost = 20;
//         public int ColdSteelCooldownMaxStacks = 2;
//         public int ColdSteelBuffMaxStacks = 2;
//         public static Castable ColdSteelActive;

//         //wildfire base variables
//         public float[] WildfireCooldown = new float[] { 12, 11, 10, 9, 8 };
//         //wildfire damage variables
//         public float[] WildFireBaseDamage = new float[] { 60, 105, 150, 195, 240 };
//         public float WildFirePowerScaling = .6f;
//         //wildfire resource variables
//         public float WildFireResourceCost = 40;
//         //wildfire buff variables
//         public static BuffDebuff WildFireAttackBuff;
//         public float WildFireBuffDuration = 5.0f;
//         public int WildFireRange = 325;
//         public int WildFireWidth = 200;
//         public static Castable WildFireActive;

//         //avalanche base variables
//         public float[] AvalancheCooldown = new float[] { 8, 7.5f, 7, 6.5f, 6 };
//         public float AvalancheDashRange = 750;
//         public float AvalancheConeRange = 275;
//         public float AvalancheConeAngle = 170;
//         //avalanche damage variables
//         public float[] AvalancheBaseDamage = new float[] { 50, 90, 130, 170, 210 };
//         public float AvalanchePowerScaling = .5f;
//         //avalanche dash variables
//         public float AvalancheDashDelay = .5f;
//         public float AvalancheDashSpeed = 1000;
//         public float AvalancheDashWidth = 225;
//         public float AvalancheResourceCost = 60;
//         //avalanche dash CC variables
//         public float AvalancheSlowAmount = .35f;
//         public float AvalancheSlowDuration = 1.5f;
//         public float AvalancheCrippleDuration = .75f;
//         public static Castable AvalancheActive;

//         //alvkhret's honor basic variables
//         public float[] AlvkhretsHonorCooldown = new float[] { 140, 125, 110 }; //needs numbers
//         public float AlvkhretsHonorCastRange = 800;
//         //alvkhret's honor projectile variables
//         public float AlvkhretsHonorMissileSpeed = 900;
//         public float AlvkhretsHonorMissileWidth = 75;
//         public string AlvkhretsHonorProjectile = "alvkhrets_honor_projectile";
//         //alvkhret's honor damage variables
//         public float[] AlvkhretsHonorBaseDamage = new float[] { 125, 150, 175 };
//         public float AlvkhretsHonorPowerScaling = .4f;
//         //alvkhret's honor blink variables
//         public float AlvkhretsHonorBlinkRange = 450;
//         //alvkhret's honor blink timer variables
//         public Timer alvkhretsHonorBlinkTimer;
//         public float alvkhretsHonorBlinkDelay = .2f;
//         /*
//         public override string ObsoleteName { get { return "nibra"; } }

//         public override float AttackHitTime
//         {
//             get
//             {
//                 return 0.09f;
//             }
//         }
//         */
//         protected override void Initialize(Unit me)
//         {
//             var ParticleColdSteelIceflames = me.ParticleHelper.Initialize(
//                 "ForgerParticles/Ember/EmberQ/EmberQ_iceflames",
//                 new string[] { "handAttachment .l", "handAttachment .r" }
//             );
//             var ParticleWildfireWhirl = me.ParticleHelper.Initialize(
//                 "ForgerParticles/Ember/EmberW/EmberWwhirl",
//                 "chest"
//             );
//             var ParticleWildfireTrail = me.ParticleHelper.Initialize(
//                 "ForgerParticles/Ember/EmberW/EmberW_trail",
//                 new string[] { "handAttachment .l", "handAttachment .r" }
//             );
//             var ParticleAvalanceTrail = me.ParticleHelper.Initialize(
//                 "ForgerParticles/Ember/EmberE/EmberE_ability_trail",
//                 "NibraArmature"
//             );
//             var ParticleAvalanceMainHit = me.ParticleHelper.Initialize(
//                 "ForgerParticles/Ember/EmberE/EmberE_maintarget_hit",
//                 "chest"
//             );
//             var ParticleAvalanceSlashThroughHit = me.ParticleHelper.Initialize(
//                 "ForgerParticles/Ember/EmberE/EmberE_slashtrough_hit",
//                 "chest"
//             );
//             var ParticleUltGroundEffect = me.ParticleHelper.Initialize(
//                 "ForgerParticles/Ember/EmberR/NibraR_particle",
//                 ""
//             );
//             var ParticleUltDebuff = me.ParticleHelper.Initialize(
//                 "ForgerParticles/Ember/EmberR/NibraR_debuff",
//                 "chest"
//             );

//             #region passive ability

//             EmberBuff = new BuffDebuff()
//             {
//                 Name = "ember_attack_speed",
//                 DynamicStats = (SourcePair pair, Unit owner, PassiveTracker data) => {
//                     return UnitStats.Stats(
//                         AttackSpeed: EmberAttackSpeed
//                     );
//                 },
//                 AlsoObsolete_Duration = (pair) => {
//                     return emberBuffDuration;
//                 },
//                 OnKillOrAssistForger = (SourcePair pair, Unit caster, PassiveTracker data, Unit deceased) => {
//                     caster.Resource += InnerFireResourceGain;
//                 },
//                 Parent = EmberPassive
//             };

//             EmberPassive  = new Passive() {
//                 Name = "ember",
//                 Cooldowns = EmberCooldown,
//                 OnHealthChanged = (SourcePair pair, Unit owner, PassiveTracker info, float delta) => {
//                     if (pair.CanTrigger && delta < 0 && owner.HPRatio <= EmberBuffHealthThreshold) {
//                         owner.AddBuff(EmberBuff.WithSource(pair.Source));
//                         owner.StartCooldown(pair);
//                     }
//                 }
//             };
//             Passives.Add(0, EmberPassive);

//             InnerFireOutOfCombatDrain = new BuffDebuff
//             {
//                 Name = "inner fire cooling off",
//                 Cooldowns = new float[] { 1f },
//                 OnTick = (SourcePair pair, Unit owner, PassiveTracker data, float deltaTime) => {
//                     owner.Resource.Degrade(0, InnerFireResourceDegrade);
//                     owner.StartBareCooldown(pair);
//                 }
//             };

//             InnerFirePassive = new Passive() {
//                 Name = "inner fire",
//                 OnBasicAttack = ((SourcePair pair, Unit owner, Unit attackTarget, DamagePacket packet) => {
//                     if ((owner.HasBuff(ColdSteelBuff.WithSource(pair.Source)) && owner.Resource.Amount >= ColdSteelResourceCost) || attackTarget != packet.PrimaryTarget) {
                        
//                     return packet;
//                     }
//                     else {
//                         Debug.Log("gain resource: innerfire");
//                         float remainingHealthOfTarget = attackTarget.Health / attackTarget.MaxHealth;
//                         if (remainingHealthOfTarget > InnerFireEnhancedThreshold) {
//                             owner.Resource += InnerFireBaseGain;
//                         }
//                         else {
//                             owner.Resource += InnerFireEnhancedGain;
//                         }
//                     }
//                     return packet;
//                 }),
//                 OnExitCombat = (SourcePair pair, Unit owner, PassiveTracker data) => {
//                     InnerFireResourceDegrade = owner.Resource.MaxAmount / 20f;
//                     owner.AddBuff(InnerFireOutOfCombatDrain.WithSource(pair.Source));
//                 },
//                 OnEnterCombat = (SourcePair pair, Unit owner, PassiveTracker data) => {
//                     owner.RemoveBuff(InnerFireOutOfCombatDrain.WithSource(pair.Source), fullClear: true);
//                 },
//             };
//             Passives.Add("resource",InnerFirePassive);
//             #endregion

//             #region first active
//             ColdSteelCooldownBuff = new BuffDebuff() {
//                 Name = "cold_steel_cooldown_buff",
//                 MaximumStacks = ColdSteelCooldownMaxStacks,
//                 Parent = ColdSteelActive
//             };

//             ColdSteelBuff = new BuffDebuff()
//             {
//                 Name = "cold_steel_buff",
//                 OnBasicAttack = (pair, holder, target, packet) => {
//                     holder.AddShield(ColdSteelShield.WithSource(pair.Source));
//                     if(holder.Resource >= ColdSteelResourceCost) {
//                         holder.AddBuff(ColdSteelCooldownBuff.WithSource(pair.Source));
//                         holder.Resource -= ColdSteelResourceCost;
//                     }
//                     holder.RemoveBuff(pair);
//                     return packet;
//                 },
//                 OnRemove = (pair, holder) => {
//                     emberResourceGainIsDisabled = false;
//                     float currentCooldownPercentReduction = holder.StacksOf(ColdSteelCooldownBuff.WithSource(pair.Source)) * ColdSteelCooldownReduction;
//                     float finalRawCooldown = ColdSteelActive.Cooldowns[pair.Source.GetAbilityLevelLessOne(ColdSteelActive)] * (1 - currentCooldownPercentReduction);
//                     pair.Source.SetCooldown(ColdSteelActive, finalRawCooldown, false);
//                     holder.RemoveBuff(ColdSteelCooldownBuff.WithSource(pair.Source), fullClear: true);
//                 },
//                 AlsoObsolete_Duration = (pair) => {
//                     return ColdSteelBuffDuration;
//                 },
//                 Parent = ColdSteelActive
//             };

//             ColdSteelShield = new ShieldBuff()
//             {
//                 Name = "cold_steel_shield",
//                 AlsoObsolete_Duration = (pair) => {
//                     return ColdSteelShieldDuration;
//                 },
//                 NoMaxStacks = true,
//                 Amount = (pair) => {
//                     return coldSteelShieldValue[pair.Source.GetAbilityLevelLessOne(pair.ability)];
//                 },
//                 Maximum = (pair) => {
//                     return coldSteelShieldMaxShield[pair.Source.GetAbilityLevelLessOne(pair.ability)];
//                 },
//                 Parent = ColdSteelActive
//             };

//             ColdSteelActive = new Castable() {
//                 Name = "cold_steel",
//                 Cooldowns = ColdSteelCooldown,
//                 GetIconSubtext = (ability, caster) => {
//                     return 10.ToString();
//                 },
//                 OnNonTargetedCast = (ability, caster) => {
//                     caster.PlayAnimation("Ability1");
//                     caster.AddBuff(ColdSteelBuff.WithSource(caster), ColdSteelBuffMaxStacks);
//                     ParticleColdSteelIceflames.Activate();
//                 },
//             };
//             Skills.Add(1, ColdSteelActive);
//             #endregion

//             #region second active
//             //FIXME - needs a complete overhaul in its code design
//             WildFireAttackBuff = new BuffDebuff()
//             {
//                 Name = "wildfire_basic_attack",
//                 BasicAttackAnimationOverride = "Ability2",
//                 //FIXME - this animation shouldn't happen at the start of the basic attack, since the attack can still be canceled at this point.
//                 //Should probably instead be a sweeping animation that starts at the target and goes 360 degrees in onbasicattack
//                 Obsolete_OnBasicAttackStart = (Ability ability, Unit caster, Unit target) => {
//                     Debug.Log("Starting basic attack!");
//                     ParticleWildfireWhirl.Activate();
//                     ParticleWildfireTrail.Activate();
//                     new Timer().Start(() => {
//                         ParticleWildfireWhirl.Deactivate();
//                         ParticleWildfireTrail.Deactivate();
//                         return 0f;
//                     }, 0.45f);
//                 },
//                 OnBasicAttack = (SourcePair pair, Unit caster, Unit target, DamagePacket packet) => {
//                     DamagePacket extraPacket = new DamagePacket(DamageActionType.SkillOrAbility, caster, singletarget: false);
//                     packet.PhysicalDamage += WildFireBaseDamage[pair.Source.GetAbilityLevelLessOne(WildFireActive)] + (WildFirePowerScaling * caster.Power);
//                     Unit.ForEachEnemyInLine(WildFireRange, WildFireWidth, caster.Position, target.Position, caster.Team, (Unit enemy) => {
//                         caster.DealDamage(enemy, packet);
//                     });
//                     return packet;
//                 },
//                 AlsoObsolete_Duration = (pair) => {
//                     return WildFireBuffDuration;
//                 },
//                 Parent = WildFireActive
//             };
//             WildFireActive = new Castable() {
//                 Name = "wildfire",
//                 Cooldowns = WildfireCooldown,
//                 LineTargetCast = (Castable ability, Unit caster, Vector2 location) => {
//                     DamagePacket packet = new DamagePacket(DamageActionType.SkillOrAbility, caster, singletarget: false);
//                     packet.PhysicalDamage += WildFireBaseDamage[caster.GetAbilityLevelLessOne(ability)] + (WildFirePowerScaling * caster.Power);
//                     Unit.ForEachEnemyInLine(WildFireRange, WildFireWidth, caster.Position, location, caster.Team, (Unit enemy) => {
//                         caster.DealDamage(enemy, packet);
//                     });
//                     if(caster.Resource >= WildFireResourceCost) {
//                         caster.AddBuff(WildFireAttackBuff.WithSource(caster));
//                         caster.Resource -= WildFireResourceCost;
//                     }
//                     caster.StartCooldown(ability);
//                 }
//             };
//             Skills.Add(2, WildFireActive);
//             #endregion

//             #region third active
//             AvalancheActive = new Castable() {
//                 Name = "avalanche",
//                 Cooldowns = AvalancheCooldown,
//                 TargetedCastingConditions = Ability.UnitTargetShortcuts.EnemySimple,
//                 LineTargetCast = (Castable ability, Unit caster, Vector2 currentTargetLocation) => {
//                     //needs total rework anyway
//                     float dashBaseDamage = new float[] { 30, 50, 70, 90, 110 }[caster.GetAbilityLevelLessOne(ability)];
//                     float dashEnhancedDamage = new float[] { 20, 40, 60, 80, 100 }[caster.GetAbilityLevelLessOne(ability)];
//                     float damage = dashBaseDamage;
//                     float actualDelay = 0;
//                     if(caster.Resource >= 80) {
//                         caster.Resource -= 80;
//                         damage = dashBaseDamage + dashEnhancedDamage;
//                         actualDelay = avalancheDashDelay;
//                     }
//                     //TODO: If the delay is greater than 0, lock ability to act during the animation
                    
//                     new Timer().Start(() => {
//                         //TODO: Replace this with an actually functioning dash
//                         caster.PlayAnimation("Ability3");
//                         ParticleAvalanceTrail.Activate();
//                         caster.Dash(
//                             ability,
//                             location,
//                             AvalancheDashSpeed,
//                             onStop: (dash) => {
//                                 ParticleAvalanceTrail.Deactivate();
//                                 caster.PlayAnimation("Ability3.End");
//                                 //do damage in area in front of Nibra
//                                 Vector2 newEndPoint = caster.Position + (caster.Position - initialPosition).normalized;
//                                 Unit.ForEachEnemyInCone(AvalancheConeAngle, AvalancheConeRange, caster.Position, newEndPoint, caster.Team, (Unit enemy) => {
//                                     caster.DealDamage(enemy, packet);
//                                     Status AvalancheSlow = Status.CreateStaticSlow((Unit slowedUnit) => { return AvalancheSlowDuration; }, AvalancheSlowAmount, false);
//                                     enemy.ApplySlow(AvalancheSlow);
//                                     if (isEmpowered) {
//                                         //TODO: Add cripples to the game and update this to a cripple
//                                         Status AvalancheCripple = Status.CreateSilence((Unit crippledUnit) => { return AvalancheSlowDuration; });
//                                         enemy.AddBuff(AvalancheCripple.WithSource(caster));
//                                     }
//                                 });
//                                 dash.BreakMe();
//                             }
//                         );
//                         return 0f; },actualDelay);
//                     caster.StartCooldown(ability);
//                 }
//             };
//             Skills.Add(3, AvalancheActive);
//             #endregion

//             #region fourth active
//             AlvkhretsHonorImmunity = new BuffDebuff() {
//                 Name =  "Alvkhret's Honor Immunity Dummy Buff",
//                 Duration = (SourcePair pair) => {
//                     return AlvkhretsHonorImmunityDuration;
//                 },
//                 Parent = AlvkhretsHonorActive
//             };

//             AlvkhretsHonorActive = new Castable() {
//                 Name = "Alvkhret's Honor",
//                 Cooldowns = AlvkhretsHonorCooldown,
//                 LineTargetCast = (ability, caster, location) => {
//                     Boolean forgerIsInRange = true;
//                     Unit AlvkhretsHonorFirstHit;
//                     Unit AlvkhretsHonorCurrentTarget;
//                     Unit closestUntargetedForger;
//                     float totalDamagePerHit = AlvkhretsHonorBaseDamage[caster.GetAbilityLevelIndex(ability)] + (caster.Power * AlvkhretsHonorPowerScaling);
//                     DamagePacket packet = new DamagePacket(DamageActionType.SkillOrAbility, caster);
//                     packet.PhysicalDamage += totalDamagePerHit;
//                     ProjectileManager.Instance.Gimme("", location, caster, AlvkhretsHonorMissileSpeed,
//                         OnHitUnit: (proj, hit_unit) => {
//                             if (hit_unit.Kind == UnitKind.Forger)
//                             {
//                                 //TODO: Update blink so that it can blink to a unit either in front or behind as needed
//                                 //caster.BlinkToUnit(ability, hit_unit, true);
//                                 //needs to play animation before doing the next action
//                                 caster.DealDamage(hit_unit, packet);
//                                 AlvkhretsHonorFirstHit = hit_unit;
//                                 AlvkhretsHonorCurrentTarget = hit_unit;
//                                 while (forgerIsInRange) {
//                                     closestUntargetedForger = null;
//                                     Unit.ForEachEnemyInRadius(AlvkhretsHonorBlinkRange, AlvkhretsHonorCurrentTarget.Position, caster.Team, (Unit enemy) => {
//                                         if(enemy.Kind == UnitKind.Forger && !enemy.HasBuff(AlvkhretsHonorImmunity.WithSource(caster)) && enemy != AlvkhretsHonorCurrentTarget) {
//                                             if(closestUntargetedForger == null) {
//                                                 closestUntargetedForger = enemy;
//                                             }
//                                             else if(AlvkhretsHonorCurrentTarget.GetDistance(enemy) < AlvkhretsHonorCurrentTarget.GetDistance(closestUntargetedForger)) {
//                                                 closestUntargetedForger = enemy;
//                                             }
//                                         }
//                                     });
//                                     if (closestUntargetedForger == null) {
//                                         forgerIsInRange = false;
//                                         if (AlvkhretsHonorCurrentTarget != hit_unit) {
//                                             //caster.BlinkToUnit(ability, hit_unit, true);
//                                             //needs to play animation before doing the next action
//                                             caster.DealDamage(hit_unit, packet);
//                                         }
//                                     }
//                                     else {
//                                         //caster.BlinkToUnit(ability,closestUntargetedForger,true);
//                                         //needs to play animation before doing the next action
//                                         caster.DealDamage(closestUntargetedForger, packet);
//                                         AlvkhretsHonorCurrentTarget = closestUntargetedForger;
//                                         AlvkhretsHonorCurrentTarget.AddBuff(AlvkhretsHonorImmunity.WithSource(caster));
//                                     }
//                                 }
//                                 proj.BreakMe();
//                             }
//                         });
//                     caster.StartCooldown(ability);
//                 }
//             };
//             Skills.Add(4, AlvkhretsHonorActive);
//             #endregion
//         }
//     }
// }