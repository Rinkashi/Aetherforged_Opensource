﻿using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using controllers;
using System.Collections.Generic;
using managers;
using core.utility;

namespace feature.kit
{
    public class PredatorKit : Imprint
    {
        /*
        public override Obsolete_Archetype obsolete_archetype
        {
            get
            {
                return new MeleeCarry();
            }
        }
        */
        //taste the air variables
        public float[] tastetheAirCooldown = new float[] { 30 };
        //taste the air initial variables
        Timer TasteTheAirVisionTimer = null;
        public float[] tasteTheAirVisionDuration = new float[] { 5, 4.5f, 4, 3.5f, 3, 2.5f, 2 };
        public float tastetheAirInitialRange = 800;
        //taste the air debuff variables
        public static BuffDebuff TastetheAirDebuff;
        public float tastetheAirDebuffDuration = 12.0f;
        public float tastetheAirDebuffRange = 1200;

        //blood spray base variables
        public float[] bloodSprayCooldown = new float[] { 10, 10, 10, 10, 10 };
        //blood spray damage variables
        public float[] bloodSprayBaseDamage = new float[] { 30, 45, 60, 75, 90 };
        public float bloodSprayPowerScaling = .5f;
        //blood spray bload soaked debuff variables
        public static BuffDebuff BloodSoakedDebuff;
        public float bloodSoakedDebuffDuration = 3f;
        public float[] bloodSoakedBonusDamage = { 10, 15, 20, 25, 30 };

        //ability 2 base variables
        public float[] ability2Cooldown = new float[] { 1, 1, 1, 1, 1 }; //needs numbers
        public float[] ability2ChargeTime = new float[] { 20, 18.5f, 17, 16.5f, 14 };
        //ability 2 damage variables
        public float[] ability2BaseDamage = new float[] { 50, 75, 100, 125, 150 }; //needs numbers
        public float ability2PowerScaling = .2f;
        //ability 2 dash variables
        public float ability2DashSpeed = 800;
        public float ability2DashRange = 300;
        //ability 2 cc variables
        public float ability2CrippleDuration = 1.5f;
        public float ability2StunDuration = .75f;

        //pounce base variables
        public float[] pounceCooldown = new float[] { 1, 1, 1, 1, 1 }; //needs numbers
        public float pounceInitialTargetRange = 250;
        public float pounceMaxTargetRange = 950;
        //pounce damage variables
        public float[] pounceBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float pouncePowerScaling = .2f;
        //pounce dash variables
        public float pounceSelfSlow = .5f;
        public float pounceDashSpeed = 1000;
        public float pounceDamageRange = 225;
        //pounce buff variables
        public BuffDebuff PounceBuff;
        public float[] pounceBuffAttackSpeed = { .7f,.75f,.8f,.85f,.9f };
        public float pounceBuffDuration = 2f;
        public float pounceBuffMaxAttacks = 3;


        //the hunt basic variables
        public float[] theHuntCooldown = new float[] { 1, 1, 1 }; //needs numbers
        public float theHuntCastRange = 700;
        //the hunt damage variables
        public float[] theHuntBaseDamage = new float[] { 1, 1, 1 };
        public float theHuntPowerScaling = .2f;
        public float theHuntPureDamagePercent = .5f;
        //the hunt buff variables
        public BuffDebuff TheHuntBuff;
        public float theHuntBuffDuration = 3.5f;
        public float theHuntBuffMovementSpeed = 60;
        public float theHuntBuffAttackRange = 250;
        /*
        public override string ObsoleteName { get { return "cor'mata"; } }
        */
        protected override void Initialize(Unit me)
        {
            var ParticlebloodSprayIceflames = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberQ/EmberQ_iceflames",
                new string[] { "handAttachment .l", "handAttachment .r" }
            );
            var ParticleWildfireWhirl = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberW/EmberWwhirl",
                "chest"
            );
            var ParticleWildfireTrail = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberW/EmberW_trail",
                new string[] { "handAttachment .l", "handAttachment .r" }
            );
            var ParticlepounceTrail = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberE/EmberE_ability_trail",
                "NibraArmature"
            );
            var ParticlepounceMainHit = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberE/EmberE_maintarget_hit",
                "chest"
            );
            var ParticlepounceSlashThroughHit = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberE/EmberE_slashtrough_hit",
                "chest"
            );
            var ParticleUltGroundEffect = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberR/NibraR_particle",
                ""
            );
            var ParticleUltDebuff = me.ParticleHelper.Initialize(
                "ForgerParticles/Ember/EmberR/NibraR_debuff",
                "chest"
            );

            #region passive ability
            var tasteTheAirPassive = new Passive()
            {
                Name = "tasteTheAir",
                Cooldowns = tastetheAirCooldown
            };
            Passives.Add(0, tasteTheAirPassive);
            #endregion

            #region first active
            var BloodSprayActive = new Castable()
            {
                Name = "blood spray",
                Cooldowns = bloodSprayCooldown,
                GetIconSubtext = (Ability ability, Unit caster) => {
                    return 10.ToString();
                },
                OnNonTargetedCast = (ability, caster) => {
                },
            };
            Skills.Add(1, BloodSprayActive);
            #endregion

            #region second active
            var Ability2Active = new Castable()
            {
                Name = "ability2",
                Cooldowns = ability2Cooldown,
                OnNonTargetedCast = (ability, caster) => {
                }
            };
            Skills.Add(2, Ability2Active);
            #endregion

            #region third active
            var PounceActive = new Castable()
            {
                Name = "pounce",
                Cooldowns = pounceCooldown,
                TargetedCastingConditions = Ability.UnitTargetShortcuts.EnemySimple,
                OnNonTargetedCast = (ability, caster) => {
                }
            };
            Skills.Add(3, PounceActive);
            #endregion

            #region fourth active
            var TheHuntActive = new Castable()
            {
                Name = "The Hunt",
                Cooldowns = theHuntCooldown,
                OnNonTargetedCast = (ability, caster) => {
                }
            };
            Skills.Add(4, TheHuntActive);
            #endregion
        }
    }
}
