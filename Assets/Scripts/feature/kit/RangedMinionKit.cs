﻿using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using controllers;

namespace feature.kit {

    [CreateAssetMenu(fileName = "Ranged Minion Kit", menuName = "Units/Ranged Minion Kit")]
    public class RangedMinionKit : Kit {
        public static BuffDebuff miniBuff;
        /*
        public override string ObsoleteName { get { return "ranged minion"; } }
        */
        protected override void Initialize (Unit me)
        {
            
            base.Initialize(me);
        }
        /*
        public override float AttackHitTime {
            get {
                return 0.2f;
            }
        }
        public override ForgerResource characterResource() {
            return new ForgerResource() {
                Name = "derp_juice",
                MinAmount = 0,
                MaxAmount = 0,
                Amount = 0,
                Color = new Color(1, 0, 0),
            };
        }
        */

        public override UnitStats GetBaseStats() {
                PerLevelStats = UnitStats.Stats(
                    MaxHealth: 7f,
                    AutoAttackDamage: 1f,
                    Hephisalt: 0.5f,
                    Experience: .45f

                );

    		return UnitStats.Stats(
    			MaxHealth: 350,
    			MovementSpeed: 340,
    			AutoAttackDamage: 22,
    			BaseAttackRange: 525,
    			BaseAttackTime: 1.6f,
    			/*AttackHitTime: 0.4f,*/

                Hephisalt: 18f,
                Experience: 42f
    		);
        }
    }
}