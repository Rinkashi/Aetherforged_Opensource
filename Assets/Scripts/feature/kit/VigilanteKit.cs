﻿using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using controllers;
using System.Collections.Generic;
using managers;
using core.utility;

namespace feature.kit
{
    public class VigilanteKit : Imprint
    {
        /*
        public override Obsolete_Archetype obsolete_archetype
        {
            get
            {
                return new Tank();
            }
        }
        */
        #region justified variables
        //justified aura variables
        public int justifiedRadius = 350;
        public bool justifiedIsAura = true;
        //justified debuff variables
        public Ability justifiedDebuff;
        public float justifiedDuration = 5;
        //justified debuff damage variables
        public float[] justifiedBaseDamage = new float[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
        #endregion

        #region riptide variables
        //riptide base variables
        public float[] riptideCooldown = new float[] { 10, 9, 8, 7, 6 };
        public float riptideRange = 800;
        //riptide projectile variables
        public float riptideMissileSpeed = 1000;
        public float riptideMissileCurve = 40; //I have no idea how this will work
        public string riptideProjectile = "riptide_projectile";
        //riptide damage variables
        public float[] riptideBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float riptideDamagePowerScaling = .5f;
        //riptide game object variables
        public float riptideWhirlpoolDuration = 2.5f;
        public float riptideWhirlpoolRadius = 130;
        //riptide game object CC variables
        public float riptidePullDuration = .3f;
        public float riptidePullDistance = 75;
        public float riptidePullHeight = 25;
        public float riptideSlowDuration = .1f;
        public float[] riptideSlowAmount = new float[] { .25f, .3f, .35f, .4f, .45f };
        #endregion

        #region aegis variables
        //aegis base variables
        public float[] aegisCooldown = new float[] { 1, 1, 1, 1, 1 };
        //aegis recast variables
        public bool aegisIsRecast= false;
        public bool aegisCanRecast = false;
        //aegis buff variables
        public BuffDebuff aegisBuff;
        public float aegisBuffDuration = 1;
        //aegis buff stat variables
        public float aegisDRAmount = .8f;
        //aegis buff CC variables
        public float aegisPullDuration = .25f;
        public int aegisPullRadius = 1;
        public float aegisPullDistance = 1f;
        public float aegisPullHeight =  1;
        #endregion

        #region heroic charge variables
        //heroic charge base variables
        public float[] heroicChargeCooldown = new float[] { 1, 1, 1, 1, 1 };
        public float heroicChargeBaseRange = 650;
        public float heroicChargeIncreasedRange = 100;
        //heroic charge dash variables
        public float heroicChargeDashSpeed = 850;
        public float heroicChargeDashWidth = 300;
        //heroic charge dash damage variables
        public float[] heroicChargeBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float heroicChargePowerScaling = .5f;
        //heroic charge dash CC variables
        public float heroicChargeKnockupDuration = .5f;
        public float heroicChargeKnockupRange = 325;
        public float heroicChargeKnockupWidth = 300;
        public float heroicChargeKnockupHeight = 75;
        #endregion

        #region burial at sea base variables
        //burial at sea base variables
        public float[] burialAtSeaCooldown = new float[] { 1, 1, 1 };
        public float burialAtSeaRange = 675;
        //burial at sea game object variables
        public int burialAtSeaBaseRadius = 300;
        //burial at sea game object timer variables
        public Timer burialAtSeaDelayTimer;
        public float burialAtSeaDelay = 1f;
        //burial at sea game object damage variables
        public float[] burialAtSeaDamagePerSpike = new float[] { 1, 1, 1 };
        public float burialAtSeaPowerScaling = .5f;
        //burial at sea game object CC variables
        public float burialAtSeaRootDuration = .75f;
        //burial at sea game object other variables
        public float burialAtSeaAdditionalSpikeEfficiency = .25f;
        #endregion
        /*
        public override string ObsoleteName { get { return "vigilante"; } }
        */
        protected override void Initialize(Unit me)
        {
            #region passive ability
            justifiedDebuff = new BuffDebuff()
            {
                Name = "justified_debuff",
                BeforeTakeDamagePostMit = (pair, holder, data, packet) => 
                {
                    if (packet.Kind == DamageActionType.AutoAttack && packet.Source == pair.Source)
                    {
                        var bonusDamage = new DamagePacket(DamageActionType.SkillOrAbility, pair.Source)
                        {
                            MagicalDamage = justifiedBaseDamage[pair.ParentAbilityLevelLessOne]
                        };
                        holder.RemoveBuff(pair);
                        packet.Source.DealDamage(holder, bonusDamage);
                    }
                    return packet;
                }
            };

            var justifiedPassive = new Passive()
            {
                Name = "justified",
                IsAura = justifiedIsAura,
                AuraRadius = justifiedRadius,
                //create the trigger for an ally taking damage
            };
            Passives.Add(0, justifiedPassive);
            #endregion

            #region first active
            var riptideActive = new Castable()
            {
                Name = "riptide",
                Cooldowns = riptideCooldown,
                GetIconSubtext = (ability, caster) => {
                    return 10.ToString();
                },
                OnNonTargetedCast = (Castable ability, Unit caster) => {

                    float baseDamage = riptideBaseDamage[ability.Level - 1];
                    float totalDamage = baseDamage + (riptideDamagePowerScaling * caster.Power);
                    caster.PlayAnimation("Ability1");
                    //set up missile logic so that when the projectile reaches the end of its range, it stops and the whirlpool forms
                },
            };
            Skills.Add(1, riptideActive);
            #endregion

            #region second active
            aegisBuff = new BuffDebuff()
            {
                Name = "aegis_buff",
                /*
                //grant dr to the character
                Obsolete_OnRemove = (Passive self, Unit buffedUnit, Unit caster) => {
                    Unit.ForEachEnemyInRadius(aegisPullRadius, buffedUnit.Position, buffedUnit.Team, (Unit enemy) => {
                        //apply the pull towards the caster
                    });
                },
                Obsolete_Duration = (Passive self) =>{
                    return aegisBuffDuration;
                }
                */
            };

            var aegisActive = new Castable()
            {
                Name = "aegis",
                Cooldowns = aegisCooldown,
                OnNonTargetedCast = (Castable ability, Unit caster) => {
                    if (!aegisIsRecast) {
                        caster.AddBuff(aegisBuff);
                        aegisIsRecast = true;
                        aegisCanRecast = true; //TODO this should probably just be built in to recastable abilities in the first place
                        //set the full cooldown
                        
                    }
                    else {
                        caster.RemoveBuff(aegisBuff);
                        aegisIsRecast = false;
                        aegisCanRecast = false;
                    }
                }
            };
            Skills.Add(2, aegisActive);
            #endregion

            #region third active
            var heroicChargeActive = new Castable()
            {
                Name = "heroic_charge",
                Cooldowns = heroicChargeCooldown,
                OnNonTargetedCast = (Castable ability, Unit caster) => {
                    float baseDamage = heroicChargeBaseDamage[ability.Level - 1];
                    float totalDamage = baseDamage + (heroicChargePowerScaling * caster.Power);
                    Vector2 locationTarget;
                    Vector2 updatedLocationTarget;
                    float hitAllies = 0;
                    if (Utils.TryGetPlanarPointWithinRangeFromMousePosition(out locationTarget, caster.Position, heroicChargeBaseRange)) {
                        caster.Dash(ability, locationTarget, heroicChargeDashSpeed, onUnitInRadius: (dash, hit_unit) => {
                            if(hit_unit is Forger) {
                                if (hit_unit.Team == caster.Team) {
                                    hitAllies++;
                                    float pointDistance = Vector2.Distance(caster.Position, locationTarget);
                                    float updatedPointDistance = pointDistance + (hitAllies * heroicChargeIncreasedRange);
                                    //update the location that the dash is going to
                                }
                                else {
                                    //stop, deal damage in area in front of Ramorah and knock them into the air
                                }
                            }
                            else {
                                caster.DealDamage(hit_unit, new DamagePacket { MagicalDamage = totalDamage });
                            }
                        });
                    }
                }
            };
            Skills.Add(3, heroicChargeActive);
            #endregion

            #region fourth active
            var burialAtSeaActive = new Castable()
            {
                Name = "burial_at_sea",
                Cooldowns = burialAtSeaCooldown,
                OnNonTargetedCast = (Castable ability, Unit caster) => {
                    float totalDamage = burialAtSeaDamagePerSpike[ability.Level - 1] + (burialAtSeaPowerScaling* caster.Power);
                    burialAtSeaDelayTimer = new Timer();
                    burialAtSeaDelayTimer.Start(() => {
                        //create the spikes in the targeted area, then have them check to see who is the closest forger within a certain radius and have them target that person
                        new GroundEffect() {
                            Position = caster.Position,
                            //on spawning, do thing
                        };
                        return 0f;
                    }, burialAtSeaDelay);
                }
            };
            Skills.Add(4, burialAtSeaActive);
            #endregion
        }
    }
}