﻿using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using core.utility;
using controllers;
using System.Collections.Generic;
using managers;

using UnityEngine.Networking;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Visionary Imprint", menuName = "Units/Visionary Imprint")]
    public class VisionaryKit : Imprint
    {
        /*
        public override Obsolete_Archetype obsolete_archetype
        {
            get
            {
                return new RangedSupport();
            }
        }

        public override string ObsoleteName { get { return "Visionary"; } }
        */
        protected override void Initialize(Unit me)
        {
            base.Initialize(me);

        }
    }
}