﻿using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using core.utility;
using controllers;
using managers;

namespace feature.kit
{
    public class YetiKit : Imprint
    {
        /*
        public override Obsolete_Archetype obsolete_archetype
        {
            get
            {
                return new Tank();
            }
        }
        */
        #region stone cold variables
        //stone cold base variables
        public float[] StoneColdCooldown = new float[] { 3, 3, 3, 3 };
        //stone cold timer variables
        public float StoneColdInCombatTimerDuration = 5;
        public static BuffDebuff StoneColdTimerBuff;
        //stone cold buff variables
        public static BuffDebuff StoneColdBuff;
        public float StoneColdMinDuration = .25f;
        public float[] StoneColdDurationReduction;
        public static Passive StoneColdPassive;
        #endregion

        #region breathtaker
        //breathtaker base variables
        public float[] BreathtakerCooldown = new float[] { 10, 9, 8, 7, 6 };
        public float BreathtakerTargetRange = 195;
        //breathtaker damage variables
        public float[] BreathtakerBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float[] BreathtakerBasePercentHealthDamage = new float[] { .1f, .1f, .1f, .1f, .1f };
        public float BreathtakerPowerScaling = .1f;
        //breathtaker CC variables
        public float BreathtakerKnockbackDuration = .05f;
        public int BreathtakerKnockbackRange = 25;
        public int BreathtakerKnockbackHeight = 20;
        public float BreathtakerSlowDuration = .1f;
        public float BreathtakerSlowAmount = .75f;
        public static Castable BreathtakerActive;
        #endregion

        #region frigid lance
        //frigid lance base variables
        public float[] FrigidLanceCooldown = new float[] { 1, 1, 1, 1, 1 };
        public float FrigidLanceRange = 800;
        //frigid lance projectile varibles
        public float FrigidLanceMissileSpeed = 1200;
        public float FrigidLanceMissileWidth = 50;
        public float FrigidLanceAoERadius = 175;
        public string FrigidLanceProjectile = "frigid_lance_projectile";
        //frigid lance damage variables
        public float[] FrigidLanceBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float FrigidLancePowerScaling = .6f;
        public float[] FrigidLanceSecondaryDamage = new float[] { 1, 1, 1, 1, 1 };
        public float FrigidLanceSecondaryPowerScaling = .3f;
        //frigid lance CC variables
        public float FrigidLanceStunDuration = .75f;
        public float FrigidLanceStunDurationPowerScaling = .0025f;
        public float FrigidLanceSlowDuration = 1.5f;
        public float[] FrigidLanceSlowAmount = { .2f, .25f, .3f, .35f, .4f };
        public static Castable FrigidLanceActive;
        #endregion

        #region frozen squall variables
        //frozen squall base variables
        public float[] FrozenSquallCooldown = new float[] { 1, 1, 1, 1, 1 };
        public int FrozenSquallRadius = 250;
        //frozen squall damage variables
        public float[] FrozenSquallBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float FrozenSquallPowerScaling = .5f;
        //frozen squall CC variables
        public float FrozenSquallSlowDuration = 2;
        public float FrozenSquallSlowAmount = .25f ;
        //frozen squall Boost variables
        public float FrozenSquallFullDuration = 1.5f;
        public float FrozenSquallBuildDuration = 1;
        public float FrozenSquallInitialBoostAmount = .05f;
        public float[] FrozenSquallFullBoostAmount = new float[] { .15f, .175f, .2f, .225f, .25f };
        public static Castable FrozenSquallActive;
        #endregion

        #region skarstind onslaught variables
        //skarstind onslaught base variables
        public float[] SkarstindOnslaughtCooldown = new float[] { 140, 130, 120 };
        public float SkarstindOnslaughtLeapRange = 400;
        public float SkarstindOnslaughtLeapSpeed = 850;
        public float SkarstindOnslaughtInitialAoERadius = 150;
        public float SkarstindOnslaughtLeapAoERadius = 250;
        public float SkarstindOnslaughtFullRange = 1000;
        public float SkarstindOnslaughtChargeSpeed = 700;
        public float SkarstindOnslaughtChargeAoEWidth = 150;
        public float SkarstindOnslaughtChargeAoELength = 200;
        //skarstind onslaught damage variables
        public float[] SkarstindOnslaughtChargeBaseDamage = new float[] { 175, 250, 325 };
        public float SkarstindOnslaughtChargePowerScaling = .8f;

        public float[] SkarstindOnslaughtLeapBaseDamage = new float[] { 200, 300, 400 };
        public float SkarstindOnslaughtLeapPowerScaling = 1;
        //skarstind onslaught CC variables
        public float SkarstindOnslaughtChargeStunDuration = .5f;
        public float SkarstindOnslaughtLeapStunDuration = .75f;
        public static Castable SkarstindOnslaughtActive;
        #endregion
        /*
        public override string ObsoleteName { get { return "jaromir"; } }
        */
        protected override void Initialize(Unit me)
        {

            #region passive ability
            StoneColdTimerBuff = new BuffDebuff() {
                Name = "stone_cold_timer_buff",
                AlsoObsolete_Duration = (SourcePair pair) => {
                    return StoneColdInCombatTimerDuration;
                },
                OnRemove = (SourcePair pair, Unit holder) => {
                    if(holder.InCombat){ 
                        holder.AddBuff(StoneColdBuff.WithSource(pair.Source));
                    }
                },
                Parent = StoneColdPassive
            };
            StoneColdBuff = new BuffDebuff()
            {
                Name = "stone_cold_buff",
                //Commented out since this causes an compiler error
                // BeforeTakeCC = (SourcePair pair, Unit holder, Unit source, Status disable) => {
                //     //TODO: Reduce the starting duration of the CC
                //     float remainingDuration = 0;
                //     //TODO: Use TryGetRemainingDuration to find the duration of the disable
                //     float alteredduration =  remainingDuration - StoneColdDurationReduction[pair.Source.GetAbilityLevelIndex(StoneColdPassive)];
                //     float statusFinalDuration = Mathf.Max(StoneColdMinDuration, alteredduration);
                    
                //     pair.Source.StartBareCooldown(StoneColdPassive.WithSource(pair.Source));
                //     pair.Source.RemoveBuff(pair);
                //     return disable;
                // },
                Parent = StoneColdPassive
            };

            StoneColdPassive = new Passive()
            {
                Name = "stone_cold",
                Cooldowns = StoneColdCooldown,
                OnEnterCombat = (SourcePair pair, Unit source, PassiveTracker data) => {
                    if (!source.HasBuff(StoneColdTimerBuff.WithSource(source))) {
                        source.AddBuff(StoneColdTimerBuff.WithSource(source));
                    }
                },
                OnExitCombat = (SourcePair pair, Unit source, PassiveTracker data) => {
                    source.RemoveBuff(StoneColdTimerBuff.WithSource(source));
                    source.RemoveBuff(StoneColdBuff.WithSource(source));
                }
                
            };
            Passives.Add(0, StoneColdPassive);
            #endregion

            #region first active
            BreathtakerActive = new Castable()
            {
                Name = "breathtaker",
                Cooldowns = BreathtakerCooldown,
                GetIconSubtext = (ability, caster) => {
                    return 10.ToString();
                },
                TargetedCastingConditions = Ability.UnitTargetShortcuts.EnemySimple,
                UnitTargetedCast = (Castable ability, Unit caster, Unit target) => {
                    caster.PlayAnimation("Ability1");
                    float baseDamage = BreathtakerBaseDamage[caster.GetAbilityLevelLessOne(ability)] + (BreathtakerPowerScaling * caster.Power);
                    float percentHealthDamage = target.BonusStats.MaxHealth * (BreathtakerBasePercentHealthDamage[caster.GetAbilityLevelLessOne(ability)]);
                    float totalDamage = baseDamage + percentHealthDamage;
                    DamagePacket packet = new DamagePacket(DamageActionType.SkillOrAbility, caster);
                    packet.PhysicalDamage += totalDamage;
                    caster.DealDamage(target, packet);
                    //TODO: Make sure that knockback is set up ocrrectly
                    target.Knockback(caster.Position, BreathtakerKnockbackDuration, BreathtakerKnockbackRange, BreathtakerKnockbackHeight);
                    Status breathtakerSlow = Status.CreateStaticSlow((Unit enemy) => { return BreathtakerSlowDuration; }, BreathtakerSlowAmount, false);
                    target.ApplySlow(breathtakerSlow);
                },
            };
            Skills.Add(1, BreathtakerActive);
            #endregion

            #region second active
            FrigidLanceActive = new Castable()
            {
                Name = "frigid_lance",
                Cooldowns = FrigidLanceCooldown,
                LineTargetCast = (Castable ability, Unit caster, Vector2 location) => {
                    float totalDamage = FrigidLanceBaseDamage[caster.GetAbilityLevelLessOne(ability)] + (FrigidLancePowerScaling * caster.Power);
                    float totalSecondaryDamage = totalDamage * .6f;
                    float totalStunDuration = FrigidLanceStunDuration + (caster.Power * FrigidLanceStunDurationPowerScaling);

                    DamagePacket primaryPacket = new DamagePacket(DamageActionType.SkillOrAbility, caster);
                    DamagePacket secondaryPacket = new DamagePacket(DamageActionType.SkillOrAbility, caster, singletarget: false);
                    ProjectileController frigidLanceProjectilePiece;
                    Vector2 locationTarget = Utils.PointWithinRange(caster.Position, location, FrigidLanceRange, FrigidLanceRange);
                    {
                        frigidLanceProjectilePiece = ProjectileManager.Instance.Gimme("", locationTarget, caster, FrigidLanceMissileSpeed, FrigidLanceMissileWidth, 
                            OnHitUnit:(proj, hit_unit) => {
                                if(hit_unit.Team != caster.Team) {
                                    caster.DealDamage(hit_unit, primaryPacket);
                                    Status frigidLanceStun = Status.CreateStun((Unit stunnedUnit) => { return totalStunDuration; });
                                    frigidLanceStun.Parent = ability;
                                    //TODO: Update once stuns have their own apply function
                                    hit_unit.AddBuff(frigidLanceStun.WithSource(caster));
                                    Status frigidLanceSlow = Status.CreateStaticSlow((Unit slowedUnit) => { return FrigidLanceSlowDuration; }, FrigidLanceSlowAmount[caster.GetAbilityLevelLessOne(ability)], false);
                                    Unit.ForEachEnemyInRadius(FrigidLanceAoERadius, hit_unit.Position, caster.Team, (Unit enemy) => {
                                        if(enemy != hit_unit) {
                                            caster.DealDamage(enemy, secondaryPacket);
                                        }
                                        enemy.ApplySlow(frigidLanceSlow);

                                    });
                                    proj.BreakMe();
                                }
                            });
                    }
                    
                }
            };
            Skills.Add(2, FrigidLanceActive);
            #endregion

            #region third active
            FrozenSquallActive = new Castable()
            {
                Name = "frozen_squall",
                Cooldowns = FrozenSquallCooldown,
                OnNonTargetedCast = (Castable ability, Unit caster) => {
                    float totalDamage = FrozenSquallBaseDamage[caster.GetAbilityLevelLessOne(ability)] + (FrozenSquallPowerScaling * caster.Power);
                    DamagePacket packet = new DamagePacket(DamageActionType.SkillOrAbility, caster, singletarget:false);
                    packet.MagicalDamage += totalDamage;
                    caster.PlayAnimation("Ability3");
                    Status frozenSquallSlow = Status.CreateStaticSlow((Unit slowedUnit) => { return FrozenSquallSlowDuration; }, FrozenSquallSlowAmount, false);
                    ForEnemiesInRadius(FrozenSquallRadius, caster, (Unit enemy) => {
                        caster.DealDamage(enemy, packet);
                        enemy.ApplySlow(frozenSquallSlow);
                    });
                    //TODO: Once scaling speed buffs are a thing
                    //caster.AddBuff(new BoostBuff(frozenSquallInitialBoostAmount, frozenSquallFullBoostAmount, frozenSquallBuildDuration, frozenSquallFullDuration, 0));
                }
            };
            Skills.Add(3,FrozenSquallActive);
            #endregion

            #region fourth active
            SkarstindOnslaughtActive = new Castable()
            {
                Name = "skarstind_onslaught",
                Cooldowns = SkarstindOnslaughtCooldown,
                LineTargetCast = (Castable ability, Unit caster, Vector2 location) => {
                    //do a dash to the targeted location then continue the dash forward until you hit an enemy
                    Vector2 casterOriginalPosition = caster.Position;
                    float totalLeapDamage = SkarstindOnslaughtLeapBaseDamage[caster.GetAbilityLevelLessOne(ability)] + (SkarstindOnslaughtLeapPowerScaling * caster.Power);
                    float totalChargeDamage = SkarstindOnslaughtChargeBaseDamage[caster.GetAbilityLevelLessOne(ability)] + (SkarstindOnslaughtChargePowerScaling * caster.Power);
                    DamagePacket leapPacket = new DamagePacket(DamageActionType.SkillOrAbility, caster, singletarget: false);
                    leapPacket.PhysicalDamage += totalLeapDamage;
                    DamagePacket chargePacket = new DamagePacket(DamageActionType.SkillOrAbility, caster, singletarget: false);
                    chargePacket.PhysicalDamage += totalChargeDamage;
                    Vector2 leapLandingPoint = Utils.PointWithinRange(caster.Position, location, SkarstindOnslaughtLeapRange, 0);
                    float actualLeapDistance = caster.GetDistance(leapLandingPoint);
                    caster.Dash(ability, leapLandingPoint, SkarstindOnslaughtLeapSpeed, onStop: (leap) => {
                        float enemiesInRange = 0;
                        Unit.ForEachEnemyInRadius(SkarstindOnslaughtInitialAoERadius, caster.Position, caster.Team, (enemy) =>{
                            if(enemy.Kind == UnitKind.Forger) {
                                enemiesInRange++;
                                return;
                            }
                        });
                        if (enemiesInRange > 0)
                        {
                            Status leapStun = Status.CreateStun((stunnedUnit) => { return SkarstindOnslaughtLeapStunDuration; });
                            Unit.ForEachEnemyInRadius(SkarstindOnslaughtLeapAoERadius, caster.Position, caster.Team, (enemy) =>
                            {
                                caster.DealDamage(enemy, leapPacket);
                                //TODO: Update this once it's got its own apply function
                                caster.AddBuff(leapStun.WithSource(caster));
                            });
                        }
                        else {
                            float dashDist = SkarstindOnslaughtFullRange - actualLeapDistance;
                            Vector2 normalizedCharge = caster.Position + (caster.Position - casterOriginalPosition).normalized;
                            Vector2 chargeEndpoint = Utils.PointWithinRange(caster.Position, normalizedCharge, dashDist, dashDist);
                            //TODO: Update this once mox adds the action onhitunit so that this second part only triggers when you collide with someone
                            caster.Dash(ability, chargeEndpoint, SkarstindOnslaughtChargeSpeed);
                        }
                    });
                }
            };
            Skills.Add(4, SkarstindOnslaughtActive);
            #endregion
        }
    }
}