﻿using UnityEngine;
using core;

namespace feature
{
	[CreateAssetMenu(fileName = "Generic Buff", menuName = "Passives/Generic/GenericBuff")]
	public class GenericBuff : BuffDebuff
	{
		public GenericBuff()
		{
			Name = "Generic Buff";
            duration = float.NaN;
		}
	}
}