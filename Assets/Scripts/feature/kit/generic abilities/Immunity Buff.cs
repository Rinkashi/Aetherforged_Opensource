﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "New Immunity", menuName = "Passives/Generic/Immunity")]
    public class ImmunityBuff : BuffDebuff
    {
        [SerializeField] BuffDebuff[] immunities = new BuffDebuff[1];

        public ImmunityBuff()
        {
            duration = 1f;
            BeforeTakeCC = (pair, holder, crowdControl) =>
            {
                foreach (var buff in immunities)
                {
                    if (crowdControl.ability.Equals(buff))
                    {
                        crowdControl.ability = null;
                        return crowdControl;
                    }
                }
                return crowdControl;
            };
        }
    }
}