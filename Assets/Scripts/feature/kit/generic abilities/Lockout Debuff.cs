﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "New Lockout", menuName = "Passives/Generic/Lockout")]
    public class LockoutDebuff : BuffDebuff
    {
        [SerializeField] BuffDebuff[] lockout = new BuffDebuff[1];

        public LockoutDebuff()
        {
            
        }

    }
}