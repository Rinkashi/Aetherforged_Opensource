﻿using UnityEngine;
using core;
using System.Collections.Generic;
using core.forger;
using managers;

namespace feature
{
	[CreateAssetMenu(fileName = "New Health Regen", menuName = "Passives/Generic/Static Health Regen")]
	public class StaticHealthRegen : Status
	{
		[SerializeField]
		float regenAmount;
		[SerializeField]
		float regenDuration;

		public StaticHealthRegen()
		{
			Name = "Static Health Regen";
            duration = regenDuration;
			OnTick = (SourcePair pair, Unit owner, PassiveTracker data, float deltaTimer) =>
			{
				HealPacket healPacket = new HealPacket(owner, HealActionType.HealBurst);
				healPacket.HealAmount += regenAmount / regenDuration * Time.deltaTime;
				pair.Source.ProvideHeal(owner, healPacket);
			};
		}
	}
}
