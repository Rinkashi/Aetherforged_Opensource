﻿using UnityEngine;
using core;
using System.Collections.Generic;

namespace feature
{
    [CreateAssetMenu(fileName = "New Stun", menuName = "Passives/Generic/Stun")]
    public class StunStatus : Status
    {
        public StunStatus()
        {

            Name = "Stun";
            duration = .75f;
            CanBeCleansed = true;
            AffectedByTenacity = true;
            Disables = new DisablingFeatures
            {
                NameOfStatus = "Stun",
                Movement = true,
                Casting = true,
                Cripple = false
            };


            OnApply = (SourcePair statusDebuff, Unit targetUnit) =>
            {
                var status = (Status)statusDebuff.ability;
                targetUnit.ApplyDisablingFeatures(status.Disables);
            };
            OnRemove = (SourcePair statusDebuff, Unit targetUnit) =>
            {
                var status = (Status)statusDebuff.ability;
                targetUnit.RemoveDisablingFeatures(status.Disables);
            };
        }

    }
}