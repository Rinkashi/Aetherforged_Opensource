﻿using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using core.utility;
using controllers;
using managers;

namespace feature.kit
{
	[CreateAssetMenu(fileName = "Breathtaker", menuName = "Ability/Nomad/Breathtaker")]
	public class Breathtaker : Castable
	{
		[SerializeField] float range;
		[SerializeField] float[] damage = new float[5];
		[SerializeField] float powerScaling;
		[SerializeField] float[] percentBonusHealthDamage = new float[5];
		[SerializeField] float knockbackTime;
		[SerializeField] int knockbackDis;
		[SerializeField] BuffDebuff breathtakerSlow;

		public Breathtaker()
		{
			UnitTargetedCast = (ability, caster, target) =>
			{
				Kit.ForEnemiesInRadius(range, caster, (Unit enemy) =>
				{
					float totalDamage = damage[caster.GetAbilityLevelLessOne(ability)] 
						+ (powerScaling * caster.Power) 
							+ ((caster.MaxHealth - caster.BaseStats.MaxHealth) * percentBonusHealthDamage[caster.GetAbilityLevelLessOne(ability)]);
					caster.DealDamage(enemy, new DamagePacket (DamageActionType.SkillOrAbility, caster) {PhysicalDamage = totalDamage});
					caster.StartCooldown(ability);
					enemy.Knockback(caster.Position, knockbackTime, knockbackDis, 0);
					enemy.AddBuff(breathtakerSlow.WithSource(caster));

					caster.PlayAnimation("Ability1", 0);
				});
			};
		}
	}
}