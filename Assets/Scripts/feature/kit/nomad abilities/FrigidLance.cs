﻿using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using core.utility;
using controllers;
using managers;

namespace feature.kit
{
	[CreateAssetMenu(fileName = "FrigidLance", menuName = "Ability/Nomad/FrigidLance")]
	public class FrigidLance : Castable
	{
		[SerializeField] float range;
		[SerializeField] float speed;
		[SerializeField] float radius;
		[SerializeField] float[] damage = new float[5];
		[SerializeField] float powerScaling;
		[SerializeField] BuffDebuff FrigidLanceStun;
		[SerializeField] string projectile;
		[SerializeField] ProjectileController projectilePiece;
		[SerializeField] float arcAngle;
		[SerializeField] float arcRange;
		[SerializeField] float passthroughDamage;
		[SerializeField] BuffDebuff passthroughSlow;

		public FrigidLance()
		{
			LineTargetCast = (ability, caster, location) =>
			{
				float totalDamage = damage[caster.GetAbilityLevelLessOne(ability)] 
					+ (powerScaling * caster.Power);
				
				Vector2 initLocation = caster.Position;
				Vector2 locationTarget = Utils.PointWithinRange
					(caster.Position, location, range, range);
				{
					caster.PlayAnimation("Ability2", locationTarget);
					projectilePiece = ProjectileManager.Instance.Gimme
						(projectile, locationTarget, caster, speed, 
							radius, OnHitUnit: (proj, hitUnit) =>
					{
						if(hitUnit.Team != caster.Team)
						{
							caster.DealDamage(hitUnit, new DamagePacket(DamageActionType.SkillOrAbility, caster) 
								{PhysicalDamage = totalDamage});
							hitUnit.AddBuff(FrigidLanceStun.WithSource(caster));
							proj.BreakMe();
							Vector2 endPoint = caster.Position + (caster.Position - initLocation).normalized;

							Unit.ForEachEnemyInCone(arcAngle, arcRange, locationTarget, endPoint, hitUnit.Team, (Unit hitUnit2) =>
							{
								float extraDamage = totalDamage * passthroughDamage;
								caster.DealDamage(hitUnit2, new DamagePacket(DamageActionType.SkillOrAbility, caster)
									{PhysicalDamage = extraDamage});
								hitUnit2.AddBuff(passthroughSlow.WithSource(caster));
							});
						}
					});
				}
				caster.StartCooldown(ability);
			};
		}
	}
}