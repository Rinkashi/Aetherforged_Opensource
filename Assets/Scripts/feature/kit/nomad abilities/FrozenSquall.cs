﻿using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using core.utility;
using controllers;
using managers;

namespace feature.kit
{
	[CreateAssetMenu(fileName = "FrozenSquall", menuName = "Ability/Nomad/FrozenSquall")]
	public class FrozenSquall : Castable
	{
		[SerializeField] float[] damage;
		[SerializeField] float powerScaling;
		[SerializeField] float radius;
		[SerializeField] BuffDebuff slow;
		[SerializeField] BuffDebuff speedUp;

		public FrozenSquall()
		{		
			OnNonTargetedCast = (ability, caster) =>
			{
				float totalDamage = damage[caster.GetAbilityLevelLessOne(ability)] 
					+ (powerScaling * caster.Power);

                caster.AddBuff(speedUp.WithSource(caster));

				Kit.ForEnemiesInRadius(radius, caster, (Unit enemy) =>
                {
                    caster.DealDamage(enemy, new DamagePacket (DamageActionType.SkillOrAbility, caster)
						{ PhysicalDamage = totalDamage });
                    enemy.AddBuff(slow.WithSource(caster));
                });
				caster.PlayAnimation("Ability3");
				caster.StartCooldown(ability);
			};
		}
	}
}