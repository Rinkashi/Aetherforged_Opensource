﻿using UnityEngine;
using System;
using System.Collections;
using core.forger;
using core;
using core.utility;
using controllers;
using managers;

namespace feature.kit
{
	[CreateAssetMenu(fileName = "Stone Cold", menuName = "Ability/Nomad/Stone Cold")]
	public class StoneCold : Passive
	{
		[SerializeField]
		BuffDebuff StoneColdTrigger;
		[SerializeField]
		BuffDebuff StoneColdBuff;
		[SerializeField]
		float combatDuration;

		float timeInCombat = 0;
		bool inCombat = false;

		public StoneCold()
		{
			OnEnterCombat = (pair, holder, data) =>
            {
				inCombat = true;
            };

			OnExitCombat = (pair, holder, data) =>
			{
				inCombat = false;
			};
			
			BeforeTakeCC = (pair, holder, status) =>
			{
				if(holder.HasBuff(StoneColdTrigger.WithSource(pair.Source)) && timeInCombat >= combatDuration)
				{
					//reduce CC duration
				}
				return status;
			};
		}

		public void Update()
		{
			if(inCombat == true)
			{
				if(timeInCombat >= combatDuration)
					{
						return;
					}
				
				timeInCombat += Time.deltaTime;
			}
		}
    }
}