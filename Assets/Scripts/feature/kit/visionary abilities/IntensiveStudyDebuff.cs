﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "IntensiveStudyDebuff", menuName = "Ability/Visionary/IntensiveStudyDebuff")]
    public class IntensiveStudyDebuff : BuffDebuff
    {
        #region intensive study variables
        //intensive study base variables
        public float[] intensiveStudyCooldown = new float[] { 1, 1, 1, 1, 1 };
        //intensive study debuff variables
        public BuffDebuff intensiveStudyDebuff;
        public float intensiveStudyDebuffDuration = 5;
        public bool intensiveStudyReducePower = false;
        public bool intensiveStudyReapply = false;
        public float[] intensiveStudyPowerReduction = new float[] { 1, 1, 1, 1, 1 };
        public float[] intensiveStudyResistanceReduction = new float[] { 1, 1, 1, 1, 1 };
        public float intensiveStudyDebuffAmp = 2.0f;
        public float intensiveStudyReappDuration = 2.5f;
        #endregion

        public IntensiveStudyDebuff()
        {
            Name = "intensive_study_debuff";
            //reduce the appropriate stats of the unit for the duration
            DynamicStats = (SourcePair pair, Unit holder, PassiveTracker arg3) => 
            {
                //check to determine unit's archetype/stats
                return UnitStats.Stats(Power: -(intensiveStudyPowerReduction[pair.ParentAbilityLevelLessOne]));
            };
            //on break vision/on leave radius, end the debuff
            //on end, if duration is run out, reapply the debuff and don't require that the vision be maintained
            AlsoObsolete_Duration = (SourcePair pair) => 
            {
                return intensiveStudyDebuffDuration;
            };
        }

    }
}