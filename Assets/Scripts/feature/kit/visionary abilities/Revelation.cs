﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Revelation", menuName = "Ability/Visionary/Revelation")]
    public class Revelation : Castable
    {
        #region revelation variables
        //revelation base variables
        //public float[] revelationCooldown = new float[] { 10, 9, 8, 7, 6 };
        public float revelationRange = 900;
        //revelation projectile variables
        public float revelationMissileSpeed = 1000;
        public string revelationProjectile = "revelation_projectile";
        public ProjectileController revelationProjectilePiece;
        //revelation damage variables
        public float[] revelationBaseDamage = new float[] { 100, 100, 100, 100, 100 };
        public float revelationDamagePowerScaling = .5f;
        //revelation CC variables
        public float revelationSlowDuration = 1f;
        public float[] revelationSlowAmount = new float[] { .25f, .3f, .35f, .4f, .45f };
        //revelation other variables
        public bool revelationUnitIsStudied = false;

        [SerializeField] float turnDuration = 0.2f;
        #endregion

        public Revelation()
        {
            Name = "revelation";
            //Cooldowns = revelationCooldown;
            GetIconSubtext = (ability, caster) =>
            {
                return 10.ToString();
            };
            LineTargetCast = (Castable ability, Unit caster, Vector2 location) =>
            {
                float baseDamage = revelationBaseDamage[ability.Level - 1];
                float totalDamage = baseDamage + (revelationDamagePowerScaling * caster.Power);
                revelationUnitIsStudied = false;



                //TODO set up missile logic so that it allows a recast of the ability if a unit is targeted by the W

                Vector2 locationTarget = Utils.PointWithinRange(caster.Position, location, revelationRange, revelationRange);
                {
                    // face the targeted location
                    caster.HaltAndFaceTarget(locationTarget, turnDuration);
                    // animate this active ability
                    caster.PlayAnimation("Ability1", locationTarget);
                    revelationProjectilePiece = ProjectileManager.Instance.Gimme(revelationProjectile, locationTarget, caster, revelationMissileSpeed
                        , OnHitUnit: (proj, hit_unit) =>
                        {
                            if (hit_unit.Team != caster.Team)
                            {
                                caster.DealDamage(hit_unit, new DamagePacket(DamageActionType.SkillOrAbility, caster) { MagicalDamage = totalDamage });
                            //Add slow
                            //hit_unit.AddBuff();
                            proj.BreakMe();
                            }
                        }
                    );

                }
                caster.StartCooldown(ability);
            };
        }
    }
}