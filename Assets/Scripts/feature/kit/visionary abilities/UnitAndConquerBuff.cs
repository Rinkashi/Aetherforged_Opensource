﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Unite And Conquer Buff", menuName = "Ability/Visionary/Unite And Conquer Buff")]
    public class UniteAndConquerBuff : BuffDebuff
    {
        public float[] UniteAndConquerPowerAmount;

        public UniteAndConquerBuff()
        {
            Name = "unite_and_conquer_buff";

            //AdjustStats = (Passive self) =>
            //{
            //    return UnitStats.Stats(Power: uniteandConquerPowerBuff[self.Parent.Level - 1]);
            //};
        }

    }
}