﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Unite And Conquer", menuName = "Ability/Visionary/UniteAndConquer")]
    public class UniteAndConquer : Castable
    {

        #region unite and conquer variables
        //unite and conquer base variables
        public float[] uniteandConquerCooldown = new float[] { 10, 8, 6 };
        public float uniteandConquerRange = 3000;
        //unite and conquer timer variables
        public Timer uniteandConquerDormantTimer;
        public float uniteandConquerDormantDuration = 30;
        public Timer uniteandConquerActiveTimer;
        public float uniteandConquerActiveDuration = 5;
        public Timer uniteandConquerManualDelayTimer;
        public float uniteandConquerManualPrimeDuration = 10;
        public Timer uniteandConquerAutomaticDelayTimer;
        public float uniteandConquerAutomaticPrimeDuration = 3;
        //unite and conquer recast variabless
        public float uniteandConquerCurrentRecasts = 0;
        public float uniteandConquerMaxRecasts = 1;
        //unite and conquer game object variables
        public GroundEffect uniteandConquerGroundEffect;
        public int uniteandConquerRadius = 300;
        public int uniteandConquerAutomaticRadius = 100;
        public bool uniteandConquerCanbeAutoActivated = false;
        public bool uniteandConquerBuffIsActive = false;
        //unite and conquer buff variables
        public BuffDebuff uniteandConquerBuff;

        public float uniteandConquerBuffDuration = .1f;
        public float[] uniteandConquerPowerBuff = new float[] { 1, 1, 1 };

        public float uniteandConquerPowerScaling = .5f;
        #endregion

        public UniteAndConquer()
        {
            Name = "unite_and_conquer";
            Cooldowns = uniteandConquerCooldown;
            IsUltimate = true;
            /*// V ========== Blink Debug =========== V
            LineTargetCast = (Castable ability, Unit caster, Vector2 targetPosition) =>
            {
                var coroutine = GameManager.Instance.DoDelay(() =>
                {
                    caster.Blink(ability, targetPosition);
                    return 0f;
                }, uniteandConquerActiveDuration);
                GameManager.Instance.StartCoroutine(coroutine);
                caster.StartCooldown(ability);
            };
            //  ^^^^^^^^^^^ Blink Debug ^^^^^^^^^^^^^ */
            AreaCast = (Castable ability, Unit caster, Vector2 targetPosition) => {      

                if (uniteandConquerCurrentRecasts <= 0)
                {
                    Vector2 locationTarget = Utils.PointWithinRange(caster.Position, targetPosition, uniteandConquerRange, 0f);
                    {
                        caster.PlayAnimation("Ability4", locationTarget);
                        float totalPowerBuff = uniteandConquerPowerBuff[ability.Level - 1] + (uniteandConquerPowerScaling * caster.Power);
                        uniteandConquerCurrentRecasts++;
                        //create a new game object that sits on the ground
                        uniteandConquerGroundEffect = new GroundEffect()
                        {
                            Position = locationTarget, //TODO change to calculate out to the mouse location/closest possible if out of range
                            OnUnitEnter = (Unit target) =>
                            {
                                if (target is Forger && uniteandConquerBuffIsActive)
                                {
                                    target.AddBuff(uniteandConquerBuff);
                                }
                                else if (!uniteandConquerBuffIsActive && uniteandConquerCanbeAutoActivated)
                                {
                                    uniteandConquerBuffIsActive = true;
                                    target.AddBuff(uniteandConquerBuff);
                                }
                            },
                            OnUnitLeave = (Unit target) =>
                            {
                                target.RemoveBuff(uniteandConquerBuff);
                            },
                            Radius = 425,
                            Duration = uniteandConquerDormantDuration
                        };

                        //put the ability on the cooldown
                        //set the manual recast delay
                        uniteandConquerManualDelayTimer = new Timer();
                        uniteandConquerManualDelayTimer.Start(() =>
                        {
                            //set up the manual recast
                            return 0f;
                        }, uniteandConquerManualPrimeDuration);

                        uniteandConquerAutomaticDelayTimer = new Timer();
                        uniteandConquerAutomaticDelayTimer.Start(() =>
                        {
                            //TODO change this 
                            uniteandConquerCanbeAutoActivated = true;
                            return 0f;
                        }, uniteandConquerAutomaticPrimeDuration);
                    }
                }
                else
                {
                    // FIXME static tracking breaks if a kit exists more than once.
                    uniteandConquerBuffIsActive = true;
                    uniteandConquerGroundEffect.Duration = uniteandConquerActiveDuration;
                }

            };
        }
        /*             var coneTestAbility = new Castable()
                    {
                        Name = "test_cone",
                        Cooldowns = new float[] {.5f, .5f, .5f},
                        OnConeTargetCast = (Castable ability, Unit caster) => {
                            Vector2 orig = Utils.PlanarPoint(caster.gameObject.transform.position);
                            Vector2 target = new Vector2();
                            if(Utils.TryGetPlanarPointFromMousePosition(out target))
                            {
                                Utils.GetUnitsInCone(orig, target, 70f, 500);
                            }
                        },

                    }; */


    }




        
}