﻿using System.Collections.Generic;
using UnityEngine;

using controllers;
using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Cardinal Call", menuName = "Ability/Warcardinal/Cardinal Call")]
    public class CardinalCall : Castable
    {

        #region cardinal call variables
        //cardinal call base variables
        public float[] cardinalCallCooldown = new float[] { 1, 1, 1, 1, 1 };
        public int cardinalCallAoERadius = 325;
        public int cardinalCallBaseRangeIncrease = 100;
        public float cardinalCallShieldRangePowerScaling = .25f;
        //cardinal call shield variables
        public float cardinalCallShieldDuration = 3;
        public float[] cardinalCallBaseShield = new float[] { 1, 1, 1, 1, 1 };
        public float cardinalCallPowerScaling = .5f;
        public float cardinalCallShieldReductionPercentage = .2f;
        public float[] cardinalCallMinimumShieldEffect = new float[] { .2f, .3f, .4f, .5f, .6f };
        //cardinal call empowered variables
        public bool cardinalCallIsEmpowered = false;

        [SerializeField]
        ShieldBuff cardinalShield;
        #endregion

        #region second active
        public CardinalCall()
        {
            Name = "cardinal_call";

            OnNonTargetedCast = (ability, caster) =>
            {
                float numAllies = 0;
                List<Unit> alliesEffected = new List<Unit>();
                alliesEffected.Add(caster);
                int finalRange = cardinalCallAoERadius;
                float rawShieldEfficency;
                float minShieldEfficiency = cardinalCallMinimumShieldEffect[caster.GetAbilityLevelLessOne(ability)];
                float currentShieldEffect;
                float finalShieldValue;
                caster.PlayAnimation("Ability2");
                int bonusRange = (int)(cardinalCallShieldRangePowerScaling * caster.Power);

                if (caster.Resource.Amount >= caster.Resource.MaxAmount)
                {
                    cardinalCallIsEmpowered = true;
                    caster.Resource.Amount -= caster.Resource.Amount;
                }

                if (cardinalCallIsEmpowered)
                {
                    finalRange += cardinalCallBaseRangeIncrease + bonusRange;
                }
                Kit.ForAlliesInRadius(finalRange, caster, (Unit ally) =>
                {
                    if (ally is Forger && ally != caster)
                    {
                        numAllies++;
                        alliesEffected.Add(ally);
                    }
                });

                alliesEffected.ToArray();
                rawShieldEfficency = 1 - (numAllies * cardinalCallShieldReductionPercentage);
                if (cardinalCallIsEmpowered)
                {
                    rawShieldEfficency = 1;
                }
                currentShieldEffect = (rawShieldEfficency >= minShieldEfficiency) ? rawShieldEfficency : minShieldEfficiency;
                finalShieldValue = (cardinalCallBaseShield[caster.GetAbilityLevelLessOne(ability)] + (cardinalCallPowerScaling * caster.Power)) * currentShieldEffect;


                foreach (Unit target in alliesEffected)
                {
                    target.AddShield(cardinalShield.WithSource(caster));
                }
                caster.StartCooldown(ability);
            };
        }
        #endregion
    }
}