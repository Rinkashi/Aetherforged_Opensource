﻿using System.Collections.Generic;
using UnityEngine;

using controllers;
using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Cardinal Shield Buff", menuName = "Ability/Warcardinal/Cardinal Shield Buff")]
    public class CardinalShieldBuff : ShieldBuff
    {
        #region cardinal call variables
        //cardinal call base variables
        public float[] cardinalCallCooldown = new float[] { 1, 1, 1, 1, 1 };
        public int cardinalCallAoERadius = 325;
        public int cardinalCallBaseRangeIncrease = 100;
        public float cardinalCallShieldRangePowerScaling = .25f;
        //cardinal call shield variables
        public float cardinalCallShieldDuration = 3;
        public float[] cardinalCallBaseShield = new float[] { 1, 1, 1, 1, 1 };
        public float cardinalCallPowerScaling = .5f;
        public float cardinalCallShieldReductionPercentage = .2f;
        public float[] cardinalCallMinimumShieldEffect = new float[] { .2f, .3f, .4f, .5f, .6f };
        //cardinal call empowered variables
        public bool cardinalCallIsEmpowered = false;
        #endregion

        public CardinalShieldBuff()
        {
            Name = "cardinal_shield";
            OnApply = (pair, holder) =>
            {
                List<Unit> alliesEffected = new List<Unit>();

                int finalRange = cardinalCallAoERadius;
                float rawShieldEfficency;

                float currentShieldEffect;
                float finalShieldValue;

                //int bonusRange = (int)(cardinalCallShieldRangePowerScaling * caster.Power);

                int numAllies = alliesEffected.Count;

                alliesEffected.ToArray();
                rawShieldEfficency = 1 - (numAllies * cardinalCallShieldReductionPercentage);
                if (cardinalCallIsEmpowered)
                {
                    rawShieldEfficency = 1;
                }
            };


            Amount = (pair) =>
            {
                int numAllies = 1;
                float currentShieldEffect = Mathf.Max(1 - (numAllies * cardinalCallShieldReductionPercentage), cardinalCallMinimumShieldEffect[pair.AbilityLevelLessOne]);
                return (cardinalCallBaseShield[pair.AbilityLevelLessOne] + (cardinalCallPowerScaling * pair.Source.Power)) * currentShieldEffect;
            };
            Maximum = (pair) =>
            {
                int numAllies = 1;
                float currentShieldEffect = Mathf.Max(1 - (numAllies * cardinalCallShieldReductionPercentage), cardinalCallMinimumShieldEffect[pair.AbilityLevelLessOne]);
                return (cardinalCallBaseShield[pair.AbilityLevelLessOne] + (cardinalCallPowerScaling * pair.Source.Power)) * currentShieldEffect;
            };
            AlsoObsolete_Duration = (self) =>
            {
                return cardinalCallShieldDuration;
            };
        }
    }
}