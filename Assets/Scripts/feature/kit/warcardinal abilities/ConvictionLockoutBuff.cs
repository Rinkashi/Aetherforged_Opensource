﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Conviction", menuName = "Ability/Warcardinal/Conviction Lockout Buff")]
    public class ConvictionLockoutBuff : BuffDebuff
    {

        public ConvictionLockoutBuff()
        {
            Name = "conviction_immunity";
        }
    }
}