﻿using System.Collections.Generic;
using UnityEngine;

using controllers;
using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Fist of Ouros Debuff", menuName = "Ability/Warcardinal/Fist of Ouros Debuff")]
    public class FistOfOurosDebuff : BuffDebuff
    {

        #region fist of ouros variables
        //fist of ouros base variables
        public float[] fistofOurosCooldown = new float[] { 1, 1, 1 };
        public float fistofOurosRange = 375;
        public int fistofOurosWidth = 250;
        //fist of ouros damage variables
        public float[] fistofOurosBaseDamage = new float[] { 1, 1, 1 };
        public float fistofOurosPowerScaling = .5f;
        //fist of ouros cc variables
        public int fistofOurosKnockbackDistance = 150;
        public int fistofOurosKnockbackHeight = 100;
        public float fistofOurosKnockbackDuration = .5f;
        //fist of ouros debuff variables
        public BuffDebuff fistofOurosDebuff;
        public float fistofOurosStunDuration = 1.5f;
        #endregion

        public FistOfOurosDebuff()
        {
            Name = "fist_of_ouros_debuff";
            Obsolete_Duration = (self) =>
            {
                return fistofOurosKnockbackDuration;
            };
                //set a trigger to activate upon the unit colliding with terrain. Once it does so, stun for a set duration
        }
    }
}