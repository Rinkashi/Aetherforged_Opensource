﻿using System.Collections.Generic;
using UnityEngine;

using controllers;
using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Empowered Sinbrand Debuff", menuName = "Ability/Warcardinal/Empowered Sinbrand Debuff")]

    public class SinbrandEmpoweredDebuff : BuffDebuff
    {

        #region sinbrand variables
        //sinbrand base variables
        public float[] sinbrandCooldown = new float[] { 1, 1, 1, 1, 1 };
        public float sinbrandRange = 450;
        //sinbrand damage variables
        public float[] sinbrandBaseDamage = new float[] { 1, 1, 1, 1, 1 };
        public float sinbrandPowerScaling = .5f;
        //sinbrand empowered variables
        public bool sinbrandIsEmpowered = false;
        //sinbrand debuff variables
        public BuffDebuff sinbrandDebuff;
        public float sinbrandDebuffDuration = 2;
        public float[] sinbrandPercentDamageIncrease = new float[] { .04f, .05f, .06f, .07f, .08f };
        //sinbrand empowered debuff
        public BuffDebuff sinbrandEmpoweredDebuff;
        public float sinbrandEmpoweredDebuffDuration = 2;
        //sinbrand buff variables
        public float sinbrandEmpoweredBoost = 30;
        public float sinbrandMoveBoostDuration = .5f;
        #endregion

        public SinbrandEmpoweredDebuff()
        {
            Name = "zealot_debuff";
            Obsolete_OnTakeDamagePostMit = (Passive self, Unit attackSource, Unit debuffedUnit, DamagePacket packet) =>
            {
                //Add move speed buff to ally
                //attackSource.AddBuff();
                return packet;
            };
            Obsolete_Duration = (Passive self) =>
            {
                return sinbrandEmpoweredDebuffDuration;
            };
        }
    }
}