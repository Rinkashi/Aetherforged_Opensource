﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using feature.mainmenu.models;
using core.http;
using System;
using System.Net;
using SimpleJSON;
using System.Linq;
using UnityEngine.UI;

public class FriendslistScript : MonoBehaviour {
    public bool isDirty = false;
    bool isFetching = false;

    public FriendsListModel FriendsList;

    public GameObject FriendPanelPrefab;
    public Transform FriendsPanelRootTransform;

    public Uri GetFriendsListEndpoint { get { return new Uri(NetworkStore.BaseUrl + "/api/Friend/GetFriendsList"); } }

    void Start () {
		
	}
	void Update () {
		if(isDirty || FriendsList == null) {
            if(NetworkStore.SessionIdentifer.Length > 1 && !isFetching) {
                isDirty = false;
                isFetching = true;
                var httppost = new HttpPost() {
                    Endpoint = GetFriendsListEndpoint,
                    JsonModel = new SessionModel() {
                        StringIdentifier = NetworkStore.SessionIdentifer
                    }
                };

                StartCoroutine(httppost.CoSend((response) => {
                    if (response.Status == HttpStatusCode.OK) {
                        var root = JSON.Parse(response.Data);
                        List<Friend> friends = new List<Friend>();
                        foreach (var friend in root["Friends"].Children)
                            friends.Add(new Friend() {
                                Username = friend["Username"].Value,
                                Status = friend["Status"].Value,
                            });
                        FriendsList = new FriendsListModel() {
                            Friends = friends.ToArray(),
                        };
                        BuildFriendsList();
                    } else {
                        isDirty = true;
                    }
                    isFetching = false;
                }));
            }
        }
	}

    void BuildFriendsList() {
        foreach(Transform t in FriendsPanelRootTransform) {
            DestroyImmediate(t.gameObject);
        }
        var index = 0;
        foreach(Friend friend in FriendsList.Friends) {
            index++;
            var fPanel = Instantiate(FriendPanelPrefab);
            fPanel.transform.SetParent(FriendsPanelRootTransform);
            var fRect = fPanel.GetComponent<RectTransform>();
            fRect.localScale = new Vector3(1, 1, 1);
            fRect.localPosition = new Vector3(0, 0, 0);
            fRect.offsetMin = new Vector2(4, 0);
            fRect.offsetMax = new Vector2(-4, -100 + (index - 1) * -200);
            fRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 100);

            fPanel.transform.GetChild(0).GetComponent<Text>().text = friend.Username;
            fPanel.transform.GetChild(1).GetComponent<Text>().text = friend.Status;
        }
        FriendsPanelRootTransform.gameObject.GetComponent<RectTransform>().offsetMin = new Vector2(0, -Mathf.Max(0, -868 + index * 100));
    }
}
