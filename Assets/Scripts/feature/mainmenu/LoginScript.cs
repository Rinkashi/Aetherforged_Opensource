﻿using core.http;
using feature.mainmenu.models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginScript : MonoBehaviour {

    public Uri LoginEndpoint { get { return new Uri(NetworkStore.BaseUrl + "/api/User/Login"); } }

    public GameObject UsernameInputField;
    public GameObject PasswordInputField;
    public GameObject LoginMenu;
    public GameObject FriendPanelForSelf;

    public GameObject ErrorPanel;
    private MainMenuErrorScript ErrorLogging {
        get {
            return ErrorPanel.GetComponent<MainMenuErrorScript>();
        }
    }

    public void Login() {
        var username = UsernameInputField.GetComponent<InputField>().text;
        var password = PasswordInputField.GetComponent<InputField>().text;

        var data = "{\"username\": \"" + username + "\", \"password\":\"" + password + "\"}";

        var httppost = new HttpPost() {
            Endpoint = LoginEndpoint,
            JsonModel = new UserModel() {
                Username = username.Length == 0 ? "testuser" : username,
                Password = password.Length == 0 ? "password" : password
            }
        };
        StartCoroutine(httppost.CoSend((response) => {
            if (response.Status == HttpStatusCode.OK) {
                NetworkStore.SessionIdentifer = response.Data.Replace("\"","");
                var loginUsername = ((UserModel)httppost.JsonModel).Username;
                NetworkStore.Username = loginUsername;
                FriendPanelForSelf.transform.GetChild(0).GetComponent<Text>().text = loginUsername;
            } else {
                ErrorLogging.Throw(response);
            }
        }));
    }
}
