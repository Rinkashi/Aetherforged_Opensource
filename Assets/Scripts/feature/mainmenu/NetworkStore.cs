﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class NetworkStore : MonoBehaviour {
    public static string BaseUrl = "http://o7studios.com:8085";    

    public static string SessionIdentifer = "";

    public GameObject LoginMenu;

    public GameObject LobbyMenu;

    public static string Username { get; internal set; }

    void Update() {
        if (SessionIdentifer.Length > 0) {
            LoginMenu.SetActive(false);
            LobbyMenu.SetActive(true);
        } else {
            LoginMenu.SetActive(true);
            LobbyMenu.SetActive(false);
        }
    }
}
