﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocialLinkButtonScript : MonoBehaviour {

	public void GoToAddress(string address) {
        Application.OpenURL(address);
    }
}
