﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace feature.mainmenu.models {
    public class GameServerModel {
        public int Id;
        public int LobbyId;
        public int PId;
        public int Port;

        public string SecretKey;
        public string IpAddress;
    }
}
