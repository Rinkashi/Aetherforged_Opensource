﻿using core;
using managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ParticleHelper {
    public Unit Unit;

    public ParticleIdentifier Initialize(string particlePrefabPath, string[] attachPoints) {
        return new ParticleIdentifier(particlePrefabPath, attachPoints, Unit);
    }
    public ParticleIdentifier Initialize(string particlePrefabPath, string attachPoint) {
        return new ParticleIdentifier(particlePrefabPath, new string[] { attachPoint }, Unit);
    }
}

public class ParticleIdentifier {
    string particlePrefabPath;
    string[] attachPoints;
    GameObject ParticleBase;
    List<GameObject> WorldParticles = new List<GameObject>();
    Transform Root;
    Unit unit;

    public ParticleIdentifier(string particlePrefabPath, string[] attachPoints, Unit unit) {
        this.particlePrefabPath = particlePrefabPath;
        this.ParticleBase = Resources.Load<GameObject>("Particles/" + particlePrefabPath);

        this.attachPoints = attachPoints;

        this.Root = unit.transform;
        this.unit = unit;

        if (attachPoints.Contains("static_ground")) {
            this.Root = Instantiate((GameObject)Resources.Load("EmptyPrefab")).transform;
        }
    }

    private bool isActivated = false;

    private GameManager _lazyLoadedGM;
    GameManager GM {
        get {
            if(_lazyLoadedGM == null) {
                _lazyLoadedGM = GameObject.Find("/GM").GetComponent<GameManager>();
            }
            return _lazyLoadedGM;
        }
    }

    public void Activate() {
        if(isActivated) {
            return;
        }

        if(attachPoints.Contains("static_ground")) {
            this.Root.position = unit.transform.position;
        }

        for (int i = 0; i < attachPoints.Count(); i++) {
            var particle = Instantiate(ParticleBase);
            WorldParticles.Add(particle);
            particle.transform.SetParent(GetAttachPoint(Root, attachPoints[i]));
            particle.transform.localPosition = Vector3.zero;
            particle.transform.localRotation = Quaternion.Euler(Vector3.zero);
            particle.transform.localScale = Vector3.one;
        }
        isActivated = true;
    }

    private void _deactivateParticleSystem(GameObject go)
    {
        if (go.GetComponent<ParticleSystem>() != null)
        {
            go.GetComponent<ParticleSystem>().Stop(false, ParticleSystemStopBehavior.StopEmitting);
        }
        if (go.GetComponent<TrailRenderer>() != null)
        {
            go.GetComponent<TrailRenderer>().time = -1;
        }
        for (int i = 0; i < go.transform.childCount; i++)
        {
            _deactivateParticleSystem(go.transform.GetChild(i).gameObject);
        }
        UnityEngine.Object.Destroy(go, 8);
    }

    // FIXME this has been reverted and may need to be fixed to work with code built on its use.
    public void Deactivate() {
        if(!isActivated) 
        {
            return;
        }

        foreach(var gob in WorldParticles) 
        {
            _deactivateParticleSystem(gob);
            UnityEngine.Object.Destroy(gob, 8);
        }

        WorldParticles = new List<GameObject>();

        isActivated = false;
    }

    private Transform GetAttachPoint(Transform root, string name) {
        if(name == "static_ground") {
            return root;
        }
        for(int i = 0; i < root.childCount; i++) {
            var child = root.GetChild(i);
            if(child.name == name) {
                return child;
            } else {
                var searchResults = GetAttachPoint(child, name);
                if(searchResults != null) {
                    return searchResults;
                }
            }
        }
        return null;
    }

    private GameObject Instantiate(GameObject obj) {
        return UnityEngine.Object.Instantiate(obj);
    }
}