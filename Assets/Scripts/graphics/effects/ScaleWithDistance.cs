﻿using UnityEngine;
using System.Collections;

public class ScaleWithDistance : MonoBehaviour {

	public ParticleSystem particle_system;
	private float distance;
	public Transform start_point;
	public Transform end_point;
	public float SpeedScaling;
	public bool EnableSpeedScale;
	public float ScaleScaling;
	public bool EnableScaleScale;
	public float GravityScaling;
	public bool EnableGravityScale;

	// Use this for initialization
	void Start () {
		
		distance = Vector3.Distance(start_point.position, end_point.position);

	}

	[ContextMenu("Reset Scales To Zero")]
	void ResetScales () {

		SpeedScaling = 0;
		ScaleScaling = 0;
		GravityScaling = 0;
		
	}

	// Update is called once per frame
	void Update () {

		distance = Vector3.Distance(start_point.position, end_point.position);

		// gets a reference to this particle system's main module.
		ParticleSystem.MainModule ourMain = particle_system.main;   

		if (EnableSpeedScale == true) {
			ourMain.startSpeed = SpeedScaling * distance;
		}

		if (EnableScaleScale == true) {
			ourMain.startSize = ScaleScaling * distance;
		}

		if (EnableGravityScale == true) {
			ourMain.gravityModifier = GravityScaling * distance;
		}

	}
}