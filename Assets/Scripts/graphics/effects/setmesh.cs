﻿using UnityEngine;
using System.Collections;

public class setmesh : MonoBehaviour {

	public SkinnedMeshRenderer modelskin; //mesh that the particle system uses to skin the particles to
	private ParticleSystem thisparticlesystem; //This particle system
	private ParticleSystem.ShapeModule thisshapemodule; //This shape module the particle system uses to change the mesh value to the parent model
	private Transform grandparent; //The grandparent for searching the second layer

	void Start () {
		
		thisparticlesystem = GetComponent<ParticleSystem>();
		thisshapemodule = thisparticlesystem.shape;

		//go trough all the children to find a model with "Body" in the name
		SkinnedMeshRenderer[] children = gameObject.transform.parent.GetComponentsInChildren<SkinnedMeshRenderer>();
		foreach(SkinnedMeshRenderer child in children){
			if (child.name.Contains ("Body")) {
				modelskin = child;
			}
		}

		//Second pass if the particle is parented to another particle.
		if(modelskin==null){
			grandparent = gameObject.transform.parent;
			grandparent = grandparent.transform.parent;
			SkinnedMeshRenderer[] children2 = grandparent.GetComponentsInChildren<SkinnedMeshRenderer>();
			foreach(SkinnedMeshRenderer child in children2){
				if (child.name.Contains ("Body")) {
					modelskin = child;
					}
				}
			}

		//set the mesh
		thisshapemodule.skinnedMeshRenderer = modelskin;
		}

	}