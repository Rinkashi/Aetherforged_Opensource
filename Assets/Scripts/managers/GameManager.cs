using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

using core;
using ui;
using core.utility;
using core.lane;
using core.forger;
using controllers;
using controllers.ai;
using feature.map;
using feature;

namespace managers {
    
    public class GameManager : Singleton<GameManager> {
        const float RESPAWN_DELAY = 5f;
        const float RECALL_DELAY = 7f;

        float nextTickTime = 0f;

        uint currentTick;
        public uint CurrentTick {
            get { return currentTick; }
        }

        public uint checkTick = 60;

        public bool DoNotSpawnMinons = false;
        public bool DoNotSpawnTower = false;
        public bool DidThirtySec = false;
        public bool DidForgesLight = false;
        public bool DidMinionSpawn = false;

        [SerializeField]
        Vector3[] towerPositions;
        [SerializeField]
        float nextMinionSpawnTime;
        [SerializeField]
        const float firstMinionSpawnTime = 0f; // 60f;
        [SerializeField]
        const float minionSpawnPeriod = 30f;

        [SerializeField]
        private bool gameOver;
        public bool GameOver {
            get { return gameOver; }
            protected set { gameOver = value; }
        }

        [SerializeField]
        private Team victory;
        public Team Victor {
            get { return victory; }
            set { victory = value; }
        }

        [SerializeField]
        float passiveMoney = 1.5f;

        [SerializeField]
        public string defaultKit;

		[SerializeField]
		int maxMiners = 18;

		[SerializeField]
		float moneyPerMiner = 0.025f;
        
		// FIXME once we have miners, we'll get Miner.CountOnTeam(team), I think. 
		List<MineCapture> allMines;

        [SerializeField]
        public bool opposingTeams;

        [SerializeField]
        public bool randomCharacter;

        [SerializeField]
        SpawnLocationLists allSpawns;

        bool spawnedTowers = false;

        [SerializeField]
        Team CurrentTeam = Team.UpRight;

        Team BalanceTeam {
            get {
                var upright = Unit.ListOf.Where(((Unit arg) => {
                    return arg.Team == Team.UpRight;
                })).Count();

                var downleft = Unit.ListOf.Where(((Unit arg) => {
                    return arg.Team == Team.DownLeft;
                })).Count();

                if (upright == downleft) {
                    
                    return ((UnityEngine.Random.value < 0.5f) ? Team.UpRight : Team.DownLeft);
                } else if (upright > downleft) {
                    return Team.DownLeft;
                }
                return Team.UpRight;
            }
        }


        #if UNITY_EDITOR
        // ============= EDITOR ONLY SECTION ==============

        // show/hide spawn options
        [SerializeField]
        public bool hideAetherSpawns;
        [SerializeField]
        public bool hideAerisSpawns;
        [SerializeField]
        public bool hideJungleSpawns;
        [SerializeField]
        public bool persistentGizmos;

        // Hidden
        GameObject centerAeris;
        GameObject centerAether;

        // Bins
        [HideInInspector]
        public Transform minionRoot;
        [HideInInspector]
        public Transform towerRoot;
        [HideInInspector]
        public Transform forgerRoot;
        [HideInInspector]
        public Transform corpseRoot;
        [HideInInspector]
        public Transform projectileRoot;

        [HideInInspector]
        public Transform recycleBin;

        // Display Mesh
        [HideInInspector]
        public Mesh towerMesh;
        [HideInInspector]
        public Mesh towerCircle;

        // ^^^^^^^^^^^^^ EDITOR ONLY SECTION ^^^^^^^^^^^^^^
        #endif


        Dictionary<Team,Dictionary<UnitKind,Dictionary<char,Vector3>>> spawns = new Dictionary<Team, Dictionary<UnitKind, Dictionary<char, Vector3>>>();

        // FIXME once Unit handles this, we won't need this. 
        public static List<Unit> Guardians = new List<Unit>();

        void Awake() {
            instance = this;

            foreach (SpawnLocation spiel in allSpawns.aeris) {
                AddSpawnPoint(spiel);
            }

            foreach (SpawnLocation spiel in allSpawns.aether) {
                AddSpawnPoint(spiel);
            }

            foreach (SpawnLocation spiel in allSpawns.jungle) {
                AddSpawnPoint(spiel);
            }

            CurrentTeam = BalanceTeam;

            #if UNITY_EDITOR
            var Dynamic = GameObject.Find("Dynamic").transform;

            if (minionRoot == null) {
                minionRoot = Dynamic.Find("Minions!").transform;
            }

            if (towerRoot == null) {
                towerRoot = Dynamic.Find("Towers").transform; 
            }

            if (forgerRoot == null) {
                forgerRoot = Dynamic.Find("Forgers").transform; 
            }

            if (projectileRoot == null) {
                projectileRoot = Dynamic.Find("Projectiles").transform;
            }

            if (corpseRoot == null) {
                corpseRoot = Dynamic.Find("Corpses");
            }

            if (recycleBin == null) {
                recycleBin = Dynamic.Find("RecycleBin");
            }
            #endif
        }

        void Start() {

            var mineObjects = FindObjectsOfType<MineCapture>();
            if (allMines == null)
            {
                allMines = new List<MineCapture>();
            }
            if(allMines != null){
                foreach (var minecob in mineObjects) {
                    allMines.Add(minecob);
                }
			}

            #if UNITY_EDITOR
            if (null == FindObjectOfType<ui.UIManager>()) {
                Debug.LogErrorFormat("GameManager UIManager not found. Attach EmptyUIscene or OnlyUI (if available).");
            }
            #endif


        }

        void Update() {
            if (NetworkServer.active && !GameOver) {
                if (!DoNotSpawnMinons && GameTime.time >= nextMinionSpawnTime) {
                    nextMinionSpawnTime = Mathf.RoundToInt(GameTime.time / minionSpawnPeriod + 1) * minionSpawnPeriod;

                    Debug.LogFormat("Spawning Minions");
                    SpawnMinionSquad(Team.UpRight, Lane.Top);
                    SpawnMinionSquad(Team.UpRight, Lane.Bottom);
                    SpawnMinionSquad(Team.DownLeft, Lane.Top);
                    SpawnMinionSquad(Team.DownLeft, Lane.Bottom);
                }

                if (!spawnedTowers && !DoNotSpawnTower) {
                    SpawnAllTowers();
                }

                if(!DidForgesLight && GameTime.time >= 0f)
                {
                    AnnouncerEvent currEvent = new AnnouncerEvent(AnnouncementType.ForgesLight, "The Forges Light");
                    AnnouncerController.Instance.QueueEvent(currEvent);
                    DidForgesLight = true;
                    // Start our game timer.
                    GameTime.time = 0;
                }

                if(DidForgesLight && !DidThirtySec && GameTime.time >= 30f)
                {
                    AnnouncerEvent currEvent = new AnnouncerEvent(AnnouncementType.ThirtySecs, "Thirty seconds until minions spawn");
                    AnnouncerController.Instance.QueueEvent(currEvent);
                    DidThirtySec = true;
                }

                if(DidThirtySec && !DidMinionSpawn && GameTime.time >= 60f)
                {
                    AnnouncerEvent currEvent = new AnnouncerEvent(AnnouncementType.MinionSpawn, "Minions have spawned");
                    AnnouncerController.Instance.QueueEvent(currEvent);
                    DidMinionSpawn = true;
                }

                //TODO as soon as guardian units are made, this becomes redundent
                if  (CurrentTick >= checkTick) {
                    for (int index = 0; index < Guardians.Count;) {
                        if (Guardians [index] == null || Guardians [index].IsDying) {
                            Guardians.RemoveAt(index);
                        } else {
                            ++index;
                        }
                    }

                    if (Guardians.Count == 1) {
                        victory = Guardians [0].Team;
                        gameOver = true;
                    } else if (Guardians.Count == 0) {
                        victory = Team.Jungle;
                        gameOver = true;
                    }
                }
            }
        }

        public void Reset () {
            spawnedTowers = false;
        }

        public void ExitApp()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE
            Application.Quit();
#endif
        }
        

        #if UNITY_EDITOR
        [ContextMenu("Mirror Aeris -> Aether")]
        void MirrorAerisToAether () {
            allSpawns.MirrorAerisToAether();
        }
        [ContextMenu("Mirror Aether -> Aeris")]
        void MirrorAetherToAeris () {
            allSpawns.MirrorAerisToAether();
        }
        [ContextMenu("Mirror Jungle -> Jungle (after Jungle -> Backup)")]
        void MirrorJungleToJungle () {
            allSpawns.BackupAndMirrorJungle();
        }
        [ContextMenu("Swap Jungle <-> Backup")]
        void SwapJungleAndBackup () {
            allSpawns.SwapBackupAndJungle();
        }
        [ContextMenu("Remove Duplicates Jungle")]
        void RemoveDuplicatesFromJungle () {
            allSpawns.RemoveDuplicatesFromJungle();
        }

        #endif
        [ContextMenu("Spawn Towers")]
        private void SpawnAllTowers () {

            SpawnTowers(Team.UpRight, Vector2.one);
            SpawnTowers(Team.UpRight, new Vector2(1, -1));
            SpawnTowers(Team.DownLeft, new Vector2(-1, 1));
            SpawnTowers(Team.DownLeft, -Vector2.one);

            SpawnGuardianAndBaseStructures(Team.UpRight);
            SpawnGuardianAndBaseStructures(Team.DownLeft);

            spawnedTowers = true;
        }

        private void AddSpawnPoint(SpawnLocation spiel) {
            SanitizeSpawnPoint(spiel);
            if (spawns.ContainsKey(spiel.team)) {
                if (spawns [spiel.team].ContainsKey(spiel.kind)) {
                    if (spawns[spiel.team][spiel.kind].ContainsKey(spiel.spec)) {
                        // if we have the exact same kind, team, and detail specifier, no go.
                        Debug.LogErrorFormat( "Spawn Location collision: {0}", spiel);
                    } else {
                        // if we already defined the kind and team, add the detailed spawn point.
                        spawns [spiel.team] [spiel.kind].Add(spiel.spec, spiel.unity_spot);
                    }
                } else {
                    // if we have that team, but it's the first of its kind, add spawn point.
                    var createkind = new Dictionary<char, Vector3>();
                    createkind.Add(spiel.spec, spiel.unity_spot);
                    spawns [spiel.team].Add(spiel.kind, createkind);
                }
            } else {
                // if this is the first spawn point on a team, make all the necessary bits,
                // and then add the spawn point.
                var createkind = new Dictionary<char, Vector3>();
                var createteam = new Dictionary<UnitKind,Dictionary<char,Vector3>>();
                createkind.Add(spiel.spec, spiel.unity_spot);
                createteam.Add(spiel.kind, createkind);
                spawns.Add(spiel.team, createteam);
            }

        }



        void SanitizeSpawnPoint(SpawnLocation spiel) {
            if (spiel.spec < ' ') {
                spiel.spec = ' ';
            }
            if (spiel.spec > '~') {
                spiel.spec = '~';
            }
        }

        public void SetRespawn(Unit caller, float respawnDelay = RESPAWN_DELAY ) {
            var deadTime = GameTime.time;

            new Timer().Start(() => {
                RespawnUnit( caller );
                return 0f;
            }, respawnDelay);

        }

        public void SetRecall(Forger caller, float recallDelay = RECALL_DELAY ) {
            var deadTime = GameTime.time;

            var recallBuff = new BuffDebuff() {
                Name = "Recall",
                OnEnterCombat = (SourcePair pair, Unit owner, PassiveTracker data) => {
                    owner.InterruptChannel();
                },
                OnBasicAttackStart = (SourcePair pair, Unit owner, Unit target) => {
                    owner.InterruptChannel();
                },
                AfterTakeDamage = (SourcePair pair, Unit owner, PassiveTracker data, Unit damageSource) => {
                    owner.InterruptChannel();
                },
                OnMove = (SourcePair pair, Unit owner, PassiveTracker data, float deltaTime) => {
                    owner.InterruptChannel();
                },
            };

            if (!caller.IsChanneling) {
                caller.AddBuff(recallBuff.WithSource(caller));
                caller.RecallDressings();

                var recallPair = recallBuff.WithSource(caller);

                var timerChan = new Timer(recallBuff, (Ability ability, Unit caster) => {
                    caster.RemoveBuff(recallPair);
                }).Start(() => {
                    if (caller.HasBuff(recallPair)) {
                        caller.ActivateRecallDressings();
                        caller.GetComponent<NavMeshAgent>().enabled = false;
                        caller.transform.position = GetMySpawn(caller);
                        caller.RemoveBuff(recallPair);
                    }
                    caller.InterruptChannel();
                    return 0f;
                }, recallDelay);

                caller.CurrentChannel = timerChan;
            }
        }

        public GameObject SpawnPlayer (NetworkConnection conn) {

            var player = MakePlayerKnob();

            if (opposingTeams) {
                CurrentTeam = BalanceTeam;
            }

            player.MyTeam = CurrentTeam;

            if (UnitManager.Ready)
            {
                
                if (randomCharacter)
                {
                    var randomImprint = UnitManager.Instance.GetRandomImprintName();
                    SpawnFromImprint(player, randomImprint);
                }
                else
                {
                    SpawnFromImprint(player, defaultKit);
                }

            }


            NetworkServer.SpawnWithClientAuthority(player.gameObject, conn);

            return player.gameObject;

        }

        PlayerKnob MakePlayerKnob () {
            GameObject playerGenericGameObject = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("PlayerKnob"), Vector3.zero, Quaternion.Euler(0, 0, 0));

            var player = playerGenericGameObject.GetComponent<PlayerKnob>();


            return player;
        }

        public void SpawnFromImprint(PlayerKnob player, string imprintName ) {

            Forger forger;

            if (UnitManager.Instance.TrySpawnForger(imprintName, player.MyTeam, Vector3.zero, out forger)) {

                forger.SetPlayer(player);

                forger.transform.position = GetMySpawn(forger);

                //                GameManager.Instance.SetRespawn(forger, 0f);
                NetworkServer.Spawn(forger.gameObject);



                player.ControlForger = forger;
//
//                // assign the parent's ID (SyncVar) to it before we spawn it on clients. 
//                fig.GetComponent<Kit>().parentNetID = forger.GetComponent<NetworkIdentity>().netId;
//
//                // Spawn our model on Clients
//                NetworkServer.Spawn(fig);
            }


        }

        private void RespawnUnit(Unit unit) {
            unit.Spawn();
            unit.transform.position = GetMySpawn(unit);
            unit.gameObject.SetActive(true);
            unit.RpcActivate();

        }

        private void RecallUnit(Unit unit) {
            unit.transform.position = GetMySpawn(unit);

        }       

        public Vector3 GetMySpawn (Unit unit) {
            // Find the appropriate spawn location from the Team and Kind of unit. 
            if (spawns.ContainsKey(unit.Team)) {
                if (spawns [unit.Team].ContainsKey(unit.Kind)) {
                    if (spawns [unit.Team] [unit.Kind].ContainsKey(unit.Spec)) {
                        return spawns [unit.Team] [unit.Kind] [unit.Spec];
                    } else {
                        return spawns [unit.Team] [unit.Kind] [' '];
                    }
                }
            }
            return unit.transform.position; // if we have no spawn point, spawn in place. 
        }

        private void RespawnForger(Forger forger) {
            forger.gameObject.SetActive(true);
        }

        private void SpawnGuardianAndBaseStructures( Team team ) {
            Vector3 guardianLocation = new Vector3(70, 0, 0);
            Vector3 fountainLocation = new Vector3(88, 0, 0);
            if (team == Team.DownLeft) {
                guardianLocation *= -1;
                fountainLocation *= -1;
            }

            GameObject unitTower = Instantiate<GameObject>((GameObject)Resources.Load("Towers/" + "Tower"));


            var ai = unitTower.AddComponent<TowerAI>();

            var tower = unitTower.GetComponent<Tower>();

            tower.transform.localScale = Vector3.one * 2f;
            tower.transform.position = guardianLocation;

            #if UNITY_EDITOR
            tower.transform.SetParent(towerRoot);
            #endif

            tower.name = "Guardian Placeholder";
            tower.SetTier(5);
            tower.Team = team;
            tower.Spawn();


            Guardians.Add(tower);

            var fountain = Instantiate(Resources.Load("Fountain") as GameObject).GetComponent<Fountain>();
            fountain.transform.position = fountainLocation;
            fountain.Team = team;

            NetworkServer.Spawn(tower.gameObject);
            tower.Spawn();    
        }

        private void SpawnTowers(Team myTeam, Vector2 laneMultiplier) {

            for (int oneless = 0; oneless < towerPositions.Length; oneless++) {
                var tVectorPosition = towerPositions [oneless];

                var tower = UnitManager.Instance.SpawnTower(myTeam, 
                    new Vector3(tVectorPosition.x * laneMultiplier.x, tVectorPosition.y, tVectorPosition.z * laneMultiplier.y));
                
                tower.SetTier(3 - oneless);

                #if UNITY_EDITOR
                tower.transform.SetParent(towerRoot);
                #endif

                tower.Spawn();
                NetworkServer.Spawn(tower.gameObject);

                tower.Spawn();
            }
        }

        private void SpawnMinionSquad (Team myteam, Lane spawnLane) {
            // for now we're only spawning 3 melee, 3 ranged. Without variation.

            float TIME_BETWEEN_MINIONS_IN_SQUAD = 0.3f;

            GameObject friendlyMinionLane =
                GameObject.Find(myteam.ToString() + spawnLane.ToString());

            GameObject enemyMinionLane =
                GameObject.Find(TeamUtils.Opposite( myteam ).ToString() + spawnLane.ToString());

            Transform myLine = friendlyMinionLane.transform.Find("LaneLine");
            // Whose line is it anyways?
            Transform oppLine = enemyMinionLane.transform.Find("LaneLine");


            List<Transform> laneLine = new List<Transform>();
            foreach (Transform zoop in myLine) {
                laneLine.Add(zoop);
            }

            {
                List<Transform> temp = new List<Transform>();
                foreach (Transform zipa in oppLine) {
                    temp.Add(zipa);
                }
                temp.Reverse();
                laneLine.AddRange(temp);
            }

            Minion minion;
            for (int ixplx = 0; ixplx < 3; ixplx++) {

                StartCoroutine(DoDelay( () => {
                    {   // melee
                        minion = UnitManager.Instance.SpawnMinion(myteam, spawnLane, 'M');

                        // minion individual kit object
//                        Kit miko = minion.GetComponentInChildren<Kit>();

                        var minionAI = minion.gameObject.AddComponent<controllers.ai.MinionAI>();
                        minionAI.ClearBrain();

                        // laneLine needs to be rethought, probably. 
                        minionAI.laneLine.Clear();

                        foreach (Transform waypoint in laneLine) {
                            if (waypoint.gameObject.activeInHierarchy) {
                                minionAI.laneLine.Add(waypoint.position);
                            }
                        }

                        minion.transform.position = myLine.Find("Waypoint" + 1).position;


                        NetworkServer.Spawn(minion.gameObject);
//                        miko.GetComponent<Kit>().parentNetID = minion.netId;
//
//                        NetworkServer.Spawn(miko.gameObject);
                        minion.Spawn();

                        return 0;
                    }
                }, (ixplx + 1) * TIME_BETWEEN_MINIONS_IN_SQUAD
                 
                ));
                StartCoroutine(DoDelay( () => {
                    {   // ranged
                        minion = UnitManager.Instance.SpawnMinion(myteam, spawnLane, 'R');

                        // minion individual kit object
//                        Kit miko = minion.GetComponentInChildren<Kit>();

                        var minionAI = minion.gameObject.AddComponent<MinionAI>();
                        if (minionAI != null) 
                        {
                            minionAI.ClearBrain();

                            // laneLine needs to be rethought, probably. 
                            minionAI.laneLine.Clear();

                            foreach (Transform waypoint in laneLine) {
                                if (waypoint.gameObject.activeInHierarchy) {
                                    /*
                                    minionAI.AddWaypoint(waypoint.position);
                                    */
                                    minionAI.laneLine.Add(waypoint.position);
                                }
                            }

                        }
                        minion.transform.position = myLine.Find("Waypoint" + 1).position;

                        NetworkServer.Spawn(minion.gameObject);
//                        miko.GetComponent<Kit>().parentNetID = minion.netId;
//
//                        NetworkServer.Spawn(miko.gameObject);
                        minion.Spawn();
                        return 0;
                    }
                }, (4+ixplx) * TIME_BETWEEN_MINIONS_IN_SQUAD

                ));
            }

        }


        public IEnumerator DoDelay(Func<float> onTrigger, float delay = 0f) {
            if (delay == 0) {
                yield break;
            }
            yield return new WaitForSeconds(delay);
            delay = onTrigger();
            while (delay > 0) {
                yield return new WaitForSeconds(delay);
                delay = onTrigger();
            }
        }

        void FixedUpdate() {
            // This is awful, FAIL FAST,
            if (NetworkServer.active && nextTickTime < GameTime.time) {
                // do this tick!
                nextTickTime += 1f; // increment by one second.
                currentTick++;

                // For each player give passive salt
                {
                    float passiveMoney = GetMoneyGainForTeam(Team.DownLeft);
                    foreach (var unit in UnitManager.GetForgersOnTeam(Team.DownLeft)) {
                        unit.GetComponent<Forger>().QuietSaltGain(passiveMoney); 
                    }
                }
                {
                    float passiveMoney = GetMoneyGainForTeam(Team.UpRight);
                    foreach (var unit in UnitManager.GetForgersOnTeam(Team.UpRight)) {
                        unit.GetComponent<Forger>().QuietSaltGain(passiveMoney); 
                    }
                }


            if (currentTick % 60000 == 0) {
            }
            }
        }

        public float GetMoneyGainForTeam(Team team) {
			
            var ourMines = allMines.Where(mine => {
                return mine.Current == team;
            });

			// TODO trade maxMiners for number of miners, when we have miners on the mines.
			return passiveMoney + ourMines.Count() * maxMiners * moneyPerMiner;
        }

        public void SetBonusMoneyGainForTeam(float bonusMoneyGain, Team team) {
            // TODO 2017 May 4 We probably gonna do a thing with the miners here, yeah?
            if (team == Team.DownLeft) {
            } else if (team == Team.UpRight) {
            } else {
                throw new ArgumentException("Invalid team specified.");
            }
        }

        public void SetGameOver(bool yesorno, Team team) {
            victory = team;

            gameOver = true;

        }

        #if UNITY_EDITOR
        void OnDrawGizmos () {
            
            if (persistentGizmos) {


                //            foreach (SpawnLocation spiel in spawnList) {
                //                Gizmos.color = spiel.color;
                //                Gizmos.DrawSphere(spiel.unity_spot, 2f);
                //            }

                if (!hideAerisSpawns) {
                    var total = Vector3.zero;
                    foreach (SpawnLocation spiel in allSpawns.aeris) {
                        SanitizeSpawnPoint(spiel);
                        Gizmos.color = spiel.color;
                        Gizmos.DrawSphere(spiel.unity_spot, 2f);
                        total += spiel.unity_spot;
                    }
                    if (centerAeris == null) {
                        centerAeris = GameObject.Find("Aeris Center");
                    }
                    if (centerAeris == null) {
                        centerAeris = new GameObject("Aeris Center");
                    } else if(allSpawns.aeris.Count > 0) {
                        centerAeris.transform.position = total / allSpawns.aeris.Count;
                        centerAeris.transform.SetParent(GameObject.Find("Static ").transform);
                    }
                } else {
                    if (centerAeris != null) {
                        DestroyImmediate(centerAeris);
                    }
                }

                if (!hideAetherSpawns) {
                    var total = Vector3.zero;
                    foreach (SpawnLocation spiel in allSpawns.aether) {
                        SanitizeSpawnPoint(spiel);
                        Gizmos.color = spiel.color;
                        Gizmos.DrawSphere(spiel.unity_spot, 2f);
                        total += spiel.unity_spot;
                    }
                    if (centerAether == null) {
                        centerAether = GameObject.Find("Aether Center");
                    }
                    if (centerAether == null) {
                        centerAether = new GameObject("Aether Center");
                    } else if(allSpawns.aether.Count > 0) {
                        centerAether.transform.position = total / allSpawns.aether.Count;
                        centerAether.transform.SetParent(GameObject.Find("Static ").transform);
                    }
                } else {
                    if (centerAether != null) {
                        DestroyImmediate(centerAether);
                    }
                }

                if (!hideJungleSpawns) {
                    foreach (SpawnLocation spiel in allSpawns.jungle) {
                        SanitizeSpawnPoint(spiel);
                        Gizmos.color = spiel.color;
                        Gizmos.DrawSphere(spiel.unity_spot, 2f);
                    }
                }

                foreach (Vector2 victor in new Vector2[] {
                    Vector2.one, new Vector2(1, -1), new Vector2(-1, 1), -Vector2.one
                }) {
                    foreach (Vector3 victoria in towerPositions) {
                        Gizmos.color = Color.grey;

                        Gizmos.DrawWireMesh(towerMesh, 
                            new Vector3(victoria.x * victor.x, victoria.y, victoria.z * victor.y), 
                            Quaternion.identity);

                        Gizmos.DrawWireMesh(towerCircle, 
                            new Vector3(victoria.x * victor.x, victoria.y, victoria.z * victor.y), 
                            Quaternion.identity);
                    }
                }
            } else {
                if (centerAeris != null) {
                    DestroyImmediate(centerAeris);
                }
                if (centerAether != null) {
                    DestroyImmediate(centerAether);
                }
            }
        }

        void OnDrawGizmosSelected () {
            // Display the radius when selected

            if (!persistentGizmos) {

//            foreach (SpawnLocation spiel in spawnList) {
//                Gizmos.color = spiel.color;
//                Gizmos.DrawSphere(spiel.unity_spot, 2f);
//            }

                if (!hideAerisSpawns) {
                    foreach (SpawnLocation spiel in allSpawns.aeris) {
                        Gizmos.color = spiel.color;
                        Gizmos.DrawSphere(spiel.unity_spot, 2f);
                    }
                }

                if (!hideAetherSpawns) {
                    foreach (SpawnLocation spiel in allSpawns.aether) {
                        Gizmos.color = spiel.color;
                        Gizmos.DrawSphere(spiel.unity_spot, 2f);
                    }
                }

                if (!hideJungleSpawns) {
                    foreach (SpawnLocation spiel in allSpawns.jungle) {
                        SanitizeSpawnPoint(spiel);
                        Gizmos.color = spiel.color;
                        Gizmos.DrawSphere(spiel.unity_spot, 2f);
                    }
                }

                foreach (Vector2 victor in new Vector2[] {
                    Vector2.one, new Vector2(1, -1), new Vector2(-1, 1), -Vector2.one
                }) {
                    foreach (Vector3 victoria in towerPositions) {
                        Gizmos.color = Color.grey;

                        Gizmos.DrawWireMesh(towerMesh, 
                            new Vector3(victoria.x * victor.x, victoria.y, victoria.z * victor.y), 
                            Quaternion.identity);

                        Gizmos.DrawWireMesh(towerCircle, 
                            new Vector3(victoria.x * victor.x, victoria.y, victoria.z * victor.y), 
                            Quaternion.identity);
                    }
                }
            }
        }
        #endif
    } // GameManager

        

    [Serializable]
    public class SpawnLocationLists {
        public List<SpawnLocation> aeris;
        public List<SpawnLocation> aether;
        public List<SpawnLocation> jungle;
        public List<SpawnLocation> backup;

        #if UNITY_EDITOR
        public void MirrorAerisToAether () {
            MirrorForge(aeris, aether);
        }

        public void MirrorAetherToAeris () {
            MirrorForge(aether, aeris);
        }

        public void BackupAndMirrorJungle () {
            backup.Clear();
            backup.AddRange(jungle);
            MirrorJungle(jungle, jungle);

            jungle = RemoveDuplicates(jungle);
        }

        public void SwapBackupAndJungle () {
            List<SpawnLocation> copy = new List<SpawnLocation>();
            copy.AddRange(jungle);
            jungle.Clear();
            jungle.AddRange(backup);
            backup.Clear();
            backup.AddRange(copy);
        }

        public void RemoveDuplicatesFromJungle () {
            backup.Clear();
            backup.AddRange(jungle);

            jungle = RemoveDuplicates(jungle);
        }

        public static void MirrorForge(List<SpawnLocation> source, List<SpawnLocation> target) {
            target.Clear();
            foreach (var spiel in source) {
                SpawnLocation schtick = new SpawnLocation();
                var flipPosition = spiel.spot;
                flipPosition.x *= -1;
                schtick.spot = flipPosition;

                var invertColor = spiel.color;
                invertColor.b = 1f - invertColor.b;
                invertColor.g = spiel.color.r;
                invertColor.r = spiel.color.g;
                schtick.color = invertColor;

                var opposingTeam = spiel.team.Opposite();
                schtick.team = opposingTeam;

                schtick.spec = spiel.spec;
                schtick.kind = spiel.kind;

                target.Add(schtick);
            }
        }

        public static void MirrorJungle(List<SpawnLocation> source, List<SpawnLocation> target) {
            List<SpawnLocation> mirror = new List<SpawnLocation>();

            foreach (var spiel in source) {
                SpawnLocation schtick = new SpawnLocation();
                var flipPosition = spiel.spot;
                flipPosition.x *= -1;
                flipPosition.y *= -1;
                schtick.spot = flipPosition;

                schtick.color = spiel.color;

                schtick.team = spiel.team;

                schtick.spec = ModInvert(spiel.spec);
                schtick.kind = spiel.kind;

                mirror.Add(schtick);
            }

            target.AddRange(mirror);

        }

        public static char ModInvert(char incoming) {
            // ABCDEFGHIJKLMNOPQRSTUVWXYZ
            // ZYXWVUTSRQPONMLKJIHGFEDCBA
            switch (incoming) {
            case 'P':
                // Power
                return 'W';
            case 'W':
                // Power
                return 'P';
            case 'D':
                // Defense
                return 'F';
            case 'F':
                // Defence
                return 'D';
            case 'L':
                // Leech
                return 'L';
            case 'O':
                return 'K';
            case 'U':
                return 'U';
            case 'K':
                return 'O';
            default:
                return (char)('A' + 'Z' - incoming);

            }

        }

        public List<SpawnLocation> RemoveDuplicates( List<SpawnLocation> source, List<SpawnLocation> target = null) {
            Dictionary<char, SpawnLocation> result = new Dictionary<char, SpawnLocation>();

            foreach (var spiel in source) {

                if (result.ContainsKey(spiel.spec)) {
                    Debug.LogErrorFormat("COLLISION: {0}", spiel);
                } else {
                    result.Add(spiel.spec, spiel);
                }

            }
            if (target == null) {
                target = new List<SpawnLocation>();
            }
            target.AddRange(result.Values);

            return target;

        }

        #endif
    }

    [Serializable]
    public class SpawnLocationSide {
        
        public List<SpawnLocation> forge;

        public List<SpawnLocation> jungle;

        public void ApplyMirror (SpawnLocationSide other) {
            /*
            jungle.Clear();
            foreach (var spiel in other.jungle) {
                SpawnLocation schtick = new SpawnLocation();
                var flipPosition = spiel.unity_spot;
                flipPosition.x *= -1;
                flipPosition.z *= -1;
                schtick.unity_spot = flipPosition;
                schtick.spec = (char)('A' + 'Z' - spiel.spec);
                schtick.color = spiel.color;
                schtick.kind = spiel.kind;
                schtick.team = spiel.team;

                jungle.Add(schtick);
            }
            */
        }
    }

}