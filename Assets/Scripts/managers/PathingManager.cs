﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using core;

public class PathingManager : Singleton<PathingManager> {

    [SerializeField]
    bool persistentGizmos;

    public PathingFlowfield oneTeam;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public bool TryGetNearestNode(Team team, Vector2 position, out FieldNodeTuple node)
    {
        node = oneTeam.NearestNode(position);
        return (node != null);

    }

#if UNITY_EDITOR


    void OnDrawGizmos()
    {
        if (persistentGizmos)
        {
            ShowTheGizmos();
        }
    }

    void OnDrawGizmosSelected()
    {
        if (!persistentGizmos)
        {
            ShowTheGizmos();
        }
    }

    void ShowTheGizmos() {
        for (int index = 0; index < oneTeam.vectorList.Count; ++index)
        {
            var keyvalue = oneTeam.vectorList[index];
            float size = 5f;


            Vector3 position = new Vector3(keyvalue.position.x, 50, keyvalue.position.y) * Constants.TO_UNITY_UNITS;
            Vector3 direction = new Vector3(keyvalue.direction.x, 0, keyvalue.direction.y);



            if (direction != Vector3.zero)
            {
                direction.Normalize();
                direction *= size;
            }


            //Handles.RotationHandle(Quaternion.LookRotation(direction),position).;

            //Handles.ArrowHandleCap(index,position, Quaternion.LookRotation(direction),HandleUtility.GetHandleSize(position),EventType.mouseDrag);


            //keyvalue.position = new Vector2(position.x, position.z);

            //Gizmos.DrawCube(position, .1f*Vector3.one);
            Gizmos.DrawSphere(position + direction, .2f);
            Gizmos.DrawRay(position, direction);
        }

    }


#endif

}
