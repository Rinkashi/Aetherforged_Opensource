﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

using core;
using core.lane;
using core.forger;
using controllers.ai;
using controllers;

namespace managers {

    public class UnitManager : Singleton<UnitManager> {

        [SerializeField]
        private UnitAssemblage stable;

        public GameObject GetUnitModel(Unit unit) {
            return stable.GetPrefab(unit);
        }

        public string GetRandomImprintName() {
            var pickList = stable.GetImprintList();

            int index = UnityEngine.Random.Range(0, pickList.Count);

            return pickList[index];
        }

        public List<string> GetListOfImprints() {
            return stable.GetImprintList();

        }
        
        public bool TrySpawnForger(string kitName, Team team, Vector3 spawnPos, out Forger forger) {
            GameObject forgerGenericGameObject = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("UnitForger"), new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0));

//			GameObject forgerIndividualGameObject = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Forgers/" + kitName), new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0));


//            fig = forgerIndividualGameObject;

//            if (fig == null)
//            {
//                forger = null;
//
//                // if that kit wasn't found
//                return false;
//            }
//            else
            {
                // we managed to grab that kit object.

//                Debug.Assert(forgerIndividualGameObject != null);

                forger = forgerGenericGameObject.GetComponent<Forger>();
                forger.SetKit(kitName);

//                forgerIndividualGameObject.transform.parent = forgerGenericGameObject.transform;

                // FIXME once Frame types are a thing, we'll want to change this if it's a frame
//                forgerIndividualGameObject.transform.localPosition = Vector3.zero;

                forgerGenericGameObject.transform.position = spawnPos;
                forgerGenericGameObject.AddComponent<ForgerAI>();

                forger.Team = team;

                forger.Spawn();

                return true;
            }


            /*
            var player = forgerGenericGameObject.GetComponent<PlayerKnob>();

            return player;
            */
        }




        [SerializeField]
        Team homeTeam = Team.Neutral;
        public Team HomeTeam {
            get { return homeTeam; }
            set { 
                homeTeam = value;

                foreach (Unit unit in Unit.ListOf) {
                    if (unit.isActiveAndEnabled) {
                        unit.Colourize();
                    }
                }
            }

        }

        [SerializeField]
        Forger localForger = null;
        public Forger LocalForger {
            get { return localForger; }
            set { 
                localForger = value;
            }

        }

        [SerializeField]
        PlayerKnob playerKnob = null;
        public PlayerKnob LocalKnob {
            get { return playerKnob; }
            set { 
                playerKnob = value;
                if (AetherForgedNetworkManager.Ready && AetherForgedNetworkManager.Instance.DisplayName != "") {
                    playerKnob.PlayerName = AetherForgedNetworkManager.Instance.DisplayName;
                }
            }
        }

        [SerializeField]
        TeamColours teamColors;


        public Color AppropriateColour (Team team) {
            if (homeTeam == team) {
                return teamColors.allyTeamColor;
            } else if (team == Team.Neutral) {
                return teamColors.neutralTeamColor;
            } else {
                return teamColors.enemyTeamColor;
            }
        }

        public Color OwnForgerColour {
            get {
                return teamColors.selfForgerColor;
            }

        }


        [Obsolete]
        public void SpawnAllMinions() {
            SpawnMinions(core.Team.DownLeft, core.Lane.Top);
            SpawnMinions(core.Team.DownLeft, core.Lane.Bottom);
            SpawnMinions(core.Team.UpRight, core.Lane.Top);
            SpawnMinions(core.Team.UpRight, core.Lane.Bottom);
        }

        [Obsolete]
        Unit SpawnMinions(Team team, Lane spawnLane) {
            GameObject minionGameObject =
              UnityEngine.Object.Instantiate(Resources.Load(team.ToString() + "TeamMeleeMinion") as GameObject);
            var ai = minionGameObject.GetComponent<MinionAI>();

            GameObject friendlyMinionLane =
              GameObject.Find(team.ToString() + spawnLane.ToString());
            
            Transform laneLine = friendlyMinionLane.transform.Find("LaneLine");

            foreach (Transform waypoint in laneLine) {
                if (waypoint.gameObject.activeInHierarchy) {
                    ai.AddWaypoint(waypoint.position);
                }
            }

            minionGameObject.transform.position = laneLine.Find("Waypoint" + 1).position;
            /*
            minionGameObject.transform.position = friendlyMinionLane.transform.position;
            ai.AddWaypoint(friendlyMinionLane.transform.position);
            for (int i = 1; i <= friendlyMinionLane.transform.childCount; i++) {
                ai.AddWaypoint(friendlyMinionLane.transform.Find("Waypoint" + i).transform.position);
            }
            */

            var minion = minionGameObject.GetComponent<Unit>();
            minion.Team = team;

            return minion;
        }

        public Tower SpawnTower(Team team, Vector3 position) {
            
            GameObject baseTower = Instantiate<GameObject>((GameObject)Resources.Load("Towers/" + "Tower"));

            Debug.Assert(baseTower != null);
            var ai = baseTower.AddComponent<TowerAI>();

            var tower = baseTower.GetComponent<Tower>();

//            baseTower.transform.SetParent(baseTower.transform);

            baseTower.transform.position = position;

//            NetworkServer.Spawn(baseTower);

//            var frame = baseTower.GetComponent<AnimationController>();
//
//            // tiki == tower individual kit
//            var tiki = baseTower.GetComponent<Kit>();
//
//
//            baseTower.transform.localPosition = Vector3.up * 3f;
//            baseTower.transform.localRotation = Quaternion.Euler(0,0,0);
//
//            Debug.Assert(tower != null);

            tower.Team = team;

//            tower.enabled = (false);


//            tower.gameObject.SetActive(true);

            return tower;

        }

        public Minion SpawnMinion(Team team, Lane spawnLane, char weapon) {

            // Minion Game Object
            GameObject miniGob = null;
            miniGob = Instantiate<GameObject>((GameObject)Resources.Load("Minions/" + "UnitMinion"));


            var minion = miniGob.GetComponent<Minion>();
            minion.Spec = weapon;

            var minionSpawn = 
                GameObject.Find(team.ToString() + spawnLane.ToString());

            miniGob.transform.position = minionSpawn.transform.position;
            //          ai.AddWaypoint(minionSpawn.transform.position);
            //          for (int i = 1; i <= minionSpawn.transform.childCount; i++) {
            //              ai.AddWaypoint(minionSpawn.transform.FindChild("Waypoint" + i).transform.position);
            //          }


            Debug.Assert(minion != null);

            minion.Team = team;
            miniGob.SetActive(true);
            minion.Spawn();
            minion.aliveTime = GameTime.time;


            Debug.Assert(miniGob != null);
            var ai = miniGob.GetComponent<MinionAI>();
            if (ai != null)
            {
                ai.ClearBrain();
            }


            return minion;
            /*
			return MinionPoolManager.SpawnMinion(team, spawnLane);
            */         
		}
            
        public static IEnumerable<Unit> GetForgersOnTeam(Team team) {
            return Unit.ListOf.Where(unit => {
                return unit.Kind == UnitKind.Forger && unit.Team == team;
            });
        }
        public static IEnumerable<Unit> GetForgers() {
            return Unit.ListOf.Where(unit => {
                return unit.GetComponent<Forger>() != null; 
            });
            /*  
            return (IEnumerable<GameObject>)UnityEngine.Object.FindObjectsOfType<GameObject>().Where(unit => {
                return (unit as GameObject).layer == LayerMask.NameToLayer("Unit") && (unit as GameObject).GetComponent<Forger>() != null;
            });
            */
        }

    }

    [System.Serializable]
    public class TeamColours {
        public Color selfForgerColor;
        public Color allyTeamColor;
        public Color enemyTeamColor;
        public Color neutralTeamColor;
    }

    [System.Serializable]
    public class TeamColour {
        public string description;
        public Color color;
        public Sprite healthBar;
    }
}
