﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace net {
    class ServerAutostarter : MonoBehaviour {
        bool isLoadingScene = false;

        void Update() {
            if (File.Exists(".matchmaking")) {
                var lines = System.IO.File.ReadAllLines(".matchmaking");
                if (lines.Length > 0) {
                    if (lines[0] == "server") {
                        SceneManager.LoadScene("AetherForged");
                        isLoadingScene = true;
                    } else if (!isLoadingScene && lines[0] == "client") {
                        try {
                            File.Delete(".matchmaking");
                        } catch(Exception) { }
                    }
                }
            }
        }
    }
}
