﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;
using core.lane;
using core.forger;
using managers;

namespace tests {
    public class AbilityTest : MonoBehaviour {

        #if UNITY_EDITOR
        [ContextMenu("Test Current Stacks")]
        private void DEBUG_PASSIVE_TEST() {
            Tower minionA = UnitManager.Instance.SpawnTower(Team.Neutral, Vector3.one);

            Tower minionB = UnitManager.Instance.SpawnTower(Team.Neutral, Vector3.zero);

            minionA.gameObject.name = "Tower A";
            minionB.gameObject.name = "Tower B";

            BuffDebuff testPassive = new BuffDebuff() {
                Name = "ember",
                NoMaxStacks = true,
                OnSpawn = (pair, holder, data) => 
                {
                    data.stacks++;
                    Debug.LogFormat("I am named {0} and I have {1} stacks. The source is {2}."
                        , holder.gameObject.name, data.stacks, pair.Source.gameObject.name);
                }
            };

            minionA.AddBuff(testPassive.WithSource(minionA));
            minionB.AddBuff(testPassive.WithSource(minionB));

            minionA.Spawn();
            minionB.Spawn();
        }

        [ContextMenu("Test Passive Cooldowns")]
        private void DEBUG_Cooldowns_Test() {
            Tower minionA = UnitManager.Instance.SpawnTower(Team.Neutral, Vector3.one);

            Tower minionB = UnitManager.Instance.SpawnTower(Team.Neutral, Vector3.zero);

            minionA.gameObject.name = "Tower A";
            minionB.gameObject.name = "Tower B";

            BuffDebuff testPassive = new BuffDebuff() {
                Name = "test_cooldown",
                NoMaxStacks = true,
                OnApply = (SourcePair pair, Unit holder) => {
                    var data = holder.GetPassiveData(pair);
                    if (data.Cooldown == 0) {
                        data.Cooldown = 4f;
                    }

                    Debug.LogFormat("I am named {0} and I have {1} stacks. The source is {2}. The cooldown is {3}. "
                        , holder.gameObject.name, data.stacks, pair.Source.gameObject.name, data.Cooldown);
                },
                OnTick = (SourcePair pair, Unit holder, PassiveTracker data, float deltaTime) => {

                    holder.SetCooldown(pair, 5f);


                    Debug.LogFormat("I am named {0} and I have {1} stacks. The source is {2}. The cooldown is {3} also {4}. "
                        , holder.gameObject.name, data.stacks, pair.Source.gameObject.name, data.Cooldown, holder.CooldownOf(pair));
                }
            };

            minionA.AddBuff(testPassive.WithSource(minionA));
            minionB.AddBuff(testPassive.WithSource(minionA));

            StartCoroutine(GameManager.Instance.DoDelay(() =>
               {

                   minionA.AddBuff(testPassive.WithSource(minionA));
                   return 0f;
               }, 5f));

//            minionA.Spawn();
//
//            minionB.Spawn();
        }

        [ContextMenu("Test Brute-force Skill Leveling")]
        private void DEBUG_Brute_LevelUp_SkillUp ()
        {
            Forger forgerA;

            if(UnitManager.Instance.TrySpawnForger("visionary", Team.DownLeft, Vector3.zero, out forgerA))
            {
                StartCoroutine(GameManager.Instance.DoDelay(() =>
                {
                    
                    forgerA.SkillUp(3);
                    DumpForgerLog(forgerA, "tried to skill up ult");
                    forgerA.SkillUp(0);
                    DumpForgerLog(forgerA, "tried to skill up one");
                    forgerA.SkillUp(0);
                    DumpForgerLog(forgerA, "tried to skill up one");
                    forgerA.SkillUp(1);
                    DumpForgerLog(forgerA, "tried to skill up two");
                    forgerA.SkillUp(2);
                    DumpForgerLog(forgerA, "tried to skill up three");
                    var xpGain = forgerA.ToNextLevel;
                    forgerA.GainXP(xpGain);
                    DumpForgerLog(forgerA, string.Format("added {0} XP", xpGain));


                    return 3f;
                }, 1f));
            }



        }

        public void DumpForgerLog(Forger forger, string memo = "")
        {
            Debug.LogFormat("{5} \tLevel {4} A {0} B {1} C {2} U {3}"
                            , forger.LevelList[0]
                            , forger.LevelList[1]
                            , forger.LevelList[2]
                            , forger.LevelList[3]
                            , forger.Level
                            , memo);
        }
        // ^^^^^^^^^^^^^ DEBUG SECTION ^^^^^^^^^^^^^^^^^^^^^
        #endif  // UNITY_EDITOR
    }
}