﻿using UnityEditor;
using UnityEngine;

using UnityEngine.Networking;

using core;
using core.forger;
using controllers;
using managers;

public class ForgerSwapWindow : EditorWindow {
    string kitName;

    PlayerKnob player;

    Vector3 saveLocation = default(Vector3);

    [MenuItem ("Window/Forger Select Window")]
    static void Init () {
        // Get existing open window or if none, make a new one:
        ForgerSwapWindow window = (ForgerSwapWindow)EditorWindow.GetWindow (typeof (ForgerSwapWindow));
        window.Show();
    }

    void OnEnable () {
        this.titleContent.text = "Forger Swapper";
    }

    void OnGUI () {


        // ==== Grab the player =====
        GameObject sel = Selection.activeGameObject;

        player = UnitManager.Instance.LocalKnob;

        /*
        var forger = sel.GetComponent<Forger>();

        if (forger != null && forger.player != null) {
            player = forger.player;
        } else if (sel.GetComponent<PlayerKnob>() != null) {
            player = sel.GetComponent<PlayerKnob>();
        }
        */
        // ^^^^ End Grab the player ^^^^^

        if (player != null) {

            EditorGUILayout.LabelField(player.PlayerName);
            kitName = EditorGUILayout.TextField(kitName);
            var findImprintResource = (GameObject)Resources.Load("Forgers/" + kitName);
            bool available = (findImprintResource != null && findImprintResource.GetComponent<Imprint>() != null);
            string beans = (available ? "Spawn" : "Not found");

//            if (EditorGUILayout.ToggleLeft(beans, true)) {
//                Debug.LogFormat("One Thing");
//
//
//            } else {
//                Debug.LogFormat("Other Thing");
//            }
            GUI.enabled = available;
            if (GUILayout.Button(beans)) {
                if (player.ControlForger != null) {
                    saveLocation = player.ControlForger.transform.position;
                    // remove the current one 
                    NetworkServer.Destroy(player.ControlForger.gameObject);
                }
                // and spawn this imprint and forger
                GameManager.Instance.SpawnFromImprint(player, kitName);
                if (saveLocation != default(Vector3)) {
                    player.ControlForger.transform.position = saveLocation;
                }
            }
            GUI.enabled = true;
            if (player.ControlForger != null) {


                if (GUILayout.Button("Destroy Player Forger")) {
                    NetworkServer.Destroy(player.ControlForger.gameObject);
                }

            }
            
//            var editor = Editor.CreateEditor(forger.player);
//            editor.OnInspectorGUI();            
        } 

    }
}