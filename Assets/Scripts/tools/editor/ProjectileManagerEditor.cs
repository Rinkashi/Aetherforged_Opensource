﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using managers;

[CustomEditor(typeof(ProjectileManager))]
public class ProjectileManagerEditor : Editor {
    private ReorderableList list;

    private void OnEnable() {
        list = new ReorderableList(serializedObject, 
            serializedObject.FindProperty("register"), 
            true, true, true, true);

        list.drawHeaderCallback = (rect) => {
            EditorGUI.LabelField(rect, "Projectile Dressings");
        };

        list.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = list.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;

            float spaceWidth = 5f;
            float dressWidth = 140f;
            float nameWidth = rect.width - dressWidth - spaceWidth;
                
            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, nameWidth, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("Name"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(rect.x + nameWidth + spaceWidth, rect.y, dressWidth, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("DressingPrefab"), GUIContent.none);
        };
    }

	public override void OnInspectorGUI () {
        serializedObject.Update();

        EditorGUILayout.ObjectField(serializedObject.FindProperty("bareProjectile"));

        list.DoLayoutList();

        EditorGUILayout.ObjectField(serializedObject.FindProperty("defaultDressing"));
           

        serializedObject.ApplyModifiedProperties();
    }
}
