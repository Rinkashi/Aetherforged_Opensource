﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

using core;

[CustomPropertyDrawer(typeof(SpawnLocation))]
public class SpawnLocationDrawer : PropertyDrawer {

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        var spacer = position.height / 3f;

        float middlePosition = position.y + spacer + spacer/2;
        float bottomPosition = position.y + spacer * 2f;

        // Draw label
        var kindbit = property.FindPropertyRelative("kind");
        var teambit = property.FindPropertyRelative("team");

        label.text = kindbit.enumDisplayNames [kindbit.enumValueIndex] + ": ";

        switch((Team)teambit.enumValueIndex) {

        case Team.Jungle:
            switch ((char)property.FindPropertyRelative("spec").intValue) {
            case 'L':
                label.text += "Leech";
                break;
            case 'D':
            case 'W':
                label.text += "Power";
                break;
            case 'H':
            case 'S':
                label.text += "Haste";
                break;
            case 'R':
            case 'I':
                label.text += "Defense";
                break;

            case 'M':
            case 'N':
                label.text += "Money";
                break;
            }
            break;
        default:
            label.text += teambit.enumDisplayNames [teambit.enumValueIndex];
            break;
        }

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);


        // Grab gogogo position and put it into spot
        var gogo = (GameObject)property.FindPropertyRelative("gogogo").objectReferenceValue;

        if (gogo != null) {
            var victor = new Vector2(gogo.transform.position.x, gogo.transform.position.z) * Constants.TO_AETHERFORGED_UNITS;
            property.FindPropertyRelative("spot").vector2Value = victor;
            property.FindPropertyRelative("gogogo").objectReferenceValue = null;
        }


        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        var teamRect = new Rect(position.x, position.y, 80, spacer);
        var kindRect = new Rect(position.x + 85, position.y, 80, spacer);
        var specRect = new Rect(position.x + 170, position.y, 40, spacer);

        var colorRect = new Rect(position.x + 215, position.y, 50, spacer);

        var gogoRect = new Rect(position.x - 60, middlePosition, 50, spacer);
        var buttonRect = new Rect(position.x - 110, middlePosition, 45, spacer);

        var spotRect = new Rect(position.x, middlePosition, position.width, spacer);

        var specsRect = new Rect(position.x, bottomPosition, position.width, spacer);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        EditorGUI.PropertyField(teamRect, property.FindPropertyRelative("team"), GUIContent.none);
        EditorGUI.PropertyField(kindRect, property.FindPropertyRelative("kind"), GUIContent.none);
        EditorGUI.PropertyField(specRect, property.FindPropertyRelative("spec"), GUIContent.none);

        EditorGUI.PropertyField(colorRect, property.FindPropertyRelative("color"), GUIContent.none);

        EditorGUI.PropertyField(spotRect, property.FindPropertyRelative("spot"), GUIContent.none);
        EditorGUI.PropertyField(gogoRect, property.FindPropertyRelative("gogogo"), GUIContent.none);

        if (GUI.Button(buttonRect,"Show"))
        {
            GameObject gob = new GameObject("_temp_");
            var spot = property.FindPropertyRelative("spot").vector2Value;
            Vector3 v3position = new Vector3(spot.x, 0, spot.y) * Constants.TO_UNITY_UNITS;
            gob.transform.position = v3position;

            var saveObjects = Selection.objects;
            Selection.activeObject = gob;
            SceneView.lastActiveSceneView.FrameSelected();
            Selection.objects = saveObjects;
            GameObject.DestroyImmediate(gob);
        }

//        EditorGUI.PropertyField(specsRect, property.FindPropertyRelative("extras"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) * 3f;
    }

}
#endif