﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using core;
using managers;

namespace ui
{
    public class AbilityKeyGroupSetter : InputKeySetter {

        [SerializeField]
        Toggle quickCastDefault;

        [SerializeField]
        Toggle showLevelUpKey;


        protected override void GetUI()
        {
            base.GetUI();

            quickCastDefault = transform.Find("QuickCastToggle").GetComponent<Toggle>();
            showLevelUpKey = button.transform.Find("SkillToggle").GetComponent<Toggle>();


            quickCastDefault.isOn = SettingManager.IntToBool(PlayerPrefs.GetInt(string.Format("{0}.QuickcastToggle", inputControl), 1));
            quickCastDefault.onValueChanged.AddListener((bool value) =>
            {
                PlayerPrefs.SetInt(string.Format("{0}.QuickcastToggle", inputControl), SettingManager.BoolToInt(value));
                InitText();
            });
        }

        protected override void InitText()
        {
            if (quickCastDefault.isOn)
            {
                keyText.text = KeyConfig.Instance.ShowQuickKey(inputControl);
            }
            else
            {
                keyText.text = KeyConfig.Instance.ShowSimpleKey(inputControl);
            }

            keyLabel.text = KeyConfig.Instance.Describe(inputControl);
                               
        }

        protected override void SetLoop(Event cevent)
        {
            var saveKey = KeyCode.None;
            var saveMod = EventModifiers.None;
            if (cevent.keyCode != KeyCode.None)
            {
                // Keyboard Event
                saveKey = cevent.keyCode;
                saveMod = cevent.modifiers;

                // stop looking for a key
                SetKeyNow = false;

                if (KeyConfig.CancelKey == saveKey)
                {
                    // TODO we should have a Clear Key for this and Cancel can just cancel
                    // If we hit the Back Key, Clear the key we're setting.
                    saveKey = KeyCode.None;
                    saveMod = EventModifiers.None;



                    keyText.text = KeyCode.None.ToString();
                }




                if (showLevelUpKey.isOn)
                {
                    KeyConfig.Instance.SaveGroupKey(inputControl, skillup: saveKey, skillupMods: saveMod);
                    keyText.text = KeyConfig.Instance.ShowLevelUpKey(inputControl);
                }
                else if (quickCastDefault.isOn)
                {
                    KeyConfig.Instance.SaveGroupKey(inputControl, quick: saveKey, quickMods: saveMod);

                    // Set the skill up key to be the same, but add control key.
                    KeyConfig.Instance.SaveGroupKey(inputControl, skillup: saveKey, skillupMods: EventModifiers.Control);
                    // NOTE: this might not be desirable long term

                    keyText.text = KeyConfig.Instance.ShowQuickKey(inputControl);
                }
                else
                {
                    KeyConfig.Instance.SaveGroupKey(inputControl, simple: saveKey, simpleMods: saveMod);
                    keyText.text = KeyConfig.Instance.ShowSimpleKey(inputControl);
                }


            }

            // if a mouse is clicked down
            if (cevent.type == EventType.MouseDown)
            {
                // stop looking for a key
                SetKeyNow = false;
                saveKey = (KeyCode)((int)KeyCode.Mouse0 + Event.current.button);

            }

            if (!SetKeyNow)
            {
                // Set the key, or clear the key if saveKey is KeyCode None
                //KeyConfig.Instance.SaveKey(inputControl, saveKey, whichPair);
                //keyText.text = saveKey.ToString();
            }
        }
    }
}