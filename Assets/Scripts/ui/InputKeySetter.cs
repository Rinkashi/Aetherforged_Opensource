﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using core;

namespace ui {
    public class InputKeySetter : MonoBehaviour {
        static bool setKeyLock;

        public ICE inputControl;

        public int whichPair;

        [SerializeField]
        bool keySetterActive;

        public static bool CanSetKeyNow {
            get { return !setKeyLock; }
        }

        protected bool SetKeyNow {
            get { return keySetterActive; }
            set { 
                setKeyLock = value;
                keySetterActive = value;
            }
        }

        [SerializeField]
        protected Button button;
        [SerializeField]
        protected Text keyText;
        [SerializeField]
        protected Text keyLabel;

    	// Use this for initialization
    	void Start () {
            GetUI();

            // If we're the option menu, don't let the player accidentally unset it.
            if (inputControl == ICE.BackOptionMenu) {
                button.enabled = false;
            } else {
                button.onClick.AddListener( SetMyKey );
            }

            InitText();
    	}
    	
    	// Update is called once per frame
    	void Update () {
            
    	}

        public void Init() {
            Start();
        }

        protected virtual void GetUI()
        {
            button = GetComponentInChildren<Button>();
            keyText = button.GetComponentInChildren<Text>();
            keyLabel = GetComponentInChildren<Text>();
        }

        protected virtual void InitText()
        {
            keyText.text = KeyConfig.Instance.ShowKey(inputControl, whichPair);
            keyLabel.text = KeyConfig.Instance.Describe(inputControl);
        }

        void OnGUI () {
            if (SetKeyNow) {
                var cevent = Event.current;
                SetLoop(cevent);
            }

        }

        protected virtual void SetLoop (Event cevent) {
            
            var saveKey = KeyCode.None;
            if (cevent.keyCode != KeyCode.None)
            {
                // Keyboard Event
                saveKey = cevent.keyCode;

                // stop looking for a key
                SetKeyNow = false;

                if (KeyConfig.CancelKey == saveKey)
                {
                    // If we hit the Back Key, Clear the key we're setting.
                    saveKey = KeyCode.None;
                    KeyConfig.Instance.SaveKey(inputControl, KeyCode.None, whichPair);

                    keyText.text = KeyCode.None.ToString();
                }
                else
                {
                    KeyConfig.Instance.SaveKey(inputControl, saveKey, whichPair);

                }

            }

            // if a mouse is clicked down
            if (cevent.type == EventType.MouseDown)
            {
                // stop looking for a key
                SetKeyNow = false;
                saveKey = (KeyCode)((int)KeyCode.Mouse0 + Event.current.button);

            }

            if (!SetKeyNow)
            {
                // Set the key, or clear the key if saveKey is KeyCode None
                KeyConfig.Instance.SaveKey(inputControl, saveKey, whichPair);
                keyText.text = saveKey.ToString();
            }
        }

        public void SetMyKey() {
            if (CanSetKeyNow) {
                // start looking for a key to set
                SetKeyNow = true;
                keyText.text = "(Key)";
            }
        }

        // TODO 2017 Apr 15 
    }
}