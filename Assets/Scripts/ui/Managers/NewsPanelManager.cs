﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Net;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class NewsPanelManager : MonoBehaviour {

    public GameObject NewsItemPanel;
    public ScrollRect ScrollView;
    public GridLayoutGroup GridLayout;
    public RectTransform ScrollContent;

    public string NewsFeedUrl = "https://dinkdunkblog.wordpress.com/feed/";

    public XDocument NewsFeed { get; set; }
    public IEnumerable<Post> Posts { get; set; }

    void Start () {
        var newsFeedURI = new Uri(NewsFeedUrl);
        var request = (HttpWebRequest)WebRequest.Create(newsFeedURI);
        request.Method = "GET";

        var response = (HttpWebResponse)request.GetResponse();
        using (var reader = new StreamReader(response.GetResponseStream()))
        {
            var newsContent = reader.ReadToEnd();
            NewsFeed = XDocument.Parse(newsContent);

            Posts = (from p in NewsFeed.Descendants("item")
                         select new Post
                         {
                             Title = p.Element("title").Value,
                             Description = p.Element("description").Value,
                             Link = p.Element("link").Value,
                             PubDate = DateTime.Parse(p.Element("pubDate").Value)
                         }).ToList();

            foreach (var post in Posts)
            {
                var newPost = Instantiate(NewsItemPanel);
                newPost.transform.parent = GridLayout.transform;
                newPost.transform.localScale = Vector3.one;
                var newsItem = newPost.GetComponent<NewsItemPanel>();
                newsItem.Initialize(post.Title, post.Description, post.PubDate, post.Link);
                newPost.SetActive(true);
            }

            SetContentHeight();
        }
	}

    public void OnLoad()
    {
        //TODO : Make loading scripts for each of the panels instead of having them loaded in all the time.
    }

    public void SetContentHeight()
    {
        float scrollContentHeight = (GridLayout.transform.childCount * GridLayout.cellSize.y) + ((GridLayout.transform.childCount - 1) * GridLayout.spacing.y);
        ScrollContent.sizeDelta = new Vector2(400, scrollContentHeight);
    }
}

public class Post
{
    public string Title { get; set; }
    public string Description { get; set; }
    public string Link { get; set; }
    public DateTime PubDate { get; set; }
}
