﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ui 
{
public class SetTimer : MonoBehaviour {
	Text ingameclock;

	// Use this for initialization
	void Start () {
		ingameclock = GetComponent<Text> ();
		
	}
	
	// Update is called once per frame
	void Update () {
        ingameclock.text = string.Format ("{0}:{1:00}",Mathf.FloorToInt(GameTime.time/60f),Mathf.RoundToInt(GameTime.time%60f));
		
	}
}
}