﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

using core.forger;
using core;
using controllers;

namespace managers
{
    public class SettingManager : Singleton<SettingManager>
    {
        private GameObject ItemsPanelObject;
        private GameObject StatsPanelObject;

        public Resolution[] resolutions;
        string kitName;
        [SerializeField]
        bool FPSCounterVisible;
        [SerializeField]
        float Fps;
        float Msec;
        bool PINGCounterVisible;
        int ItemsPanelToggle;
        int StatsPanelToggle;
        bool isPlayerNameSet = false;
        PlayerKnob player;
        public AudioMixer masterMixer;

        void Start()
        {
            instance = this;

            string[] volumeNameList = { "Master Volume", "Music Volume", "SFX Volume", "Ambience Volume", "Voice Volume", "Announcer Volume" };

            foreach (var volumeName in volumeNameList)
            {
                var mixerVolume = PlayerPrefs.GetFloat(volumeName);
                var mixerName = volumeName;
                //set volume to saved setting
                masterMixer.SetFloat(mixerName, mixerVolume);
            }

            ItemsPanelObject = GameObject.Find("ItemsPanel");
            StatsPanelObject = GameObject.Find("StatsPanel");

            Screen.fullScreen = IntToBool(PlayerPrefs.GetInt("fullscreen"));
            FPSCounterVisible = IntToBool(PlayerPrefs.GetInt("FPS"));
            PINGCounterVisible = IntToBool(PlayerPrefs.GetInt("PING"));
            //ItemsPanelObject.GetComponent<ui.UISlideStates>().Set(PlayerPrefs.GetInt("ItemsPanel"));
            //StatsPanelObject.GetComponent<ui.UISlideStates>().Set(PlayerPrefs.GetInt("StatsPanel"));

            resolutions = Screen.resolutions;
            player = UnitManager.Instance.LocalKnob;

            QualitySettings.masterTextureLimit = PlayerPrefs.GetInt("QualityIndex");
            QualitySettings.antiAliasing = (int)Mathf.Pow(2, PlayerPrefs.GetInt("AntialiasingIndex"));
            QualitySettings.vSyncCount = PlayerPrefs.GetInt("VSyncIndex");
            if(UnitManager.Ready && UnitManager.Instance.LocalKnob != null)
                UnitManager.Instance.LocalKnob.PlayerName = PlayerPrefs.GetString("PlayerName", "<Name Not Found>");
            
            InvokeRepeating("FPSCounter", 0f, 0.3f);
        }

        public void OnCameraSettingChange(string name, float value)
        {
            switch (name){
                case "edgeWidth":
                {
                    PlayerPrefs.SetFloat("edgeWidth", value);
                    break;
                }
                case "arrowSpeed":
                {
                    PlayerPrefs.SetFloat("arrowSpeed", value);
                    break;
                }
                case "panSensitivity":
                {
                    PlayerPrefs.SetFloat("panSensitivity", value);
                    break;
                }
                case "edgeSpeed":
                {
                    PlayerPrefs.SetFloat("edgeSpeed", value);
                    break;
                }
                case "zoomSpeed":
                {
                    PlayerPrefs.SetFloat("zoomSpeed", value);
                    break;
                }
            }
        }
        
        public void OnFullScreenToggle(bool value)
        {
            Screen.fullScreen = value;
            PlayerPrefs.SetInt("fullscreen", BoolToInt(value));
        }

        public void OnFPSToggle(bool value)
        {
            PlayerPrefs.SetInt("FPS", BoolToInt(value));
            FPSCounterVisible = IntToBool(PlayerPrefs.GetInt("FPS"));
        }
        public void OnPINGToggle(bool value)
        {
            PlayerPrefs.SetInt("PING", BoolToInt(value));
            PINGCounterVisible = IntToBool(PlayerPrefs.GetInt("PING"));    
        }
        public void OnItemsPanelToggle(bool value)
        {
            PlayerPrefs.SetInt("ItemsPanel", BoolToInt(value));
            ItemsPanelObject.GetComponent<ui.UISlideStates>().Set(BoolToInt(value));
        }
        public void OnStatsPanelToggle(bool value)
        {
            PlayerPrefs.SetInt("StatsPanel", BoolToInt(value));
            StatsPanelObject.GetComponent<ui.UISlideStates>().Set(BoolToInt(value));
        }
        public void OnResolutionChange(int resolutionsIndex)
        {
            Screen.SetResolution(resolutions[resolutionsIndex].width, resolutions[resolutionsIndex].height, Screen.fullScreen);
            PlayerPrefs.SetInt("ResolutionIndex", resolutionsIndex);
        }
        public void OnTextureQualityChange(int qualityIndex)
        {
            GameSettings.textureQuality = qualityIndex;
            QualitySettings.masterTextureLimit = GameSettings.textureQuality;
            PlayerPrefs.SetInt("QualityIndex", qualityIndex);
        }
        public void OnAntialiasingChange(int antiAliasingIndex)
        {
            PlayerPrefs.SetInt("AntialiasingIndex", antiAliasingIndex);
            QualitySettings.antiAliasing = (int)Mathf.Pow(2, antiAliasingIndex);
        }
        public void OnVSyncChange(int vSyncIndex)
        {
            PlayerPrefs.SetInt("VSyncIndex", vSyncIndex);
            QualitySettings.vSyncCount = vSyncIndex;
        }

        public void OnVolumeSliderChange(float value , ref float volumeSetting, string mixerName)
        {
            volumeSetting = value;
            masterMixer.SetFloat(mixerName, value);
            PlayerPrefs.SetFloat(mixerName, value);
        }

        
        private void FPSCounter()
        {
            Fps = 1.0f / Time.deltaTime;
            Msec = Time.deltaTime * 1000.0f;
        }
        
        public void OnGUI()
        {
            Rect fpsRect;
            Rect pingRect;

            GUIStyle style = new GUIStyle();

            {
                int w = Screen.width;
                int h = Screen.height;

                fpsRect = new Rect(0, 0, w, h * 2 / 100);
                pingRect = new Rect(0, fpsRect.y + fpsRect.height, w, h * 2 / 100);
                style.alignment = TextAnchor.UpperRight;
                style.fontSize = h * 2 / 125;
            }

            if(FPSCounterVisible == true){          
                style.normal.textColor = new Color (1f, 1f, 1f, 1.0f);
                string text = string.Format("{0:0.0} ms ({1:0.} fps)", Msec, Fps);
                GUI.Label(fpsRect, text, style);               
            }
            if(PINGCounterVisible == true)
            { 

                if (AetherForgedNetworkManager.Ready && AetherForgedNetworkManager.Instance.client != null)
                {
                    // Gets the return trip time. So simple. <3 UNet.
                    int pingRTT = AetherForgedNetworkManager.Instance.client.GetRTT();

                    // Do we want to put a delay on this?

                    Color colour;

                    {
                        var pingtime = pingRTT;
                        // Ping colors indigo <30 - green 30+ - yellow 50+ - 100+ red
                        if (pingtime < 30)
                        {
                            colour = new Color(0.58f, 0.49f, 0.89f);
                        }
                        else if (pingtime < 50)
                        {
                            colour = Color.green;
                        }
                        else if (pingtime < 100)
                        {
                            colour = Color.yellow;
                        }
                        else
                        {
                            colour = Color.red;
                        }
                    }
                    style.normal.textColor = colour;

                    string text = string.Format("ping rtt {0} ms", pingRTT);
                    GUI.Label(pingRect, text, style);
                }
                else 
                {
                    string text = "currently disconnected";
                    GUI.Label(pingRect, text, style);
                }
            }
        }

        public static bool IntToBool(int value)
        {
            if(value == 0){
                return false;
            }
            else{
                return true;
            }
        }
        
        public static int BoolToInt(bool value)
        {
            if(value == false){
                return 0;
            }
            else{
                return 1;
            }
        }
        void Update(){
            if(UnitManager.Ready && UnitManager.Instance.LocalKnob != null && isPlayerNameSet == false)
                if(UnitManager.Instance.LocalKnob.PlayerName == null)
                {
                    UnitManager.Instance.LocalKnob.PlayerName = PlayerPrefs.GetString("PlayerName", "<Name Not Found>");
                    isPlayerNameSet = true;
                }
        }
    }
}