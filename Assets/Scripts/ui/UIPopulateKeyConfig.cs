﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using core;
using ui;

public class UIPopulateKeyConfig : MonoBehaviour {

    [SerializeField]
    LayoutGroup contentPane;

    [SerializeField]
    LayoutElement keySetterPrefab;

    [SerializeField]
    LayoutElement keyGroupPrefab;


    // Use this for initialization
    void OnEnable()
    {
        
        if (contentPane.transform.childCount == 0)
        {

            var keyGroups = KeyConfig.Instance.ListAbilityGroupKeys;

            foreach (ICE inputControlEnum in keyGroups)
            {
                var topGob = Instantiate(keyGroupPrefab, contentPane.transform);

                var setter = topGob.GetComponent<AbilityKeyGroupSetter>();
                setter.inputControl = inputControlEnum;
            }

            var keys = KeyConfig.Instance.AsArray();

            foreach (var keypair in keys)
            {
                var key = keypair.Key;
                var pair = keypair.Value;

                {

                    var topGob = Instantiate(keySetterPrefab, contentPane.transform);

                    var setter = topGob.GetComponent<InputKeySetter>();

                    setter.inputControl = key;


                }
            }

        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
