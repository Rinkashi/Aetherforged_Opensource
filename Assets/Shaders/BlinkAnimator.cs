﻿using UnityEngine;
using System.Collections;
namespace graphics
{
  class BlinkAnimator : MonoBehaviour
  {
    public int columns = 4;
    public int rows = 1;
    public float frameDelay = 0.03f;
    public float blinkFrequency = 7f;
    public float blinkVariation = 0.35f;
    int[] blinkAnimation = new int[] { 0, 1, 2, 3, 2, 0 };

    //the current frame to display
    private int frame = 0;

    void Start()
    {
      StartCoroutine( updateBlinking() );

      //set the tile size of the texture (in UV units), based on the rows and columns
      Vector2 size = new Vector2(1f / columns, 1f / rows);
      GetComponent<Renderer>().sharedMaterial.SetTextureScale("_MainTex", size);
    }

    private IEnumerator updateBlinking()
    {
      while( true )
      {
        frame += 1;
        //move to the next index
        var index = blinkAnimation[frame];

        if (index >= rows * columns)
          index = 0;

        //split into x and y indexes
        Vector2 offset = new Vector2((float)index / columns - (index / columns), 
          //x index
          (index / columns) / (float)rows);
        //y index

        GetComponent<Renderer>().sharedMaterial.SetTextureOffset("_MainTex", offset);

        if( frame == blinkAnimation.Length - 1 )
        {
          frame = 0;
          var variedTime = blinkFrequency * blinkVariation;
          yield return new WaitForSeconds( blinkFrequency + UnityEngine.Random.Range( -variedTime, variedTime ) );
        } else
        {
          yield return new WaitForSeconds( frameDelay );
        }
      }

    }
  }
}