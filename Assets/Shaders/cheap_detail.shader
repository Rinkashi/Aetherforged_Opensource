Shader "Custom/Cheap-detail Particle " {
Properties {
	_TintColor ("Tint Color", Color) = (0.5,0.5,0.5,0.5)
	_MainTex ("Particle Texture", 2D) = "white" {}
	_SideTex1 ("Mask texture", 2D) = "white" {}
	_SideTex2 ("Mask texture", 2D) = "white" {}
	_SideTex3 ("Mask texture", 2D) = "white" {}
	_InvFade ("Soft Particles Factor", Range(0.01,3.0)) = 1.0
	scrollrate1 ("Mask 1 speed", vector) = (0,0,0,0)
	scrollrate2 ("Mask 2 speed", vector) = (0,0,0,0)
	scrollrate3 ("Mask 3 speed", vector) = (0,0,0,0)
	_AlphaMultiplier("Alpha Multiplier", Range(0.0,10.0)) = 1.0
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" }
	Blend SrcAlpha OneMinusSrcAlpha
	ColorMask RGB
	Cull Off Lighting Off ZWrite Off

	SubShader {
		Pass {
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile_particles
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _SideTex1;
			vector scrollrate1;
			vector scrollrate2;
			vector scrollrate3;
			sampler2D _SideTex2;
			sampler2D _SideTex3;
			fixed4 _TintColor;
			float _AlphaMultiplier;
			
			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;
				float2 texcoord2 : TEXCOORD2;
				float2 texcoord3 : TEXCOORD3;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;
				float2 texcoord2 : TEXCOORD2;
				float2 texcoord3 : TEXCOORD3;
				UNITY_FOG_COORDS(1)
				#ifdef SOFTPARTICLES_ON
				//float4 projPos : TEXCOORD2; - Commented for preventing builds DD 24/10
				#endif
			};
			
			float4 _MainTex_ST;
			float4 _SideTex1_ST;
			float4 _SideTex2_ST;
			float4 _SideTex3_ST;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
//				#ifdef SOFTPARTICLES_ON
//				o.projPos = ComputeScreenPos (o.vertex);
//				COMPUTE_EYEDEPTH(o.projPos.z);
//				#endif
				o.color = v.color;
				o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
				v.texcoord1.xy += scrollrate1.xy * _Time.y;
				v.texcoord2.xy += scrollrate2.xy * _Time.y;
				v.texcoord3.xy += scrollrate3.xy * _Time.y;
				o.texcoord1 = TRANSFORM_TEX(v.texcoord1,_SideTex1);
				o.texcoord2 = TRANSFORM_TEX(v.texcoord2,_SideTex2);
				o.texcoord3 = TRANSFORM_TEX(v.texcoord3,_SideTex3);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			sampler2D_float _CameraDepthTexture;
			float _InvFade;
			
			fixed4 frag (v2f i) : SV_Target
			{
				#ifdef SOFTPARTICLES_ON
				//float sceneZ = LinearEyeDepth (SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos))); - Commented for preventing builds DD 24/10
				//float partZ = i.projPos.z; - Commented for preventing builds DD 24/10
				//float fade = saturate (_InvFade * (sceneZ-partZ)); - Commented for preventing builds DD 24/10
				//i.color.a *= fade; - Commented for preventing builds DD 24/10

				#endif
				i.color.a*=_AlphaMultiplier;
				fixed4 col = 2.0f * i.color * _TintColor * tex2D(_MainTex, i.texcoord) * tex2D(_SideTex1, i.texcoord1) * tex2D(_SideTex2, i.texcoord2) * tex2D(_SideTex3, i.texcoord3);
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG 
		}
	}	
}
}
