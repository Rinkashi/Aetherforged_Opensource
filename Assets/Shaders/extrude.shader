﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/extrude" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Extrudeint ("Extrude amount", range(-5,5)) = 0
	}
	SubShader 
	{

	pass
	{
	CGPROGRAM

	#pragma vertex vertexFunction
	#pragma fragment fragmentFunction

	#include "UnityCG.cginc"

	//get info from the model
	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv :TEXCOORD0;
		float3 normal : NORMAL;
	};

	struct v2f
	{
		float4 position : SV_POSITION;
		float2 uv : TEXCOORD0;
		float3 normal : NORMAL;
	};

	//bring properties
	float4 _Color;
	sampler2D _MainTex;
	float _Extrudeint;

	//set out the vertices
	v2f vertexFunction(appdata IN)
	{
		v2f OUT;

		IN.vertex.xyz += IN.normal.xyz * _Extrudeint;
		OUT.position = UnityObjectToClipPos(IN.vertex);
		OUT.uv = IN.uv;



		return OUT;
	}

	//color inside vertices
	fixed4 fragmentFunction(v2f IN) : SV_Target
	{
		float4 textureColor = tex2D( _MainTex, IN.uv);

		return textureColor * _Color;
	}
	ENDCG
	}

	}
}
